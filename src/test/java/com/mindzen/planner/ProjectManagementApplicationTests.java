/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectManagementApplicationTests.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("h2")
public class ProjectManagementApplicationTests {

	/** The application context. */
	@Autowired
	private WebApplicationContext applicationContext;

	/** The mock mvc. */
	public MockMvc mockMvc;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

}
