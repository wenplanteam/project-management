/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.mindzen.planner.ProjectManagementApplicationTests;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.ScheduleDTO;
import com.mindzen.planner.dto.SubTaskDTO;
import com.mindzen.planner.dto.TaskDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class LookaheadAccessControllerTest.
 */
public class LookaheadAccessControllerTest extends ProjectManagementApplicationTests{

	/**
	 * Lookahead date info test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void lookaheadDateInfoTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/lookaheadDateInfo?projectId=1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	/**
	 * Creates the lookahead test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void createLookaheadTest() throws Exception {
		LookaheadDTO lookaheadDTO = new LookaheadDTO();
		List<ScheduleDTO> scheduleDTOList = new ArrayList<>();
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		scheduleDTO.setWorkerCount(3L);
		scheduleDTO.setWorkingDate("05-Oct-2018");
		scheduleDTOList.add(scheduleDTO);
		List<SubTaskDTO> subTaskDTOList = new ArrayList<>();
		SubTaskDTO subTaskDTO = new SubTaskDTO();
		subTaskDTO.setDescription("Sample SubTask");
		subTaskDTO.setSchedules(scheduleDTOList);
		subTaskDTO.setTaskType("Demo");
		subTaskDTO.setTrade("TRY");
		subTaskDTO.setVisibility("PUBLIC");
		subTaskDTO.setBallInCourt("MINDZEN");
		subTaskDTO.setNote("IS IT INSERT?");
		subTaskDTOList.add(subTaskDTO);
		List<TaskDTO> taskDTOList = new ArrayList<>();
		TaskDTO taskDTO = new TaskDTO();
		taskDTO.setDescription("Sample Task");
		taskDTO.setSubTasks(subTaskDTOList);
		taskDTO.setTaskType("Demo");
		taskDTO.setTrade("TRY");
		taskDTO.setVisibility("PUBLIC");
		taskDTO.setBallInCourt("MINDZEN");
		taskDTO.setNote("IS IT INSERT?");
		lookaheadDTO.setTasks(taskDTOList);
		lookaheadDTO.setProjectId(24L);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] lookaheadDTOByte = mapper.writeValueAsBytes(lookaheadDTO);
		
		MvcResult mvcResult =  mockMvc.perform(post("/lookahead")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(lookaheadDTOByte))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	/**
	 * Delete task by id test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void deleteTaskByIdTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(delete("/taskById?taskId=1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	/**
	 * Delete sub task by id test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void deleteSubTaskByIdTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(delete("/subTaskById?subTaskId=1000"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

}
