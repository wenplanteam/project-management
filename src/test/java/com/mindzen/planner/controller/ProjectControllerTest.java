/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.mindzen.planner.ProjectManagementApplicationTests;
import com.mindzen.planner.dto.ProjectDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectControllerTest.
 */
public class ProjectControllerTest extends ProjectManagementApplicationTests {

	/**
	 * Creates the projects.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void create_projects() throws Exception {
		ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setProjectNo("p-123-123");
		projectDTO.setProjectName("new Buildimh");
		projectDTO.setProjectOwner("sdgfr00");
		projectDTO.setProjectDescription("asfsageyhdhdhd");
		projectDTO.setProjectStage("In Construction");
		projectDTO.setProjectManager("lingareddy");
		projectDTO.setLicenseDistributionId(1L);

		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] projectUser = mapper.writeValueAsBytes(projectDTO);
		MvcResult mvcResult = mockMvc
				.perform(post("/project").contentType(MediaType.APPLICATION_JSON_UTF8).content(projectUser))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	/**
	 * Update project.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void update_project() throws Exception {
		ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setProjectId(1L);
		projectDTO.setProjectNo("p-123-123");
		projectDTO.setProjectName("new Building");
		projectDTO.setProjectOwner("sdgfr00");
		projectDTO.setProjectDescription("new building construction");
		projectDTO.setProjectStage("In Construction");
		projectDTO.setProjectManager("lingareddy");
		projectDTO.setLicenseDistributionId(1L);

		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] projectUser = mapper.writeValueAsBytes(projectDTO);
		MvcResult mvcResult =  mockMvc.perform(put("/project")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(projectUser))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

	/**
	 * Delete project.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void delete_project() throws Exception{
		long[] projectIds = {20,21,22};
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		byte[] projectUser = mapper.writeValueAsBytes(projectIds);
		MvcResult mvcResult =  mockMvc.perform(delete("/project")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(projectUser))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	/**
	 * Gets the project by id test.
	 *
	 * @return the project by id test
	 * @throws Exception the exception
	 */
	@Test
	public void getProjectById_Test() throws Exception {
		MvcResult mvcResult =  mockMvc.perform(get("/project?projectId=13"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}
	
	/**
	 * Load all projects test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void loadAllProjectsTest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/projects?status=true"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		MockHttpServletResponse response = mvcResult.getResponse();
		assertEquals(200, response.getStatus());
	}

}
