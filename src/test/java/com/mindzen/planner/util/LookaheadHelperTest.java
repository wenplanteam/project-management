/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.util;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;

public class LookaheadHelperTest {
	
	/**
	 * Test pivot query.
	 */
	@Test
	public void testPivotQuery() {
		LocalDate currentDate = LocalDate.now();
		List<LocalDate> localDates = DateUtil.getLocalDatesList(currentDate, 28);
		String query = LookaheadHelper.pivotQuery(localDates, LocalDate.now(), DateUtil.plusNumberDaysInLocalDate(currentDate, 28), 1L, 1L, "GENERAL_CONTRACTOR");
		//log.info("Query: {}", query);
		
		assertNotNull(query);
	}

}
