/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service.impl;

import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.NotificationRepository;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.service.impl.ProjectShareServiceImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectShareTestImpl.
 */
public class ProjectShareTestImpl {
	
	/** The mock mvc. */
	public MockMvc mockMvc;
	
	/** The project share service impl. */
	ProjectShareServiceImpl projectShareServiceImpl;
	
	/** The project user repository. */
	ProjectUserRepository projectUserRepository;
	
	/** The message queue service. */
	MessageQueueService messageQueueService;
	
	/** The project repository. */
	ProjectRepository projectRepository;
	
	/** The project company repository. */
	ProjectCompanyRepository projectCompanyRepository;
	
	/** The notification repository. */
	NotificationRepository notificationRepository;
	
	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		projectUserRepository = mock(ProjectUserRepository.class);
		messageQueueService = mock(MessageQueueService.class);
		projectRepository = mock(ProjectRepository.class);
		projectCompanyRepository = mock(ProjectCompanyRepository.class);
		projectShareServiceImpl = new ProjectShareServiceImpl(projectUserRepository, messageQueueService, projectRepository, projectCompanyRepository, notificationRepository);
	}

	/**
	 * Share project test.
	 *
	 * @param email the email
	 * @param source the source
	 */
	@Test
	public void share_project_test(String email,String source) {
		
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail("lingaiah.kondra@mindzen.co.in");
		userDTO.setSource("sharing");
		
	}
}
