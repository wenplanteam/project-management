/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.repository.NotificationRepository;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.repository.ScheduleRepository;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectServiceImplTest.
 *
 * @author mindzen
 */

public class ProjectServiceImplTest {

	/** The mock mvc. */
	public MockMvc mockMvc;

	/** The project service impl. */
	ProjectServiceImpl projectServiceImpl;
	
	/** The project repository. */
	ProjectRepository projectRepository;
	
	/** The project user repository. */
	ProjectUserRepository projectUserRepository;
	
	/** The project company repository. */
	ProjectCompanyRepository projectCompanyRepository;
	
	/** The schedule repository. */
	private ScheduleRepository scheduleRepository;
	
	/** The model mapper. */
	ModelMapper modelMapper;
	
	/** The notification repository. */
	NotificationRepository notificationRepository;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		projectRepository = mock(ProjectRepository.class);
		projectUserRepository = mock(ProjectUserRepository.class);
		scheduleRepository=mock(ScheduleRepository.class);
		projectCompanyRepository = mock(ProjectCompanyRepository.class);
		modelMapper = mock(ModelMapper.class);
		projectServiceImpl = new ProjectServiceImpl(projectRepository, projectUserRepository, 
				projectCompanyRepository, scheduleRepository, notificationRepository, modelMapper, null, null);
	}

	/**
	 * Test createnew project unit test.
	 *
	 * @throws Exception testing Controller for creating new project
	 */

	@Test
	public void test_createnewProject_unitTest() throws Exception {
		Project project = new Project();
		project.setProjectNo("p1234567");
		project.setName("new building construction");
		project.setDescription("new project");
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = format.format(new Date());
		Date date = format.parse("2018-10-14");
		Date date1 = format.parse("2019-11-13");
		project.setEstimatedStartDate(date);
		project.setEstimatedEndDate(date1);
		project.setLicenseDistributionId(1);
		project.setProjectManager("Alaxander");
		project.setOwnerCompany("abcdf");
		project.setJobPhone("+1-202-505-3452");
		project.setProjectStage("in Construction");
		project.setCity("dallas");
		project.setZipCode("99501 thru 99524");
		project.setCounty("America");
		project.setCountry("USA");

		/**
		 * getting data from entity class and comparing with setting data in the object
		 */

		when(projectRepository.save(any(Project.class))).thenReturn(project);

		ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setProjectId(13l);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date date2 = null;
		Date date3 = null;
		try {
			date2 = sdf.parse("24-nov-2018");
			date3 = sdf.parse("30-nov-2018");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		project.setEstimatedStartDate(date2);
		project.setEstimatedEndDate(date3);
		projectDTO.setLicenseDistributionId(1l);
		projectDTO.setProjectName("new building construction");
		projectDTO.setProjectManager("Alaxander");
		projectDTO.setProjectOwner("abcdf");
		projectDTO.setBusinessPhone("+1-202-505-3452");
		projectDTO.setProjectStage("in Construction");
		projectDTO.setCity("dallas");
		projectDTO.setZipCode("99501 thru 99524");
		projectDTO.setCounty("America");
		projectDTO.setCountry("USA");
		projectDTO.setProjectStatus("Active");

		/**
		 * sending data to Projectservice layer
		 */
		MSAResponse status = projectServiceImpl.postProject(projectDTO);
		// String results = projectServiceImpl.putProject(projectDTO);

		/**
		 * checking result with service class return value
		 */
		ArgumentCaptor<Project> projectArgument = ArgumentCaptor.forClass(Project.class);
		verify(projectRepository, times(1)).save(projectArgument.capture());
		verifyNoMoreInteractions(projectRepository);

		assertEquals(projectDTO.getProjectName(), projectArgument.getValue().getName());
		assertEquals("project created successfully", status);

	}

	/**
	 * Test update project unit test.
	 *
	 * @throws Exception the exception
	 */
	@Test //(expected=MzEntityException.class)
	public void test_updateProject_unitTest() throws Exception {
		Project project = new Project();
		project.setId(13L);
		project.setProjectNo("p1234567");
		project.setName("new building construction");
		project.setDescription("new project");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = format.format(new Date());
		Date date = format.parse("2018-10-14");
		Date date1 = format.parse("2019-11-13");
		project.setEstimatedStartDate(date);
		project.setEstimatedEndDate(date1);
		project.setLicenseDistributionId(1);
		project.setProjectManager("Alaxander");
		project.setOwnerCompany("abcdf");
		project.setJobPhone("+1-202-505-3452");
		project.setProjectStage("in Construction");
		project.setCity("dallas");
		project.setZipCode("99501 thru 99524");
		project.setCounty("America");
		project.setCountry("USA");
		


		ProjectDTO projectDTO = new ProjectDTO();
		when(projectRepository.findById(project.getId()).get()).thenReturn(project);
		doNothing().when(projectServiceImpl).postProject(projectDTO);

		/**
		 * getting data from entity class and comparing with setting data in the object
		 */

		projectDTO.setProjectId(13L);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date date2 = null;
		Date date3 = null;
		try {
			date2 = sdf.parse(projectDTO.getEstimatedStartDate());
			date3 = sdf.parse(projectDTO.getEstimatedEndDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		projectDTO.setLicenseDistributionId(1l);
		projectDTO.setProjectName("new building construction");
		projectDTO.setProjectManager("Alaxander");
		projectDTO.setProjectOwner("abcdf");
		projectDTO.setBusinessPhone("+1-202-505-3452");
		projectDTO.setProjectStage("in Construction");
		projectDTO.setCity("dallas");
		projectDTO.setZipCode("99501 thru 99524");
		projectDTO.setCounty("America");
		projectDTO.setCountry("USA");
		projectDTO.setProjectStatus("Active");

		/**
		 * sending data to Projectservice layer
		 */
		MSAResponse results = projectServiceImpl.putProject(projectDTO);

		/**
		 * checking result with service class return value
		 */

		ArgumentCaptor<Project> projectArgument1 = ArgumentCaptor.forClass(Project.class);
		verify(projectRepository, times(1)).findById(project.getId()).get();
		verify(projectRepository,times(1)).save(project);
		verifyNoMoreInteractions(projectRepository);

		assertEquals(project.getProjectManager(), projectArgument1.getValue().getProjectManager());
		assertEquals("project updated successfully", results);

	}
	

	/**
	 * get method for project controller.
	 *
	 * @return the all projects
	 * @throws Exception the exception
	 */

	public void getAllProjects() throws Exception {
		MvcResult mvcResult = mockMvc.perform(get("/projects").contentType(MediaType.APPLICATION_JSON)).andReturn();
		assertEquals(200, mvcResult.getResponse().getStatus());

	}

}
