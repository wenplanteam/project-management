/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.AppConfig;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.repository.ScheduleRepository;
import com.mindzen.planner.repository.SubTaskRepository;
import com.mindzen.planner.repository.TaskRepository;

// TODO: Auto-generated Javadoc
/**
 * The Class LookaheadAccessServiceImplTest.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 29-Oct-2018
 */
public class LookaheadAccessServiceImplTest {

	/** The access service impl. */
	LookaheadAccessServiceImpl accessServiceImpl = null;

	/** The task repository. */
	TaskRepository taskRepository;
	
	/** The sub task repository. */
	SubTaskRepository subTaskRepository;
	
	/** The schedule repository. */
	ScheduleRepository scheduleRepository;
	
	/** The project repository. */
	ProjectRepository projectRepository;
	
	/** The project user repository. */
	ProjectUserRepository projectUserRepository;
	
	/** The message queue service. */
	MessageQueueService messageQueueService;
	
	/** The project company repository. */
	ProjectCompanyRepository projectCompanyRepository;

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		
		UserInfoContext.setUserThreadLocal(1L);
		taskRepository = mock(TaskRepository.class);
		subTaskRepository = mock(SubTaskRepository.class);
		scheduleRepository = mock(ScheduleRepository.class);
		projectRepository = mock(ProjectRepository.class);
		projectUserRepository = mock(ProjectUserRepository.class);
		messageQueueService = mock(MessageQueueService.class);
		AppConfig appConfig = mock(AppConfig.class);
		ProjectCompanyRepository projectCompanyRepository = mock(ProjectCompanyRepository.class);
		
		accessServiceImpl = new LookaheadAccessServiceImpl(taskRepository, subTaskRepository, scheduleRepository, 
				projectRepository, messageQueueService, projectUserRepository, appConfig,projectCompanyRepository, null);
	}
	
	/**
	 * Test project user is null in lookahead info.
	 */
	@Test
	public void testProjectUserIsNullInLookaheadInfo() {
		MSAResponse msaResponse = accessServiceImpl.lookaheadInfo(99999L);
		assertEquals(false, msaResponse.isSuccess());
	}

}
