/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

// TODO: Auto-generated Javadoc
/**
 * The Class BaseEntity.
 *
 * @author Alexpandiyan Chokkan
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

	/** The created by. */
	@CreatedBy
	@Column(updatable=false)
	protected Long createdBy;
	
	/** The created on. */
	@CreatedDate
	@Column(updatable=false)
	protected LocalDateTime createdOn;
	
	/** The modified by. */
	@LastModifiedBy
	protected Long modifiedBy;
	
	/** The modified on. */
	@LastModifiedDate
	protected LocalDateTime modifiedOn;
	
}
