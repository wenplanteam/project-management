/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.EqualsAndHashCode;

// TODO: Auto-generated Javadoc
/**
 * The Class BaseIdEntity.
 *
 * @author Alexpandiyan Chokkan
 */
@MappedSuperclass

/* (non-Javadoc)
 * @see java.lang.Object#hashCode()
 */
@EqualsAndHashCode(callSuper=false)
public class BaseIdEntity extends BaseEntity {

	

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
}
