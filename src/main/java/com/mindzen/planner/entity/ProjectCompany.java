/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.bridge.builtin.BooleanBridge;

import com.fasterxml.jackson.annotation.JsonBackReference;


// TODO: Auto-generated Javadoc
/**
 * The Class ProjectCompany.
 */
@Entity
@Indexed
@Table(name="project_companies")
@NamedQueries({
	@NamedQuery(name="ProjectCompany.countAll", query="SELECT COUNT(x) FROM ProjectCompany x")
})
public class ProjectCompany extends BaseIdEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4467379987216894944L;

	/** The company id. */
	@Field
	@NumericField
	@Column(name="company_id", nullable=false)
	private Long companyId;

	/** The project. */
	@ContainedIn
	@JsonBackReference
	@OneToOne
	@JoinColumn(name="project_id", nullable=false)
	private Project project;

    /** The active. */
    @Field
    @FieldBridge(impl=BooleanBridge.class)
	@Column(name="is_active", nullable=false)
	private boolean active;
	
	/** The project role. */
	@Column(name = "project_role" )
	private String    projectRole   ;
	
    /** The deleted. */
    @Field
    @FieldBridge(impl=BooleanBridge.class)
	@Column(name = "is_deleted" )
	private boolean deleted;

	//----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	/**
	 * Instantiates a new project company.
	 */
	//----------------------------------------------------------------------
	public ProjectCompany()
	{
		super();
	}


	//----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	//----------------------------------------------------------------------
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	//--- DATABASE MAPPING : id (  ) 
	public Long getId()
	{
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 * @return the long
	 */
	public Long setId(Long id)
	{
		return this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	//--- DATABASE MAPPING : createdBy (  ) 
	public Long getCreatedBy()
	{
		return createdBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	//--- DATABASE MAPPING : createdOn (  ) 
	public LocalDateTime getCreatedOn()
	{
		return createdOn;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	//--- DATABASE MAPPING : modifiedBy (  ) 
	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	/**
	 * Gets the modified on.
	 *
	 * @return the modified on
	 */
	//--- DATABASE MAPPING : modifiedOn (  ) 
	public LocalDateTime getModifiedOn()
	{
		return modifiedOn;
	}

	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public Long getCompanyId() {
		return companyId;
	}


	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}


	/**
	 * Gets the project.
	 *
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}


	/**
	 * Sets the project.
	 *
	 * @param project the new project
	 */
	public void setProject(Project project) {
		this.project = project;
	}


	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	//--- DATABASE MAPPING : active (  ) 
	public void setActive( boolean active )
	{
		this.active = active;
	}
	
	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive()
	{
		return this.active;
	}
	
	/**
	 * Checks if is deleted.
	 *
	 * @return true, if is deleted
	 */
	//--- DATABASE MAPPING : deleted (  ) 
	public boolean isDeleted() {
		return deleted;
	}


	/**
	 * Sets the deleted.
	 *
	 * @param deleted the new deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Gets the project role.
	 *
	 * @return the project role
	 */
	//--- DATABASE MAPPING : projectRole (  ) 
	public String getProjectRole() {
		return projectRole;
	}


	/**
	 * Sets the project role.
	 *
	 * @param projectRole the new project role
	 */
	public void setProjectRole(String projectRole) {
		this.projectRole = projectRole;
	}



	//----------------------------------------------------------------------
	// GETTERS & SETTERS FOR LINKS
	//----------------------------------------------------------------------

	//----------------------------------------------------------------------
	// toString METHOD
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	//----------------------------------------------------------------------
	public String toString() { 
		StringBuffer sb = new StringBuffer(); 
		sb.append(companyId);
		sb.append("|");
		sb.append(project.getId());
		sb.append("|");
		sb.append(active);
		return sb.toString(); 
	}
}