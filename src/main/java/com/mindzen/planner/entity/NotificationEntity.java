/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class NotificationEntity.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Audited
@Table(name="notification")

/**
 * Checks if is mail sent.
 *
 * @return true, if is mail sent
 */
@Getter

/**
 * Sets the mail sent.
 *
 * @param isMailSent the new mail sent
 */
@Setter
public class NotificationEntity extends BaseIdEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The is notification. */
	@Column(name= "is_notification")
	@Audited
	private boolean isNotification;

	/** The type. */
	@Column(name= "type")
	@Audited
	private String type; 

	/** The frequency. */
	@Column(name= "frequency")
	@Audited
	private String frequency;

	/** The day of the week. */
	@Column(name= "day_of_the_week")
	@Audited
	private String dayOfTheWeek; 

	/** The information. */
	@Column(name= "information")
	@Audited
	private String information; 

	/** The scheduled at. */
	@Column(name= "scheduled_at")
	@Audited
	private LocalDateTime scheduledAt;

	/** The scheduled on. */
	@Column(name= "mail_sent_on")
	@Audited
	private LocalDateTime mailSentOn;

	/** The is scheduled. */
	@Column(name= "is_scheduled")
	@Audited
	private boolean isScheduled;

	/** The is mail sent. */
	@Column(name= "is_mail_sent")
	@Audited
	private boolean isMailSent;

	//----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	//----------------------------------------------------------------------
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	//--- DATABASE MAPPING : id (  ) 
	public Long getId()
	{
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 * @return the long
	 */
	public Long setId(Long id)
	{
		return this.id = id;
	}
	
	}