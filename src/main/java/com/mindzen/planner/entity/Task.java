/*
 * 
 * @author Raja Gopal MRG
 */
// This Bean has a basic Primary Key (not composite) 

package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * Persistent class for entity stored in table "Task".
 *
 * @author Telosys Tools Generator
 */

@Entity
@Table(name = "task")
@Indexed
// Define named queries here
@NamedQueries({ @NamedQuery(name = "Task.countAll", query = "SELECT COUNT(x) FROM Task x") })
public class Task extends BaseIdEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------------
	// ENTITY DATA FIELDS
	/** The description. */
	// ----------------------------------------------------------------------
	@Column(name = "description")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String description;

	/** The project id. */
	@Column(name = "project_id")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private Long projectId;

	/** The is active. */
	@Column(name = "is_active", nullable = false)
	private boolean isActive;

	/** The sub tasks. */
	@JsonIgnore
	@JsonManagedReference
	@OneToMany(mappedBy = "task", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@IndexedEmbedded
	private List<SubTask> subTasks;

	/** The task type. */
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Column(name = "task_type")
	private String taskType;

	/** The trade. */
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Column(name = "trade")
	private String trade;

	/** The spec division. */
	@Column(name = "spec_division")
	private String specDivision;

	/** The visibility. */
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	@Column(name = "visibility")
	private String visibility;

	/** The ball in court. */
	@Column(name = "ball_in_court")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String ballInCourt;

	/** The project company id. */
	@Column(name = "project_company_id", nullable = false)
	private Long projectCompanyId;

	/** The note. */
	@Column(name = "note")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String note;

	/** The child. */
	@Column(name = "is_child")
	private boolean child;

	/** The s no. */
	@Column(name = "sno")
	private String sNo;

	/** The task start date. */
	@Column(name = "task_start_date")
	private Date taskStartDate;

	/** The expected completion date. */
	@Column(name = "expected_completion_date")
	private Date expectedCompletionDate;

	/** The required completion date. */
	@Column(name = "required_completion_date")
	private Date requiredCompletionDate;

	/** The dead line date. */
	@Column(name = "dead_line_date")
	private Date deadLineDate;

	// New Columns added by RamachandraRao
	/** The activity id. */
	@Column(name = "activity_id")
	private String activityId;

	/** The activity description. */
	@Column(name = "activity_description")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String activityDescription;

	/** The import start date. */
	@Column(name = "import_start_date")
	private Date importStartDate;

	/** The import finish date. */
	@Column(name = "import_finish_date")
	private Date importFinishDate;

	/** The import target completion date. */
	@Column(name = "import_target_completion_date")
	private Date importTargetCompletionDate;

	/** The import dead line date. */
	@Column(name = "import_deadline_date")
	private Date importDeadLineDate;

	/** The import id. */
	@Column(name = "import_id")
	private Long importId;

	/** The import unique id. */
	@Column(name = "import_unique_id")
	private Integer importUniqueId;

	/** The is lookaheadChange. */
	@Column(name = "is_lookahead_change")
	private Boolean isLookAheadChange;
	
	@Column(name="import_notes")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String importNotes;

	// ----------------------------------------------------------------------
	// ENTITY LINKS ( RELATIONSHIP )
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	/**
	 * Instantiates a new task.
	 */
	// ----------------------------------------------------------------------
	public Task() {
		super();
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	// ----------------------------------------------------------------------
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	// --- DATABASE MAPPING : id ( )
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 * @return the long
	 */
	public Long setId(Long id) {
		return this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	// --- DATABASE MAPPING : createdBy ( )
	public Long getCreatedBy() {
		return createdBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	// --- DATABASE MAPPING : createdOn ( )
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	// --- DATABASE MAPPING : modifiedBy ( )
	public Long getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * Gets the modified on.
	 *
	 * @return the modified on
	 */
	// --- DATABASE MAPPING : modifiedOn ( )
	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	// --- DATABASE MAPPING : description ( )
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the project id.
	 *
	 * @param projectId the new project id
	 */
	// --- DATABASE MAPPING : projectId ( )
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public Long getProjectId() {
		return this.projectId;
	}

	// --- DATABASE MAPPING : isActive ( )

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR LINKS
	// ----------------------------------------------------------------------

	/**
	 * Gets the sub tasks.
	 *
	 * @return the sub tasks
	 */
	public List<SubTask> getSubTasks() {
		return subTasks;
	}

	/**
	 * Sets the task id.
	 *
	 * @param subTasks the new task id
	 */
	public void setTaskId(List<SubTask> subTasks) {
		this.subTasks = subTasks;
	}

	/**
	 * Gets the s no.
	 *
	 * @return the s no
	 */
	public String getsNo() {
		return sNo;
	}

	/**
	 * Sets the s no.
	 *
	 * @param sNo the new s no
	 */
	public void setsNo(String sNo) {
		this.sNo = sNo;
	}

	/**
	 * Gets the task type.
	 *
	 * @return the task type
	 */
	public String getTaskType() {
		return taskType;
	}

	/**
	 * Sets the task type.
	 *
	 * @param taskType the new task type
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	/**
	 * Gets the trade.
	 *
	 * @return the trade
	 */
	public String getTrade() {
		return trade;
	}

	/**
	 * Sets the trade.
	 *
	 * @param trade the new trade
	 */
	public void setTrade(String trade) {
		this.trade = trade;
	}

	/**
	 * Gets the spec division.
	 *
	 * @return the spec division
	 */
	public String getSpecDivision() {
		return specDivision;
	}

	/**
	 * Sets the spec division.
	 *
	 * @param specDivision the new spec division
	 */
	public void setSpecDivision(String specDivision) {
		this.specDivision = specDivision;
	}

	/**
	 * Sets the sub tasks.
	 *
	 * @param subTasks the new sub tasks
	 */
	public void setSubTasks(List<SubTask> subTasks) {
		this.subTasks = subTasks;
	}

	/**
	 * Gets the visibility.
	 *
	 * @return the visibility
	 */
	public String getVisibility() {
		return visibility;
	}

	/**
	 * Sets the visibility.
	 *
	 * @param visibility the new visibility
	 */
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	/**
	 * Gets the ball in court.
	 *
	 * @return the ball in court
	 */
	public String getBallInCourt() {
		return ballInCourt;
	}

	/**
	 * Sets the ball in court.
	 *
	 * @param ballInCourt the new ball in court
	 */
	public void setBallInCourt(String ballInCourt) {
		this.ballInCourt = ballInCourt;
	}

	/**
	 * Gets the note.
	 *
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * Sets the note.
	 *
	 * @param note the new note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * Checks if is child.
	 *
	 * @return true, if is child
	 */
	public boolean isChild() {
		return child;
	}

	/**
	 * Sets the child.
	 *
	 * @param child the new child
	 */
	public void setChild(boolean child) {
		this.child = child;
	}

	/**
	 * Gets the project company id.
	 *
	 * @return the project company id
	 */
	public Long getProjectCompanyId() {
		return projectCompanyId;
	}

	/**
	 * Sets the project company id.
	 *
	 * @param projectCompanyId the new project company id
	 */
	public void setProjectCompanyId(Long projectCompanyId) {
		this.projectCompanyId = projectCompanyId;
	}

	/**
	 * Gets the task start date.
	 *
	 * @return the task start date
	 */
	public Date getTaskStartDate() {
		return taskStartDate;
	}

	/**
	 * Sets the task start date.
	 *
	 * @param taskStartDate the new task start date
	 */
	public void setTaskStartDate(Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	/**
	 * Gets the expected completion date.
	 *
	 * @return the expected completion date
	 */
	public Date getExpectedCompletionDate() {
		return expectedCompletionDate;
	}

	/**
	 * Sets the expected completion date.
	 *
	 * @param expectedCompletionDate the new expected completion date
	 */
	public void setExpectedCompletionDate(Date expectedCompletionDate) {
		this.expectedCompletionDate = expectedCompletionDate;
	}

	/**
	 * Gets the required completion date.
	 *
	 * @return the required completion date
	 */
	public Date getRequiredCompletionDate() {
		return requiredCompletionDate;
	}

	/**
	 * Sets the required completion date.
	 *
	 * @param requiredCompletionDate the new required completion date
	 */
	public void setRequiredCompletionDate(Date requiredCompletionDate) {
		this.requiredCompletionDate = requiredCompletionDate;
	}

	/**
	 * Gets the dead line date.
	 *
	 * @return the dead line date
	 */
	public Date getDeadLineDate() {
		return deadLineDate;
	}

	/**
	 * Sets the dead line date.
	 *
	 * @param deadLineDate the new dead line date
	 */
	public void setDeadLineDate(Date deadLineDate) {
		this.deadLineDate = deadLineDate;
	}
	
	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public Date getImportStartDate() {
		return importStartDate;
	}

	public void setImportStartDate(Date importStartDate) {
		this.importStartDate = importStartDate;
	}

	public Date getImportFinishDate() {
		return importFinishDate;
	}

	public void setImportFinishDate(Date importFinishDate) {
		this.importFinishDate = importFinishDate;
	}

	public Date getImportTargetCompletionDate() {
		return importTargetCompletionDate;
	}

	public void setImportTargetCompletionDate(Date importTargetCompletionDate) {
		this.importTargetCompletionDate = importTargetCompletionDate;
	}

	public Date getImportDeadLineDate() {
		return importDeadLineDate;
	}

	public void setImportDeadLineDate(Date importDeadLineDate) {
		this.importDeadLineDate = importDeadLineDate;
	}

	public Long getImportId() {
		return importId;
	}

	public void setImportId(Long importId) {
		this.importId = importId;
	}

	public int getImportUniqueId() {
		return importUniqueId;
	}

	public void setImportUniqueId(int importUniqueId) {
		this.importUniqueId = importUniqueId;
	}

	public boolean isLookAheadChange() {
		return isLookAheadChange;
	}

	public void setLookAheadChange(boolean isLookAheadChange) {
		this.isLookAheadChange = isLookAheadChange;
	}

	public String getImportNotes() {
		return importNotes;
	}

	public void setImportNotes(String importNotes) {
		this.importNotes = importNotes;
	}

	// ----------------------------------------------------------------------
	// toString METHOD
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	// ----------------------------------------------------------------------
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(description);
		sb.append("|");
		sb.append(projectId);
		sb.append("|");
		sb.append(isActive);
		return sb.toString();
	}

}
