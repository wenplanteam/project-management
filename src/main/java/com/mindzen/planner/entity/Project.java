/*
 * 
 * @author Raja Gopal MRG
 */
// This Bean has a basic Primary Key (not composite) 

package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.bridge.builtin.BooleanBridge;

import com.fasterxml.jackson.annotation.JsonManagedReference;

// TODO: Auto-generated Javadoc
/**
 * Persistent class for entity stored in table "Projects".
 *
 * @author Telosys Tools Generator
 */

@Entity
@Indexed
@Table(name="projects" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="Project.countAll", query="SELECT COUNT(x) FROM Project x" )
} )
//@Where(clause = "is_deleted='false'")
public class Project extends BaseIdEntity implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    /** The name. */
    //----------------------------------------------------------------------    
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="name")
    private String     name         ;

    /** The description. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="description")
    private String     description  ;

    /** The license distribution id. */
    @Column(name="license_distribution_id", nullable=false)
    private Long       licenseDistributionId ;

    /** The is active. */
    @Field
    @FieldBridge(impl=BooleanBridge.class)
    @Column(name="is_active", nullable=false)
    private boolean    isActive     ;

    /** The project no. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="project_no", nullable=false)
    private String     projectNo    ;

    /** The project manager. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="project_manager", nullable=false)
    private String     projectManager ;

    /** The owner company. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="owner_company")
    private String     ownerCompany ;

    /** The job phone. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="job_phone")
    private String     jobPhone     ;

    /** The estimated start date. */
    @Column(name="estimated_start_date")
    private Date       estimatedStartDate ;

    /** The estimated end date. */
    @Column(name="estimated_end_date")
    private Date       estimatedEndDate ;

    /** The project stage. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="project_stage")
    private String     projectStage ;

    /** The city. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="city")
    private String     city         ;

    /** The zip code. */
    @Column(name="zip_code")
    private String     zipCode      ;

    /** The state. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="state")
    private String     state        ;

    /** The county. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="county")
    private String     county       ;

    /** The country. */
    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @Column(name="country")
    private String     country      ;

    /** The latitude. */
    @Column(name="latitude")
    private Long       latitude     ;

    /** The longitude. */
    @Column(name="longitude")
    private Long       longitude    ;

    /** The timezone. */
    @Column(name="timezone")
    private String     timezone     ;

    /** The show project address. */
    @Column(name="show_project_address")
    private Boolean    showProjectAddress ;

    /** The lookahead notification. */
    @Column(name="lookahead_notification")
    private Boolean    lookaheadNotification ;

    /** The anchor date. */
    @Column(name="anchor_date")
    private Date       anchorDate   ;

    /** The anchor week. */
    @Column(name="anchor_week")
    private String     anchorWeek   ;
    
    /** The is deleted. */
    @Field
    @FieldBridge(impl=BooleanBridge.class)
    @Column(name="is_deleted")
    private boolean    isDeleted     ;

    /** The project user. */
    @IndexedEmbedded
    @JsonManagedReference
    @OneToMany(mappedBy="project", fetch=FetchType.EAGER)
    private List<ProjectUser> projectUser;
    
    /** The leave days. */
    @Column(name = "leave_days")
    private String leaveDays;
    
    /** The week start day. */
    @Column(name = "week_start_day")
    private String weekStartDay;
    
    /** The float days. */
    @Column(name = "float_days")
    private int floatDays;
    
    /** The trade. */
    @Column(name = "trade",length=1000)
    private String trade;
    
    /** The spec division. */
    @Column(name = "spec_division", columnDefinition="text")
    private String specDivision;
    
    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------


	//----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    /**
     * Instantiates a new project.
     */
    //----------------------------------------------------------------------
    public Project()
    {
		super();
    }
    
   

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    /**
     * Gets the id.
     *
     * @return the id
     */
    //--- DATABASE MAPPING : id (  ) 
    public Long getId()
    {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    //--- DATABASE MAPPING : createdBy (  ) 
    public Long getCreatedBy()
    {
        return createdBy;
    }

    /**
     * Gets the created on.
     *
     * @return the created on
     */
    //--- DATABASE MAPPING : createdOn (  ) 
    public LocalDateTime getCreatedOn()
    {
        return createdOn;
    }

    /**
     * Gets the modified by.
     *
     * @return the modified by
     */
    //--- DATABASE MAPPING : modifiedBy (  ) 
    public Long getModifiedBy()
    {
        return modifiedBy;
    }

    /**
     * Gets the modified on.
     *
     * @return the modified on
     */
    //--- DATABASE MAPPING : modifiedOn (  ) 
    public LocalDateTime getModifiedOn()
    {
        return modifiedOn;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    //--- DATABASE MAPPING : name (  ) 
    public void setName( String name )
    {
        this.name = name;
    }
    
    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    //--- DATABASE MAPPING : description (  ) 
    public void setDescription( String description )
    {
        this.description = description;
    }
    
    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Sets the license distribution id.
     *
     * @param licenseDistributionId the new license distribution id
     */
    //--- DATABASE MAPPING : licenseDistributionId (  ) 
    public void setLicenseDistributionId( long licenseDistributionId )
    {
        this.licenseDistributionId = licenseDistributionId;
    }
    
    /**
     * Gets the license distribution id.
     *
     * @return the license distribution id
     */
    public long getLicenseDistributionId()
    {
        return this.licenseDistributionId;
    }

    /**
     * Sets the checks if is active.
     *
     * @param isActive the new checks if is active
     */
    //--- DATABASE MAPPING : isActive (  ) 
    public void setIsActive( boolean isActive )
    {
        this.isActive = isActive;
    }
    
    /**
     * Checks if is checks if is active.
     *
     * @return true, if is checks if is active
     */
    public boolean isIsActive()
    {
        return this.isActive;
    }

    /**
     * Checks if is deleted.
     *
     * @return true, if is deleted
     */
    public boolean isDeleted() {
		return isDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted the new checks if is deleted
	 */
	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	/**
	 * Sets the project no.
	 *
	 * @param projectNo the new project no
	 */
	//--- DATABASE MAPPING : projectNo (  ) 
    public void setProjectNo( String projectNo )
    {
        this.projectNo = projectNo;
    }
    
    /**
     * Gets the project no.
     *
     * @return the project no
     */
    public String getProjectNo()
    {
        return this.projectNo;
    }

    /**
     * Sets the project manager.
     *
     * @param projectManager the new project manager
     */
    //--- DATABASE MAPPING : projectManager (  ) 
    public void setProjectManager( String projectManager )
    {
        this.projectManager = projectManager;
    }
    
    /**
     * Gets the project manager.
     *
     * @return the project manager
     */
    public String getProjectManager()
    {
        return this.projectManager;
    }

    /**
     * Sets the owner company.
     *
     * @param ownerCompany the new owner company
     */
    //--- DATABASE MAPPING : ownerCompany (  ) 
    public void setOwnerCompany( String ownerCompany )
    {
        this.ownerCompany = ownerCompany;
    }
    
    /**
     * Gets the owner company.
     *
     * @return the owner company
     */
    public String getOwnerCompany()
    {
        return this.ownerCompany;
    }

    /**
     * Sets the job phone.
     *
     * @param jobPhone the new job phone
     */
    //--- DATABASE MAPPING : jobPhone (  ) 
    public void setJobPhone( String jobPhone )
    {
        this.jobPhone = jobPhone;
    }
    
    /**
     * Gets the job phone.
     *
     * @return the job phone
     */
    public String getJobPhone()
    {
        return this.jobPhone;
    }

    /**
     * Sets the estimated start date.
     *
     * @param EstimatedStartDate the new estimated start date
     */
    //--- DATABASE MAPPING : estimatedStartDate (  ) 
    public void setEstimatedStartDate( Date EstimatedStartDate )
    {
        this.estimatedStartDate = EstimatedStartDate;
    }
    
    /**
     * Gets the estimated start date.
     *
     * @return the estimated start date
     */
    public Date getEstimatedStartDate()
    {
        return this.estimatedStartDate;
    }

    /**
     * Sets the estimated end date.
     *
     * @param estimatedEndDate the new estimated end date
     */
    //--- DATABASE MAPPING : estimatedEndDate (  ) 
    public void setEstimatedEndDate( Date estimatedEndDate )
    {
        this.estimatedEndDate = estimatedEndDate;
    }
    
    /**
     * Gets the estimated end date.
     *
     * @return the estimated end date
     */
    public Date getEstimatedEndDate()
    {
        return this.estimatedEndDate;
    }

    /**
     * Sets the project stage.
     *
     * @param projectStage the new project stage
     */
    //--- DATABASE MAPPING : projectStage (  ) 
    public void setProjectStage( String projectStage )
    {
        this.projectStage = projectStage;
    }
    
    /**
     * Gets the project stage.
     *
     * @return the project stage
     */
    public String getProjectStage()
    {
        return this.projectStage;
    }

    /**
     * Sets the city.
     *
     * @param city the new city
     */
    //--- DATABASE MAPPING : city (  ) 
    public void setCity( String city )
    {
        this.city = city;
    }
    
    /**
     * Gets the city.
     *
     * @return the city
     */
    public String getCity()
    {
        return this.city;
    }

    /**
     * Sets the zip code.
     *
     * @param zipCode the new zip code
     */
    //--- DATABASE MAPPING : zipCode (  ) 
    public void setZipCode( String zipCode )
    {
        this.zipCode = zipCode;
    }
    
    /**
     * Gets the zip code.
     *
     * @return the zip code
     */
    public String getZipCode()
    {
        return this.zipCode;
    }

    /**
     * Sets the state.
     *
     * @param state the new state
     */
    //--- DATABASE MAPPING : state (  ) 
    public void setState( String state )
    {
        this.state = state;
    }
    
    /**
     * Gets the state.
     *
     * @return the state
     */
    public String getState()
    {
        return this.state;
    }

    /**
     * Sets the county.
     *
     * @param county the new county
     */
    //--- DATABASE MAPPING : county (  ) 
    public void setCounty( String county )
    {
        this.county = county;
    }
    
    /**
     * Gets the county.
     *
     * @return the county
     */
    public String getCounty()
    {
        return this.county;
    }

    /**
     * Sets the country.
     *
     * @param country the new country
     */
    //--- DATABASE MAPPING : country (  ) 
    public void setCountry( String country )
    {
        this.country = country;
    }
    
    /**
     * Gets the country.
     *
     * @return the country
     */
    public String getCountry()
    {
        return this.country;
    }

    /**
     * Sets the latitude.
     *
     * @param latitude the new latitude
     */
    //--- DATABASE MAPPING : latitude (  ) 
    public void setLatitude( Long latitude )
    {
        this.latitude = latitude;
    }
    
    /**
     * Gets the latitude.
     *
     * @return the latitude
     */
    public Long getLatitude()
    {
        return this.latitude;
    }

    /**
     * Sets the longitude.
     *
     * @param longitude the new longitude
     */
    //--- DATABASE MAPPING : longitude (  ) 
    public void setLongitude( Long longitude )
    {
        this.longitude = longitude;
    }
    
    /**
     * Gets the longitude.
     *
     * @return the longitude
     */
    public Long getLongitude()
    {
        return this.longitude;
    }

    /**
     * Sets the timezone.
     *
     * @param timezone the new timezone
     */
    //--- DATABASE MAPPING : timezone (  ) 
    public void setTimezone( String timezone )
    {
        this.timezone = timezone;
    }
    
    /**
     * Gets the timezone.
     *
     * @return the timezone
     */
    public String getTimezone()
    {
        return this.timezone;
    }

    /**
     * Sets the show project address.
     *
     * @param showProjectAddress the new show project address
     */
    //--- DATABASE MAPPING : showProjectAddress (  ) 
    public void setShowProjectAddress( Boolean showProjectAddress )
    {
        this.showProjectAddress = showProjectAddress;
    }
    
    /**
     * Gets the show project address.
     *
     * @return the show project address
     */
    public Boolean getShowProjectAddress()
    {
        return this.showProjectAddress;
    }

    /**
     * Sets the lookahead notification.
     *
     * @param lookaheadNotification the new lookahead notification
     */
    //--- DATABASE MAPPING : lookaheadNotification (  ) 
    public void setLookaheadNotification( Boolean lookaheadNotification )
    {
        this.lookaheadNotification = lookaheadNotification;
    }
    
    /**
     * Gets the lookahead notification.
     *
     * @return the lookahead notification
     */
    public Boolean getLookaheadNotification()
    {
        return this.lookaheadNotification;
    }

    /**
     * Sets the anchor date.
     *
     * @param anchorDate the new anchor date
     */
    //--- DATABASE MAPPING : anchorDate (  ) 
    public void setAnchorDate( Date anchorDate )
    {
        this.anchorDate = anchorDate;
    }
    
    /**
     * Gets the anchor date.
     *
     * @return the anchor date
     */
    public Date getAnchorDate()
    {
        return this.anchorDate;
    }

    /**
     * Sets the anchor week.
     *
     * @param anchorWeek the new anchor week
     */
    //--- DATABASE MAPPING : anchorWeek (  ) 
    public void setAnchorWeek( String anchorWeek )
    {
        this.anchorWeek = anchorWeek;
    }
    
    /**
     * Gets the anchor week.
     *
     * @return the anchor week
     */
    public String getAnchorWeek()
    {
        return this.anchorWeek;
    }

	/**
	 * Gets the project user.
	 *
	 * @return the projectUser
	 */
	public List<ProjectUser> getProjectUser() {
		return projectUser;
	}
	
	/**
	 * Sets the project user.
	 *
	 * @param projectUser the projectUser to set
	 */
	public void setProjectUser(List<ProjectUser> projectUser) {
		this.projectUser = projectUser;
	}
	
    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    /**
     * Gets the leave days.
     *
     * @return the leave days
     */
    public String getLeaveDays() {
		return leaveDays;
	}

	/**
	 * Sets the leave days.
	 *
	 * @param leaveDays the new leave days
	 */
	public void setLeaveDays(String leaveDays) {
		this.leaveDays = leaveDays;
	}

	/**
	 * Gets the week start day.
	 *
	 * @return the week start day
	 */
	public String getWeekStartDay() {
		return weekStartDay;
	}

	/**
	 * Sets the week start day.
	 *
	 * @param weekStartDay the new week start day
	 */
	public void setWeekStartDay(String weekStartDay) {
		this.weekStartDay = weekStartDay;
	}

	/**
	 * Gets the trade.
	 *
	 * @return the trade
	 */
	public String getTrade() {
		return trade;
	}

	/**
	 * Sets the trade.
	 *
	 * @param trade the new trade
	 */
	public void setTrade(String trade) {
		this.trade = trade;
	}
	
	/**
	 * Gets the spec division.
	 *
	 * @return the spec division
	 */
	public String getSpecDivision() {
		return specDivision;
	}

	/**
	 * Sets the spec division.
	 *
	 * @param specDivision the new spec division
	 */
	public void setSpecDivision(String specDivision) {
		this.specDivision = specDivision;
	}

	/**
	 * Gets the float days.
	 *
	 * @return the float days
	 */
	public int getFloatDays() {
		return floatDays;
	}

	/**
	 * Sets the float days.
	 *
	 * @param floatDays the new float days
	 */
	public void setFloatDays(int floatDays) {
		this.floatDays = floatDays;
	}



	//----------------------------------------------------------------------
    // toString METHOD
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	//----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append(name);
        sb.append("|");
        sb.append(description);
        sb.append("|");
        sb.append(licenseDistributionId);
        sb.append("|");
        sb.append(isActive);
        sb.append("|");
        sb.append(projectNo);
        sb.append("|");
        sb.append(projectManager);
        sb.append("|");
        sb.append(ownerCompany);
        sb.append("|");
        sb.append(jobPhone);
        sb.append("|");
        sb.append(estimatedStartDate);
        sb.append("|");
        sb.append(estimatedEndDate);
        sb.append("|");
        sb.append(projectStage);
        sb.append("|");
        sb.append(city);
        sb.append("|");
        sb.append(zipCode);
        sb.append("|");
        sb.append(state);
        sb.append("|");
        sb.append(county);
        sb.append("|");
        sb.append(country);
        sb.append("|");
        sb.append(latitude);
        sb.append("|");
        sb.append(longitude);
        sb.append("|");
        sb.append(timezone);
        sb.append("|");
        sb.append(showProjectAddress);
        sb.append("|");
        sb.append(lookaheadNotification);
        sb.append("|");
        sb.append(anchorDate);
        sb.append("|");
        sb.append(anchorWeek);
        sb.append("|");
        sb.append(leaveDays);
        sb.append("|");
        sb.append(weekStartDay);
        return sb.toString(); 
    }  

}
