/*
 * 
 * @author Raja Gopal MRG
 */
// This Bean has a basic Primary Key (not composite) 

package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.bridge.builtin.BooleanBridge;

import com.fasterxml.jackson.annotation.JsonBackReference;

// TODO: Auto-generated Javadoc
/**
 * Persistent class for entity stored in table "ProjectUsers".
 *
 * @author Telosys Tools Generator
 */

/**
 * @author mindzen
 *
 */
@Entity
@Indexed
@Table(name="project_users" )
// Define named queries here
@NamedQueries ( {
	@NamedQuery ( name="ProjectUser.countAll", query="SELECT COUNT(x) FROM ProjectUser x" )
} )
public class ProjectUser extends BaseIdEntity implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	//----------------------------------------------------------------------
	// ENTITY DATA FIELDS 
	/** The user id. */
	//----------------------------------------------------------------------    
	@Field
	@NumericField
	@Column(name="user_id", nullable=false)
	private Long       userId       ;

	/** The access. */
	@Column(name="access")
	private String       access       ;

	/** The project. */
	@ContainedIn
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="project_id", nullable=false)
	private Project project;

	/** The project company. */
	@ContainedIn
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="project_company_id")
	private ProjectCompany projectCompany;
	
    /** The is active. */
    @Field
    @FieldBridge(impl=BooleanBridge.class)
	@Column(name="is_active", nullable=false)
	private boolean    isActive     ;
	
	/** The project role. */
	@Column(name = "project_role" )
	private String    projectRole   ;
	
    /** The notification. */
    @OneToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="notification_id")
    private NotificationEntity notification;
	

	/** The is deleted. */
	@Field
    @FieldBridge(impl=BooleanBridge.class)
	@Column(name = "is_deleted" )
	private boolean isDeleted;

	//----------------------------------------------------------------------
	// ENTITY LINKS ( RELATIONSHIP )
	//----------------------------------------------------------------------

	/**
	 * Checks if is deleted.
	 *
	 * @return true, if is deleted
	 */
	public boolean isDeleted() {
		return isDeleted;
	}


	/**
	 * Sets the checks if is deleted.
	 *
	 * @param isDeleted the new checks if is deleted
	 */
	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	//----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	/**
	 * Instantiates a new project user.
	 */
	//----------------------------------------------------------------------
	public ProjectUser()
	{
		super();
	}


	//----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	//----------------------------------------------------------------------
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	//--- DATABASE MAPPING : id (  ) 
	public Long getId()
	{
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 * @return the long
	 */
	public Long setId(Long id)
	{
		return this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	//--- DATABASE MAPPING : createdBy (  ) 
	public Long getCreatedBy()
	{
		return createdBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	//--- DATABASE MAPPING : createdOn (  ) 
	public LocalDateTime getCreatedOn()
	{
		return createdOn;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	//--- DATABASE MAPPING : modifiedBy (  ) 
	public Long getModifiedBy()
	{
		return modifiedBy;
	}

	/**
	 * Gets the modified on.
	 *
	 * @return the modified on
	 */
	//--- DATABASE MAPPING : modifiedOn (  ) 
	public LocalDateTime getModifiedOn()
	{
		return modifiedOn;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	//--- DATABASE MAPPING : userId (  ) 
	public void setUserId( long userId )
	{
		this.userId = userId;
	}
	
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public long getUserId()
	{
		return this.userId;
	}



	/**
	 * Gets the access.
	 *
	 * @return the access
	 */
	public String getAccess() {
		return access;
	}


	/**
	 * Sets the access.
	 *
	 * @param access the new access
	 */
	public void setAccess(String access) {
		this.access = access;
	}


	/**
	 * Sets the project.
	 *
	 * @param project the new project
	 */
	//--- DATABASE MAPPING : projectId (  ) 
	public void setProject( Project project )
	{
		this.project = project;
	}
	
	/**
	 * Gets the project.
	 *
	 * @return the project
	 */
	public Project getProject()
	{
		return this.project;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive the new checks if is active
	 */
	//--- DATABASE MAPPING : isActive (  ) 
	public void setIsActive( boolean isActive )
	{
		this.isActive = isActive;
	}
	
	/**
	 * Checks if is checks if is active.
	 *
	 * @return true, if is checks if is active
	 */
	public boolean isIsActive()
	{
		return this.isActive;
	}
	
	/**
	 * Gets the project role.
	 *
	 * @return the project role
	 */
	//--- DATABASE MAPPING : projectRole (  ) 
	public String getProjectRole() {
		return projectRole;
	}


	/**
	 * Sets the project role.
	 *
	 * @param projectRole the new project role
	 */
	public void setProjectRole(String projectRole) {
		this.projectRole = projectRole;
	}

	/**
	 * Gets the project company.
	 *
	 * @return the project company
	 */
	public ProjectCompany getProjectCompany() {
		return projectCompany;
	}


	/**
	 * Sets the project company.
	 *
	 * @param projectCompany the new project company
	 */
	public void setProjectCompany(ProjectCompany projectCompany) {
		this.projectCompany = projectCompany;
	}

	/**
     * Gets the notification.
     *
     * @return the notification
     */
    public NotificationEntity getNotification() {
		return notification;
	}


	/**
	 * Sets the notification.
	 *
	 * @param notification the new notification
	 */
	public void setNotification(NotificationEntity notification) {
		this.notification = notification;
	}

	//----------------------------------------------------------------------
	// toString METHOD
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	//----------------------------------------------------------------------
	public String toString() { 
		StringBuffer sb = new StringBuffer(); 

		sb.append(userId);
		sb.append("|");
		sb.append(access);
		sb.append("|");
		sb.append(project.getId());
		sb.append("|");
		sb.append(isActive);
		return sb.toString(); 
	} 

}