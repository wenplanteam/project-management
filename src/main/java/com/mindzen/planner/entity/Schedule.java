/*
 * 
 * @author Raja Gopal MRG
 */
// This Bean has a basic Primary Key (not composite) 

package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.search.annotations.IndexedEmbedded;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * Persistent class for entity stored in table "Schedule".
 *
 * @author Telosys Tools Generator
 */

@Entity
@Table(name="schedule" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="Schedule.countAll", query="SELECT COUNT(x) FROM Schedule x" )
} )
public class Schedule extends BaseIdEntity implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    
    /** The sub task. */
    @JsonBackReference
    @ManyToOne
	@JoinColumn(name="sub_task_id")
    @IndexedEmbedded
    private SubTask       subTask;

    /** The working date. */
    @Column(name="working_date")
    private Date       workingDate  ;

    /** The worker count. */
    @Column(name="worker_count")
    private Long    workerCount  ;

    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    /**
     * Instantiates a new schedule.
     */
    //----------------------------------------------------------------------
    public Schedule()
    {
		super();
    }
    
   

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    /**
     * Gets the id.
     *
     * @return the id
     */
    //--- DATABASE MAPPING : id (  ) 
    public Long getId()
    {
        return id;
    }
    
    /**
     * Sets the id.
     *
     * @param id the id
     * @return the long
     */
    public Long setId(Long id)
   	{
   		return this.id = id;
   	}

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    //--- DATABASE MAPPING : createdBy (  ) 
    public Long getCreatedBy()
    {
        return createdBy;
    }

    /**
     * Gets the created on.
     *
     * @return the created on
     */
    //--- DATABASE MAPPING : createdOn (  ) 
    public LocalDateTime getCreatedOn()
    {
        return createdOn;
    }

    /**
     * Gets the modified by.
     *
     * @return the modified by
     */
    //--- DATABASE MAPPING : modifiedBy (  ) 
    public Long getModifiedBy()
    {
        return modifiedBy;
    }

    /**
     * Gets the modified on.
     *
     * @return the modified on
     */
    //--- DATABASE MAPPING : modifiedOn (  ) 
    public LocalDateTime getModifiedOn()
    {
        return modifiedOn;
    }

    //--- DATABASE MAPPING : subTaskId (  ) 
    /**
     * Gets the sub task.
     *
     * @return the sub task
     */
    /*public void setSubTaskId( Long subTaskId )
    {
        this.subTaskId = subTaskId;
    }
    public Long getSubTaskId()
    {
        return this.subTaskId;
    }*/
    public SubTask getSubTask() {
		return subTask;
	}

	/**
	 * Sets the sub task.
	 *
	 * @param subTask the new sub task
	 */
	public void setSubTask(SubTask subTask) {
		this.subTask = subTask;
	}
    

    /**
     * Sets the working date.
     *
     * @param workingDate the new working date
     */
    //--- DATABASE MAPPING : workingDate (  ) 
    public void setWorkingDate( Date workingDate )
    {
        this.workingDate = workingDate;
    }

	/**
	 * Gets the working date.
	 *
	 * @return the working date
	 */
	public Date getWorkingDate()
    {
        return this.workingDate;
    }

    /**
     * Sets the worker count.
     *
     * @param workerCount the new worker count
     */
    //--- DATABASE MAPPING : workerCount (  ) 
    public void setWorkerCount( Long workerCount )
    {
        this.workerCount = workerCount;
    }
    
    /**
     * Gets the worker count.
     *
     * @return the worker count
     */
    public Long getWorkerCount()
    {
        return this.workerCount;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append(subTask);
        sb.append("|");
        sb.append(workingDate);
        sb.append("|");
        sb.append(workerCount);
        return sb.toString(); 
    } 

}