/*
 * 
 * @author Raja Gopal MRG
 */
// This Bean has a basic Primary Key (not composite) 

package com.mindzen.planner.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * Persistent class for entity stored in table "SubTask".
 *
 * @author Telosys Tools Generator
 */

@Entity
@Table(name = "sub_task")
// Define named queries here
@NamedQueries({ @NamedQuery(name = "SubTask.countAll", query = "SELECT COUNT(x) FROM SubTask x") })
public class SubTask extends BaseIdEntity implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------------
	// ENTITY DATA FIELDS
	// ----------------------------------------------------------------------

	/** The description. */
	@Column(name = "description")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String description;

	/** The task. */
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "task_id")
	private Task task;

	/** The is active. */
	@Column(name = "is_active", nullable = false)
	private boolean isActive;

	/** The task type. */
	@Column(name = "task_type")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String taskType;

	/** The trade. */
	@Column(name = "trade")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String trade;

	/** The spec division. */
	@Column(name = "spec_division")
	private String specDivision;

	/** The ball in court. */
	@Column(name = "ball_in_court")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String ballInCourt;

	/** The project company id. */
	@Column(name = "project_company_id", nullable = false)
	private Long projectCompanyId;

	/** The visibility. */
	@Column(name = "visibility")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String visibility;

	/** The note. */
	@Column(name = "note")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String note;

	/** The s no. */
	@Column(name = "sno")
	private String sNo;

	/** The schedules. */
	@JsonIgnore
	@JsonManagedReference
	@OneToMany(mappedBy = "subTask", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Schedule> schedules;

	/** The task start date. */
	@Column(name = "task_start_date")
	private Date taskStartDate;

	/** The expected completion date. */
	@Column(name = "expected_completion_date")
	private Date expectedCompletionDate;

	/** The required completion date. */
	@Column(name = "required_completion_date")
	private Date requiredCompletionDate;

	/** The dead line date. */
	@Column(name = "dead_line_date")
	private Date deadLineDate;

	// New Columns added by RamachandraRao
	/** The activity id. */
	@Column(name = "activity_id")
	private String activityId;

	/** The activity description. */
	@Column(name = "activity_description")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String activityDescription;

	/** The import start date. */
	@Column(name = "import_start_date")
	private Date importStartDate;

	/** The import finish date. */
	@Column(name = "import_finish_date")
	private Date importFinishDate;

	/** The import target completion date. */
	@Column(name = "import_target_completion_date")
	private Date importTargetCompletionDate;

	/** The import dead line date. */
	@Column(name = "import_deadline_date")
	private Date importDeadLineDate;

	/** The import unique id. */
	@Column(name = "import_unique_id")
	private Integer importUniqueId;

	/** The is lookaheadChange. */
	@Column(name = "is_lookahead_change")
	private Boolean isLookAheadChange;
	
	@Column(name="import_notes")
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
	private String importNotes;


	// ----------------------------------------------------------------------
	// ENTITY LINKS ( RELATIONSHIP )
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	/**
	 * Instantiates a new sub task.
	 */
	// ----------------------------------------------------------------------
	public SubTask() {
		super();
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	// ----------------------------------------------------------------------
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	// --- DATABASE MAPPING : id ( )
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 * @return the long
	 */
	public Long setId(Long id) {
		return this.id = id;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	// --- DATABASE MAPPING : createdBy ( )
	public Long getCreatedBy() {
		return createdBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	// --- DATABASE MAPPING : createdOn ( )
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	/**
	 * Gets the modified by.
	 *
	 * @return the modified by
	 */
	// --- DATABASE MAPPING : modifiedBy ( )
	public Long getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * Gets the modified on.
	 *
	 * @return the modified on
	 */
	// --- DATABASE MAPPING : modifiedOn ( )
	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	// --- DATABASE MAPPING : description ( )
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Gets the task.
	 *
	 * @return the task
	 */
	// --- DATABASE MAPPING : taskId ( )
	public Task getTask() {
		return task;
	}

	/**
	 * Sets the task.
	 *
	 * @param task the new task
	 */
	public void setTask(Task task) {
		this.task = task;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	// --- DATABASE MAPPING : isActive ( )
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Sets the task type.
	 *
	 * @param taskType the new task type
	 */
	// --- DATABASE MAPPING : taskType ( )
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	/**
	 * Gets the task type.
	 *
	 * @return the task type
	 */
	public String getTaskType() {
		return this.taskType;
	}

	/**
	 * Sets the trade.
	 *
	 * @param trade the new trade
	 */
	// --- DATABASE MAPPING : trade ( )
	public void setTrade(String trade) {
		this.trade = trade;
	}

	/**
	 * Gets the trade.
	 *
	 * @return the trade
	 */
	public String getTrade() {
		return this.trade;
	}

	/**
	 * Gets the spec division.
	 *
	 * @return the spec division
	 */
	public String getSpecDivision() {
		return specDivision;
	}

	/**
	 * Sets the spec division.
	 *
	 * @param specDivision the new spec division
	 */
	public void setSpecDivision(String specDivision) {
		this.specDivision = specDivision;
	}

	/**
	 * Sets the ball in court.
	 *
	 * @param ballInCourt the new ball in court
	 */
	// --- DATABASE MAPPING : ballInCourt ( )
	public void setBallInCourt(String ballInCourt) {
		this.ballInCourt = ballInCourt;
	}

	/**
	 * Gets the ball in court.
	 *
	 * @return the ball in court
	 */
	public String getBallInCourt() {
		return this.ballInCourt;
	}

	/**
	 * Sets the visibility.
	 *
	 * @param visibility the new visibility
	 */
	// --- DATABASE MAPPING : visibility ( )
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	/**
	 * Gets the visibility.
	 *
	 * @return the visibility
	 */
	public String getVisibility() {
		return this.visibility;
	}

	/**
	 * Sets the note.
	 *
	 * @param note the new note
	 */
	// --- DATABASE MAPPING : note ( )
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * Gets the note.
	 *
	 * @return the note
	 */
	public String getNote() {
		return this.note;
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR LINKS
	// ----------------------------------------------------------------------

	/**
	 * Gets the s no.
	 *
	 * @return the s no
	 */
	public String getsNo() {
		return sNo;
	}

	/**
	 * Sets the s no.
	 *
	 * @param sNo the new s no
	 */
	public void setsNo(String sNo) {
		this.sNo = sNo;
	}

	/**
	 * Gets the schedules.
	 *
	 * @return the schedules
	 */
	public List<Schedule> getSchedules() {
		return schedules;
	}

	/**
	 * Sets the schedules.
	 *
	 * @param schedules the new schedules
	 */
	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	/**
	 * Gets the project company id.
	 *
	 * @return the project company id
	 */
	public Long getProjectCompanyId() {
		return projectCompanyId;
	}

	/**
	 * Sets the project company id.
	 *
	 * @param projectCompanyId the new project company id
	 */
	public void setProjectCompanyId(Long projectCompanyId) {
		this.projectCompanyId = projectCompanyId;
	}

	/**
	 * Gets the task start date.
	 *
	 * @return the task start date
	 */
	public Date getTaskStartDate() {
		return taskStartDate;
	}

	/**
	 * Sets the task start date.
	 *
	 * @param taskStartDate the new task start date
	 */
	public void setTaskStartDate(Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}

	/**
	 * Gets the expected completion date.
	 *
	 * @return the expected completion date
	 */
	public Date getExpectedCompletionDate() {
		return expectedCompletionDate;
	}

	/**
	 * Sets the expected completion date.
	 *
	 * @param expectedCompletionDate the new expected completion date
	 */
	public void setExpectedCompletionDate(Date expectedCompletionDate) {
		this.expectedCompletionDate = expectedCompletionDate;
	}

	/**
	 * Gets the required completion date.
	 *
	 * @return the required completion date
	 */
	public Date getRequiredCompletionDate() {
		return requiredCompletionDate;
	}

	/**
	 * Sets the required completion date.
	 *
	 * @param requiredCompletionDate the new required completion date
	 */
	public void setRequiredCompletionDate(Date requiredCompletionDate) {
		this.requiredCompletionDate = requiredCompletionDate;
	}

	/**
	 * Gets the dead line date.
	 *
	 * @return the dead line date
	 */
	public Date getDeadLineDate() {
		return deadLineDate;
	}

	/**
	 * Sets the dead line date.
	 *
	 * @param deadLineDate the new dead line date
	 */
	public void setDeadLineDate(Date deadLineDate) {
		this.deadLineDate = deadLineDate;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public Date getImportStartDate() {
		return importStartDate;
	}

	public void setImportStartDate(Date importStartDate) {
		this.importStartDate = importStartDate;
	}

	public Date getImportFinishDate() {
		return importFinishDate;
	}

	public void setImportFinishDate(Date importFinishDate) {
		this.importFinishDate = importFinishDate;
	}

	public Date getImportTargetCompletionDate() {
		return importTargetCompletionDate;
	}

	public void setImportTargetCompletionDate(Date importTargetCompletionDate) {
		this.importTargetCompletionDate = importTargetCompletionDate;
	}

	public Date getImportDeadLineDate() {
		return importDeadLineDate;
	}

	public void setImportDeadLineDate(Date importDeadLineDate) {
		this.importDeadLineDate = importDeadLineDate;
	}

	public int getImportUniqueId() {
		return importUniqueId;
	}

	public void setImportUniqueId(int importUniqueId) {
		this.importUniqueId = importUniqueId;
	}

	public boolean isLookAheadChange() {
		return isLookAheadChange;
	}

	public void setLookAheadChange(boolean isLookAheadChange) {
		this.isLookAheadChange = isLookAheadChange;
	}

	public String getImportNotes() {
		return importNotes;
	}

	public void setImportNotes(String importNotes) {
		this.importNotes = importNotes;
	}

	// ----------------------------------------------------------------------
	// toString METHOD
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	// ----------------------------------------------------------------------
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append(description);
		sb.append("|");
		sb.append(task);
		sb.append("|");
		sb.append(isActive);
		sb.append("|");
		sb.append(taskType);
		sb.append("|");
		sb.append(trade);
//		sb.append("|");
//		sb.append(ballInCourt);
		sb.append("|");
		sb.append(visibility);
		sb.append("|");
		sb.append(note);
		return sb.toString();
	}

}
