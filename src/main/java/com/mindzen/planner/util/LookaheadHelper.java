/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;

import com.mindzen.infra.util.GsonUtil;
import com.mindzen.planner.dto.LookaheadJasperReportDetailDTO;
import com.mindzen.planner.dto.LookaheadJasperReportRootDTO;
import com.mindzen.planner.dto.SubTaskDTO;
import com.mindzen.planner.dto.TaskDTO;
import com.mindzen.planner.entity.SubTask;
import com.mindzen.planner.entity.Task;

import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/**
 * The Class LookaheadHelper.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 30-Oct-2018
 */

/** The Constant log. */
@Slf4j
public class LookaheadHelper {

	/**
	 * Ball in court filter.
	 *
	 * @param tableAlias the table alias
	 * @param ballInCourtList the ball in court list
	 * @return the string
	 */
	public static String ballInCourtFilter(String tableAlias, List<String> ballInCourtList) {
		return formQuery(tableAlias, "project_company_id", ballInCourtList);
	}

	/**
	 * Visibility filter.
	 *
	 * @param tableAlias the table alias
	 * @param accessibilityList the accessibility list
	 * @return the string
	 */
	public static String visibilityFilter(String tableAlias, List<String> accessibilityList) {
		return formQuery(tableAlias, "visibility", accessibilityList);
	}

	/**
	 * Task type filter.
	 *
	 * @param tableAlias the table alias
	 * @param taskList the task list
	 * @return the string
	 */
	public static String taskTypeFilter(String tableAlias, List<String> taskList) {
		return formQuery(tableAlias, "task_type", taskList);
	}

	/**
	 * Trade filter.
	 *
	 * @param tableAlias the table alias
	 * @param tradeList the trade list
	 * @return the string
	 */
	public static String tradeFilter(String tableAlias, List<String> tradeList) {
		return formQuery(tableAlias, "trade", tradeList);
	}
	
	/**
	 * specDivision filter.
	 *
	 * @param tableAlias the table alias
	 * @param tradeList the trade list
	 * @return the string
	 */
	public static String specDivisionFilter(String tableAlias, List<String> tradeList) {
		return formQuery(tableAlias, "spec_division", tradeList);
	}
	

	/**
	 * Form query.
	 *
	 * @param tableAlias the table alias
	 * @param field the field
	 * @param data the data
	 * @return the string
	 */
	private static String formQuery(String tableAlias, String field, List<String> data) {
		if(null != data && !data.isEmpty())
			return " AND "+tableAlias+"."+field+" IN ('" + String.join("','", data) +"')";
		else
			return "";
	}

	/**
	 * Write json to file.
	 *
	 * @param lookaheadJasperReportRootDTO the lookahead jasper report root DTO
	 * @param fileName the file name
	 * @return the string
	 */
	public static String writeJsonToFile(LookaheadJasperReportRootDTO lookaheadJasperReportRootDTO, String fileName) {
		String json = GsonUtil.toJsonStringFromObject(lookaheadJasperReportRootDTO);
		try {
			Path path = Paths.get(PathUtil.WENPLAN_LOOKAHED_JSON_HOME+PathUtil.FILE_SEPARATOR+fileName);
			Files.write(path, json.getBytes());
		}catch(Exception e) {
			log.error(e.getMessage());
		}
		return json;
	}

	/**
	 * Null check.
	 *
	 * @param object the object
	 * @return the string
	 */
	public static String nullCheck(Object object) {
		if(null != object)
			return "x";
		return "";
	}

	
	/**
	 * Sets the color code.
	 *
	 * @param object the object
	 * @param requiredTime the required time
	 * @param leaveDay the leave day
	 * @param floatDayslist the float dayslist
	 * @return the string
	 */
	public static String setColorCode(Object object,Timestamp requiredTime, boolean leaveDay, List<Timestamp> floatDayslist) {
		if(leaveDay) {
			return "offDays";
		}
		Timestamp workingDateTime = null;
		if(null != object ) {
			String workingCount = (String) object;
			String[] workingCountAndWorkingDate = workingCount.split("_");
			String workingDate = workingCountAndWorkingDate[1];
			workingDateTime = Timestamp.valueOf(workingDate);
			final Timestamp finalWorkingDateTime = workingDateTime;
			if(floatDayslist.stream().anyMatch(obj->obj.equals(finalWorkingDateTime))) {
				return "warning";
			}
			
		}else{
			return "notScheduledYet";
		}
		if(requiredTime!=null && workingDateTime.after(requiredTime)) {
				return "behindSchedule";
		}
		 
		return "onTrack";
	}
	
	/**
	 * Fill day and date.
	 *
	 * @param lookaheadJasperData the lookahead jasper data
	 * @param localDates the local dates
	 * @return the lookahead jasper report detail DTO
	 */
	public static LookaheadJasperReportDetailDTO fillDayAndDate(LookaheadJasperReportDetailDTO lookaheadJasperData, List<LocalDate> localDates) {
		lookaheadJasperData.setDay_1(DateUtil.shortMonth(localDates.get(0)));
		lookaheadJasperData.setDay_2(DateUtil.shortMonth(localDates.get(1)));
		lookaheadJasperData.setDay_3(DateUtil.shortMonth(localDates.get(2)));
		lookaheadJasperData.setDay_4(DateUtil.shortMonth(localDates.get(3)));
		lookaheadJasperData.setDay_5(DateUtil.shortMonth(localDates.get(4)));
		lookaheadJasperData.setDay_6(DateUtil.shortMonth(localDates.get(5)));
		lookaheadJasperData.setDay_7(DateUtil.shortMonth(localDates.get(6)));
		lookaheadJasperData.setDay_8(DateUtil.shortMonth(localDates.get(7)));
		lookaheadJasperData.setDay_9(DateUtil.shortMonth(localDates.get(8)));
		lookaheadJasperData.setDay_10(DateUtil.shortMonth(localDates.get(9)));
		lookaheadJasperData.setDay_11(DateUtil.shortMonth(localDates.get(10)));
		lookaheadJasperData.setDay_12(DateUtil.shortMonth(localDates.get(11)));
		lookaheadJasperData.setDay_13(DateUtil.shortMonth(localDates.get(12)));
		lookaheadJasperData.setDay_14(DateUtil.shortMonth(localDates.get(13)));
		lookaheadJasperData.setDay_15(DateUtil.shortMonth(localDates.get(14)));
		lookaheadJasperData.setDay_16(DateUtil.shortMonth(localDates.get(15)));
		lookaheadJasperData.setDay_17(DateUtil.shortMonth(localDates.get(16)));
		lookaheadJasperData.setDay_18(DateUtil.shortMonth(localDates.get(17)));
		lookaheadJasperData.setDay_19(DateUtil.shortMonth(localDates.get(18)));
		lookaheadJasperData.setDay_20(DateUtil.shortMonth(localDates.get(19)));
		lookaheadJasperData.setDay_21(DateUtil.shortMonth(localDates.get(20)));
		lookaheadJasperData.setDay_22(DateUtil.shortMonth(localDates.get(21)));
		lookaheadJasperData.setDay_23(DateUtil.shortMonth(localDates.get(22)));
		lookaheadJasperData.setDay_24(DateUtil.shortMonth(localDates.get(23)));
		lookaheadJasperData.setDay_25(DateUtil.shortMonth(localDates.get(24)));
		lookaheadJasperData.setDay_26(DateUtil.shortMonth(localDates.get(25)));
		lookaheadJasperData.setDay_27(DateUtil.shortMonth(localDates.get(26)));
		lookaheadJasperData.setDay_28(DateUtil.shortMonth(localDates.get(27)));

		lookaheadJasperData.setDay_1_header(DateUtil.getDayOfMonth(localDates.get(0)));
		lookaheadJasperData.setDay_2_header(DateUtil.getDayOfMonth(localDates.get(1)));
		lookaheadJasperData.setDay_3_header(DateUtil.getDayOfMonth(localDates.get(2)));
		lookaheadJasperData.setDay_4_header(DateUtil.getDayOfMonth(localDates.get(3)));
		lookaheadJasperData.setDay_5_header(DateUtil.getDayOfMonth(localDates.get(4)));
		lookaheadJasperData.setDay_6_header(DateUtil.getDayOfMonth(localDates.get(5)));
		lookaheadJasperData.setDay_7_header(DateUtil.getDayOfMonth(localDates.get(6)));
		lookaheadJasperData.setDay_8_header(DateUtil.getDayOfMonth(localDates.get(7)));
		lookaheadJasperData.setDay_9_header(DateUtil.getDayOfMonth(localDates.get(8)));
		lookaheadJasperData.setDay_10_header(DateUtil.getDayOfMonth(localDates.get(9)));
		lookaheadJasperData.setDay_11_header(DateUtil.getDayOfMonth(localDates.get(10)));
		lookaheadJasperData.setDay_12_header(DateUtil.getDayOfMonth(localDates.get(11)));
		lookaheadJasperData.setDay_13_header(DateUtil.getDayOfMonth(localDates.get(12)));
		lookaheadJasperData.setDay_14_header(DateUtil.getDayOfMonth(localDates.get(13)));
		lookaheadJasperData.setDay_15_header(DateUtil.getDayOfMonth(localDates.get(14)));
		lookaheadJasperData.setDay_16_header(DateUtil.getDayOfMonth(localDates.get(15)));
		lookaheadJasperData.setDay_17_header(DateUtil.getDayOfMonth(localDates.get(16)));
		lookaheadJasperData.setDay_18_header(DateUtil.getDayOfMonth(localDates.get(17)));
		lookaheadJasperData.setDay_19_header(DateUtil.getDayOfMonth(localDates.get(18)));
		lookaheadJasperData.setDay_20_header(DateUtil.getDayOfMonth(localDates.get(19)));
		lookaheadJasperData.setDay_21_header(DateUtil.getDayOfMonth(localDates.get(20)));
		lookaheadJasperData.setDay_22_header(DateUtil.getDayOfMonth(localDates.get(21)));
		lookaheadJasperData.setDay_23_header(DateUtil.getDayOfMonth(localDates.get(22)));
		lookaheadJasperData.setDay_24_header(DateUtil.getDayOfMonth(localDates.get(23)));
		lookaheadJasperData.setDay_25_header(DateUtil.getDayOfMonth(localDates.get(24)));
		lookaheadJasperData.setDay_26_header(DateUtil.getDayOfMonth(localDates.get(25)));
		lookaheadJasperData.setDay_27_header(DateUtil.getDayOfMonth(localDates.get(26)));
		lookaheadJasperData.setDay_28_header(DateUtil.getDayOfMonth(localDates.get(27)));

		return lookaheadJasperData;
	}

	/**
	 * Fill empty day values.
	 *
	 * @param lookaheadJasperData the lookahead jasper data
	 * @return the lookahead jasper report detail DTO
	 */
	public static LookaheadJasperReportDetailDTO fillEmptyDayValues(LookaheadJasperReportDetailDTO lookaheadJasperData) {
		lookaheadJasperData.setDay_1_value("");
		lookaheadJasperData.setDay_2_value("");
		lookaheadJasperData.setDay_3_value("");
		lookaheadJasperData.setDay_4_value("");
		lookaheadJasperData.setDay_5_value("");
		lookaheadJasperData.setDay_6_value("");
		lookaheadJasperData.setDay_7_value("");
		lookaheadJasperData.setDay_8_value("");
		lookaheadJasperData.setDay_9_value("");
		lookaheadJasperData.setDay_10_value("");
		lookaheadJasperData.setDay_11_value("");
		lookaheadJasperData.setDay_12_value("");
		lookaheadJasperData.setDay_13_value("");
		lookaheadJasperData.setDay_14_value("");
		lookaheadJasperData.setDay_15_value("");
		lookaheadJasperData.setDay_16_value("");
		lookaheadJasperData.setDay_17_value("");
		lookaheadJasperData.setDay_18_value("");
		lookaheadJasperData.setDay_19_value("");
		lookaheadJasperData.setDay_20_value("");
		lookaheadJasperData.setDay_21_value("");
		lookaheadJasperData.setDay_22_value("");
		lookaheadJasperData.setDay_23_value("");
		lookaheadJasperData.setDay_24_value("");
		lookaheadJasperData.setDay_25_value("");
		lookaheadJasperData.setDay_26_value("");
		lookaheadJasperData.setDay_27_value("");
		lookaheadJasperData.setDay_28_value("");
		return lookaheadJasperData;
	}
	
	/**
	 * Fill task day values cd.
	 *
	 * @param lookaheadJasperData the lookahead jasper data
	 * @return the lookahead jasper report detail DTO
	 */
	public static LookaheadJasperReportDetailDTO fillTaskDayValuesCd(LookaheadJasperReportDetailDTO lookaheadJasperData) {
		lookaheadJasperData.setDay_1_value_cd("true");
		lookaheadJasperData.setDay_2_value_cd("true");
		lookaheadJasperData.setDay_3_value_cd("true");
		lookaheadJasperData.setDay_4_value_cd("true");
		lookaheadJasperData.setDay_5_value_cd("true");
		lookaheadJasperData.setDay_6_value_cd("true");
		lookaheadJasperData.setDay_7_value_cd("true");
		lookaheadJasperData.setDay_8_value_cd("true");
		lookaheadJasperData.setDay_9_value_cd("true");
		lookaheadJasperData.setDay_10_value_cd("true");
		lookaheadJasperData.setDay_11_value_cd("true");
		lookaheadJasperData.setDay_12_value_cd("true");
		lookaheadJasperData.setDay_13_value_cd("true");
		lookaheadJasperData.setDay_14_value_cd("true");
		lookaheadJasperData.setDay_15_value_cd("true");
		lookaheadJasperData.setDay_16_value_cd("true");
		lookaheadJasperData.setDay_17_value_cd("true");
		lookaheadJasperData.setDay_18_value_cd("true");
		lookaheadJasperData.setDay_19_value_cd("true");
		lookaheadJasperData.setDay_20_value_cd("true");
		lookaheadJasperData.setDay_21_value_cd("true");
		lookaheadJasperData.setDay_22_value_cd("true");
		lookaheadJasperData.setDay_23_value_cd("true");
		lookaheadJasperData.setDay_24_value_cd("true");
		lookaheadJasperData.setDay_25_value_cd("true");
		lookaheadJasperData.setDay_26_value_cd("true");
		lookaheadJasperData.setDay_27_value_cd("true");
		lookaheadJasperData.setDay_28_value_cd("true");
		return lookaheadJasperData;
	}

	/**
	 * Pivot query.
	 *
	 * @param localDates the local dates
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param projectId the project id
	 * @param projectCompanyId the project company id
	 * @param projectRole the project role
	 * @return the string
	 */
	public static String pivotQuery(List<LocalDate> localDates, LocalDate startDate, LocalDate endDate, Long projectId, Long projectCompanyId, String projectRole) {
		int i = 0;
		StringBuilder crosstabQuery = new StringBuilder("select * from crosstab($$ select s.sub_task_id, date, CONCAT(CAST(worker_count AS text), '_', CAST(working_date AS text)) AS worker_count from calendar c, schedule s, sub_task st, task t where c.date=s.working_date AND s.sub_task_id=st.id AND st.task_id=t.id and s.working_date between '"+startDate+"' AND '"+endDate+"' AND project_id="+projectId+" AND t.is_active=true AND st.is_active=true ");
	//	if(!Constants.SUB_CONTRACTOR.equals(projectRole))
		if(!projectRole.contains(Constants.SC_USERS))
			crosstabQuery.append("order by 1 $$, $$values ");
		else {
			//crosstabQuery.append("AND t.created_by="+userId+" order by 1 $$, $$values ");
			crosstabQuery.append("AND st.project_company_id="+projectCompanyId+" order by 1 $$, $$values ");
		}
		StringBuilder outerCrosstabQuery = new StringBuilder();
		StringBuilder resultOuterQuery = new StringBuilder("as (sub_task_id int, ");
		while(i<28) {
			LocalDate localDate = localDates.get(i);
			StringBuilder resultInnerQuery = new StringBuilder("");
			StringBuilder innerCrosstabQuery = new StringBuilder("('");
			if(i!=27) {
				innerCrosstabQuery.append(localDate+"'), ");
				resultInnerQuery.append("\""+localDate+"\" text, ");
			}
			else {
				innerCrosstabQuery.append(localDate+"') $$) ");
				resultInnerQuery.append("\""+localDate+"\" text)");
			}
			outerCrosstabQuery.append(innerCrosstabQuery);
			resultOuterQuery.append(resultInnerQuery);
			i++;
		}
		return crosstabQuery.append(outerCrosstabQuery).append(resultOuterQuery).toString();
	}

	/**
	 * Full project location.
	 *
	 * @param city the city
	 * @param state the state
	 * @return the string
	 */
	public static String fullProjectLocation(String city, String state) {
		String location = (city+", "+state).trim();
		if(location.endsWith(","))
			location = location.substring(0, location.length()-1);
		if(location.startsWith(","))
			location = location.substring(1, location.length());

		return location.trim();
	}

	/**
	 * Load week month.
	 *
	 * @param lookaheadJasperData the lookahead jasper data
	 * @param week_1_month the week 1 month
	 * @param week_2_month the week 2 month
	 * @param week_3_month the week 3 month
	 * @param week_4_month the week 4 month
	 * @return the lookahead jasper report detail DTO
	 */
	public static LookaheadJasperReportDetailDTO loadWeekMonth(LookaheadJasperReportDetailDTO lookaheadJasperData, String week_1_month, String week_2_month, String week_3_month, String week_4_month) {
		lookaheadJasperData.setWeek_1_month(week_1_month);
		lookaheadJasperData.setWeek_2_month(week_2_month);
		lookaheadJasperData.setWeek_3_month(week_3_month);
		lookaheadJasperData.setWeek_4_month(week_4_month);

		return lookaheadJasperData;
	}

	/**
	 * Load project info.
	 *
	 * @param lookaheadJasperData the lookahead jasper data
	 * @param projectCompany the project company
	 * @param projectLocation the project location
	 * @param projectName the project name
	 * @return the lookahead jasper report detail DTO
	 */
	public static LookaheadJasperReportDetailDTO loadProjectInfo(LookaheadJasperReportDetailDTO lookaheadJasperData, String projectCompany, String projectLocation, String projectName) {
		lookaheadJasperData.setProjectName(projectName);
		lookaheadJasperData.setProjectLocation(projectLocation);
		lookaheadJasperData.setProjectCompany(projectCompany);
		return lookaheadJasperData;
	}

	/**
	 * Sets the task payload.
	 *
	 * @param task the task
	 * @param taskDTO the task DTO
	 * @return the task
	 */
	public static Task setTaskPayload(Task task, TaskDTO taskDTO) {
		if(null != taskDTO.getDescription()) {
            
	task.setDescription(taskDTO.getDescription());
		}
		if(taskDTO.getActivityDescription()!=null)
			task.setActivityDescription(taskDTO.getActivityDescription());
		
		if(null != taskDTO.getTaskType())
			task.setTaskType(taskDTO.getTaskType());
		if(null != taskDTO.getTrade())		
			task.setTrade(taskDTO.getTrade());
		if(null != taskDTO.getSpecDivision())		
			task.setSpecDivision(taskDTO.getSpecDivision());
		if(null != taskDTO.getVisibility())
			task.setVisibility(taskDTO.getVisibility());
		if(null != taskDTO.getBallInCourt())
			task.setBallInCourt(taskDTO.getBallInCourt());
		if(null != taskDTO.getNote())
			task.setNote(taskDTO.getNote());
		if(null != taskDTO.getSNo())
			task.setsNo(taskDTO.getSNo());
		if(null != taskDTO.getProjectCompanyId())
			task.setProjectCompanyId(taskDTO.getProjectCompanyId());
		
		if(taskDTO.getTaskStartDate()!=null) {
			task.setTaskStartDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
//			task.setImportStartDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		}else {
			task.setTaskStartDate(null);
		}
		if(taskDTO.getTaskFinishDate()!=null) {
			task.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
//			task.setImportFinishDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
//			task.setImportTsetTaskPayloadsetTaskPayloadsetTaskPayloadsetTaskPayloadargetCompletionDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		}else {
			task.setExpectedCompletionDate(null);
		}
		if(taskDTO.getRequiredCompletionDate()!=null) {
			task.setRequiredCompletionDate(DateUtil.stringToDateViaInstant(taskDTO.getRequiredCompletionDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			
		}else {
			task.setRequiredCompletionDate(null);
		}
		if(taskDTO.getDeadLineDate()!=null) {
			task.setDeadLineDate(DateUtil.stringToDateViaInstant(taskDTO.getDeadLineDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
//			task.setImportDeadLineDate(DateUtil.stringToDateViaInstant(taskDTO.getDeadLineDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			
		}else {
			task.setDeadLineDate(null);
		}
		if(taskDTO.isLookAheadChange()) {
			task.setLookAheadChange(true);
		}else {
			task.setLookAheadChange(false);
		}
		
		return task;
	}

	/**
	 * Sets the sub task payload.
	 *
	 * @param subTask the sub task
	 * @param subTaskDTO the sub task DTO
	 * @return the sub task
	 */
	public static SubTask setSubTaskPayload(SubTask subTask, SubTaskDTO subTaskDTO) {
		if(null != subTaskDTO.getDescription())
			subTask.setDescription(subTaskDTO.getDescription());
		if(null != subTaskDTO.getTaskType())
			subTask.setTaskType(subTaskDTO.getTaskType());
		if(null != subTaskDTO.getTrade())
			subTask.setTrade(subTaskDTO.getTrade());
		if(null != subTaskDTO.getSpecDivision())
			subTask.setSpecDivision(subTaskDTO.getSpecDivision());
		if(null != subTaskDTO.getVisibility())
			subTask.setVisibility(subTaskDTO.getVisibility());
		if(null != subTaskDTO.getBallInCourt())
			subTask.setBallInCourt(subTaskDTO.getBallInCourt());
		if(null != subTaskDTO.getNote())
			subTask.setNote(subTaskDTO.getNote());
		if(null != subTaskDTO.getSNo())
			subTask.setsNo(subTaskDTO.getSNo());
		if(null != subTaskDTO.getProjectCompanyId())
			subTask.setProjectCompanyId(subTaskDTO.getProjectCompanyId());
		
		if(subTaskDTO.getTaskStartDate()!=null) {
			subTask.setTaskStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));	
		}else {
			subTask.setTaskStartDate(null);
		}
		if(subTaskDTO.getTaskFinishDate()!=null) {
			subTask.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));	
		}else {
			subTask.setExpectedCompletionDate(null);
		}
		if(subTaskDTO.getRequiredCompletionDate()!=null) {
			subTask.setRequiredCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));	
		}else {
			subTask.setRequiredCompletionDate(null);
		}
		if(subTaskDTO.getDeadLineDate()!=null) {
			subTask.setDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getDeadLineDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));	
		}else {
			subTask.setDeadLineDate(null);
		}
		if(subTaskDTO.isLookAheadChange()) {
			subTask.setLookAheadChange(true);
		}else {
			subTask.setLookAheadChange(false);
		}
		
		return subTask;
	}
 

}
