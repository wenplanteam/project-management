/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

import org.springframework.http.HttpStatus;

import com.mindzen.infra.exception.CustomRuntimeException;

// TODO: Auto-generated Javadoc
/**
 * The Class TaskCompletionUtil for Calculations.
 * 
 * @author Raja Gopal MRG
 */
public class TaskCompletionUtil {

	/**
	 * Instantiates a new task completion utilize for Calculations.
	 */
	private TaskCompletionUtil() {
		super();
	}

	/**
	 * Gets the task completed percentage.
	 *
	 * @param totalNoOfDays the total no of days
	 * @param completedDays the completed days
	 * @return the task completed percentage
	 */
	public static BigDecimal getTaskCompletedPercentage(Long totalNoOfDays, Long completedDays){
		BigDecimal percentageValue;
		try {
			Float percentage = (new Float(completedDays)/new Float(totalNoOfDays))*100;
			percentageValue = new BigDecimal(Math.round(percentage));
			//percentageValue = new BigDecimal(completedDays).divide(new BigDecimal(totalNoOfDays), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
		} catch (ArithmeticException e) {
			throw new CustomRuntimeException("Airthimetic Exception divided by zero", HttpStatus.EXPECTATION_FAILED);
		}
		return percentageValue;
	}

	/**
	 * Gets the total no of days.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the total no of days
	 */
	public static Long getTotalNoOfDays(Date startDate, Date endDate) {
		LocalDate endLocalDate = DateUtil.getLocalDate(endDate);
		LocalDate startLocalDate = DateUtil.getLocalDate(startDate);
		return ChronoUnit.DAYS.between(startLocalDate, endLocalDate);
	}

	/**
	 * Gets the total no of completed days.
	 *
	 * @param startDate the start date
	 * @return the total no of completed days
	 */
	public static Long getTotalNoOfCompletedDays(Date startDate) {
		LocalDate startLocalDate = DateUtil.getLocalDate(startDate);
		return ChronoUnit.DAYS.between(startLocalDate, LocalDate.now());
	}

	public static UUID generateUUIdV4() {
		return UUID.randomUUID();
	}

}
