package com.mindzen.planner.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.HttpStatus;

import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.infra.util.ImportExcelData;
import com.mindzen.planner.dto.ImportExcelTaskDTO;

/**
 * The Class ExcelUtil.
 * 
 * @author MRG
 */
public class ExcelUtil {

	/**
	 * Instantiates a new excel util.
	 */
	private ExcelUtil() {
		super();
	}


	/**
	 * Transform excel file to object.
	 *
	 * @param fileName the file name
	 * @return the list
	 */
	public static Map<String, List<ImportExcelTaskDTO>> transformExcelFileToObject(String fileName) {
		Map<String, List<ImportExcelTaskDTO>> importTaskMap= new HashMap<>();
		Workbook workbook = null;
		try {
			workbook = ImportExcelData.readExcelFileToObject(PathUtil.WENPLAN_IMPORT_EXCEL+PathUtil.FILE_SEPARATOR+fileName);
		} catch (IOException e) {
			throw new CustomRuntimeException(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
		if(workbook != null)
		{
			convertSheetRecordToObj(importTaskMap, workbook);
		}
		else
			return Collections.emptyMap();
		return importTaskMap;
	}


	/**
	 * Transform excel stream to object.
	 *
	 * @param inputStream the input stream
	 * @param fileName the file name
	 * @return the map
	 */
	public static Map<String, List<ImportExcelTaskDTO>> transformExcelStreamToObject(FileInputStream inputStream, String fileName) {
		Map<String, List<ImportExcelTaskDTO>> importTaskMap= new HashMap<>();
		Workbook workbook = null;
		try {
			workbook = ImportExcelData.getWorkbook(inputStream, fileName);
		} catch (IOException e) {
			throw new CustomRuntimeException(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
		if(workbook != null)
		{
			convertSheetRecordToObj(importTaskMap, workbook);
		}
		else
			return Collections.emptyMap();
		return importTaskMap;
	}

	
	/**
	 * Convert sheet record to object.
	 *
	 * @param importTaskMap the import task map
	 * @param workbook the workbook
	 * @return the map
	 */
	private static Map<String, List<ImportExcelTaskDTO>> convertSheetRecordToObj(Map<String, List<ImportExcelTaskDTO>> importTaskMap, Workbook workbook) {
		List<ImportExcelTaskDTO> importExcelTaskList= null;
		DataFormatter dataFormatter = new DataFormatter();
		String key = null;
		for (Row row : workbook.getSheetAt(0)) {
			if(row.getRowNum() != 0)
			{
				if(!dataFormatter.formatCellValue(row.getCell(0)).equals("")){
					importExcelTaskList= new ArrayList<>();
					key= dataFormatter.formatCellValue(row.getCell(0));
					importExcelTaskList.add(transformCellValToObj(row));
					importTaskMap.put(key, importExcelTaskList);
				}
				else if(key != null){
					importExcelTaskList.add(transformCellValToObj(row));
					importTaskMap.put(key, importExcelTaskList);
				}
			}
		}
		return importTaskMap;
	}

	/**
	 * Transform cell val to obj.
	 *
	 * @param row the row
	 * @return the import excel task DTO
	 */
	private static ImportExcelTaskDTO transformCellValToObj(Row row) {
		DataFormatter dataFormatter = new DataFormatter();
		ImportExcelTaskDTO importExcelTaskDTO=new ImportExcelTaskDTO();
		if(!dataFormatter.formatCellValue(row.getCell(0)).equals(""))
			importExcelTaskDTO.setTaskDescription(dataFormatter.formatCellValue(row.getCell(0)));
		if(!dataFormatter.formatCellValue(row.getCell(1)).equals(""))
			importExcelTaskDTO.setSubTaskDescription(dataFormatter.formatCellValue(row.getCell(1)));
		if(!dataFormatter.formatCellValue(row.getCell(2)).equals(""))
			importExcelTaskDTO.setStartDate(dataFormatter.formatCellValue(row.getCell(2)));
		if(!dataFormatter.formatCellValue(row.getCell(3)).equals(""))
			importExcelTaskDTO.setFinishDate(dataFormatter.formatCellValue(row.getCell(3)));
		if(!dataFormatter.formatCellValue(row.getCell(4)).equals(""))
			importExcelTaskDTO.setTargetCompletionDate(dataFormatter.formatCellValue(row.getCell(4)));
		if(!dataFormatter.formatCellValue(row.getCell(5)).equals(""))
			importExcelTaskDTO.setTaskType(dataFormatter.formatCellValue(row.getCell(5)));
		if(!dataFormatter.formatCellValue(row.getCell(6)).equals(""))
			importExcelTaskDTO.setResponsibility(dataFormatter.formatCellValue(row.getCell(6)));
		if(!dataFormatter.formatCellValue(row.getCell(7)).equals(""))
			importExcelTaskDTO.setSpecDivision(dataFormatter.formatCellValue(row.getCell(7)));
		return importExcelTaskDTO;
	}

}
