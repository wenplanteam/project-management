package com.mindzen.planner.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.TaskDTO;
import com.mindzen.planner.service.ImportService;
import com.mindzen.planner.service.LookaheadAccessService;

@RestController
public class ImportController {

	@Autowired
	ImportService importService;

	@Resource
	LookaheadAccessService lookaheadAccessService;

	@PostMapping("/importExcel")
	public MSAResponse importExcel(@RequestParam String excelFileName, @RequestParam Long projectId,
			@RequestParam Long projectCompanyId) throws Exception {
		return importService.importExcel(excelFileName, projectId, projectCompanyId);
	}

	@PostMapping("/importMppFile")
	public MSAResponse importMppFile(@RequestParam String mppFileName, @RequestParam Long projectId,
			@RequestParam Long projectCompanyId, @RequestParam(required = false) boolean isRenew,
			@RequestParam(required = false) Long importId) throws Exception {
		return importService.importMppFile(mppFileName, projectId, projectCompanyId, isRenew, importId);
	}

	@PostMapping("/importXerFile")
	public MSAResponse importXerFile(@RequestParam String xerFileName, @RequestParam Long projectId,
			@RequestParam Long projectCompanyId, @RequestParam(required = false) boolean isRenew) throws Exception {
		return importService.importXerFile(xerFileName, projectId, projectCompanyId, isRenew);
	}

	@PostMapping("/exportMppFile")
	public String exportMppFile(@RequestParam Long projectId) {
		return lookaheadAccessService.exportMppFile(projectId);
	}

	@GetMapping("/importHistory")
	public MSAResponse importHistory(@RequestParam Long projectId, @RequestParam  Integer pageNo,@RequestParam Integer pageSize) {
		return importService.importHistory(projectId, pageNo,pageSize);
	}

	@PutMapping("/updateMppFile")
	public MSAResponse updateMppFile(@RequestParam String mppFileName, @RequestParam Long projectId,
			@RequestParam Long projectCompanyId, @RequestParam Long importHistoryId, HttpServletRequest request) {
		return importService.updateImportMppFile(mppFileName, projectId, projectCompanyId, importHistoryId);
	}

	@PutMapping("/clearSchedule")
	public MSAResponse clearSchedule(@RequestParam Long projectId,
			@RequestParam Long projectCompanyId, @RequestParam Long importHistoryId, HttpServletRequest request) {
		return importService.clearSchedule( projectId, projectCompanyId, importHistoryId);
	}

}
