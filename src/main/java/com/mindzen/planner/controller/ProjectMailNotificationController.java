/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.scheduler.ProjectEmailNotificationScheduler;
import com.mindzen.planner.service.ProjectEmailNotificationService;

/**
 * The Class ProjectMailNotificationController.
 * 
 * @author Raja Gopal MRG
 */
@RestController
public class ProjectMailNotificationController {

	/** The project service. */
	@Autowired
	private ProjectEmailNotificationService projectNotifyService;
	
	@Autowired
	private ProjectEmailNotificationScheduler scheduler;
	
	@GetMapping("/callCron")
	public void callCron() {
		scheduler.projectSummaryScheduler();
	}
	/**
	 * Gets the task summary mail.
	 *
	 * @param projectId the project id
	 * @return the task summary mail
	 */
	@GetMapping("/taskSummaryMail/{projectId}")
	public MSAResponse getTaskSummaryMail(@PathVariable(required =true, value= "projectId") Long projectId)
	{
		return projectNotifyService.getTaskSummaryByProjectId(projectId);
	}
	
}