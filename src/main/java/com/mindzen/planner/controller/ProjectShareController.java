/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProjectUserDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.service.ProjectShareService;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectShareController.
 *
 * @author Naveenkumar Boopathi
 */
@RestController
public class ProjectShareController {

	/** The project share service. */
	@Resource
	ProjectShareService projectShareService;

	/**
	 * Share project to user.
	 *
	 * @param projectUserDTO the project user DTO
	 * @return the MSA response
	 */
	@PostMapping("/shareProject")
	public MSAResponse shareProjectToUser(@RequestBody ProjectUserDTO projectUserDTO) {
		return projectShareService.shareProject(projectUserDTO);
	}
	
	/**
	 * Edits the share project.
	 *
	 * @param projectUserDTO the project user DTO
	 * @return the MSA response
	 */
	@PutMapping("/shareProject")
	public MSAResponse editShareProject(@RequestBody ProjectUserDTO projectUserDTO) {
		return projectShareService.editShareProject(projectUserDTO);
	}
	
	/**
	 * Update project shared user details.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	@PostMapping("/updateProjectSharedUserDetails")
	public MSAResponse updateProjectSharedUserDetails(@RequestBody UserDTO userDTO) {
		return projectShareService.updateProjectSharedUserDetails(userDTO);
	}

}
