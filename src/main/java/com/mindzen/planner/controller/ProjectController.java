/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.controller;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.LookaheadJasperReportRootDTO;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.service.PercentageCompleteService;
import com.mindzen.planner.service.ProjectService;


/**
 * The Class ProjectController.
 */
@RestController
@CrossOrigin
public class ProjectController {

	/** The project service. */
	@Resource
	ProjectService projectService;

	@Resource
	PercentageCompleteService percentageCompleteService;

	/**
	 * load all projects.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param status the status
	 * @param searchTerm the search term
	 * @param created the created
	 * @return the MSA response
	 */

	@GetMapping("/projects")
	public MSAResponse loadProjects(@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false, value = "status") Boolean status,
			@RequestParam(required = false, value = "searchTerm") String searchTerm,
			@RequestParam(required = true, value = "created") Long created) {
		if (null == pageNo)
			pageNo = 0;
		else
			pageNo = pageNo - 1;
		if(null == pageSize)
			pageSize = 40;

		PageRequest pageRequest = PageRequest.of(pageNo, pageSize, Sort.Direction.DESC, "id");
		return projectService.projects(pageRequest, status, searchTerm, created);
	}

	/**
	 * create new projects.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/project")
	public MSAResponse postProject(@RequestBody ProjectDTO projectDTO) {
		return projectService.postProject(projectDTO);
	}

	/**
	 * Update project.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PutMapping("/project")
	public MSAResponse putProject(@RequestBody ProjectDTO projectDTO) {
		return projectService.putProject(projectDTO);
	}

	/**
	 * deleting projects.
	 *
	 * @param projectIds the project ids
	 * @return the MSA response
	 */
	@DeleteMapping("/project")
	public MSAResponse deleteProject(@RequestBody Long[] projectIds) {
		return projectService.deleteProject(projectIds);
	}

	/**
	 * Find by project id.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@GetMapping("/project")
	public MSAResponse findByProjectId(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectService.findByProjectId(projectId);
	}

	/**
	 * Report.
	 *
	 * @param jasperReportRootDTO the jasper report root DTO
	 * @return the MSA response
	 */
	@PostMapping("/report")
	public MSAResponse report(@RequestBody LookaheadJasperReportRootDTO jasperReportRootDTO) {
		return null;
	}

	/*@GetMapping("/projectPlanShare")
	public MSAResponse findByProjectForPlanSharing(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectService.findByProjectForPlanSharing(projectId);
	}*/
	
	/**
	 * Gets the project info.
	 *
	 * @param projectId the project id
	 * @return the project info
	 */
	@GetMapping("/projectInfo")
	public MSAResponse getProjectInfo(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectService.getProjectInfo(projectId);
	}

	/**
	 * Insert project info.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/projectInfo")
	public MSAResponse insertProjectInfo(@RequestBody @Valid ProjectDTO projectDTO) {
		return projectService.insertProjectInfo(projectDTO);
	}
	
	/**
	 * Insert trade.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/trade")
	public MSAResponse insertTrade(@RequestBody ProjectDTO projectDTO) {
		return projectService.insertTrade(projectDTO);
	}
	
	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 */
	@GetMapping("/trade")
	public MSAResponse getTrade(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectService.getTrade(projectId);
	}
	
	
	/**
	 * Insert trade.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/specDivision")
	public MSAResponse insertSpecDivision(@RequestBody ProjectDTO projectDTO) {
		return projectService.insertSpecDivision(projectDTO);
	}
	
	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 */
	@GetMapping("/specDivision")
	public MSAResponse getSpecDivision(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectService.getSpecDivision(projectId);
	}
	
	/**
	 * Gets the GC by user.
	 *
	 * @return the GC by user
	 */
	@GetMapping("/gcByUser")
	public MSAResponse getGCByUser() {
		return projectService.getGCByUser();
	}
	
	/**
	 * Creates the project user default notification for existing projects.
	 */
	@PostConstruct
	private void createProjectUserDefaultNotification() {
		projectService.insertDefaultNotificationForProjectUser();
	}
	
	@PostMapping("/progressCompletionByProjectId")
	public MSAResponse getProgressCompletionByProjectId(@Valid @RequestBody ProgressCompletionRequestDTO projectCompletionRequestDTO){
		return projectService.getProgressCompletionByProjectId(projectCompletionRequestDTO);
	}

	@PostMapping("/downloadPercentageCompletePDF")
	public MSAResponse downloadPercentageCompletePDF(@RequestBody ProgressCompletionRequestDTO progressCompletionRequestDTO) {
		return percentageCompleteService.downloadPercentageCompletePDF(progressCompletionRequestDTO);
	}

}
