/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProjectCompanyDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.service.ProjectCompanyService;


// TODO: Auto-generated Javadoc
/**
 * The Class ProjectCompanyController.
 *
 * @author Naveenkumar Boopathi
 */
@RestController
public class ProjectCompanyController {

	/** The project company service. */
	@Resource
	ProjectCompanyService projectCompanyService;
	
	/**
	 * Project companies.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@GetMapping("/projectCompany")
	public MSAResponse projectCompanies(
			@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectCompanyService.getProjectCompanies(projectId);
	}
	
	/**
	 * Project companies.
	 *
	 * @param projectCompanyDTO the project company DTO
	 * @return the MSA response
	 */
	@PostMapping("/projectCompany")
	public MSAResponse projectCompanies(@RequestBody ProjectCompanyDTO projectCompanyDTO) {
		return projectCompanyService.insertProjectCompanies(projectCompanyDTO);
	}

	/**
	 * User company associate to project.
	 *
	 * @param userId the user id
	 * @return the MSA response
	 */
	@GetMapping("/userCompanyAssociateToProject")
	public MSAResponse userCompanyAssociateToProject(
			@RequestParam(required = true, value = "userId") Long userId) {
		return projectCompanyService.userCompanyAssociateToProject(userId);
	}

	/**
	 * Update project companies.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	@PutMapping("/projectCompany")
	public MSAResponse updateProjectCompanies(@RequestBody UserDTO userDTO) {
		return projectCompanyService.updateProjectCompanies(userDTO);
	}

}	
