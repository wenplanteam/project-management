/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.controller;


import java.io.FileInputStream;
import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.service.LookaheadAccessService;


/**
 * The Class LookaheadAccessController.
 *
 * @author Naveenkumar Boopathi
 */
@RestController
public class LookaheadAccessController {

	/** The lookahead access service. */
	@Resource
	LookaheadAccessService lookaheadAccessService;

	/**
	 *  Return all tasks.
	 *
	 * @return the MSA response
	 */
	@GetMapping("/tasks")
	public MSAResponse tasks() {
		return lookaheadAccessService.loadAllTasks();
	}

	/**
	 *   Return Lookahead Date details.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@GetMapping("/lookaheadDateInfo")
	public MSAResponse lookaheadInfo(@RequestParam(required=true) Long projectId) {
		return lookaheadAccessService.lookaheadInfo(projectId);
	}

	/**
	 *  Create new lookahead service.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the MSA response
	 */
	@PostMapping("/lookahead")
	public MSAResponse createLookahead(@RequestBody LookaheadDTO lookaheadDTO) {
		return lookaheadAccessService.createLookahead(lookaheadDTO);
	}

	/**
	 *  Return the lookahead based on lookaheadListDTO conditions.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	@PostMapping("/lookaheads") // written by alex without async
	public MSAResponse listLookahead(@RequestBody LookaheadListDTO lookaheadListDTO) {
		lookaheadListDTO.setUserId(UserInfoContext.getUserThreadLocal());
		Long daysCount = 27L;
		boolean isLookahead = true;
		return lookaheadAccessService.listLookahead(lookaheadListDTO,daysCount,isLookahead);
	}

	/**
	 * Update lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the MSA response
	 */
	@PutMapping("/lookahead")
	public MSAResponse updateLookahead(@RequestBody LookaheadDTO lookaheadDTO) {
		return lookaheadAccessService.updateLookahead(lookaheadDTO);
	}

	/**
	 * Lookahead PDF report.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	@PostMapping("/lookaheadPDFReport")
	public MSAResponse lookaheadPDFReport(@RequestBody LookaheadListDTO lookaheadListDTO) {
		return lookaheadAccessService.jasperReportData(lookaheadListDTO);
	}

	/**
	 * Lookahead by id.
	 *
	 * @param taskId the task id
	 * @param subTaskId the sub task id
	 * @return the MSA response
	 */
	@DeleteMapping("lookaheadById")
	public MSAResponse lookaheadById(
			@RequestParam(required = false, value = "taskId") Long taskId,
			@RequestParam(required = false, value = "subTaskId") Long subTaskId) {
		return lookaheadAccessService.lookaheadById(taskId,subTaskId);
	}

	/**
	 * New get lookaheads.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	@PostMapping("/getLookaheads") // written by naveen for performance tuning
	public MSAResponse newGetLookaheads(@RequestBody LookaheadListDTO lookaheadListDTO) {
		Long daysCount = 27L;
		boolean isLookahead = true;
		return lookaheadAccessService.getLookahead(lookaheadListDTO, daysCount, isLookahead);
	}

	/**
	 * Download PDF.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	@PostMapping("/downloadPDF")
	public MSAResponse downloadPDF(@RequestBody LookaheadListDTO lookaheadListDTO) {
		return lookaheadAccessService.downloadPDF(lookaheadListDTO);
	}

	
	@PostMapping("/uploadLookaheadExcel")
	public MSAResponse uploadLookaheadExcel(@RequestBody MultipartFile multipartFile, @RequestParam Long projectId, @RequestParam Long projectCompanyId){
		try {
			return lookaheadAccessService.uploadExcelToLookahead((FileInputStream) multipartFile.getInputStream(), multipartFile.getName(), projectId, projectCompanyId);
		} catch (IOException e) {
			throw new CustomRuntimeException(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@PostMapping("/uploadLookaheadExcelFileName")
	public MSAResponse uploadLookaheadExcel(@RequestParam String fileName,@RequestParam Long projectId, @RequestParam Long projectCompanyId){
		return lookaheadAccessService.uploadExcelToLookahead(null, fileName, projectId, projectCompanyId);
	}
}