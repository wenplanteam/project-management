/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectCompany;

/**
 * The Interface ProjectCompanyRepository.
 */
@Repository
public interface ProjectCompanyRepository extends JpaRepository<ProjectCompany, Long>  {

	/**
	 * Find company availability by project id.
	 *
	 * @param projectId the project id
	 * @param companyId the company id
	 * @return the project company
	 */
	@Query(value = " SELECT pc FROM ProjectCompany pc JOIN pc.project p WHERE p.id = ?1 AND pc.companyId = ?2 ")
	public ProjectCompany findCompanyAvailabilityByProjectId(Long projectId, Long companyId);

	/**
	 * Delete companies by project id.
	 *
	 * @param projectId the project id
	 * @param deleteCompanies the delete companies
	 */
	@Transactional
	@Modifying
	/*@Query(value = " DELETE FROM ProjectCompany pcl where pcl.id IN " +
			" (SELECT pc.id FROM ProjectCompany pc join pc.project p" +
			" WHERE p.id  = ?1 and pc.companyId in ( ?2 )) ")*/
	@Query(value = " UPDATE ProjectCompany pcl set pcl.active = false , pcl	.deleted = true "
			+ " where pcl.id IN (SELECT pc.id FROM ProjectCompany pc join pc.project p "
			+ " WHERE p.id  = ?1 and pc.companyId in ( ?2 )) ")
	public void deleteCompaniesByProjectId(Long projectId, List<Long> deleteCompanies);

	/**
	 * Find companies byproject id.
	 *
	 * @param projectId the project id
	 * @return the list
	 */
	@Query(nativeQuery=true,
			value=" select p.id as project_id,c.id as company_id,c.name as company_name,pc.project_role as project_role "
					+ " from project_companies pc join projects p on pc.project_id = p.id join company c on c.id = pc.company_id "
					+ " where p.id = ?1 and pc.is_active = true ")
	public List<Object[]> findCompaniesByprojectId(Long projectId);

	/**
	 * Find by project and company id.
	 *
	 * @param project the project
	 * @param companyId the company id
	 * @return the project company
	 */
	public ProjectCompany findByProjectAndCompanyId(Project project, Long companyId);

	/**
	 * Find by project and company id and active and deleted.
	 *
	 * @param project the project
	 * @param companyId the company id
	 * @param active the active
	 * @param deleted the deleted
	 * @return the project company
	 */
	public ProjectCompany findByProjectAndCompanyIdAndActiveAndDeleted(Project project, Long companyId, boolean active, boolean deleted);

	/**
	 * Find company by id.
	 *
	 * @param companyId the company id
	 * @return the string
	 */
	@Query(nativeQuery=true, value=" select c.name from company c where c.id = ?1 ")
	public String findCompanyById(Long companyId);

	/**
	 * Find ball in court availability in task by project id and project company id.
	 *
	 * @param projectId the project id
	 * @param projectCompanyId the project company id
	 * @return the long
	 */
	@Query(nativeQuery=true,

			// value=" select  count(*) from task t join company c on t.ball_in_court =c.name where t.project_id = ?1 and c.id = ?2 and t.is_active = true ")
			//    public Long findBallInCourtAvailabilityInTaskByProjectIdAndCompanyId(Long projectId, Long companyId);

			value=" select  count(*) from task t where t.project_id = ?1 and t.project_company_id= ?2 and t.is_active = true ")
	public Long findBallInCourtAvailabilityInTaskByProjectIdAndProjectCompanyId(Long projectId, Long projectCompanyId);

	/**
	 * Find ball in court availability in sub task by project id and project company id.
	 *
	 * @param projectId the project id
	 * @param projectCompanyId the project company id
	 * @return the long
	 */
	@Query(nativeQuery=true,

			//	 value=" select count(*) from sub_task st join company c on st.ball_in_court =c.name join task t on t.id =st.task_id  where "
			//	     + " t.project_id = ?1 and c.id = ?2 and t.is_active = true and st.is_active = true ")
			//	        public Long findBallInCourtAvailabilityInSubTaskByProjectIdAndCompanyId(Long projectId, Long companyId);

			value=" select count(*) from sub_task st join task t on t.id =st.task_id  where "
					+ " t.project_id = ?1 and st.project_company_id = ?2 and t.is_active = true and st.is_active = true ")
	public Long findBallInCourtAvailabilityInSubTaskByProjectIdAndProjectCompanyId(Long projectId, Long projectCompanyId);

	/**
	 * Find by company id and project id.
	 *
	 * @param companyId the company id
	 * @param projectId the project id
	 * @return the project company
	 */
	public ProjectCompany findByCompanyIdAndProjectId(Long companyId, Long projectId);

	/**
	 * Find company by user id.
	 *
	 * @param userId the user id
	 * @return the long
	 */
	@Query(nativeQuery=true,
			value=" select c.id from company c where c.id = (Select c.company_id from " + 
			" company_users c where c.user_id = ?1 and  c.created_by = (select created_by from users u where u.id = ?1)) ")
	public Long findCompanyByUserId(Long userId);

	/**
	 * Find project company by company id and project id.
	 *
	 * @param companyId the company id
	 * @param projectId the project id
	 * @param activeStatus the active status
	 * @param deleteStatus the delete status
	 * @return the project company
	 */
	@Query(value = " SELECT pc FROM ProjectCompany pc JOIN pc.project p where pc.companyId = ?1 AND p.id = ?2 "
			+ "AND pc.active = ?3 AND pc.deleted = ?4 ")
	public ProjectCompany findProjectCompanyByCompanyIdAndProjectId(Long companyId, Long projectId, boolean activeStatus, boolean deleteStatus);

	@Query(nativeQuery= true,
		   value = " select c.id as company_id,c.name as company_name,pc.id as project_company_id,pc.project_role, "
		+ " pc.project_id from company c join project_companies pc on c.id =pc.company_id  where pc.project_id=?1 "
		+ " and pc.is_active = true;")
	public List<Object[]> findCompaniesByProjectId(Long projectId);
	
	@Query(nativeQuery= true,
			   value = "select pc.id  from company c join project_companies pc on c.id =pc.company_id  where pc.project_id= ?1 "
			+ "and c.name= ?2 and pc.is_active = true;")
		public Long findCompaniesByProjectIdAndCompanyName(Long projectId, String companyName);
}
