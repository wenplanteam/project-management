/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.Schedule;
import com.mindzen.planner.entity.SubTask;
import com.mindzen.planner.entity.Task;

/**
 * The Interface ScheduleRepository.
 */
@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

	/**
	 * Find by sub task id.
	 *
	 * @param id the id
	 * @return the list
	 */
	public List<Schedule> findBySubTaskId(Long id);
	
	/**
	 * Find by working date.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param projectId the project id
	 * @return the list
	 */
	@Query(nativeQuery=true, value="SELECT "
			+ "s.id AS schedule_id, s.working_date, s.worker_count, s.sub_task_id "
			+ "FROM schedule s "
			+ "JOIN sub_task st "
			+ "ON s.sub_task_id=st.id "
			+ "JOIN task t "
			+ "ON st.task_id=t.id "
			+ "WHERE "
			+ "s.working_date BETWEEN ?1 and ?2 "
			+ "AND "
			+ "t.project_id = ?3 "
			+ "AND "
			+ "t.is_active=true "
			+ "AND "
			+ "st.is_active=true "
			+ "GROUP BY s.id "
			+ "ORDER BY s.id, s.sub_task_id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId);

	/*@Query(nativeQuery=true, value="SELECT "
			+ "s.id AS schedule_id, s.working_date, s.worker_count, s.sub_task_id "
			+ "FROM schedule s "
			+ "JOIN sub_task st "
			+ "ON s.sub_task_id=st.id "
			+ "JOIN task t "
			+ "ON st.task_id=t.id "
			+ "WHERE "
			+ "s.working_date BETWEEN ?1 and ?2 "
			+ "AND "
			+ "t.project_id = ?3 "
			+ "AND "
			+ "t.created_by IN (?4) "
			+ "AND "
			+ "t.is_active=true "
			+ "AND "
			+ "st.is_active=true "
			+ "GROUP BY s.id "
			+ "ORDER BY s.id, s.sub_task_id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId, List<Long> userIds);*/
	
	/*@Query(nativeQuery=true, value="SELECT "
			+ "s.id AS schedule_id, s.working_date, s.worker_count, s.sub_task_id "
			+ "FROM schedule s "
			+ "JOIN sub_task st "
			+ "ON s.sub_task_id=st.id "
			+ "JOIN task t "
			+ "ON st.task_id=t.id "
			+ "WHERE "
			+ "s.working_date BETWEEN ?1 and ?2 "
			+ "AND "
			+ "t.project_id = ?3 "
			+ "AND "
			+ "t.created_by IN (?4) "
			+ "AND "
			+ "t.is_active=true "
			+ "AND "
			+ "st.is_active=true "
			+ "GROUP BY s.id "
			+ "ORDER BY s.id, s.sub_task_id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId, Long userIds);*/
	
	
	/**
	 * Find by working date.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param projectId the project id
	 * @param projectCompanyId the project company id
	 * @return the list
	 */
	@Query(nativeQuery=true, value="SELECT "
			+ "s.id AS schedule_id, s.working_date, s.worker_count, s.sub_task_id "
			+ "FROM schedule s "
			+ "JOIN sub_task st "
			+ "ON s.sub_task_id=st.id "
			+ "JOIN task t "
			+ "ON st.task_id=t.id "
			+ "WHERE "
			+ "s.working_date BETWEEN ?1 and ?2 "
			+ "AND "
			+ "t.project_id = ?3 "
			+ "AND "
			+ "st.project_company_id IN (?4) "
			+ "AND "
			+ "t.is_active=true "
			+ "AND "
			+ "st.is_active=true "
			+ "GROUP BY s.id "
			+ "ORDER BY s.id, s.sub_task_id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId, Long projectCompanyId);
	
	
	/**
	 * Find by working date and sub task id.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param subTask the sub task
	 * @return the list
	 */
	@Query("SELECT s FROM Schedule s WHERE "
			+ "s.workingDate BETWEEN ?1 and ?2 "
			+ "AND s.subTask = ?3 "
			+ "ORDER BY s.id, s.subTask.id")
	public List<Schedule> findByWorkingDateAndSubTaskId(Date startDate, Date endDate, SubTask subTask);
	
	/**
	 * Delete schedule by id.
	 *
	 * @param id the id
	 */
	@Modifying
	@Transactional
	@Query( " DELETE FROM Schedule WHERE id =?1 " )
	public void deleteScheduleById(Long id);
	
	@Query("SELECT s FROM Schedule s JOIN s.subTask st JOIN st.task t "
			+ "WHERE t.projectId= ?1 and s.workingDate between ?2 and ?3 "
			+ " GROUP BY t.id,st.id,s.id ORDER BY t.createdOn DESC")
	public List<Schedule> findProjectCompletionStatus(Long projectId, Date from, Date to);
	
	@Query("SELECT t FROM Schedule s JOIN s.subTask st JOIN st.task t "
			+ "WHERE t.projectId= ?1 and s.workingDate between ?2 and ?3 and st.isActive = true and t.isActive = true"
			+ " GROUP BY t.id ORDER BY t.createdOn DESC")
	public List<Task> findProjectCompletionTaskStatus(Long projectId, Date from, Date to);
	
	@Query("SELECT s.workingDate FROM Schedule s WHERE s.subTask.id= ?1")
	public Set<Date> getCountOfSchedule(Long subTaskId);
	
	@Query(nativeQuery=true, value="SELECT min(Date(s.working_date)),max(Date(s.working_date)) FROM schedule s WHERE s.sub_task_id= ?1")
	public List<Object[]> getStartDateEndDate(Long subTaskId);
	
	@Query("SELECT s.workingDate  FROM Schedule s WHERE s.subTask.id= ?1 and s.workingDate between ?2 and ?3")
	public Set<Date> getCountOfCompletedSchedule(Long subTaskId, Date from, Date to);

	@Query(value = " select s from Schedule s where s.subTask.id = ?1 and s.workingDate = ?2 ")
	public Optional<List<Schedule>> findBySubTaskIdAndWorkingDate(Long subTaskId, Date workingDate);

	@Modifying
	@Transactional
	@Query(" DELETE FROM Schedule WHERE sub_task_id = ?1 And  working_date < ?2 ")
	public void deleteScheduleBeforeStartDate(Long subTaskId, Date taskStartDate);

	@Modifying
	@Transactional
	@Query(" DELETE FROM Schedule WHERE sub_task_id = ?1 And  working_date > ?2 ")
	public void deleteScheduleAfterEndDate(Long subTaskId, Date taskEndDate);

	@Modifying
	@Transactional
	@Query(" DELETE FROM Schedule WHERE sub_task_id = ?1 ")
	public void deleteScheduleBySubTaskId(Long id);

	@Query(value="select MIN(working_date), MAX(working_date) from schedule where sub_task_id in "
			+ "(select id from sub_task where is_active = true and task_id in (select id from task where is_active = true and  project_id = 330))" , nativeQuery= true)
	public List<Object[]> findMinMaxScheduleByProjectId(Long projectId);

	@Modifying
	@Transactional
	@Query("DELETE FROM Schedule WHERE id in (?1) ")
	public void deleteExistsSchedule(List<Long> schduleIds);
}
