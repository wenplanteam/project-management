package com.mindzen.planner.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.mindzen.repository.TempRepository;

public interface CommonRepository extends TempRepository{

	@Query(
			nativeQuery = true,
	value = "select field_1,field_2,field_3,field_4,field_5,field_6,field_7,field_8,field_9, field_10, field_11, field_12 from temp where track_id = ?1 ")
	public List<Object[]> findByTrackId(Long trackId);

	@Query(nativeQuery = true,
	value ="select st from task t join sub_task st on t.id =st.task_id where t.project_id = ?1  and  "
		 + "t.description= ?2 and st.description = ?3  and "
		 + "st.task_start_date = Date(?4) and  st.expected_completion_date= Date(?5) ")
	public List<Object[]> checkTaskAndSubTaskAvalability(Long projectId, String taskDescription, String subTaskDescription,
			LocalDate startDateFormat, LocalDate finishDateFormat);
	
	@Modifying
	@Transactional
	@Query("delete from Temp t where t.trackId=?1")
	public void deleteByTrackid(Long trackId);
	
	
}
