/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.NotificationEntity;

/**
 * The Interface NotificationRepository.
 */
@Repository
public interface NotificationRepository  extends JpaRepository<NotificationEntity, Long>{

}
