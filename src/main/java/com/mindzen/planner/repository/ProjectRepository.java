/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.Project;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProjectRepository.
 */
@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

	/**
	 * Find projects by user.
	 *
	 * @param pageRequest the page request
	 * @param isProjectDeleted the is project deleted
	 * @param isProjectUserDeleted the is project user deleted
	 * @param isProjectUserActive the is project user active
	 * @param userId the user id
	 * @param created the created
	 * @return the page
	 */
	@Query("select p from Project p where p.isDeleted = ?1 and p.createdBy = ?5  and p.id in (select pu.project.id from ProjectUser pu where pu.isDeleted = ?2 and pu.isActive = ?3 and pu.userId = ?4) ORDER BY (p.id,p.modifiedOn) DESC")
	public Page<Project> findProjectsByUser(Pageable pageRequest, Boolean isProjectDeleted, Boolean isProjectUserDeleted, Boolean isProjectUserActive, Long userId, Long created);

	/**
	 * Find projects by user.
	 *
	 * @param pageRequest the page request
	 * @param isProjectDeleted the is project deleted
	 * @param isProjectActive the is project active
	 * @param isProjectUserDeleted the is project user deleted
	 * @param isProjectUserActive the is project user active
	 * @param userId the user id
	 * @param created the created
	 * @return the page
	 */
	@Query("select p from Project p where p.isDeleted = ?1 and p.createdBy = ?6 and p.isActive = ?2 and p.id in (select pu.project.id from ProjectUser pu where pu.isDeleted = ?3 and pu.isActive = ?4 and pu.userId = ?5) ORDER BY (p.id,p.modifiedOn) DESC")
	public Page<Project> findProjectsByUser(Pageable pageRequest, Boolean isProjectDeleted, Boolean isProjectActive, Boolean isProjectUserDeleted, Boolean isProjectUserActive, Long userId, Long created);

	/**
	 * Find project manager company id.
	 *
	 * @param userId the user id
	 * @return the long
	 */
	//@Query(nativeQuery=true, value="select c.id from company c join company_users u on c.id = u.user_id join projects p on u.id = p.created_by where p.id=?1 AND p.created_by=?2")
	@Query(nativeQuery=true, value=" select c.id from company c where c.id = (Select c.company_id from " + 
			"	company_users c where c.user_id = ?1 and  c.created_by = (select created_by from users u where u.id = ?1))  ")
	//	public Long findProjectManagerCompanyId(Long projectId, Long userId);
	public Long findProjectManagerCompanyId(Long userId);

	/*@Query(nativeQuery = true, value = "select u.email,u.first_name,u.last_name from users u join company c on u.company_id = c.id where c.name = "
			+ "(select c.name from company c join users u on c.id = u.company_id join project_users pu on u.id = pu.user_id  where pu.is_deleted = false and u.id = ?1 and pu.project_id = ?2)")
	public List<String> findByProjectForPlanSharing(Long userId, Long projectId);*/

	/**
	 * Find user mail by project role.
	 *
	 * @param projectId the project id
	 * @param projectRole the project role
	 * @return the list
	 */
	@Query(nativeQuery = true, value = "select pu.project_role,lower(u.email) from project_users pu join users u on u.id = pu.user_id where project_id = ?1 and project_role =?2 ")
	public List<String[]> findUserMailByProjectRole(Long projectId, String projectRole);

	/**
	 * Find all trades.
	 *
	 * @param masterId the master id
	 * @return the list
	 */
	@Query(nativeQuery = true,
			value = " select value from master_detail where master_id = ?1 and is_active =true and is_deleted = false")
	public List<String> findAllTrades(long masterId);

	/**
	 * Find user name by user id.
	 *
	 * @param userId the user id
	 * @return the string
	 */
	@Query(nativeQuery=true,value = " select  concat(u.first_name,' ',u.last_name) from users u where u.id = ?1 ")
	public String findUserNameByUserId(Long userId);

	/**
	 * Find projects by user.
	 *
	 * @param b the b
	 * @param c the c
	 * @param d the d
	 * @param userId the user id
	 * @return the list
	 */
	@Query(value = "select p from Project p where p.isDeleted = ?1 and p.id in (select pu.project.id from ProjectUser pu "
			+ " where pu.isDeleted = ?2 and pu.isActive = ?3 and pu.userId = ?4) ORDER BY (p.id,p.modifiedOn) DESC")
	public List<Project> findProjectsByUser(boolean b, boolean c, boolean d, Long userId);

	/*	@Query(value = "select p from Project p where p.createdBy = ?1 and  p.isDeleted = ?2 and p.isActive = ?1 and p.id in "
			+ " ORDER BY (p.id,p.modifiedOn) DESC")
	public Page<Project> findByCreatedBy(Pageable pageRequest, Long created, boolean isDeleted, boolean isActive);*/

	/**
	 * Find all active projects.
	 *
	 * @param status the status
	 * @return the list
	 */
	@Query(" from Project  where isDeleted = ?1 order by createdOn desc")
	public List<Project> findAllActiveProjects(boolean status);
	
	/**
	 * Find user id by active license.
	 *
	 * @param userId the user id
	 * @param isActive the is active
	 * @return true, if successful
	 */
	@Query(nativeQuery=true, value="select ul.is_active from user_license ul where ul.user_id= ?1 and ul.is_active=?2 ")
	public Boolean findUserIdByActiveLicense(Long userId, boolean isActive);
}
