/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.entity.Task;

/**
 * The Interface TaskRepository.
 *
 * @author Naveenkumar Boopathi
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

	/**
	 * Find by project id.
	 *
	 * @param email the email
	 * @return the list
	 */
	public List<Task> findByProjectId(Long email);

	/**
	 * Find by working date.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @return the list
	 */
	@Query(nativeQuery = true, value = "SELECT "
			+ "t.id AS task_id, t.description AS task_description, t.visibility AS task_visibility, "
			+ "t.trade AS task_trade, t.task_type, t.ball_in_court AS task_ball_in_court, "
			+ "t.note AS task_note, t.is_child, t.sno AS sno " + "FROM schedule s " + "JOIN sub_task st "
			+ "ON s.sub_task_id=st.id " + "JOIN task t " + "ON st.task_id=t.id " + "WHERE "
			+ "s.working_date BETWEEN ?1 and ?2 " + "AND " + "t.project_id = ?3 " + "AND " + "t.is_active=true "
			+ "AND " + "st.is_active=true " + "GROUP BY t.id " + "ORDER BY t.id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId);

	/**
	 * Find by working date.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @param userId    the user id
	 * @return the list
	 */
	@Query(nativeQuery = true, value = "SELECT "
			+ "t.id AS task_id, t.description AS task_description, t.visibility AS task_visibility, "
			+ "t.trade AS task_trade, t.task_type, t.ball_in_court AS task_ball_in_court, "
			+ "t.note AS task_note, t.is_child, t.sno AS sno " + "FROM schedule s " + "JOIN sub_task st "
			+ "ON s.sub_task_id=st.id " + "JOIN task t " + "ON st.task_id=t.id " + "WHERE "
			+ "s.working_date BETWEEN ?1 and ?2 " + "AND " + "t.project_id = ?3 " + "AND "
			// + "t.created_by = ?4 "
			+ "st.project_company_id IN (?4) " + "AND " + "t.is_active=true " + "AND " + "st.is_active=true "
			+ "GROUP BY t.id " + "ORDER BY t.id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId, Long userId);

	/**
	 * Find by working date and project id.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @return the list
	 */
	@Query("SELECT task FROM Task task " + "WHERE task.isActive=true and task.id IN (SELECT t.id FROM Task t "
			+ "JOIN t.subTasks st " + "JOIN st.schedules s " + "WHERE s.workingDate BETWEEN ?1 and ?2 AND "
			+ "t.projectId = ?3) " + "ORDER BY task.id")
	public List<Task> findByWorkingDateAndProjectId(Date startDate, Date endDate, Long projectId);

	/**
	 * Find by project id with date.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @return the list
	 */
	@Query(" SELECT t FROM Task t ")
	public List<Task> findByProjectIdWithDate(Date startDate, Date endDate, Long projectId);

	/**
	 * Find task and subtasks.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @return the list
	 */
	@Query(nativeQuery = true, value = "SELECT " + "t.id AS task_id, t.description AS task_description, "
			+ "t.ball_in_court AS task_ball_in_court, t.note AS task_note, " + "t.is_child, t.sno AS task_sno, "
			+ "st.id AS sub_task_id, st.description AS sub_task_description, "
			+ "st.ball_in_court AS sub_task_ball_in_court, st.note AS sub_task_note, st.sno AS sub_task_sno  "
			+ "FROM schedule s " + "JOIN sub_task st " + "ON s.sub_task_id=st.id " + "JOIN task t "
			+ "ON st.task_id=t.id " + "WHERE " + "s.working_date BETWEEN ?1 and ?2 " + "AND " + "t.project_id = ?3 "
			+ "AND " + "t.is_active=true " + "AND " + "st.is_active=true " + "GROUP BY st.id, t.id " + "ORDER BY t.id")
	public List<Object[]> findTaskAndSubtasks(Date startDate, Date endDate, Long projectId);

	/**
	 * Find task and subtasks.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @param userId    the user id
	 * @return the list
	 */
	@Query(nativeQuery = true, value = "SELECT " + "t.id AS task_id, t.description AS task_description, "
			+ "t.ball_in_court AS task_ball_in_court, t.note AS task_note, " + "t.is_child, t.sno AS task_sno, "
			+ "st.id AS sub_task_id, st.description AS sub_task_description, "
			+ "st.ball_in_court AS sub_task_ball_in_court, st.note AS sub_task_note, st.sno AS sub_task_sno  "
			+ "FROM schedule s " + "JOIN sub_task st " + "ON s.sub_task_id=st.id " + "JOIN task t "
			+ "ON st.task_id=t.id " + "WHERE " + "s.working_date BETWEEN ?1 and ?2 " + "AND " + "t.project_id = ?3 "
			+ "AND "
			// + "t.created_by = ?4 "
			+ "st.project_company_id IN (?4) " + "AND " + "t.is_active=true " + "AND " + "st.is_active=true "
			+ "GROUP BY st.id, t.id " + "ORDER BY t.id")
	public List<Object[]> findTaskAndSubtasks(Date startDate, Date endDate, Long projectId, Long userId);

	/**
	 * Find task details.
	 *
	 * @param projectId the project id
	 * @param isActive  the is active
	 * @param from      the from
	 * @param to        the to
	 * @return the list
	 */
	// @Query(value ="select t from Task t where t.projectId=?1 and t.isActive=?2")
	// public List<Task> findTaskDetails(Long projectId, boolean isActive);
	/*
	 * @Query(
	 * value="(select t from Task t where t.projectId=?1 and t.isActive=?2 and DATE(t.createdOn) between ?3 and ?4) "
	 * +
	 * "union (select t from Task t where t.projectId=?1 and t.isActive=?2 and DATE(t.modifiedOn) between ?3 and ?4) "
	 * +
	 * "union (select t from Task t where t.projectId=?1 and t.isActive=?2 and DATE(t.requiredCompletionDate) between ?3 and ?4) "
	 * +
	 * "union (select t from Task t where t.projectId=?1 and t.isActive=?2 and DATE(t.deadLineDate) between ?3 and ?4)"
	 * ) public List<Task> findWeeklyTaskDetails(Long projectId, boolean isActive,
	 * Date from, Date to);
	 */

	/**
	 * Find task details between given time period.
	 *
	 * @param projectId the project id
	 * @param isActive  the is active
	 * @param from      the from
	 * @param to        the to
	 * @return the list
	 */
	@Query(value = "select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.createdOn) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.modifiedOn) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " and t.projectId=?1 and t.isActive=?2")
	public List<Task> findWeeklyTaskDetails(Long projectId, boolean isActive, Date from, Date to);

	/**
	 * Find weekly task details SC.
	 *
	 * @param projectId        the project id
	 * @param isActive         the is active
	 * @param from             the from
	 * @param to               the to
	 * @param projectCompanyId the project company id
	 * @return the list
	 */
	@Query(value = "select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.createdOn) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.modifiedOn) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5)"
			+ " and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5")
	public List<Task> findWeeklyTaskDetailsSC(Long projectId, boolean isActive, Date from, Date to,
			Long projectCompanyId);

	/**
	 * Find task details.
	 *
	 * @param projectId the project id
	 * @param isActive  the is active
	 * @param lastDay   the last day
	 * @return the list
	 */
	@Query("select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.createdOn) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.modifiedOn) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " and t.projectId=?1 and t.isActive=?2")
	public List<Task> findDailyTaskDetails(Long projectId, boolean isActive, Date lastDay);

	/**
	 * Find daily task details SC.
	 *
	 * @param projectId        the project id
	 * @param isActive         the is active
	 * @param lastDay          the last day
	 * @param projectCompanyId the project company id
	 * @return the list
	 */
	@Query("select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.createdOn) = ?3) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.modifiedOn) = ?3) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) = ?3) and t.projectId=?1 and t.isActive=?2  and t.projectCompanyId=?4)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4)"
			+ " and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4")
	public List<Task> findDailyTaskDetailsSC(Long projectId, boolean isActive, Date lastDay, Long projectCompanyId);

	/**
	 * Find weekly future task details.
	 *
	 * @param projectId the project id
	 * @param isActive  the is active
	 * @param from      the from
	 * @param to        the to
	 * @return the list
	 */
	@Query(value = "select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2)"
			+ " and t.projectId=?1 and t.isActive=?2")
	public List<Task> findWeeklyFutureTaskDetails(Long projectId, boolean isActive, Date from, Date to);

	/**
	 * Find weekly future task details SC.,
	 *
	 * @param projectId the project id
	 * @param isActive  the is active
	 * @param from      the from
	 * @param to        the to
	 * @return the list
	 */
	@Query(value = "select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2  and t.projectCompanyId=?5)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) between ?3 and ?4) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5)"
			+ " and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?5")
	public List<Task> findWeeklyFutureTaskDetailsSC(Long projectId, boolean isActive, Date from, Date to,
			Long projectCompanyId);

	/**
	 * Find daily future task details.
	 *
	 * @param projectId the project id
	 * @param isActive  the is active
	 * @param futureDay the future day
	 * @return the list
	 */
	@Query("select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2)"
			+ " and t.projectId=?1 and t.isActive=?2")
	public List<Task> findDailyFutureTaskDetails(Long projectId, boolean isActive, Date futureDay);

	@Query("select t from Task t where (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.requiredCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.deadLineDate) = ?3) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4)"
			+ " or (t.id in (select st.task.id from Task t1 join SubTask st on t1.id=st.task.id where DATE(st.expectedCompletionDate) = ?3) and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4)"
			+ " and t.projectId=?1 and t.isActive=?2 and t.projectCompanyId=?4")
	public List<Task> findDailyFutureTaskDetailsSC(Long projectId, boolean isActive, Date futureDay,
			Long projectCompanyId);

	/*
	 * Find task modified details.
	 *
	 * @param projectId the project id
	 * 
	 * @param isActive the is active
	 * 
	 * @param from the from
	 * 
	 * @param to the to
	 * 
	 * @return the list
	 *//*
		 * @Query("select t from Task t where t.modifiedOn != t.createdOn and t.projectId=?1 "
		 * + "and t.isActive=?2 and DATE(t.modifiedOn) between ?3 and ?4 ") public
		 * List<Task> findTaskModifiedDetails(Long projectId, boolean isActive, Date
		 * from, Date to);
		 *
		 * 
		 * Find task req completed details.
		 *
		 * @param projectId the project id
		 * 
		 * @param isActive the is active
		 * 
		 * @param from the from
		 * 
		 * @param to the to
		 * 
		 * @return the list
		 *//*
			 * @Query("select t from Task t where t.projectId=?1 and t.isActive=?2 and DATE(t.requiredCompletionDate) between ?3 and ?4"
			 * + " and DATE(t.expectedCompletionDate) between ?3 and ?4") public List<Task>
			 * findTaskReqCompletedDetails(Long projectId, boolean isActive, Date from, Date
			 * to);
			 */
	// @Query(value ="SELECT t FROM Task t WHERE t.id in ( ?1 ) AND t.projectId = ?2
	// AND t.isActive=true GROUP BY t.id ORDER BY t.id desc")
	@Query(nativeQuery = true, value = "SELECT t.id AS task_id, t.description AS task_description, t.visibility AS task_visibility,t.trade AS task_trade, t.task_type, "
			+ " t.ball_in_court AS task_ball_in_court,t.note AS task_note, t.is_child, t.sno AS sno,t.project_company_id AS projectCompanyId , "
			+ " t.task_start_date,t.expected_completion_date,t.required_completion_date,t.dead_line_date, t.spec_division AS specDivision ,t.activity_id,t.description AS activity_description,"
			+ "t.import_start_date,t.import_finish_date,t.import_target_completion_date,t.import_deadline_date,t.import_notes,t.is_lookahead_change FROM "
			+ " task t  WHERE t.id in ( ?1 ) AND t.project_id = ?2 AND t.is_active=true  GROUP BY t.id ORDER BY t.id desc")
	public List<Object[]> findById(Set<Long> subTaskOfTaskIdList, Long projectId);

	//
	// @Query(value ="SELECT t.id AS task_id, t.description AS task_description,
	// t.visibility AS task_visibility,t.trade AS task_trade, t.task_type," +
	// " t.ball_in_court AS task_ball_in_court,t.note AS task_note, t.is_child,
	// t.sno AS sno,t.project_company_id AS projectCompanyId ," +
	// "
	// t.task_start_date,t.expected_completion_date,t.required_completion_date,t.dead_line_date,
	// t.spec_division AS specDivision FROM " +
	// " task t WHERE t.import_id = ?1 AND t.project_id = ?2 AND t.is_active=true")
	@Query("select t from Task t  where t.importId=?1 and t.projectId=?2")
	public List<Task> findByTaskImportId(Long importId, Long projectId);

	@Query("select t from Task t  where t.importId=?1 and t.projectId=?2")
	public List<Task> findByTaskImportTrackId(Long importId, Long projectId);

	public List<Task> findByImportId(Long importId);

	@Modifying
	@Transactional
	@Query("update Task t set t.activityDescription=?1 ,t.description=?2,t.taskStartDate=?3 , t.importStartDate=?4, t.requiredCompletionDate=?5, t.expectedCompletionDate=?6, t.importFinishDate=?7, t.importTargetCompletionDate=?8, t.taskType=?9, t.specDivision=?10, t.activityId=?11, t.importNotes=?12, t.isActive=?13, t.child=?14,  t.projectCompanyId=?15 where  t.importUniqueId=?16 AND t.importId=?17 AND t.projectId=?18")
	public void updateImportMppIntoTaskTable(String activityDescription, String description, Date taskStartDate,
			Date importStartDate, Date requiredCompletionDate, Date expectedCompletionDate, Date importFinishDate,
			Date importTargetCompletionDate, String taskType, String specDivision, String activityId,
			String importNotes, boolean isActive, boolean isChild, Long projectCompanyId, int importUniqueId,
			Long importId, Long projectId);

	@Query("select t from Task t  where t.importUniqueId=?1 and t.projectId=?2 and t.importId=?3 and t.isActive=true")
	public Task findByTaskId(int importUniqueId, Long id, Long trackId);

}
