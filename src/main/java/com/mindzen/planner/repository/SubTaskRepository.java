/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mindzen.planner.entity.SubTask;
import com.mindzen.planner.entity.Task;

/**
 * The Interface SubTaskRepository.
 */
@Repository
public interface SubTaskRepository extends JpaRepository<SubTask, Long> {

	/**
	 * Find by task id.
	 *
	 * @param id the id
	 * @return the list
	 */
	public List<SubTask> findByTaskId(Long id);

	/**
	 * Find by working date.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @return the list
	 */
	@Query(nativeQuery = true, value = "SELECT "
			+ "st.id AS sub_task_id, st.description AS sub_task_description, st.visibility AS sub_task_visibility, "
			+ "st.trade AS sub_task_trade, st.task_type AS sub_task_type, "
			+ "st.ball_in_court AS sub_task_ball_in_court, st.note AS sub_task_note, st.task_id, " + "st.sno AS sno "
			+ "FROM schedule s " + "JOIN sub_task st " + "ON s.sub_task_id=st.id " + "JOIN task t "
			+ "ON st.task_id=t.id " + "WHERE " + "s.working_date BETWEEN ?1 and ?2 " + "AND " + "t.project_id = ?3 "
			+ "AND " + "t.is_active=true " + "AND " + "st.is_active=true " + "GROUP BY st.id "
			+ "ORDER BY st.id, st.task_id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId);

	/**
	 * Find by working date.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @param userId    the user id
	 * @return the list
	 */
	@Query(nativeQuery = true, value = "SELECT "
			+ "st.id AS sub_task_id, st.description AS sub_task_description, st.visibility AS sub_task_visibility, "
			+ "st.trade AS sub_task_trade, st.task_type AS sub_task_type, "
			+ "st.ball_in_court AS sub_task_ball_in_court, st.note AS sub_task_note, st.task_id, " + "st.sno AS sno "
			+ "FROM schedule s " + "JOIN sub_task st " + "ON s.sub_task_id=st.id " + "JOIN task t "
			+ "ON st.task_id=t.id " + "WHERE " + "s.working_date BETWEEN ?1 and ?2 " + "AND " + "t.project_id = ?3 "
			+ "AND " + "t.created_by = ?4 " + "AND " + "t.is_active=true " + "AND " + "st.is_active=true "
			+ "GROUP BY st.id " + "ORDER BY st.id, st.task_id")
	public List<Object[]> findByWorkingDate(Date startDate, Date endDate, Long projectId, Long userId);

	/**
	 * Find by working date and task id.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param task      the task
	 * @return the list
	 */
	@Query("SELECT st FROM SubTask st " + "WHERE st.isActive=true AND " + "st.id in (SELECT t FROM SubTask t "
			+ "JOIN t.schedules s WHERE " + "s.workingDate BETWEEN ?1 and ?2 " + "AND t.task = ?3) "
			+ "ORDER BY st.id, st.task.id")
	public List<SubTask> findByWorkingDateAndTaskId(Date startDate, Date endDate, Task task);

	/**
	 * Find by working date and project id.
	 *
	 * @param startDate the start date
	 * @param endDate   the end date
	 * @param projectId the project id
	 * @return the list
	 */
	@Query("SELECT st FROM SubTask st " + "WHERE st.isActive=true AND " + "st.task.projectId=?3 AND "
			+ "st.id in (SELECT t FROM SubTask t " + "JOIN t.schedules s WHERE " + "s.workingDate BETWEEN ?1 and ?2) "
			+ "ORDER BY st.id")
	public List<SubTask> findByWorkingDateAndProjectId(Date startDate, Date endDate, Long projectId);

	@Query(nativeQuery = true, value = " SELECT st.id AS sub_task_id, st.description AS sub_task_description, st.visibility AS sub_task_visibility,"
			+ " st.trade AS sub_task_trade, st.task_type AS sub_task_type,st.ball_in_court AS sub_task_ball_in_court,"
			+ " st.note AS sub_task_note, st.task_id, st.sno AS sno,st.project_company_id , min(st.task_start_date) as tsd,"
			+ " max(st.expected_completion_date) as ecd ,max(st.required_completion_date) as rcd ,max(st.dead_line_date) as dld,"
			+ " st.spec_division AS specDivision ,st.activity_id,st.activity_description, "
			+ "st.import_start_date,st.import_finish_date,st.import_target_completion_date,st.import_deadline_date,"
			+ "st.import_notes,st.is_lookahead_change FROM schedule s JOIN sub_task st ON s.sub_task_id=st.id JOIN task t ON st.task_id=t.id "
			+ " WHERE t.id in ( ?1 ) AND t.project_id = ?2 AND t.is_active=true AND st.is_active=true GROUP BY st.id ,st.task_start_date , "
			+ " st.expected_completion_date,st.required_completion_date,st.dead_line_date ORDER BY st.id, st.task_id ")
	public List<Object[]> findByTaskId(Set<Long> taskIdList, Long projectId);

	@Transactional
	@Modifying
	@Query(value = " update SubTask st set st.isActive = false where st.task = ?1  ")
	public void setInActivateByTaskId(Task task);
 
	@Query("select st from SubTask st where st.task=?1 and st.importUniqueId=?2")
	public SubTask findByTaskIdFromImport(Task taskId, int importUniqueId);

//	@Modifying
//	@Transactional
	//	@Query("update SubTask  set activityDescription=:activityDescription ,description=:description,taskStartDate=:taskStartDate ,"
	//			+ " importStartDate=:importStartDate, requiredCompletionDate=:requiredCompletionDate,"
	//			+ " expectedCompletionDate=:expectedCompletionDate, importFinishDate=:importFinishDate,"
	//			+ " importTargetCompletionDate=:importTargetCompletionDate, taskType=:taskType, specDivision=:specDivision, importNotes=:importNotes,"
	//			+ " isActive=:isActive,"
	//			+ " where  projectCompanyId=:projectCompanyId AND importUniqueId=:importUniqueId AND activityId=:activityId")
//	@Query( value="update SubTask t set t.activityDescription=?1 "
//			+ ",t.description=?2,t.taskStartDate=?3 , t.importStartDate=?4,"
//			+ "	t.requiredCompletionDate=?5, t.expectedCompletionDate=?6,"
//			+ "		t.importFinishDate=?7, t.importTargetCompletionDate=?8, t.taskType=?9,"
//			+ "					t.specDivision=?10, t.importNotes=?11, t.isActive=?12, where"
//			+ "							t.projectCompanyId=?13 AND t.importUniqueId=?14 AND t.activityId=?15")
//	public void updateImportMppIntoSubTaskTable(String activityDescription, String description, Date taskStartDate,
//			Date importStartDate, Date requiredCompletionDate, Date expectedCompletionDate, Date importFinishDate,
//			Date importTargetCompletionDate, String taskType, String specDivision, String importNotes, boolean isActive,
//			Long projectCompanyId, int importUniqueId, String activityId);

}
