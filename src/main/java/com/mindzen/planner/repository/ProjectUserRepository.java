/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectUser;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProjectUserRepository.
 */
@Repository
public interface ProjectUserRepository extends JpaRepository<ProjectUser, Long> {

	/**
	 * Find by project id and user id.
	 *
	 * @param projectId the project id
	 * @param userId the user id
	 * @param active the active
	 * @param deleted the deleted
	 * @return the project user
	 */
	@Query("SELECT pu FROM ProjectUser pu WHERE pu.project.id=?1 AND pu.userId IN (?2) AND isActive=?3 AND isDeleted=?4")
	public ProjectUser findByProjectIdAndUserId(Long projectId, Long userId, boolean active, boolean deleted);

	/**
	 * Find by project id and user id.
	 *
	 * @param projectId the project id
	 * @param userId the user id
	 * @return the project user
	 */
	public ProjectUser findByProjectIdAndUserId(Long projectId, Long userId);

	/**
	 * Find by user id.
	 *
	 * @param userId the user id
	 * @return the list
	 */
	public List<ProjectUser> findByUserId(Long userId);

	/**
	 * Delete project users by multiple ids.
	 *
	 * @param project the project
	 * @param deleteProjectUsers the delete project users
	 */
	@Modifying
	@Transactional
	@Query( " UPDATE ProjectUser set isDeleted = true, isActive = false where project=?1 AND userId IN ( ?2 ) " )
	public void deleteProjectUsersByMultipleIds(Project project, List<Long> deleteProjectUsers);

	/*@Query(nativeQuery=true,
	value="select user_id from project_users where project_role in ( ?1 ) and project_id = ?2 ")
	public List<Long> findAllGC(List<String> roles, Long projectId);*/
	
	/**
	 * Find company name by project id and user id.
	 *
	 * @param projectId the project id
	 * @param userId the user id
	 * @return the string
	 */
	@Query(nativeQuery=true, value="select c.name from company c join users u on c.id = u.company_id join "
			+ "project_users pu on u.id = pu.user_id where pu.project_id=?1 AND pu.user_id=?2")
	public String findCompanyNameByProjectIdAndUserId(Long projectId, Long userId);
	
	@Query(nativeQuery=true, value="select c.name from company c where c.id=?1")
	public String findCompanyNameById(Long companyId);
	
	/**
	 * Find company by project id and user id.
	 *
	 * @param projectId the project id
	 * @param userId the user id
	 * @return the object
	 */
	@Query(nativeQuery=true, value="select c.name, c.id from company c join users u on c.id = u.company_id join project_users pu on u.id = pu.user_id where pu.project_id=?1 AND pu.user_id=?2")
	public Object findCompanyByProjectIdAndUserId(Long projectId, Long userId);

	/**
	 * Find project user by user id.
	 *
	 * @param userId the user id
	 * @param activeStatus the active status
	 * @param deleteStatus the delete status
	 * @return the list
	 */
	@Query("SELECT pu FROM ProjectUser pu WHERE pu.userId = ?1 AND isActive=?2 AND isDeleted=?3")
	public List<ProjectUser> findProjectUserByUserId(Long userId, boolean activeStatus, boolean deleteStatus);

	
	
	/**
	 * Findcompany id by created by.
	 *
	 * @param createdBy the created by
	 * @return the list
	 */
	@Query(nativeQuery=true, value=" select c.id,c.name from company c where c.id = (Select c.company_id from "
			+ "company_users c where c.user_id = ?1 and  c.created_by = (select created_by from users u where u.id = ?1)) ")
	public List<Object[]> findcompanyIdByCreatedBy(Long createdBy);

	/**
	 * Findcompany name by created by.
	 *
	 * @param createdBy the created by
	 * @return the string
	 */
	@Query(nativeQuery=true, value=" select c.name from company c where c.id = (Select c.company_id from "
			+ "company_users c where c.user_id = ?1 and  c.created_by = (select created_by from users u where u.id = ?1)) ")
	public String findcompanyNameByCreatedBy(Long createdBy);

	/**
	 * Find project comapany availability by project company id.
	 *
	 * @param projectCompanyId the project company id
	 * @return the long
	 */
	@Query("  SELECT COUNT(x) FROM ProjectUser x JOIN x.projectCompany pc where pc.id = ?1 AND x.isDeleted = false ")
	public Long findProjectComapanyAvailabilityByProjectCompanyId(Long projectCompanyId);

	/**
	 * Find by user id and is deleted and created by.
	 *
	 * @param id the id
	 * @param isDelete the is delete
	 * @param createdBy the created by
	 * @return the list
	 */
	public List<ProjectUser> findByUserIdAndIsDeletedAndCreatedBy(Long id, boolean isDelete,
			Long createdBy);

	/**
	 * Find time zone by user id.
	 *
	 * @param userId the user id
	 * @return the string
	 */
	@Query(nativeQuery = true,
			value = " select u.time_zone from users u where u.id = ?1 ")
	public String findTimeZoneByUserId(Long userId);

	/**
	 * Find users by project id.
	 *
	 * @param projectId the project id
	 * @param isActive the is active
	 * @return the list
	 */
	@Query("select pu from ProjectUser pu where pu.project.id=?1 and pu.userId=?2 and pu.isActive=?3")
	public Optional<ProjectUser> findUsersByProjectId(Long projectId, Long userId, boolean isActive);

	/**
	 * Gets the users by id.
	 *
	 * @param userId the user id
	 * @param isActive the is active
	 * @return the users by id
	 */
	@Query(nativeQuery = true, value = " select u.email,u.time_zone from users u where u.id = ?1 and is_active = ?2")
	public List<Object[]> getUsersById(Long userId, boolean isActive);
	
	@Query("select pu from ProjectUser pu where pu.isActive=?1 and pu.notification.id is null")
	public List<ProjectUser> getAllEmptyNotificationId(boolean isActive);
	
	public List<ProjectUser> findByProjectId(Long projectId);
}