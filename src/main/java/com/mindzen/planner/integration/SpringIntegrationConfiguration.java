/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.integration;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.dsl.Amqp;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

// TODO: Auto-generated Javadoc
/**
 * The Class SpringIntegrationConfiguration.
 */
@Configuration
public class SpringIntegrationConfiguration {

	/**
	 * Amqp user verify mail outbound adapter.
	 *
	 * @param amqpTemplate the amqp template
	 * @return the integration flow
	 */
	@Bean
	public IntegrationFlow amqpUserVerifyMailOutboundAdapter(AmqpTemplate amqpTemplate) {
		return IntegrationFlows.from("amqpVerifyUserMailChannel")
				.handle(Amqp.outboundAdapter(amqpTemplate)
						.routingKey("wenplan-user-verify-mail"))
				.channel("log")
				.bridge()
				.get();
	}
	
	/**
	 * Amqp lookahead report outbound adapter.
	 *
	 * @param amqpTemplate the amqp template
	 * @return the integration flow
	 */
	@Bean
	public IntegrationFlow amqpLookaheadReportOutboundAdapter(AmqpTemplate amqpTemplate) {
		return IntegrationFlows.from("amqpLookaheadReportChannel")
				.handle(Amqp.outboundAdapter(amqpTemplate)
						.routingKey("wenplan-lookahead-report"))
				.log()
				.bridge()
				.get();
	}
	
	/**
	 * Amqp project summary outbound adapter.
	 *
	 * @param amqpTemplate the amqp template
	 * @return the integration flow
	 */
	@Bean
	public IntegrationFlow amqpProjectSummaryOutboundAdapter(AmqpTemplate amqpTemplate) {
		return IntegrationFlows.from("amqpProjectSummaryChannel")
				.handle(Amqp.outboundAdapter(amqpTemplate)
						.routingKey("wenplan-project-summary-mail"))
				.log()
				.bridge()
				.get();
	}
	
	/**
	 * Prints the.
	 *
	 * @return the integration flow
	 */
	@Bean
	public IntegrationFlow print() {
		return IntegrationFlows.from("log")
				.log("Printing")
				.get();
	}

}
