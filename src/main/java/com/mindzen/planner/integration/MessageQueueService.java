package com.mindzen.planner.integration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.stringtemplate.v4.ST;

import com.mindzen.infra.mail.dto.MailDTO;
import com.mindzen.infra.mail.dto.MultipartInLineDTO;
import com.mindzen.infra.mail.dto.TemplateDTO;
import com.mindzen.infra.util.GsonUtil;
import com.mindzen.planner.config.AppConfig;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.ProjectSummaryDetailsDTO;
import com.mindzen.planner.util.DateUtil;
import com.mindzen.planner.util.EncodeDecode;
import com.mindzen.planner.util.PathUtil;

/**
 * The Class MessageQueueService.
 * 
 */
@Component
public class MessageQueueService {

	/** The app config. */
	@Autowired
	private AppConfig appConfig;

	/** The integration gateway. */
	@Autowired
	private SpringIntegrationGateway integrationGateway;

	/**
	 * Send project share mail.
	 *
	 * @param email the email
	 * @param templateName the template name
	 * @param projectName the project name
	 * @param firstName the first name
	 * @param senderName the sender name
	 * @param mailSubject the mail subject
	 */
	public void sendProjectShareMail(String email, String templateName, String projectName, String firstName, String senderName, String mailSubject) {

		MailDTO mailDTO = new MailDTO();
		mailDTO.setTo(email);
		//	mailDTO.setSubject("Project Permission update");
		mailDTO.setSubject(mailSubject);

		TemplateDTO templateDTO = new TemplateDTO();
		templateDTO.setFileName(templateName);
		templateDTO.setFilePath(PathUtil.WENPLAN_MAIL_TEMPLATE_HOME);
		templateDTO.setContentType(MediaType.TEXT_HTML_VALUE);

		ST urlPathTemplate = new ST(appConfig.getUserVerifyPath(), '$', '$');
		urlPathTemplate.add("source", appConfig.getSharedUserType());
		urlPathTemplate.add("email", EncodeDecode.encode(email));

		Map<String, String> dataMap = new HashMap<>();
		dataMap.put("url", appConfig.getUserVerifyHostAndPort()+urlPathTemplate.render());
		dataMap.put("projectName", projectName);
		dataMap.put("firstName", firstName);
		dataMap.put("userName", senderName);
		templateDTO.setDataMap(dataMap);
		templateDTO.setTemplate(true);
		templateDTO.setBody("<html>	<head>		<title>Verify Email</title>			<meta charset=\"utf-8\">	    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">	    <style type=\"text/css\">	    	body{	    		margin: 0px;	    		padding: 0px;	    		background-color: #f9f9f9;	    	}	   		.logo{	   			height: 100px;			    			    padding-left: 15%;			    padding-top: 50px;	   		}	    	.wSign{				font-size: 35px;				padding: 12px 12px;				background-color: #3e47cb;				display: inline-block;				border-radius: 100%;				width: 40px;				height: 35px;				font-family: 'RalewayRegular';				text-align: center;				margin: 15px 20px;				border: 1px solid black;				color: #fff;				font-weight: bold;			}	    	.content{	    		font-family: 'LatoRegular';				font-size: 40px;				font-weight: 600;				font-style: normal;				font-stretch: normal;				line-height: 1.22;				letter-spacing: normal;				text-align: left;				color: #243d66;					   	    	}	    	.verifyEmail{	    		    width: 800px;				    height: 300px;				   				    margin-left: 2%;				    				    background-color: #ffffff;	    	}	    	.verifyEmail .upperContent{	    		height:150px;	    			    		font-family: 'LatoRegular';				font-size: 25px;				font-weight: normal;				font-style: normal;				font-stretch: normal;				line-height: 1.4;				letter-spacing: normal;				text-align: left;				padding-left: 5%;    			padding-top: 5%;	    	}	    	.verifyBtn{	    		padding-left:35%;	    	}	    	.verifyBtn button{				background-color: #3e47cb;				color: #ffffff;				cursor:pointer;				width: 100px;				height: 35px;				font-family: 'LatoRegular';				font-size: 20px;				line-height: 1.22;				text-align: center;				border:none;				outline: none;				font-weight: bold;				padding: 7px 7px;	    	}	    		    </style>	</head>	<body>		<div class=\"logo\">			<span class=\"wSign\">W</span>			<span class=\"content\">WenPlan</span>		</div>		<div class=\"verifyEmail\">			<div class=\"upperContent\">Hi,<br><br>To complete your sign up, please verify your email.</div>			<div class=\"verifyBtn\">				<a href=\"\" style=\"text-decoration:none\">						<span style=\"font-size: 24px;    background-color: rgb(142, 221, 241);    line-height: 31.2px;    padding: 10px 20px;    border-radius: 25px;    border-style: double;    border-width: 10px;    border-color: rgb(255,255,255);\"/>							VERIFY				</a>			</div>		</div>	</body></html>");

		mailDTO.setTemplate(templateDTO);

		MultipartInLineDTO demoMultipartInLineDTO = new MultipartInLineDTO();
		demoMultipartInLineDTO.setFileName("logo.png");
		demoMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		demoMultipartInLineDTO.setFileContentId("logo");
		demoMultipartInLineDTO.setFileContentType(MediaType.IMAGE_JPEG_VALUE);

		MultipartInLineDTO facebookMultipartInLineDTO = new MultipartInLineDTO();
		facebookMultipartInLineDTO.setFileName("facebook.png");
		facebookMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		facebookMultipartInLineDTO.setFileContentId("facebook");
		facebookMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO linkedinMultipartInLineDTO = new MultipartInLineDTO();
		linkedinMultipartInLineDTO.setFileName("linkedin.png");
		linkedinMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		linkedinMultipartInLineDTO.setFileContentId("linkedin");
		linkedinMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO twitterMultipartInLineDTO = new MultipartInLineDTO();
		twitterMultipartInLineDTO.setFileName("twitter.png");
		twitterMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		twitterMultipartInLineDTO.setFileContentId("twitter");
		twitterMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		mailDTO.setMultipartInlines(Arrays.asList(demoMultipartInLineDTO, facebookMultipartInLineDTO, linkedinMultipartInLineDTO, twitterMultipartInLineDTO));

		Message<String> message = MessageBuilder
				.withPayload(GsonUtil.toJsonStringFromObject(mailDTO))
				.setHeader("userId", UserInfoContext.getUserThreadLocal())
				.build();

		integrationGateway.sendAmqpUserVerifyMessage(message);
	}

	/**
	 * Send json report.
	 *
	 * @param senderName the sender name
	 * @param projectName the project name
	 * @param fileName the file name
	 * @param email the email
	 * @param reportTimestamp the report timestamp
	 * @param json the json
	 * @param string 
	 */
	public void sendJsonReport(String senderName, String projectName, String fileName, String email, long reportTimestamp, 
			String json, String reportType) {
		Message<String> message = MessageBuilder
				.withPayload(json)
				.setHeader("senderName", senderName)
				.setHeader("projectName", projectName)
				.setHeader("fileName", fileName)
				.setHeader("email", email)
				.setHeader("reportTimestamp", reportTimestamp)
				.setHeader("userId", UserInfoContext.getUserThreadLocal())
				.setHeader("reportType", reportType)
				.build();

		integrationGateway.sendAmqpLookaheadMessage(message);
	}

	/**
	 * Send project summary mail.
	 *
	 * @param email the email
	 * @param templateName the template name
	 * @param projectName the project name
	 * @param mailSubject the mail subject
	 * @param projectSummaryDetails the summary result list
	 */
	public void sendProjectSummaryMail(String email, String templateName, String mailSubject, ProjectSummaryDetailsDTO projectSummaryDetails) {
		MailDTO mailDTO = new MailDTO();
		mailDTO.setTo(email);
		mailDTO.setSubject(projectSummaryDetails.getProjectName()+mailSubject);
//		mailDTO.setBcc("kondra.klr.55@hotmail.com");
//		mailDTO.setCc("deepa.hariharan@mindzen.com");
		
		TemplateDTO templateDTO = new TemplateDTO();
		templateDTO.setFileName(templateName);
		templateDTO.setFilePath(PathUtil.WENPLAN_MAIL_TEMPLATE_HOME);
		templateDTO.setContentType(MediaType.TEXT_HTML_VALUE);

		ST urlPathTemplate = new ST(appConfig.getUserVerifyPath(), '$', '$');
		urlPathTemplate.add("source", appConfig.getSharedUserType());
		urlPathTemplate.add("email", EncodeDecode.encode(email));

		Map<String, Object> dataObjMap = new HashMap<>();
		dataObjMap.put("url", appConfig.getUserVerifyHostAndPort()+urlPathTemplate.render());
		dataObjMap.put("projectName", projectSummaryDetails.getProjectName());
		dataObjMap.put("companyName", projectSummaryDetails.getCompanyName());
		dataObjMap.put("companyLogo", projectSummaryDetails.getCompanyLogo());
		if(projectSummaryDetails.getLastWeekFromDate() != null)
			dataObjMap.put("pastFrom", DateUtil.convertDate(projectSummaryDetails.getLastWeekFromDate(), DateUtil.Formats.MMMMDYYYY_COMMA.toString()));
		if(projectSummaryDetails.getLastWeekToDate() != null)
			dataObjMap.put("pastTo", DateUtil.convertDate(projectSummaryDetails.getLastWeekToDate(), DateUtil.Formats.MMMMDYYYY_COMMA.toString()));
		if(projectSummaryDetails.getFutureWeekFromDate() != null)
			dataObjMap.put("futureFrom", DateUtil.convertDate(projectSummaryDetails.getFutureWeekFromDate(), DateUtil.Formats.MMMMDYYYY_COMMA.toString()));
		if(projectSummaryDetails.getFutureWeekToDate() != null)
			dataObjMap.put("futureTo", DateUtil.convertDate(projectSummaryDetails.getFutureWeekToDate(), DateUtil.Formats.MMMMDYYYY_COMMA.toString()));
		dataObjMap.put("notifcationId", projectSummaryDetails.getNotificationId());
		dataObjMap.put("summaryResults", projectSummaryDetails);

		templateDTO.setDataObjMap(dataObjMap);
		templateDTO.setTemplate(false);

		mailDTO.setTemplate(templateDTO);

		MultipartInLineDTO demoMultipartInLineDTO = new MultipartInLineDTO();
		demoMultipartInLineDTO.setFileName("logo.png");
		demoMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		demoMultipartInLineDTO.setFileContentId("logo");
		demoMultipartInLineDTO.setFileContentType(MediaType.IMAGE_JPEG_VALUE);

		MultipartInLineDTO facebookMultipartInLineDTO = new MultipartInLineDTO();
		facebookMultipartInLineDTO.setFileName("facebook.png");
		facebookMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		facebookMultipartInLineDTO.setFileContentId("facebook");
		facebookMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO linkedinMultipartInLineDTO = new MultipartInLineDTO();
		linkedinMultipartInLineDTO.setFileName("linkedin.png");
		linkedinMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		linkedinMultipartInLineDTO.setFileContentId("linkedin");
		linkedinMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO twitterMultipartInLineDTO = new MultipartInLineDTO();
		twitterMultipartInLineDTO.setFileName("twitter.png");
		twitterMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		twitterMultipartInLineDTO.setFileContentId("twitter");
		twitterMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		mailDTO.setMultipartInlines(Arrays.asList(demoMultipartInLineDTO, facebookMultipartInLineDTO, linkedinMultipartInLineDTO, twitterMultipartInLineDTO));
		UserInfoContext.setUserThreadLocal(projectSummaryDetails.getUserId());
		Message<String> message = MessageBuilder
				.withPayload(GsonUtil.toJsonStringFromObject(mailDTO))
				.setHeader("userId", UserInfoContext.getUserThreadLocal())
				.build();

		integrationGateway.sendAmqpProjectSummaryMessage(message);
	}

	/**
	 * Send project summary mail daily.
	 *
	 * @param email the email
	 * @param templateName the template name
	 * @param projectName the project name
	 * @param mailSubject the mail subject
	 * @param projectSummaryDetails the project summary details
	 */
	public void sendProjectSummaryMailDaily(String email, String templateName, String mailSubject, ProjectSummaryDetailsDTO projectSummaryDetails) {
		MailDTO mailDTO = new MailDTO();
		mailDTO.setTo(email);
		mailDTO.setSubject(projectSummaryDetails.getProjectName()+mailSubject);
//		mailDTO.setBcc("rajagopal.mahendran@mindzen.co.in");
//		mailDTO.setCc("deepa.hariharan@mindzen.com");

		TemplateDTO templateDTO = new TemplateDTO();
		templateDTO.setFileName(templateName);
		templateDTO.setFilePath(PathUtil.WENPLAN_MAIL_TEMPLATE_HOME);
		templateDTO.setContentType(MediaType.TEXT_HTML_VALUE);

		ST urlPathTemplate = new ST(appConfig.getUserVerifyPath(), '$', '$');
		urlPathTemplate.add("source", appConfig.getSharedUserType());
		urlPathTemplate.add("email", EncodeDecode.encode(email));

		Map<String, Object> dataObjMap = new HashMap<>();
		dataObjMap.put("url", appConfig.getUserVerifyHostAndPort()+urlPathTemplate.render());
		dataObjMap.put("companyName", projectSummaryDetails.getCompanyName());
		dataObjMap.put("companyLogo", projectSummaryDetails.getCompanyLogo());
		dataObjMap.put("projectName", projectSummaryDetails.getProjectName());
		if(projectSummaryDetails.getLastDay() != null)
			dataObjMap.put("pastDay", DateUtil.convertDate(projectSummaryDetails.getLastDay(), DateUtil.Formats.MMMMDYYYY_COMMA.toString()));
		if(projectSummaryDetails.getFutureDay() != null)
			dataObjMap.put("futureDay", DateUtil.convertDate(projectSummaryDetails.getFutureDay(), DateUtil.Formats.MMMMDYYYY_COMMA.toString()));
		dataObjMap.put("notifcationId", projectSummaryDetails.getNotificationId().toString());
		dataObjMap.put("summaryResults", projectSummaryDetails);

		templateDTO.setDataObjMap(dataObjMap);
		templateDTO.setTemplate(false);

		mailDTO.setTemplate(templateDTO);

		MultipartInLineDTO demoMultipartInLineDTO = new MultipartInLineDTO();
		demoMultipartInLineDTO.setFileName("logo.png");
		demoMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		demoMultipartInLineDTO.setFileContentId("logo");
		demoMultipartInLineDTO.setFileContentType(MediaType.IMAGE_JPEG_VALUE);

		MultipartInLineDTO facebookMultipartInLineDTO = new MultipartInLineDTO();
		facebookMultipartInLineDTO.setFileName("facebook.png");
		facebookMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		facebookMultipartInLineDTO.setFileContentId("facebook");
		facebookMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO linkedinMultipartInLineDTO = new MultipartInLineDTO();
		linkedinMultipartInLineDTO.setFileName("linkedin.png");
		linkedinMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		linkedinMultipartInLineDTO.setFileContentId("linkedin");
		linkedinMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		MultipartInLineDTO twitterMultipartInLineDTO = new MultipartInLineDTO();
		twitterMultipartInLineDTO.setFileName("twitter.png");
		twitterMultipartInLineDTO.setFilePath(PathUtil.WENPLAN_IMAGES_HOME);
		twitterMultipartInLineDTO.setFileContentId("twitter");
		twitterMultipartInLineDTO.setFileContentType(MediaType.IMAGE_PNG_VALUE);

		mailDTO.setMultipartInlines(Arrays.asList(demoMultipartInLineDTO, facebookMultipartInLineDTO, linkedinMultipartInLineDTO, twitterMultipartInLineDTO));
		UserInfoContext.setUserThreadLocal(projectSummaryDetails.getUserId());
		Message<String> message = MessageBuilder
				.withPayload(GsonUtil.toJsonStringFromObject(mailDTO))
				.setHeader("userId", UserInfoContext.getUserThreadLocal())
				.build();

		integrationGateway.sendAmqpProjectSummaryMessage(message);
	}
}
