/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.integration;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

// TODO: Auto-generated Javadoc
/**
 * The Interface SpringIntegrationGateway.
 *
 * @author Alexpandiyan Chokkan
 */
@MessagingGateway
public interface SpringIntegrationGateway {

	/**
	 * Send amqp user verify message.
	 *
	 * @param message the message
	 */
	@Gateway(requestChannel="amqpVerifyUserMailChannel")
	void sendAmqpUserVerifyMessage(Message<String> message);

	/**
	 * Send amqp lookahead message.
	 *
	 * @param message the message
	 */
	@Gateway(requestChannel="amqpLookaheadReportChannel")
	void sendAmqpLookaheadMessage(Message<String> message);
	
	/**
	 * Send amqp project summary message.
	 *
	 * @param message the message
	 */
	@Gateway(requestChannel="amqpProjectSummaryChannel")
	void sendAmqpProjectSummaryMessage(Message<String> message);
	
}
