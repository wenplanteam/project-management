package com.mindzen.planner.service;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;

public interface PercentageCompleteService {

	public MSAResponse downloadPercentageCompletePDF(ProgressCompletionRequestDTO progressCompletionRequestDTO);

}
