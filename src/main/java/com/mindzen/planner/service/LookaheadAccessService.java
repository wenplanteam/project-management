/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service;

import java.io.FileInputStream;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadListDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface LookaheadAccessService.
 */
public interface LookaheadAccessService {

	/**
	 *  Create new lookaheads.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the MSA response
	 */
	public MSAResponse createLookahead(LookaheadDTO lookaheadDTO);

	/**
	 *  Return the Lookahead based on lookaheadListDTO filters.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @param isLookahead
	 * @param pageSize
	 * @param pageNo
	 * @return the MSA response
	 */
	public MSAResponse listLookahead(LookaheadListDTO lookaheadListDTO, Long daysCount, boolean isLookahead);

	/**
	 *  Return lookahead date info.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	public MSAResponse lookaheadInfo(Long projectId);

	/**
	 *  All tasks list.
	 *
	 * @return the MSA response
	 */
	MSAResponse loadAllTasks();

	/**
	 * New lookahead impl.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
//	MSAResponse newLookaheadImpl(LookaheadListDTO lookaheadListDTO);

	/**
	 * Update lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the MSA response
	 */
	public MSAResponse updateLookahead(LookaheadDTO lookaheadDTO);

	/**
	 * Jasper report data.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	public MSAResponse jasperReportData(LookaheadListDTO lookaheadListDTO);

	/**
	 * Lookahead by id.
	 *
	 * @param taskId the task id
	 * @param subTaskId the sub task id
	 * @return the MSA response
	 */
	public MSAResponse lookaheadById(Long taskId, Long subTaskId);

	/**
	 * Gets the lookahead.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @param pageSize
	 * @param pageNo
	 * @param daysCount 
	 * @param isLookahead
	 * @return the lookahead
	 */
	public MSAResponse getLookahead(LookaheadListDTO lookaheadListDTO, Long daysCount, boolean isLookahead);

	/**
	 * Download PDF.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	public MSAResponse downloadPDF(LookaheadListDTO lookaheadListDTO);

	/**
	 * Upload excel to lookahead.
	 *
	 * @param inputStream the input stream
	 * @param name the name
	 * @param projectId the project id
	 * @param projCompanyId the proj company id
	 * @return the MSA response
	 */
	public MSAResponse uploadExcelToLookahead(FileInputStream inputStream, String name, Long projectId, Long projCompanyId);

	public MSAResponse createLookaheadForImportExcel(LookaheadDTO lookaheadDTO, Long trackId);

	public String exportMppFile(Long projectId);

}
