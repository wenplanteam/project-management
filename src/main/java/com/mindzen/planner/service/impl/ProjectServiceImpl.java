/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.DEL;
import static com.mindzen.infra.api.response.ApiResponseConstants.ERR;
import static com.mindzen.infra.api.response.ApiResponseConstants.FND;
import static com.mindzen.infra.api.response.ApiResponseConstants.INS;
import static com.mindzen.infra.api.response.ApiResponseConstants.NOT;
import static com.mindzen.infra.api.response.ApiResponseConstants.REC;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCSFLY;
import static com.mindzen.infra.api.response.ApiResponseConstants.UPD;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.converter.ProjectConverter;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadFilterDTO;
import com.mindzen.planner.dto.LookaheadInfoDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.Pagination;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProgressCompletionResponseDTO;
import com.mindzen.planner.dto.ProjectAccessDTO;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.dto.ScheduleDTO;
import com.mindzen.planner.dto.SubTaskDTO;
import com.mindzen.planner.dto.SubTaskDetailsDTO;
import com.mindzen.planner.dto.TaskDTO;
import com.mindzen.planner.entity.NotificationEntity;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectCompany;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.repository.NotificationRepository;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.repository.ScheduleRepository;
import com.mindzen.planner.repository.TaskRepository;
import com.mindzen.planner.service.LookaheadAccessService;
import com.mindzen.planner.service.ProjectService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;
import com.mindzen.planner.util.EnumDays;
import com.mindzen.planner.util.TaskCompletionUtil;

import lombok.extern.slf4j.Slf4j;

/** The Constant log. */
@Slf4j
@Component
public class ProjectServiceImpl implements ProjectService {

	/** The project repository. */
	private ProjectRepository projectRepository;

	/** The project user repository. */
	private ProjectUserRepository projectUserRepository;

	/** The project converter. */
	private ProjectConverter projectConverter;

	/** The project company repository. */
	private ProjectCompanyRepository projectCompanyRepository;

	/** The notification repository. */
	private NotificationRepository notificationRepository;

	/** The schedule repository. */
	private ScheduleRepository scheduleRepository;

	/** The task repository. */
	@Resource
	private TaskRepository taskRepository;

	/** The model mapper. */
	ModelMapper modelMapper;

	/** The entity manager. */
	@PersistenceContext
	private EntityManager entityManager;

	/** The response. */
	MSAResponse response = null;

	private LookaheadAccessService lookaheadAccessService;

	private ObjectMapper objectMapper;

	/**
	 * Instantiates a new project service impl.
	 *
	 * @param projectRepository the project repository
	 * @param projectUserRepository the project user repository
	 * @param projectCompanyRepository the project company repository
	 * @param scheduleRepository the schedule repository
	 * @param notificationRepository the notification repository
	 * @param modelMapper the model mapper
	 */
	public ProjectServiceImpl(ProjectRepository projectRepository, ProjectUserRepository projectUserRepository, 
			ProjectCompanyRepository projectCompanyRepository, ScheduleRepository scheduleRepository,
			NotificationRepository notificationRepository, ModelMapper modelMapper, LookaheadAccessService lookaheadAccessService,
			ObjectMapper objectMapper) {
		this.projectRepository = projectRepository;
		this.projectUserRepository = projectUserRepository;
		this.projectConverter = new ProjectConverter();
		this.scheduleRepository=scheduleRepository;
		this.notificationRepository= notificationRepository;
		this.projectCompanyRepository = projectCompanyRepository;
		this.modelMapper = modelMapper;
		this.lookaheadAccessService = lookaheadAccessService;
		this.objectMapper = objectMapper;
	}



	/**
	 * Projects.
	 *
	 * @param pageRequest the page request
	 * @param status the status
	 * @param searchTerm the search term
	 * @param created the created
	 * @return the MSA response
	 */
	@Override
	public MSAResponse projects(Pageable pageRequest, Boolean status, String searchTerm, Long created) {
		Long userId = UserInfoContext.getUserThreadLocal();
		Page<Project> projectPage = null;
		if(!"".equals(searchTerm) && null != searchTerm)
			return projectFuzzySearch(pageRequest, status, searchTerm);
		if (null == status) {
			projectPage = projectRepository.findProjectsByUser(pageRequest, false, false, true, userId,created);
		} 
		else {
			projectPage = projectRepository.findProjectsByUser(pageRequest, false, status, false, true, userId, created);
		}
		if(projectPage.hasContent()) {
			Pagination pagination = new Pagination();
			pagination.setCurrentPageNumber(pageRequest.getPageNumber());
			pagination.setPaginationSize(pageRequest.getPageSize());
			pagination.setTotalRecords(projectPage.getTotalElements());
			return new MSAResponse(true, REC + FND, projectConverter.getProjectDTOs(projectPage.getContent(),projectCompanyRepository), pagination, HttpStatus.OK);
		}
		else {
			String msg = REC + NOT + FND;
			if(null == status)
				msg = "No Records";
			return new MSAResponse(false, HttpStatus.NOT_FOUND, msg , "E-001", null);
		}
	}

	/**
	 * Post project.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@Override
	public MSAResponse postProject(ProjectDTO projectDTO) {

		Project project = new Project();
		try {
			project.setProjectNo(projectDTO.getProjectNo());
			project.setName(projectDTO.getProjectName());
			project.setDescription(projectDTO.getProjectDescription());
			project.setLicenseDistributionId(projectDTO.getLicenseDistributionId());
			project.setProjectManager(projectDTO.getProjectManager());
			project.setOwnerCompany(projectDTO.getProjectOwner());
			project.setJobPhone(projectDTO.getBusinessPhone());
			String weekStartDay = EnumDays.SUNDAY.getDay();
			if(null != projectDTO.getWeekStartDay() && !"".equals(projectDTO.getWeekStartDay()))
				weekStartDay = projectDTO.getWeekStartDay();
			project.setWeekStartDay(weekStartDay);
			String leaveDays = new StringBuilder(EnumDays.SUNDAY.getDay()).append(",").append(EnumDays.SATURDAY.getDay()).toString();
			if(null != projectDTO.getLeaveDays() && projectDTO.getLeaveDays().size()>0)
				leaveDays = String.join(",",projectDTO.getLeaveDays());
			project.setLeaveDays(leaveDays);

			Date d = DateUtil.convertToDateViaInstant(LocalDate.now());
			String startDate = projectDTO.getEstimatedStartDate();
			String endDate = projectDTO.getEstimatedEndDate();
			Date estimatedStartDate = null;
			Date estimatedEndDate = null;

			if (!"".equals(startDate) && null != startDate)
				estimatedStartDate = DateUtil.stringToDateViaInstant(startDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
			else
				estimatedStartDate = d;
			if (!"".equals(endDate) && null != endDate)
				estimatedEndDate = DateUtil.stringToDateViaInstant(endDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
			else
				estimatedEndDate = d;
			project.setEstimatedStartDate(estimatedStartDate);
			project.setEstimatedEndDate(estimatedEndDate);
			project.setProjectStage(projectDTO.getProjectStage());
			project.setCity(projectDTO.getCity());
			project.setZipCode(projectDTO.getZipCode());
			project.setState(projectDTO.getState());
			project.setCounty(projectDTO.getCounty());
			project.setCountry(projectDTO.getCountry());
			project.setIsActive(true);
			project = projectRepository.save(project);
			Long userId = UserInfoContext.getUserThreadLocal();

			//Add projects to company
			//Long companyId = projectRepository.findProjectManagerCompanyId(project.getId(), userId);
			Long companyId = projectRepository.findProjectManagerCompanyId(userId);
			ProjectCompany projectCompany = new ProjectCompany();
			projectCompany.setProject(project);
			projectCompany.setCompanyId(companyId);
			projectCompany.setProjectRole(Constants.GENERAL_CONTRACTOR);
			projectCompany.setActive(true);
			projectCompany.setDeleted(false);
			projectCompany = projectCompanyRepository.save(projectCompany);

			ProjectUser projectUser = new ProjectUser();
			projectUser.setUserId(userId);
			projectUser.setProject(project);
			projectUser.setAccess(Constants.ACCESS_VIEW+","+Constants.ACCESS_EDIT+","+Constants.ACCESS_APPROVE);
			projectUser.setProjectRole(Constants.GC_PROJECT_ADMIN);
			projectUser.setIsActive(true);
			projectUser.setProjectCompany(projectCompany);
			projectUser = projectUserRepository.save(projectUser);


			List<ProjectUser> projectUserList =  new  ArrayList<>();
			projectUserList.add(projectUser);
			project.setProjectUser(projectUserList);
			//save notification default as disable.
			if(!projectDTO.isNotification())
				saveNotification(project, projectDTO, UserInfoContext.getUserThreadLocal());
			return new MSAResponse(true, HttpStatus.OK, REC + INS + SUCSFLY, projectConverter.getProjectDTO(project,projectCompanyRepository));
		} catch (Exception e) {
			e.printStackTrace();
			return new MSAResponse(true, HttpStatus.INTERNAL_SERVER_ERROR, ERR, e.getMessage());
		}
	}

	/**
	 * Put project.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@Override
	public MSAResponse putProject(ProjectDTO projectDTO) {
		Optional<Project> optionalProject = projectRepository.findById(projectDTO.getProjectId());
		if (!optionalProject.isPresent()) {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}

		Project project = optionalProject.get();
		project.setProjectNo(projectDTO.getProjectNo());
		project.setName(projectDTO.getProjectName());
		project.setDescription(projectDTO.getProjectDescription());
		project.setProjectManager(projectDTO.getProjectManager());
		project.setOwnerCompany(projectDTO.getProjectOwner());
		project.setJobPhone(projectDTO.getBusinessPhone());

		Date d = DateUtil.convertToDateViaInstant(LocalDate.now());
		String startDate = projectDTO.getEstimatedStartDate();
		String endDate = projectDTO.getEstimatedEndDate();
		Date estimatedStartDate = null;
		Date estimatedEndDate = null;
		if (!"".equals(startDate) && null != startDate)
			estimatedStartDate = DateUtil.stringToDateViaInstant(startDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		else
			estimatedStartDate = d;
		if (!"".equals(endDate) && null != endDate)
			estimatedEndDate = DateUtil.stringToDateViaInstant(endDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		else
			estimatedEndDate = d;
		project.setEstimatedStartDate(estimatedStartDate);
		project.setEstimatedEndDate(estimatedEndDate);
		project.setProjectStage(projectDTO.getProjectStage());
		project.setCity(projectDTO.getCity());
		project.setZipCode(projectDTO.getZipCode());
		project.setState(projectDTO.getState());
		project.setCounty(projectDTO.getCounty());
		String status = projectDTO.getProjectStatus();
		boolean projectStatuses = false;
		if (Constants.ACTIVE.equalsIgnoreCase(status))
			projectStatuses = true;
		else
			projectStatuses = false;
		project.setIsActive(projectStatuses);
		project.setCountry(projectDTO.getCountry());
		projectRepository.save(project);
		return new MSAResponse(true, HttpStatus.OK, REC + UPD + SUCSFLY, null);
	}

	/**
	 * Delete project.
	 *
	 * @param projectIds the project ids
	 * @return the MSA response
	 */
	@Override
	public MSAResponse deleteProject(Long[] projectIds) {
		for (Long projectId : projectIds) {
			Project project = projectRepository.findById(projectId).get();
			project.setIsActive(false);
			project.setIsDeleted(true);
			projectRepository.save(project);
		}
		return new MSAResponse(true, HttpStatus.OK, REC + DEL + SUCSFLY, null);
	}

	/**
	 * Find by project id.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@Override
	public MSAResponse findByProjectId(Long projectId) {
		Optional<Project> project = projectRepository.findById(projectId);
		if (project.isPresent()) {
			return new MSAResponse(true, HttpStatus.OK, REC + FND + SUCSFLY,
					projectConverter.getProjectDTO(project.get(),projectCompanyRepository));
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}
	}

	/*@Override
	public MSAResponse findByProjectForPlanSharing(Long projectId) {
		Long userId = UserInfoContext.getUserThreadLocal();
		List<String> companyEmails = projectRepository.findByProjectForPlanSharing(userId, projectId);
		if (companyEmails != null) {
			return new MSAResponse(true, HttpStatus.OK, REC + FND + SUCSFLY, companyEmails);
		}
		return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
	}*/

	/**
	 * Initialize hibernate search.
	 */
	public void initializeHibernateSearch() {
		try {
			FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
			fullTextEntityManager.createIndexer().startAndWait();
		}
		catch(InterruptedException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	/**
	 * Project fuzzy search.
	 *
	 * @param pageRequest the page request
	 * @param status the status
	 * @param searchTerm the search term
	 * @return the MSA response
	 */
	@SuppressWarnings({ "unchecked" })
	@Transactional
	private MSAResponse projectFuzzySearch(Pageable pageRequest, Boolean status, String searchTerm) {
		Long userId = UserInfoContext.getUserThreadLocal();
		initializeHibernateSearch();
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Project.class).get();

		BooleanQuery.Builder isProjectDeletedBooleanQueryBuilder = new BooleanQuery
				.Builder()
				.add(queryBuilder
						.keyword()
						.wildcard()
						.onField("isDeleted")
						.matching(false)
						.createQuery(), 
						BooleanClause.Occur.MUST);

		BooleanQuery.Builder isProjectUserDeletedBooleanQueryBuilder = new BooleanQuery
				.Builder()
				.add(queryBuilder
						.keyword()
						.wildcard()
						.onField("projectUser.isDeleted")
						.matching(false)
						.createQuery(), 
						BooleanClause.Occur.MUST);

		BooleanQuery.Builder isProjectUserActiveBooleanQueryBuilder = new BooleanQuery
				.Builder()
				.add(queryBuilder
						.keyword()
						.wildcard()
						.onField("projectUser.isActive")
						.matching(true)
						.createQuery(), 
						BooleanClause.Occur.MUST);

		Query luceneQuery = queryBuilder
				.bool()
				.must(queryBuilder.keyword()
						.fuzzy()
						.onFields("name", "description", "projectNo", "projectManager", "ownerCompany", "jobPhone", "projectStage", "city", "state", "county", "country")
						.matching("*"+searchTerm+"*")
						.createQuery())
				.must(queryBuilder
						.range()
						.onField("projectUser.userId")
						.from(userId).to(userId)
						.createQuery())
				.must(isProjectDeletedBooleanQueryBuilder.build())
				.must(isProjectUserDeletedBooleanQueryBuilder.build())
				.must(isProjectUserActiveBooleanQueryBuilder.build())
				.createQuery();
		Query combinedLuceneQuery = null;
		if(null != status) {
			BooleanQuery.Builder isProjectActiveBooleanQueryBuilder = new BooleanQuery
					.Builder()
					.add(queryBuilder
							.keyword()
							.wildcard()
							.onField("isActive")
							.matching(status)
							.createQuery(), 
							BooleanClause.Occur.MUST);
			combinedLuceneQuery = queryBuilder
					.bool()
					.must(luceneQuery)
					.must(isProjectActiveBooleanQueryBuilder.build())
					.createQuery();
		}
		else
			combinedLuceneQuery = queryBuilder
			.bool()
			.must(luceneQuery)
			.createQuery();


		javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(combinedLuceneQuery, Project.class);
		jpaQuery.setFirstResult(pageRequest.getPageNumber());
		jpaQuery.setMaxResults(pageRequest.getPageSize());
		Pagination pagination = new Pagination();
		pagination.setCurrentPageNumber(pageRequest.getPageNumber());
		pagination.setPaginationSize(pageRequest.getPageSize());
		pagination.setTotalRecords(jpaQuery.getResultList().size());
		if(jpaQuery.getResultList().size()>0)
			return new MSAResponse(true, REC + FND, projectConverter.getProjectDTOs(jpaQuery.getResultList(),projectCompanyRepository), pagination, HttpStatus.OK);
		else {
			log.error("No result found");
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, null);
		}
	}

	/**
	 * Gets the project info.
	 *
	 * @param projectId the project id
	 * @return the project info
	 */
	@Override
	public MSAResponse getProjectInfo(Long projectId) {
		MSAResponse msaResponse = null;
		if(null !=  projectId) {
			Optional<Project> projectOptional = projectRepository.findById(projectId);
			LookaheadInfoDTO lookaheadInfoDTO = new LookaheadInfoDTO();
			if(projectOptional.isPresent()) {
				Project project = projectOptional.get();
				if(null != project.getLeaveDays())
					lookaheadInfoDTO.setLeaveDays(Arrays.asList(project.getLeaveDays().split(",")));
				if(null != project.getWeekStartDay())
					lookaheadInfoDTO.setWeekStartDay(project.getWeekStartDay());
				lookaheadInfoDTO.setFloatDays(project.getFloatDays());
				getNotificationDetails(project, lookaheadInfoDTO);
				msaResponse = new MSAResponse(true, HttpStatus.OK, SUCS, lookaheadInfoDTO);
			}
			else {
				msaResponse = new MSAResponse(false, HttpStatus.BAD_REQUEST, NOT + FND, "E-001", null);
			}
		}
		else {
			msaResponse = new MSAResponse(false, HttpStatus.BAD_REQUEST, ERR, "E-001", null);
		}
		return msaResponse;
	}

	/**
	 * Gets the notification details.
	 *
	 * @param project the project
	 * @param lookaheadInfoDTO the lookahead info DTO
	 * @return the notification details
	 */
	private LookaheadInfoDTO getNotificationDetails(Project project, LookaheadInfoDTO lookaheadInfoDTO) {
		Optional<ProjectUser> projUserOpt = project.getProjectUser().stream().
				filter(projUserObj -> UserInfoContext.getUserThreadLocal().equals(projUserObj.getUserId())).findFirst();
		if(projUserOpt.isPresent())
		{
			NotificationEntity notificationObj = projUserOpt.get().getNotification();
			if(notificationObj.isNotification())
			{
				lookaheadInfoDTO.setNotification(notificationObj.isNotification());
				lookaheadInfoDTO.setFrequency(notificationObj.getFrequency());
				lookaheadInfoDTO.setInformation(Arrays.asList(notificationObj.getInformation().split(",")));
				lookaheadInfoDTO.setType(Arrays.asList(notificationObj.getType().split(",")));
				lookaheadInfoDTO.setDayOfTheWeek(notificationObj.getDayOfTheWeek());	
			}
			else
			{
				lookaheadInfoDTO.setNotification(false);
				lookaheadInfoDTO.setFrequency(notificationObj.getFrequency());
				lookaheadInfoDTO.setInformation(Arrays.asList(notificationObj.getInformation().split(",")));
				lookaheadInfoDTO.setType(Arrays.asList(notificationObj.getType().split(",")));
				lookaheadInfoDTO.setDayOfTheWeek(notificationObj.getDayOfTheWeek());
			}
		}
		return lookaheadInfoDTO;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectService#insertProjectInfo(com.mindzen.planner.dto.ProjectDTO, java.lang.Long)
	 */
	@Override
	public MSAResponse insertProjectInfo(ProjectDTO projectDTO) {
		String msg;
		boolean status;
		HttpStatus httpStatus;
		Project project = null;
		if(null !=  projectDTO.getProjectId()) {
			Optional<Project> projectOptional = projectRepository.findById(projectDTO.getProjectId());
			if(projectOptional.isPresent()){
				project = projectOptional.get();
				project.setWeekStartDay(projectDTO.getWeekStartDay());
				project.setLeaveDays(String.join(",",projectDTO.getLeaveDays()));
				if(projectDTO.getFloatDays()==null) {
					project.setFloatDays(0);
				}else {
					project.setFloatDays(projectDTO.getFloatDays());	
				}
				if(!saveNotification(project, projectDTO, UserInfoContext.getUserThreadLocal())) {
					return new MSAResponse(false, HttpStatus.BAD_REQUEST, "Failed to update configuration", null);
				}
				projectRepository.save(project);
				status=true; msg = "Project Configuration " + UPD + SUCSFLY; httpStatus = HttpStatus.OK;
			}
			else {
				status= false; msg = NOT+FND; httpStatus = HttpStatus.BAD_REQUEST;
			}
		}
		else {
			status= false; msg = ERR; httpStatus = HttpStatus.BAD_REQUEST;
		}
		return new MSAResponse(status, httpStatus, msg, null);
	}

	/**
	 * Save notification.
	 *
	 * @param project the project
	 * @param projectDTO the project DTO
	 * @param userId the user id
	 * @return the project
	 */
	public Boolean saveNotification(Project project, ProjectDTO projectDTO, Long userId) {
		Optional<ProjectUser> optProjUser=project.getProjectUser().stream().filter(projObj -> projObj != null && userId.equals(projObj.getUserId())).findFirst();
		if(optProjUser.isPresent())
		{
			ProjectUser projUserObj=optProjUser.get();
			NotificationEntity notificationEntity=projUserObj.getNotification();
			if(notificationEntity == null)
			{
				notificationEntity= new NotificationEntity();
			}	
			if(projectDTO.isNotification())
			{
				notificationEntity.setNotification(projectDTO.isNotification());
				notificationEntity.setType(String.join(",", projectDTO.getType()));
				notificationEntity.setFrequency(projectDTO.getFrequency());
				if(projectDTO.getFrequency().equalsIgnoreCase("Weekly"))
					notificationEntity.setDayOfTheWeek(projectDTO.getDayOfTheWeek());
				else
					notificationEntity.setDayOfTheWeek("");
				notificationEntity.setInformation(String.join(",", projectDTO.getInformation()));
			}
			else
			{
				notificationEntity.setNotification(projectDTO.isNotification());
				notificationEntity.setType("");
				notificationEntity.setFrequency("");
				notificationEntity.setDayOfTheWeek("");
				notificationEntity.setInformation("");
			}
			notificationEntity = notificationRepository.save(notificationEntity);
			projUserObj.setNotification(notificationEntity);
			projectUserRepository.save(projUserObj);
			log.info("Project User Notification updated");
			return true;
		}
		log.info("Project User not available");
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectService#getSpecDivision(java.lang.Long)
	 */
	@Override
	public MSAResponse getSpecDivision(Long projectId) {
		Optional<Project> project = projectRepository.findById(projectId);
		if (project.isPresent()) {
			ProjectDTO projectDTO =projectConverter.getProjectDTO(project.get(),projectCompanyRepository);
			if(null == projectDTO.getSpecDivision()) {
				projectDTO.setSpecDivision(Collections.emptyList());
			}
			return new MSAResponse(true, HttpStatus.OK, REC + FND + SUCSFLY, projectDTO.getSpecDivision().stream().sorted());
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectService#insertSpecDivision(com.mindzen.planner.dto.ProjectDTO)
	 */
	@Override
	public MSAResponse insertSpecDivision(ProjectDTO projectDTO) {
		Optional<Project> projectOptional = projectRepository.findById(projectDTO.getProjectId()); 
		if (projectOptional.isPresent()) {
			Project project = projectOptional.get();
			if(projectDTO.getSpecDivision()==null || projectDTO.getSpecDivision().isEmpty()) {
				project.setSpecDivision(null);
			}else {
				project.setSpecDivision(String.join("||", projectDTO.getSpecDivision()));
			}
			projectRepository.save(project);
			return new MSAResponse(true, HttpStatus.OK, "Spec Division Associated Successfully", null);
		}
		else
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, null);
	}

	/**
	 * Insert trade.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@Override
	public MSAResponse insertTrade(ProjectDTO projectDTO) {
		Optional<Project> projectOptional = projectRepository.findById(projectDTO.getProjectId()); 
		if (projectOptional.isPresent()) {
			Project project = projectOptional.get();
			if(projectDTO.getTrade()==null || projectDTO.getTrade().isEmpty()) {
				project.setTrade(null);
			}else {
				String trade = String.join(",", projectDTO.getTrade());
				project.setTrade(trade);
			}
			projectRepository.save(project);
			return new MSAResponse(true, HttpStatus.OK, "Trade Associated Successfully", null);
		}
		else
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, null);
	}

	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 */
	@Override
	public MSAResponse getTrade(Long projectId) {
		Optional<Project> project = projectRepository.findById(projectId);
		if (project.isPresent()) {
			ProjectDTO projectDTO =projectConverter.getProjectDTO(project.get(),projectCompanyRepository);
			if(null == projectDTO.getTrade()) {
				projectDTO.setTrade(new ArrayList<>());
			}
			return new MSAResponse(true, HttpStatus.OK, REC + FND + SUCSFLY,projectDTO.getTrade());
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, REC + NOT + FND, "E-001", null);
		}
	}

	/**
	 * Gets the GC by user.
	 *
	 * @return the GC by user
	 */
	@Override
	public MSAResponse getGCByUser() {
		List<ProjectAccessDTO> projectAccessDTOList = new ArrayList<>();
		Long userId = UserInfoContext.getUserThreadLocal();
		List<Project> projectList = projectRepository.findProjectsByUser(false, false, true, userId);
		projectList.forEach(project -> {
			ProjectAccessDTO projectAccessDTO = new ProjectAccessDTO();
			projectAccessDTO.setUserId(project.getCreatedBy());
			project.getProjectUser().forEach(projectUser -> {
				if(project.getCreatedBy() == projectUser.getUserId()) {
					projectAccessDTO.setCompanyId(projectUser.getProjectCompany().getCompanyId());
					String companyName = projectCompanyRepository.findCompanyById(projectUser.getProjectCompany().getCompanyId());
					projectAccessDTO.setCompanyName(companyName);
					project.getProjectUser().forEach(user -> {
						if(user.getUserId()== userId) {
							Long companyId = user.getProjectCompany().getCompanyId();
							String company =projectCompanyRepository.findCompanyById(companyId);
							projectAccessDTO.setCreateCompanyName(company);
						}
					});
					projectAccessDTOList.add(projectAccessDTO);
				}
			});
		});
		List<Object[]> companyList = projectUserRepository.findcompanyIdByCreatedBy(userId);
		ProjectAccessDTO projectAccessDTO = new ProjectAccessDTO();
		if(null != companyList) {
			companyList.forEach(company -> {
				Long user = UserInfoContext.getUserThreadLocal();
				Long companyId = ((BigInteger) company[0]).longValue();
				projectAccessDTO.setUserId(user);
				projectAccessDTO.setCompanyId(companyId);
				projectAccessDTO.setCompanyName((String) company[1]);
				projectAccessDTO.setCreateCompanyName((String) company[1]);
				projectAccessDTOList.add(projectAccessDTO);
			});
		} List<ProjectAccessDTO> uniqueProjectAccessDTO = projectAccessDTOList.stream() .collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingLong(ProjectAccessDTO::getUserId))),
				ArrayList::new));
		return new MSAResponse(true, HttpStatus.OK, SUCS,uniqueProjectAccessDTO);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectService#insertDefaultNotificationForProjectUser()
	 */
	@Override
	public void insertDefaultNotificationForProjectUser() {
		List<ProjectUser> projUserList = projectUserRepository.getAllEmptyNotificationId(Constants.TRUE);
		if(!projUserList.isEmpty())
		{
			for (ProjectUser projectUser : projUserList) {
				NotificationEntity notification= null;
				if(projectUser.getNotification() == null)
				{
					notification = new NotificationEntity();
					notification.setNotification(Constants.False);
					notification.setType("");
					notification.setFrequency("");
					notification.setDayOfTheWeek("");
					notification.setInformation("");
					notification = notificationRepository.save(notification);
					projectUser.setNotification(notification);
					projectUserRepository.save(projectUser);
				}
				else
					log.info("No data Updated default notification.");
			}
		}
		else
			log.info("No data Updated default notification.");
	}


	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectService#getProgressCompletionByProjectId(com.mindzen.planner.dto.ProgressCompletionRequestDTO)
	 */
	@Override
	public MSAResponse getProgressCompletionByProjectId(@Valid ProgressCompletionRequestDTO progressCompletionRequestDTO) {
		Pagination pagination = new Pagination();
		progressCompletionRequestDTO.setPageNo(progressCompletionRequestDTO.getPageNo() - 1);
		Pageable pageRequest = PageRequest.of(progressCompletionRequestDTO.getPageNo(), progressCompletionRequestDTO.getPageSize());
		int from = Math.max(0,pageRequest.getPageNumber()*pageRequest.getPageSize());
		List<ProgressCompletionResponseDTO> progressCompletionResList=new ArrayList<>();
		Optional<Project> projectObj = projectRepository.findById(progressCompletionRequestDTO.getProjectId());
		if(projectObj.isPresent()) {
			List<String> requestStatus = progressCompletionRequestDTO.getFilterDTO().getTaskStatus();
			CompletableFuture<MSAResponse> lookaheadResult = getLookaheadResult(progressCompletionRequestDTO);
			final MSAResponse msaResponse;
			try {
				msaResponse = lookaheadResult.get();
			} catch (Exception e) {
				return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "Interupted Exception", Collections.emptyList());
			}
			if(!msaResponse.isSuccess())
				return new MSAResponse(false, HttpStatus.OK, "Tasks & SubTask Not Found", Collections.emptyList());
			CompletableFuture<List<ProgressCompletionResponseDTO>> resultObj = CompletableFuture.supplyAsync(() -> {
				LookaheadFilterDTO lookaheadFilter = progressCompletionRequestDTO.getFilterDTO();
				LookaheadDTO lookaheadDTO = objectMapper.convertValue(msaResponse.getPayload(), LookaheadDTO.class);
				List<TaskDTO> taskDTOList = lookaheadDTO.getTasks();
				if(!taskDTOList.isEmpty()) {
					lookaheadFilter.setTaskStatus(requestStatus);
					progressCompletionRequestDTO.setFilterDTO(lookaheadFilter);
					transformTaskDetails(progressCompletionResList, taskDTOList, projectObj.get().getFloatDays(), 
							progressCompletionRequestDTO,from);
				}
				return progressCompletionResList;
			});
			try {
				resultObj.get();
			} catch (Exception e) {
				return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "Interupted Exception", Collections.emptyList());
			}
		}
		if(!progressCompletionResList.isEmpty()) {
			pagination.setCurrentPageNumber(pageRequest.getPageNumber());
			pagination.setPaginationSize(pageRequest.getPageSize());
			pagination.setTotalRecords(progressCompletionResList.size());
			return new MSAResponse(true, "Progress Complete percent", progressCompletionResList.stream().
					skip(from).limit(pageRequest.getPageSize()).collect(Collectors.toList()), pagination, HttpStatus.OK);
		}
		return new MSAResponse(false, HttpStatus.OK, "Tasks & SubTask Not Found", Collections.emptyList());
	}


	@Async
	private CompletableFuture<MSAResponse> getLookaheadResult(@Valid ProgressCompletionRequestDTO progressCompletionRequestDTO) {
		LookaheadListDTO lookaheadListDTO = new LookaheadListDTO();
		LookaheadFilterDTO lookaheadFilter = progressCompletionRequestDTO.getFilterDTO();
		lookaheadListDTO.setProjectId(progressCompletionRequestDTO.getProjectId());
		lookaheadListDTO.setLookaheadDate(progressCompletionRequestDTO.getFromDate());

		// for get all records in the particular time line
		lookaheadListDTO.setPageNo(1);
		lookaheadListDTO.setPageSize(100000);

		lookaheadFilter.setScheduleAvailability(Constants.SCHEDULED);
		lookaheadFilter.setTaskStatus(new ArrayList<>());
		lookaheadListDTO.setFilter(lookaheadFilter);
		lookaheadListDTO.setSearch(progressCompletionRequestDTO.isSearch());
		lookaheadListDTO.setSearchTerm(progressCompletionRequestDTO.getSearchTerm());
		LocalDate fromDate = DateUtil.stringToLocalDateViaInstant(progressCompletionRequestDTO.getFromDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		LocalDate toDate = DateUtil.stringToLocalDateViaInstant(progressCompletionRequestDTO.getToDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		Long count = ChronoUnit.DAYS.between(fromDate, toDate);
		//MSAResponse msaResponse = lookaheadAccessService.getLookahead(lookaheadListDTO, count);
		boolean isLookahead = false;
		return CompletableFuture.completedFuture(lookaheadAccessService.getLookahead(lookaheadListDTO, count,isLookahead));
	}

	public List<ProgressCompletionResponseDTO> transformTaskDetails(List<ProgressCompletionResponseDTO> progressCompletionResList,
			List<TaskDTO> taskDTOList, int floatDays, @Valid ProgressCompletionRequestDTO progressCompletionRequestDTO, int recordFrom) {
		Long start = System.currentTimeMillis();
		int currenttaskNo = 0; 
		int recordEnd = recordFrom + progressCompletionRequestDTO.getPageSize();
		List<String> taskStatus = progressCompletionRequestDTO.getFilterDTO().getTaskStatus();
		if(taskStatus == null )
			taskStatus = new ArrayList<>();
		List<ProgressCompletionResponseDTO> progressCompletionList =  new ArrayList<>();
		for (TaskDTO taskObj : taskDTOList) {
			progressCompletionList.clear();
			Boolean request = false;
			if(currenttaskNo >= recordFrom && currenttaskNo <= recordEnd)
				request = true;
			ProgressCompletionResponseDTO progressCompletionResponseDTO=new ProgressCompletionResponseDTO();
			progressCompletionResponseDTO.setSubTaskDetails(Collections.emptyList());
			Long completedDays = 0l;
			LocalDate today = LocalDate.now();
			progressCompletionList.add(progressCompletionResponseDTO);
			if(!taskObj.isChild()) {
				Set<Date> totalDaysList = new HashSet<>();
				if(request)
					totalDaysList= scheduleRepository.getCountOfSchedule(taskObj.getSubTasks().get(0).getId());
				Long totalNoOfDay = Long.valueOf(totalDaysList.size());
				if(request || (null != taskStatus && !taskStatus.isEmpty()))
					convertSubTaskOBjTOTaskObjPercentComplete(taskObj);
				if(true) {
					List<Object[]> limitDateList = new ArrayList<>();
					if(request || (null != taskStatus && !taskStatus.isEmpty())) {
						limitDateList = scheduleRepository.getStartDateEndDate(taskObj.getSubTasks().get(0).getId());
						if(taskObj.getDeadLineDate() != null && taskObj.getDeadLineDateInDate() != null )
							progressCompletionResponseDTO.setDeadlineDate(DateUtil.convertDate(taskObj.getDeadLineDateInDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						if(taskObj.getRequiredCompletionDate() != null && taskObj.getRequiredCompletionDateInDate() != null)
							progressCompletionResponseDTO.setTargetCompletionDate(DateUtil.convertDate(taskObj.getRequiredCompletionDateInDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					SubTaskDTO subTaskDTO = taskObj.getSubTasks().get(0);
					if(!limitDateList.isEmpty()) {
						try {
							progressCompletionResponseDTO.setStartDate(DateUtil.convertDate(new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString()).parse(limitDateList.get(0)[0]+""), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							progressCompletionResponseDTO.setFinishDate(DateUtil.convertDate(new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString()).parse(limitDateList.get(0)[1]+""), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						} catch (ParseException e) {
							throw new CustomRuntimeException(e.getMessage());
						}
						if(request) {
							if(progressCompletionResponseDTO.getStartDate() != null ) {
								Set<Date> completedDaysList = scheduleRepository.getCountOfCompletedSchedule(taskObj.getSubTasks().get(0).getId(),
										DateUtil.stringToDateViaInstant(progressCompletionResponseDTO.getStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()),
										DateUtil.convertToDateViaInstant(LocalDate.now().minusDays(1)));
								completedDays = Long.valueOf(completedDaysList.size());
							}
							if(progressCompletionResponseDTO.getStartDate() !=  null &&
									today.isBefore(DateUtil.getLocalDate(DateUtil.stringToDateViaInstant(progressCompletionResponseDTO.getStartDate(),
											DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()))))
							{
								progressCompletionResponseDTO.setNumberOfCompletedDays(0l);
								progressCompletionResponseDTO.setPercentageCompletion(new BigDecimal(0));
							}
							else {
								if(completedDays < 0)
									completedDays=totalNoOfDay;
								progressCompletionResponseDTO.setNumberOfCompletedDays(completedDays);
								if(totalNoOfDay != 0)
									progressCompletionResponseDTO.setPercentageCompletion(TaskCompletionUtil.getTaskCompletedPercentage(totalNoOfDay, completedDays));
							}
							progressCompletionResponseDTO.setTotalNoOfWorkingDays(totalNoOfDay);
							progressCompletionResponseDTO.setTaskDescription(taskObj.getDescription());
							progressCompletionResponseDTO.setSubTaskDetails(Collections.emptyList());
							progressCompletionResponseDTO.setBallInCourt(taskObj.getBallInCourt());
						}
					}
					if(request || !taskStatus.isEmpty())
						progressCompletionResponseDTO = setTaskStatus(subTaskDTO,progressCompletionResponseDTO,floatDays);
					if(progressCompletionRequestDTO.isFilter() && !taskStatus.isEmpty()){
						progressCompletionList.clear();
						if(taskStatus.contains(progressCompletionResponseDTO.getTrackingStatus()))
							progressCompletionList.add(progressCompletionResponseDTO);
					}
					else
						progressCompletionList.add(progressCompletionResponseDTO);
					if(!progressCompletionList.isEmpty())
						progressCompletionResList.add(progressCompletionList.get(0));
				}
			}
			else 
			{
				List<SubTaskDetailsDTO> subTaskDetailsList = new ArrayList<>();
				List<SubTaskDTO> subTaskList = taskObj.getSubTasks();
				for(SubTaskDTO  subTaskObj : subTaskList) {
					Long subCompletedDay = 0l;
					if(true) {
						SubTaskDetailsDTO subTaskDetailsDTO= new SubTaskDetailsDTO();
						Set<Date> subTotalNoOfDayList = new HashSet<>();
						List<Object[]> sublimitDateList = new ArrayList<>();
						if(request)
							subTotalNoOfDayList= scheduleRepository.getCountOfSchedule(subTaskObj.getId());
						Long subTotalNoOfDay = Long.valueOf(subTotalNoOfDayList.size());
						if(request || (null != taskStatus && !taskStatus.isEmpty())) {
							sublimitDateList = scheduleRepository.getStartDateEndDate(subTaskObj.getId());
							if(subTaskObj.getDeadLineDate() != null)
								subTaskDetailsDTO.setDeadlineDate(DateUtil.convertDate(subTaskObj.getDeadLineDateInDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							if(subTaskObj.getRequiredCompletionDate() != null)
								subTaskDetailsDTO.setTargetCompletionDate(DateUtil.convertDate(subTaskObj.getRequiredCompletionDateInDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						}
						if(!sublimitDateList.isEmpty()) {
							try {
								subTaskDetailsDTO.setStartDate(DateUtil.convertDate(new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString()).parse(sublimitDateList.get(0)[0]+""), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
								subTaskDetailsDTO.setFinishDate(DateUtil.convertDate(new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString()).parse(sublimitDateList.get(0)[1]+""), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							} catch (ParseException e) {
								throw new CustomRuntimeException(e.getMessage()); 
							}
							if(request) {
								if(subTaskDetailsDTO.getStartDate() != null) {
									Set<Date> subCompletedDayList = scheduleRepository.getCountOfCompletedSchedule(subTaskObj.getId(), 
											DateUtil.stringToDateViaInstant(subTaskDetailsDTO.getStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()),
											DateUtil.convertToDateViaInstant(LocalDate.now().minusDays(1)));
									subCompletedDay = Long.valueOf(subCompletedDayList.size());
								}
								if(subTaskDetailsDTO.getStartDate() != null && today.isBefore(DateUtil.getLocalDate(DateUtil.stringToDateViaInstant(subTaskDetailsDTO.getStartDate(),
										DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())))){
									subTaskDetailsDTO.setNumberOfCompletedDays(0l); 
									subTaskDetailsDTO.setPercentageCompletion(new BigDecimal(0));
								}
								else {
									if(subCompletedDay < 0)
										subCompletedDay=subTotalNoOfDay;
									subTaskDetailsDTO.setNumberOfCompletedDays(subCompletedDay); 
									if(subTotalNoOfDay != 0)
										subTaskDetailsDTO.setPercentageCompletion(TaskCompletionUtil.getTaskCompletedPercentage(subTotalNoOfDay, subCompletedDay)); 
								}
								subTaskDetailsDTO.setSubTaskDescription(subTaskObj.getDescription());
								subTaskDetailsDTO.setTotalNoOfWorkingDays(subTotalNoOfDay);
								subTaskDetailsDTO.setBallInCourt(subTaskObj.getBallInCourt());
							}
						}
						if(request || !taskStatus.isEmpty())
							subTaskDetailsDTO = setTaskStatus(subTaskObj,subTaskDetailsDTO,floatDays);
						if(progressCompletionRequestDTO.isFilter() && !taskStatus.isEmpty()){
							if(taskStatus.contains(subTaskDetailsDTO.getTrackingStatus()))
								subTaskDetailsList.add(subTaskDetailsDTO);
						}
						else
							subTaskDetailsList.add(subTaskDetailsDTO);
					}
				}
				progressCompletionList.get(0).setSubTaskDetails(subTaskDetailsList);
				progressCompletionList.add(progressCompletionList.get(0));
				getEmptyProgressCompletion(progressCompletionList.get(0), taskObj, 0);
				if(!progressCompletionList.isEmpty() && !progressCompletionList.get(0).getSubTaskDetails().isEmpty())
					progressCompletionResList.add(progressCompletionList.get(0));
				progressCompletionList.clear();
			}
			currenttaskNo = currenttaskNo + 1;
		}
		int  taskSnoStart= 1;
		for (ProgressCompletionResponseDTO taskResult : progressCompletionResList) {
			taskResult.setTaskId(taskSnoStart);
			List<SubTaskDetailsDTO> subTaskResultDTO = taskResult.getSubTaskDetails();
			int subtaskSnoStart = 1;
			for (SubTaskDetailsDTO subTaskResult : subTaskResultDTO) {
				subTaskResult.setSubTaskId(String.valueOf(taskSnoStart+"."+subtaskSnoStart));
				subtaskSnoStart = subtaskSnoStart + 1 ;
			}
			taskSnoStart = taskSnoStart +1 ;
		}
		Long end = System.currentTimeMillis();
		log.info("Consumed Time : "+(end-start));
		return progressCompletionResList;
	}

	private SubTaskDetailsDTO setTaskStatus(SubTaskDTO subTaskDTO, SubTaskDetailsDTO subTaskDetailsDTO, int floatDays) {
		LocalDate startDate = DateUtil.stringToLocalDateViaInstant(subTaskDetailsDTO.getStartDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		LocalDate finishDate = DateUtil.stringToLocalDateViaInstant(subTaskDetailsDTO.getFinishDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		Date reqCompletionDt = LookaheadAccessServiceImpl.
				getRequiredCompletionDate(subTaskDTO.getRequiredCompletionDateInDate(), subTaskDTO.getDeadLineDateInDate());
		List<ScheduleDTO> scheduleDTOList = subTaskDTO.getSchedules();
		ScheduleDTO scheduleDTOStartDate = new ScheduleDTO();
		scheduleDTOStartDate.setWorkingDateInDate(Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		scheduleDTOList.add(scheduleDTOStartDate);
		ScheduleDTO scheduleDTOFinsishDate = new ScheduleDTO();
		scheduleDTOFinsishDate.setWorkingDateInDate(Date.from(finishDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		scheduleDTOList.add(scheduleDTOFinsishDate);
		scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(reqCompletionDt, scheduleDTOList, subTaskDTO, floatDays);
		subTaskDTO.setSchedules(scheduleDTOList);
		subTaskDetailsDTO.setTrackingStatus(getTrackingStatusValue(subTaskDTO));
		return subTaskDetailsDTO;
	}

	private ProgressCompletionResponseDTO setTaskStatus(SubTaskDTO subTaskDTO, ProgressCompletionResponseDTO progressCompletionResponseDTO, int floatDays) {
		LocalDate startDate = DateUtil.stringToLocalDateViaInstant(progressCompletionResponseDTO.getStartDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		LocalDate finishDate = DateUtil.stringToLocalDateViaInstant(progressCompletionResponseDTO.getFinishDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		Date reqCompletionDt = LookaheadAccessServiceImpl.
				getRequiredCompletionDate(subTaskDTO.getRequiredCompletionDateInDate(), subTaskDTO.getDeadLineDateInDate());
		List<ScheduleDTO> scheduleDTOList = subTaskDTO.getSchedules();
		ScheduleDTO scheduleDTOStartDate = new ScheduleDTO();
		scheduleDTOStartDate.setWorkingDateInDate(Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		scheduleDTOList.add(scheduleDTOStartDate);
		ScheduleDTO scheduleDTOFinsishDate = new ScheduleDTO();
		scheduleDTOFinsishDate.setWorkingDateInDate(Date.from(finishDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
		scheduleDTOList.add(scheduleDTOFinsishDate);
		scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(reqCompletionDt, scheduleDTOList, subTaskDTO, floatDays);
		subTaskDTO.setSchedules(scheduleDTOList);
		progressCompletionResponseDTO.setTrackingStatus(getTrackingStatusValue(subTaskDTO));
		return progressCompletionResponseDTO;
	}



	private TaskDTO convertSubTaskOBjTOTaskObjPercentComplete(TaskDTO taskObj) {
		if(!taskObj.getSubTasks().isEmpty()) {
			List<SubTaskDTO> subTaksObj = taskObj.getSubTasks();
			taskObj.setDeadLineDate(subTaksObj.get(0).getDeadLineDate());
			taskObj.setDeadLineDateInDate(subTaksObj.get(0).getDeadLineDateInDate());
			taskObj.setTaskFinishDate(subTaksObj.get(0).getTaskFinishDate());
			taskObj.setTaskStartDate(subTaksObj.get(0).getTaskStartDate());
			taskObj.setRequiredCompletionDate(subTaksObj.get(0).getRequiredCompletionDate());
			taskObj.setRequiredCompletionDateInDate(subTaksObj.get(0).getRequiredCompletionDateInDate());
			if(taskObj.getBallInCourt() ==null || taskObj.getBallInCourt().equals(""))
				taskObj.setBallInCourt(subTaksObj.get(0).getBallInCourt());
		}
		return taskObj;
	}

	/**
	 * Transform list to page obj.
	 *
	 * @param progressCompltResPage the progress complt res page
	 * @param pagination the pagination
	 * @param progressCompletionResList the progress completion res list
	 * @param pageRequest the page request
	 * @return the page impl
	 */
	/*private Page<ProgressCompletionResponseDTO> transformListToPageObj(Page<ProgressCompletionResponseDTO> progressCompltResPage, 
			Pagination pagination, List<ProgressCompletionResponseDTO> progressCompletionResList, Pageable pageRequest) {
		int from = Math.max(0,pageRequest.getPageNumber()*pageRequest.getPageSize());
		return new PageImpl<>(progressCompletionResList.stream().skip(from).limit(pageRequest.getPageSize()).collect(Collectors.toList()),
				pageRequest, progressCompletionResList.size());
	}*/




	/**
	 * Gets the empty progress completion.
	 *
	 * @param progressCompletionResponseDTO the progress completion response DTO
	 * @param taskObj the task obj
	 * @param taskId the task id
	 * @return the empty progress completion
	 */
	public ProgressCompletionResponseDTO getEmptyProgressCompletion(ProgressCompletionResponseDTO progressCompletionResponseDTO, TaskDTO taskObj
			,int taskId){
		progressCompletionResponseDTO.setTaskDescription(taskObj.getDescription());
		progressCompletionResponseDTO.setTaskId(taskId);
		progressCompletionResponseDTO.setStartDate("");
		progressCompletionResponseDTO.setFinishDate("");
		progressCompletionResponseDTO.setDeadlineDate("");
		progressCompletionResponseDTO.setTargetCompletionDate("");
		progressCompletionResponseDTO.setTrackingStatus("");
		progressCompletionResponseDTO.setTotalNoOfWorkingDays(0L); 
		progressCompletionResponseDTO.setNumberOfCompletedDays(0L); 
		progressCompletionResponseDTO.setPercentageCompletion(new BigDecimal(0));
		return progressCompletionResponseDTO;
	}


	/**
	 * Gets the sub task tracking status.
	 *
	 * @param subTaskDTO2 the sub task
	 * @param finalRequiredDate the final required date
	 * @param floatDays the float days
	 * @return the sub task tracking status
	 */
	public String getSubTaskTrackingStatus(SubTaskDTO subTaskDTO, Date finalRequiredDate, int floatDays){
		List<ScheduleDTO> scheduleDTOList = subTaskDTO.getSchedules();
		try {
			LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, subTaskDTO.getSchedules(), subTaskDTO, floatDays);
		} catch (Exception e) {
			log.error("mapperexceptions "+e.getMessage());
		}
		if(scheduleDTOList.isEmpty())
			return "Tracking Status not found for particular task";
		return getTrackingStatusValue(subTaskDTO);
	}

	/**
	 * Gets the tracking status value.
	 *
	 * @param subTaskDTO the sub task DTO
	 * @return the tracking status value
	 */
	private String getTrackingStatusValue(SubTaskDTO subTaskDTO) {
		if(subTaskDTO.isBehindSchedule())
			return Constants.BEHINDSCHEDULEUI;
		else if(subTaskDTO.isWarning())
			return Constants.NEEDATTENTIONUI;
		return Constants.ONTRACKUI;
	}

	/**
	 * Gets the serial increment.
	 *
	 * @param number the number
	 * @return the serial increment
	 */
	public static int getSerialIncrement(int number){
		return number + 1;
	}

}
