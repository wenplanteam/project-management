/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.SUCSFLY;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.planner.config.ProjectSummaryConfig;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.ProjectSummaryDetailsDTO;
import com.mindzen.planner.dto.ScheduleDTO;
import com.mindzen.planner.dto.SubTaskDTO;
import com.mindzen.planner.entity.NotificationEntity;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.entity.SubTask;
import com.mindzen.planner.entity.Task;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.repository.TaskRepository;
import com.mindzen.planner.service.ProjectEmailNotificationService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;

import edu.emory.mathcs.backport.java.util.Arrays;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class Project Email Notification Service.
 * 
 * @author MRG
 */

/** The Constant log. */
@Slf4j
@Component
public class ProjectEmailNotificationServiceImpl implements ProjectEmailNotificationService {


	/** The project user repository. */
	private ProjectUserRepository projectUserRepository;

	/** The task repository. */
	private TaskRepository taskRepository;

	/** The proj summary config. */
	private ProjectSummaryConfig projSummaryConfig;

	/** The message queue service. */
	private MessageQueueService messageQueueService;
	

	/**
	 * Instantiates a new project email notication service impl.
	 *
	 * @param projectUserRepository the project user repository
	 * @param taskRepository the task repository
	 * @param messageQueueService the message queue service
	 * @param projSummaryConfig the proj summary config
	 */
	public ProjectEmailNotificationServiceImpl(ProjectUserRepository projectUserRepository,
			TaskRepository taskRepository, MessageQueueService messageQueueService, ProjectSummaryConfig projSummaryConfig) {
		super();
		this.projectUserRepository = projectUserRepository;
		this.taskRepository = taskRepository;
		this.projSummaryConfig = projSummaryConfig;
		this.messageQueueService = messageQueueService;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectEmailNotificationService#getTaskSummaryByProjectId(java.lang.Long)
	 */
	@Override
	public MSAResponse getTaskSummaryByProjectId(Long projectId) {
		//		UserInfoContext.setUserThreadLocal(1L);
		Optional<ProjectUser>  projUserOptional = projectUserRepository.findUsersByProjectId(projectId, UserInfoContext.getUserThreadLocal(), Constants.TRUE);
		if(projUserOptional.isPresent())
			return getTaskSummaryByProjectUserObject(projUserOptional.get());
		return new MSAResponse(Constants.False, HttpStatus.BAD_REQUEST, projSummaryConfig.getNotFound(), Collections.emptyList());
	}


	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectEmailNotificationService#getTaskSummaryByProjectUserId(java.lang.Long)
	 */
	@Override
	public MSAResponse getTaskSummaryByProjectUserId(Long projectUserId) {
		Optional<ProjectUser> projUserOptional = projectUserRepository.findById(projectUserId);
		if(projUserOptional.isPresent())
			return  getTaskSummaryByProjectUserObject(projUserOptional.get());
		return new MSAResponse(Constants.False, HttpStatus.BAD_REQUEST, "project user id is not found", null);
	}

	/**
	 * Gets the task summary by project user id.
	 *
	 * @param projUserObj the proj user obj
	 * @return the task summary by project id
	 */
	@SuppressWarnings("unchecked")
	@Override
	public  MSAResponse getTaskSummaryByProjectUserObject(ProjectUser projUserObj) {
		boolean isMail = Constants.False;
		boolean isApplication = Constants.False;
		List<Object[]> users= projectUserRepository.getUsersById(projUserObj.getUserId(), Constants.TRUE);
		//List<Object[]> companyObj = projectUserRepository.findCompanyNameById(projUserObj.getProjectCompany().getCompanyId());
		String zoneId = users.get(0)[1].toString();
		String emailId = users.get(0)[0].toString();
		ProjectSummaryDetailsDTO projectSummaryDetails = new ProjectSummaryDetailsDTO();
		Project projObj=projUserObj.getProject();
		List<String> summaryResultList = new ArrayList<>();
		List<String> futureSummaryResultList = new ArrayList<>();
		NotificationEntity notification = projUserObj.getNotification();
		projectSummaryDetails.setNotificationId(notification.getId());
		projectSummaryDetails.setUserId(projUserObj.getUserId());
		projectSummaryDetails.setProjectName(projObj.getName());
		projectSummaryDetails.setProjectId(projObj.getId());
		projectSummaryDetails.setEmailId(emailId);
		projectSummaryDetails.setCompanyName(projectUserRepository.findCompanyNameById(projUserObj.getProjectCompany().getCompanyId()));
	//	projectSummaryDetails.setCompanyLogo(companyObj.get(0)[1]+"");
		int floatDays = projObj.getFloatDays();
		List<String> notifyTypeList = Arrays.asList(notification.getType().split(","));
		if(!notifyTypeList.isEmpty()) {
			for (String typeObj : notifyTypeList) {
				if(typeObj.equals(projSummaryConfig.getThroughtEmail()))
					isMail = Constants.TRUE;
				if(typeObj.equals(projSummaryConfig.getFromApplication()))
					isApplication = Constants.TRUE;
			}
		}
		if(projUserObj.getProjectRole().contains("SC"))
		{
			getProjectSummaryDetailsForSC(projectSummaryDetails, zoneId, projUserObj.getProjectCompany().getId() , notification, isMail, isApplication, floatDays, summaryResultList, futureSummaryResultList);
		}
		else
		{
			getProjectSummaryDetailsForGC(projectSummaryDetails, zoneId,notification, isMail, isApplication, floatDays, summaryResultList, futureSummaryResultList);
		}
		if(projectSummaryDetails.isFutureSummaryResult() || projectSummaryDetails.isSummaryResult()) {
			if(isMail && projectSummaryDetails != null) {
				//sendProjectSummaryDetailsMail(emailId, projectSummaryDetails);
			return new MSAResponse(Constants.TRUE, HttpStatus.OK, SUCSFLY, projectSummaryDetails);
				
			}
			if(isApplication  && projectSummaryDetails != null) {
				return new MSAResponse(Constants.TRUE, HttpStatus.OK, SUCSFLY, projectSummaryDetails);
			}
			else {
				return new MSAResponse(Constants.TRUE, HttpStatus.OK, "From application configuration is  disabled. So "+projSummaryConfig.getNotFound(), Collections.emptyList());
			}

		}
		else
			return new MSAResponse(Constants.False, HttpStatus.BAD_REQUEST, projSummaryConfig.getNotFound(), Collections.emptyList());
	}

	/**
	 * Gets the project summary details for SC.
	 *
	 * @param projectSummaryDetails the project summary details
	 * @param zoneId the zone id
	 * @param projectCompanyId the project company id
	 * @param notification the notification
	 * @param isMail the is mail
	 * @param isApplication the is application
	 * @param floatDays the float days
	 * @param summaryResultList the summary result list
	 * @param futureSummaryResultList the future summary result list
	 * @return the project summary details for SC
	 */
	@SuppressWarnings("unchecked")
	private ProjectSummaryDetailsDTO getProjectSummaryDetailsForSC(ProjectSummaryDetailsDTO projectSummaryDetails, String zoneId, Long projectCompanyId,
			NotificationEntity notification, boolean isMail, boolean isApplication, int floatDays,
			List<String> summaryResultList, List<String> futureSummaryResultList) {
		if(notification.isNotification())
		{
			List<String> notifyInformationList = Arrays.asList(notification.getInformation().split(","));
			if( !notifyInformationList.isEmpty()) {

				String notifyInfoObj=null;
				if (notifyInformationList.size() > 4) {
					LinkedList<String> notifyLinkedList =new LinkedList<>(notifyInformationList);
					notifyInfoObj = notifyLinkedList.pollFirst();
				}
				if(notifyInfoObj != null){
					if(notification.getFrequency().equals(Constants.WEEKLY))
						getWeeklySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfoObj, futureSummaryResultList, projectCompanyId);
					else 
						getDailySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfoObj, futureSummaryResultList, projectCompanyId);
				}
				else {
					for (String notifyInfo : notifyInformationList) {
						if(notification.getFrequency().equals(Constants.WEEKLY))
							getWeeklySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfo, futureSummaryResultList, projectCompanyId);
						else 
							getDailySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfo, futureSummaryResultList, projectCompanyId);
					}	
				}
			}
		}
		return projectSummaryDetails;
	}

	/**
	 * Gets the project summary details for GC.
	 *
	 * @param projectSummaryDetails the project summary details
	 * @param zoneId the zone id
	 * @param notification the notification
	 * @param isMail the is mail
	 * @param isApplication the is application
	 * @param floatDays the float days
	 * @param summaryResultList the summary result list
	 * @param futureSummaryResultList the future summary result list
	 * @return the project summary details for GC
	 */
	@SuppressWarnings("unchecked")
	private ProjectSummaryDetailsDTO getProjectSummaryDetailsForGC(ProjectSummaryDetailsDTO projectSummaryDetails, String zoneId, NotificationEntity notification, boolean isMail, boolean isApplication, int floatDays, List<String> summaryResultList, List<String> futureSummaryResultList) {
		if(notification.isNotification())
		{
			List<String> notifyInformationList = Arrays.asList(notification.getInformation().split(","));
			if( !notifyInformationList.isEmpty()) {

				String notifyInfoObj=null;
				if (notifyInformationList.size() > 4) {
					LinkedList<String> notifyLinkedList =new LinkedList<>(notifyInformationList);
					notifyInfoObj = notifyLinkedList.pollFirst();
				}
				if(notifyInfoObj != null){
					if(notification.getFrequency().equals(Constants.WEEKLY))
						getWeeklySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfoObj, futureSummaryResultList, null);
					else 
						getDailySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfoObj, futureSummaryResultList, null);
				}
				else {
					for (String notifyInfo : notifyInformationList) {
						if(notification.getFrequency().equals(Constants.WEEKLY))
							getWeeklySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfo, futureSummaryResultList, null);
						else 
							getDailySummaryReport(projectSummaryDetails, zoneId, floatDays, summaryResultList, notifyInfo, futureSummaryResultList, null);
					}	
				}
			}
		}
		return projectSummaryDetails;
	}

	/**
	 * Gets the weekly summary report.
	 *
	 * @param projectSummaryDetails the project summary details
	 * @param zoneId the zone id
	 * @param floatDays the float days
	 * @param summaryResultList the summary result list
	 * @param notifyInfo the notify info obj
	 * @param futureSummaryResultList the future summary result list
	 * @param projectCompanyId the project company id
	 * @return the weekly summary report
	 */
	private ProjectSummaryDetailsDTO getWeeklySummaryReport(ProjectSummaryDetailsDTO projectSummaryDetails, String zoneId, int floatDays,
			List<String> summaryResultList, String notifyInfo, List<String> futureSummaryResultList, Long projectCompanyId) {
		List<Date> lastWeekDates = DateUtil.getLastWeek(zoneId);
		projectSummaryDetails.setLastWeekFromDate(DateUtil.getStartOfDay(lastWeekDates.get(0), Calendar.getInstance()));
		projectSummaryDetails.setLastWeekToDate(DateUtil.getStartOfDay(lastWeekDates.get(1), Calendar.getInstance()));
		List<Task> taskCreatedList = null;
		if(projectCompanyId != null) {
			// Query to fetch past 7 days tasks. 
			taskCreatedList = taskRepository.findWeeklyTaskDetailsSC(projectSummaryDetails.getProjectId(), Constants.TRUE, 
					projectSummaryDetails.getLastWeekFromDate(), projectSummaryDetails.getLastWeekToDate(), projectCompanyId);
		}
		else {
			taskCreatedList = taskRepository.findWeeklyTaskDetails(projectSummaryDetails.getProjectId(), Constants.TRUE, 
					projectSummaryDetails.getLastWeekFromDate(), projectSummaryDetails.getLastWeekToDate());
		}
		if (notifyInfo.equals(projSummaryConfig.getProjectTaskSummary())) {
			getWarningTask(futureSummaryResultList, notifyInfo, projectSummaryDetails, zoneId, floatDays, projectCompanyId);
			countOfBehindScheduleTask(summaryResultList, taskCreatedList, projectSummaryDetails.getLastWeekFromDate(), 
					projectSummaryDetails.getLastWeekToDate(), floatDays, projectSummaryDetails);
			countOfNewTasks(summaryResultList, taskCreatedList, projectSummaryDetails.getLastWeekFromDate(), projectSummaryDetails.getLastWeekToDate(), projectSummaryDetails);
			countOfModifyTasks(summaryResultList, taskCreatedList, projectSummaryDetails.getLastWeekFromDate(), projectSummaryDetails.getLastWeekToDate(), projectSummaryDetails);
			
		}
		else if(notifyInfo.equals(projSummaryConfig.getTasksPastDue())){
			// Query to fetch past 7 days tasks. 
			countOfBehindScheduleTask(summaryResultList, taskCreatedList, projectSummaryDetails.getLastWeekFromDate(), 
					projectSummaryDetails.getLastWeekToDate(), floatDays, projectSummaryDetails);
			List<Date> futureWeekDates = DateUtil.getFutureWeek(zoneId);
			projectSummaryDetails.setFutureWeekFromDate(DateUtil.getStartOfDay(futureWeekDates.get(0), Calendar.getInstance()));
			projectSummaryDetails.setFutureWeekToDate(DateUtil.getStartOfDay(futureWeekDates.get(1), Calendar.getInstance()));
			List<Task> futureTaskList = null;
			if(projectCompanyId != null)
				futureTaskList = taskRepository.findWeeklyFutureTaskDetailsSC(projectSummaryDetails.getProjectId(), Constants.TRUE, 
						projectSummaryDetails.getFutureWeekFromDate(), projectSummaryDetails.getFutureWeekToDate(), projectCompanyId);
			else
				futureTaskList = taskRepository.findWeeklyFutureTaskDetails(projectSummaryDetails.getProjectId(), Constants.TRUE, 
						projectSummaryDetails.getFutureWeekFromDate(), projectSummaryDetails.getFutureWeekToDate());
			countOfFutureBehindScheduleTask(futureSummaryResultList, futureTaskList, projectSummaryDetails.getFutureWeekFromDate(), 
					projectSummaryDetails.getFutureWeekToDate(), floatDays, projectSummaryDetails);
		}
		else if(notifyInfo.equals(projSummaryConfig.getNewlyAddedTask())){
			// Query to fetch past 7 days tasks. 
			countOfNewTasks(summaryResultList, taskCreatedList, projectSummaryDetails.getLastWeekFromDate(), projectSummaryDetails.getLastWeekToDate(), projectSummaryDetails);
		}
		else if(notifyInfo.equals(projSummaryConfig.getModifiedTask())){
			// Query to fetch past 7 days tasks. 
			countOfModifyTasks(summaryResultList, taskCreatedList, projectSummaryDetails.getLastWeekFromDate(), projectSummaryDetails.getLastWeekToDate(), projectSummaryDetails);
		}
		else if(notifyInfo.equals(projSummaryConfig.getTaskInAlert()))
			getWarningTask(futureSummaryResultList, notifyInfo, projectSummaryDetails, zoneId, floatDays, projectCompanyId);
		if(!summaryResultList.isEmpty()) {
			projectSummaryDetails.setSummaryResult(Constants.TRUE);
			projectSummaryDetails.setSummaryResultList(summaryResultList);
		}
		if(!futureSummaryResultList.isEmpty())
		{
			projectSummaryDetails.setFutureSummaryResult(Constants.TRUE);
			projectSummaryDetails.setFutureSummaryResultList(futureSummaryResultList);
		}
		return projectSummaryDetails;
	}	

	/**
	 * Count of future behind schedule task.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param futureTaskList the future task list
	 * @param futureWeekFromDate the future week from date
	 * @param futureWeekToDate the future week to date
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfFutureBehindScheduleTask(List<String> futureSummaryResultList, List<Task> futureTaskList,
			Date futureWeekFromDate, Date futureWeekToDate, int floatDays, ProjectSummaryDetailsDTO projectSummaryDetails) {
		long behindScheduleCount = 0;
		long unScheduleCount = 0;
		long onTimeCount=0;
		ModelMapper mapper=new ModelMapper();
		List<String> behindScheduleDescriptionList=new ArrayList<>();
		List<String> unScheduleDescList=new ArrayList<>();
		List<String> completedDescList = new ArrayList<>();
		for (Task task : futureTaskList) {
			List<SubTask> subTaskList=task.getSubTasks();
			for (SubTask subTask : subTaskList) {
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.compareTo(DateUtil.getStartOfDay(futureWeekFromDate, Calendar.getInstance())) >= 0 && finalRequiredDate.compareTo(DateUtil.getStartOfDay(futureWeekToDate, Calendar.getInstance())) <= 0)
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {
						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});

						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty()){	
							if(subTaskDTO.isBehindSchedule())
							{
								behindScheduleCount = behindScheduleCount+1;
								behindScheduleDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setUpComingBehindSchedule(Constants.TRUE);
							}else {
								onTimeCount = onTimeCount+1;
								completedDescList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setFutureCompletedTask(Constants.TRUE);
							}
						}
						else
						{
							unScheduleCount = unScheduleCount+1;
							unScheduleDescList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUpComingUnSchedule(Constants.TRUE);
						}
					} catch (Exception e) {
						throw new CustomRuntimeException(""+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setUpComingUnScheduleResultList(unScheduleDescList);
		projectSummaryDetails.setUpComingBehindScheduleList(behindScheduleDescriptionList);
		projectSummaryDetails.setUpComingCompletedList(completedDescList);
		if(behindScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, behindScheduleCount, Constants.BEHINDSCHEDULE);			
		if(unScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, unScheduleCount, Constants.UNSCHEDULED);
		if(onTimeCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, onTimeCount, Constants.ONTIME);
		return futureSummaryResultList;
	}


	/**
	 * Gets the warning task.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param notifyInfoObj the notify info obj
	 * @param projectSummaryDetails the project summary details
	 * @param zoneId the zone id
	 * @param floatDays the float days
	 * @param projectCompanyId the project company id
	 * @return the warning task
	 */
	private List<String> getWarningTask(List<String> futureSummaryResultList, String notifyInfoObj, ProjectSummaryDetailsDTO projectSummaryDetails, String zoneId, int floatDays, Long projectCompanyId) {
		List<Date> futureWeekDates = DateUtil.getFutureWeek(zoneId);
		projectSummaryDetails.setFutureWeekFromDate(DateUtil.getStartOfDay(futureWeekDates.get(0), Calendar.getInstance()));
		projectSummaryDetails.setFutureWeekToDate(DateUtil.getStartOfDay(futureWeekDates.get(1), Calendar.getInstance()));
		List<Task> futureTaskList = null;
		if(projectCompanyId != null)
			futureTaskList = taskRepository.findWeeklyFutureTaskDetailsSC(projectSummaryDetails.getProjectId(), Constants.TRUE, 
					projectSummaryDetails.getFutureWeekFromDate(), projectSummaryDetails.getFutureWeekToDate(), projectCompanyId);
		else
			futureTaskList = taskRepository.findWeeklyFutureTaskDetails(projectSummaryDetails.getProjectId(), Constants.TRUE, 
					projectSummaryDetails.getFutureWeekFromDate(), projectSummaryDetails.getFutureWeekToDate());
		if(notifyInfoObj.equals(projSummaryConfig.getTaskInAlert()))
			countOfFutureTaskInAlert(futureSummaryResultList, futureTaskList, projectSummaryDetails.getFutureWeekFromDate(), projectSummaryDetails.getFutureWeekToDate(), floatDays, projectSummaryDetails);
		else	
			countOfWarningFutureTask(futureSummaryResultList, futureTaskList, projectSummaryDetails.getFutureWeekFromDate(), projectSummaryDetails.getFutureWeekToDate(), floatDays, projectSummaryDetails);
		return futureSummaryResultList;
	}


	/**
	 * Count of future task in alert.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param futureTaskList the future task list
	 * @param futureWeekFromDate the future week from date
	 * @param futureWeekToDate the future week to date
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfFutureTaskInAlert(List<String> futureSummaryResultList, List<Task> futureTaskList,
			Date futureWeekFromDate, Date futureWeekToDate, int floatDays,
			ProjectSummaryDetailsDTO projectSummaryDetails) {
		long warningCount=0;
		ModelMapper mapper=new ModelMapper();
		List<String> warningDescriptionList=new ArrayList<>();
		for (Task task : futureTaskList) {
			List<SubTask> subTaskList=task.getSubTasks();
			for (SubTask subTask : subTaskList) {
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.compareTo(DateUtil.getStartOfDay(futureWeekFromDate, Calendar.getInstance())) >= 0 && finalRequiredDate.compareTo(DateUtil.getStartOfDay(futureWeekToDate, Calendar.getInstance())) <= 0)
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {
						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});

						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty() && subTaskDTO.isWarning()){
							warningCount = warningCount + 1;
							warningDescriptionList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUpComingWarning(Constants.TRUE);
						}
					} catch (Exception e) {
						throw new CustomRuntimeException(""+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setUpComingWarningList(warningDescriptionList);
		if(warningCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, warningCount, Constants.WARNING);
		return futureSummaryResultList;
	}


	/**
	 * Count OF warning future task.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param futureTaskList the future task list
	 * @param futureWeekFromDate the future week from date
	 * @param futureWeekToDate the future week to date
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String>  countOfWarningFutureTask(List<String> futureSummaryResultList, List<Task> futureTaskList, Date futureWeekFromDate,
			Date futureWeekToDate,  int floatDays,ProjectSummaryDetailsDTO projectSummaryDetails) {
		long warningCount=0;
		long behindScheduleCount = 0;
		long unScheduleCount = 0;
		long onTimeCount=0;
		ModelMapper mapper=new ModelMapper();
		List<String> warningDescriptionList=new ArrayList<>();
		List<String> behindScheduleDescriptionList=new ArrayList<>();
		List<String> unScheduleDescList=new ArrayList<>();
		List<String> completedDescList = new ArrayList<>();
		for (Task task : futureTaskList) {
			List<SubTask> subTaskList=task.getSubTasks();
			for (SubTask subTask : subTaskList) {
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.compareTo(DateUtil.getStartOfDay(futureWeekFromDate, Calendar.getInstance())) >= 0 && finalRequiredDate.compareTo(DateUtil.getStartOfDay(futureWeekToDate, Calendar.getInstance())) <= 0)
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {
						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});

						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty()){	
							if(subTaskDTO.isBehindSchedule())
							{
								behindScheduleCount = behindScheduleCount+1;
								behindScheduleDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setUpComingBehindSchedule(Constants.TRUE);
							}else if(subTaskDTO.isWarning()){
								warningCount = warningCount + 1;
								warningDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setUpComingWarning(Constants.TRUE);
							}else {
								onTimeCount = onTimeCount+1;
								completedDescList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setFutureCompletedTask(Constants.TRUE);
							}
						}
						else
						{
							unScheduleCount = unScheduleCount+1;
							unScheduleDescList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUpComingUnSchedule(Constants.TRUE);
						}
					} catch (Exception e) {
						throw new CustomRuntimeException(""+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setUpComingUnScheduleResultList(unScheduleDescList);
		projectSummaryDetails.setUpComingWarningList(warningDescriptionList);
		projectSummaryDetails.setUpComingBehindScheduleList(behindScheduleDescriptionList);
		projectSummaryDetails.setUpComingCompletedList(completedDescList);
		if(behindScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, behindScheduleCount, Constants.BEHINDSCHEDULE);			
		if(warningCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, warningCount, Constants.WARNING);
		if(unScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, unScheduleCount, Constants.UNSCHEDULED);
		if(onTimeCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, onTimeCount, Constants.ONTIME);
		return futureSummaryResultList;
	}


	/**
	 * Gets the daily summary report.
	 *
	 * @param projectSummaryDetails the project summary details
	 * @param zoneId the zone id
	 * @param floatDays the float days
	 * @param summaryResultList the summary result list
	 * @param notifyInfoObj the notify info obj
	 * @param futureSummaryResultList the future summary result list
	 * @param projectCompanyId the project company id
	 * @return the daily summary report
	 */
	private ProjectSummaryDetailsDTO getDailySummaryReport(ProjectSummaryDetailsDTO projectSummaryDetails,  String zoneId,
			int floatDays, List<String> summaryResultList, String notifyInfoObj, List<String> futureSummaryResultList, Long projectCompanyId) {
		projectSummaryDetails.setLastDay(DateUtil.getStartOfDay(Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.of(zoneId)).toInstant()), 
				Calendar.getInstance()));
		List<Task> taskCreatedList = null;
		// Query to fetch past day.
		if(projectCompanyId != null)
			taskCreatedList = taskRepository.findDailyTaskDetailsSC(projectSummaryDetails.getProjectId(),  Constants.TRUE, 
					projectSummaryDetails.getLastDay(), projectCompanyId);
		else
			taskCreatedList = taskRepository.findDailyTaskDetails(projectSummaryDetails.getProjectId(),  Constants.TRUE, 
					projectSummaryDetails.getLastDay());
		log.info("task list "+taskCreatedList);
		if (notifyInfoObj.equals(projSummaryConfig.getProjectTaskSummary())) {
			countOfBehindScheduleTaskDaily(summaryResultList, taskCreatedList, projectSummaryDetails.getLastDay(),
					floatDays,  projectSummaryDetails);
			countOfNewTasksDaily(summaryResultList, taskCreatedList, projectSummaryDetails.getLastDay(), projectSummaryDetails);
			countOfModifyTasksDaily(summaryResultList, taskCreatedList, projectSummaryDetails.getLastDay(), projectSummaryDetails);
			getWarningTaskDaily(futureSummaryResultList, projectSummaryDetails, notifyInfoObj, floatDays, zoneId, projectCompanyId);
		}
		else if(notifyInfoObj.equals(projSummaryConfig.getTasksPastDue())) 
		{
			projectSummaryDetails.setFutureDay(DateUtil.getStartOfDay(Date.from(LocalDate.now().atStartOfDay(ZoneId.of(zoneId)).toInstant()), Calendar.getInstance()));
			List<Task> futureTaskList = null;
			if(projectCompanyId != null)
				futureTaskList = taskRepository.findDailyTaskDetailsSC(projectSummaryDetails.getProjectId(),  
						Constants.TRUE, projectSummaryDetails.getFutureDay(), projectCompanyId);
			else
				futureTaskList = taskRepository.findDailyTaskDetails(projectSummaryDetails.getProjectId(),  
						Constants.TRUE, projectSummaryDetails.getFutureDay());
			countOfBehindScheduleTaskDaily(summaryResultList, taskCreatedList, projectSummaryDetails.getLastDay(),
					floatDays, projectSummaryDetails);
			countOfFutureBehindScheduleTaskDaily(summaryResultList, futureTaskList, projectSummaryDetails.getFutureDay(),
					floatDays, projectSummaryDetails);
		}
		else if(notifyInfoObj.equals(projSummaryConfig.getNewlyAddedTask())) 
			countOfNewTasksDaily(summaryResultList, taskCreatedList, projectSummaryDetails.getLastDay(), projectSummaryDetails);	
		else if(notifyInfoObj.equals(projSummaryConfig.getModifiedTask())) 
			countOfModifyTasksDaily(summaryResultList, taskCreatedList, projectSummaryDetails.getLastDay(), projectSummaryDetails);
		else if(notifyInfoObj.equals(projSummaryConfig.getTaskInAlert()))
			getWarningTaskDaily(futureSummaryResultList, projectSummaryDetails, notifyInfoObj, floatDays, zoneId, projectCompanyId);
		if(!summaryResultList.isEmpty()) {
			projectSummaryDetails.setSummaryResult(Constants.TRUE);
			projectSummaryDetails.setSummaryResultList(summaryResultList);
		}
		if(!futureSummaryResultList.isEmpty())
		{
			projectSummaryDetails.setFutureSummaryResult(Constants.TRUE);
			projectSummaryDetails.setFutureSummaryResultList(futureSummaryResultList);
		}
		return projectSummaryDetails;
	}


	/**
	 * Count of future behind schedule task daily.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param futureTaskList the future task list
	 * @param futureDay the future day
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfFutureBehindScheduleTaskDaily(List<String> futureSummaryResultList, List<Task> futureTaskList,
			Date futureDay, int floatDays, ProjectSummaryDetailsDTO projectSummaryDetails) {
		long behindScheduleCount = 0;
		long unScheduleCount = 0;
		long onTimeCount=0;
		ModelMapper mapper=new ModelMapper();
		List<String> behindScheduleDescriptionList=new ArrayList<>();
		List<String> unScheduleDescList=new ArrayList<>();
		List<String> completedDescList = new ArrayList<>();
		for (Task task : futureTaskList) {
			List<SubTask> subTaskList=task.getSubTasks();
			for (SubTask subTask : subTaskList) {
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.equals(DateUtil.getStartOfDay(futureDay, Calendar.getInstance())))
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {
						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});

						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty()){	
							if(subTaskDTO.isBehindSchedule())
							{
								behindScheduleCount = behindScheduleCount+1;
								behindScheduleDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setUpComingBehindSchedule(Constants.TRUE);
							}else {
								onTimeCount = onTimeCount+1;
								completedDescList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setFutureCompletedTask(Constants.TRUE);
							}
						}
						else
						{
							unScheduleCount = unScheduleCount+1;
							unScheduleDescList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUpComingUnSchedule(Constants.TRUE);
						}
					} catch (Exception e) {
						throw new CustomRuntimeException(""+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setUpComingUnScheduleResultList(unScheduleDescList);
		projectSummaryDetails.setUpComingBehindScheduleList(behindScheduleDescriptionList);
		projectSummaryDetails.setUpComingCompletedList(completedDescList);
		if(behindScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, behindScheduleCount, Constants.BEHINDSCHEDULE);			
		if(unScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, unScheduleCount, Constants.UNSCHEDULED);
		if(onTimeCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, onTimeCount, Constants.ONTIME);
		return futureSummaryResultList;
	}


	/**
	 * Gets the warning task daily.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param projectSummaryDetails the project summary details
	 * @param notifyInfoObj the notify info obj
	 * @param floatDays the float days
	 * @param zoneId the zone id
	 * @param projectCompanyId the project company id
	 * @return the warning task daily
	 */
	private List<String>  getWarningTaskDaily(List<String> futureSummaryResultList,
			ProjectSummaryDetailsDTO projectSummaryDetails, String notifyInfoObj, int floatDays, String zoneId, Long projectCompanyId) {
		projectSummaryDetails.setFutureDay(DateUtil.getStartOfDay(Date.from(LocalDate.now().atStartOfDay(ZoneId.of(zoneId)).toInstant()), Calendar.getInstance()));
		List<Task> futureTaskList=null;
		if(projectCompanyId != null)
			futureTaskList= taskRepository.findDailyTaskDetailsSC(projectSummaryDetails.getProjectId(),  
					Constants.TRUE, projectSummaryDetails.getFutureDay(), projectCompanyId);
		else
			futureTaskList= taskRepository.findDailyTaskDetails(projectSummaryDetails.getProjectId(),  
					Constants.TRUE, projectSummaryDetails.getFutureDay());
		if(notifyInfoObj.equals(projSummaryConfig.getTaskInAlert()))
			countOfFutureTaskInAlertDaily(futureSummaryResultList, futureTaskList, projectSummaryDetails.getFutureWeekFromDate(), floatDays, projectSummaryDetails);
		else
			countOfWarningFutureTaskDaily(futureSummaryResultList, futureTaskList, projectSummaryDetails.getFutureDay(), 
					floatDays, projectSummaryDetails);
		return futureSummaryResultList;
	}


	/**
	 * Count OF warning future task daily.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param futureTaskList the future task list
	 * @param futureDay the future day
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfFutureTaskInAlertDaily(List<String> futureSummaryResultList, List<Task> futureTaskList,
			Date futureDay, int floatDays, ProjectSummaryDetailsDTO projectSummaryDetails) {
		long warningCount=0;
		ModelMapper mapper=new ModelMapper();
		List<String> warningDescriptionList=new ArrayList<>();
		for (Task task : futureTaskList) {
			List<SubTask> subTaskList=task.getSubTasks();
			for (SubTask subTask : subTaskList) {
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.equals(DateUtil.getStartOfDay(futureDay, Calendar.getInstance())))
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {
						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});

						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty() && subTaskDTO.isWarning()){
							warningCount = warningCount + 1;
							warningDescriptionList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUpComingWarning(Constants.TRUE);
						}
					} catch (Exception e) {
						throw new CustomRuntimeException(""+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setUpComingWarningList(warningDescriptionList);
		if(warningCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, warningCount, Constants.WARNING);
		return futureSummaryResultList;
	}

	/**
	 * Count of warning future task daily.
	 *
	 * @param futureSummaryResultList the future summary result list
	 * @param futureTaskList the future task list
	 * @param futureDay the future day
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfWarningFutureTaskDaily(List<String> futureSummaryResultList, List<Task> futureTaskList,
			Date futureDay, int floatDays, ProjectSummaryDetailsDTO projectSummaryDetails) {
		long warningCount=0;
		long behindScheduleCount = 0;
		long unScheduleCount = 0;
		long onTimeCount=0;
		ModelMapper mapper=new ModelMapper();
		List<String> warningDescriptionList=new ArrayList<>();
		List<String> behindScheduleDescriptionList=new ArrayList<>();
		List<String> unScheduleDescList=new ArrayList<>();
		List<String> completedDescList = new ArrayList<>();
		for (Task task : futureTaskList) {
			List<SubTask> subTaskList=task.getSubTasks();
			for (SubTask subTask : subTaskList) {
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.equals(DateUtil.getStartOfDay(futureDay, Calendar.getInstance())))
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {
						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});

						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty()){	
							if(subTaskDTO.isBehindSchedule())
							{
								behindScheduleCount = behindScheduleCount+1;
								behindScheduleDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setUpComingBehindSchedule(Constants.TRUE);
							}else if(subTaskDTO.isWarning()){
								warningCount = warningCount + 1;
								warningDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setUpComingWarning(Constants.TRUE);
							}else {
								onTimeCount = onTimeCount+1;
								completedDescList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setFutureCompletedTask(Constants.TRUE);
							}
						}
						else
						{
							unScheduleCount = unScheduleCount+1;
							unScheduleDescList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUpComingUnSchedule(Constants.TRUE);
						}
					} catch (Exception e) {
						throw new CustomRuntimeException(""+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setUpComingUnScheduleResultList(unScheduleDescList);
		projectSummaryDetails.setUpComingWarningList(warningDescriptionList);
		projectSummaryDetails.setUpComingBehindScheduleList(behindScheduleDescriptionList);
		projectSummaryDetails.setUpComingCompletedList(completedDescList);
		if(behindScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, behindScheduleCount, Constants.BEHINDSCHEDULE);			
		if(warningCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, warningCount, Constants.WARNING);
		if(unScheduleCount != 0)
			futureSummaryResultList  = getFutureResult(futureSummaryResultList, unScheduleCount, Constants.UNSCHEDULED);
		if(onTimeCount != 0)
			futureSummaryResultList = getFutureResult(futureSummaryResultList, onTimeCount, Constants.ONTIME);
		return futureSummaryResultList;
	}


	/**
	 * Count of behind schedule task.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskCreatedList the task created list
	 * @param lastDay the last day
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfBehindScheduleTaskDaily(List<String> summaryResultList, List<Task> taskCreatedList, Date lastDay,
			int floatDays, ProjectSummaryDetailsDTO projectSummaryDetails) {
		long behindScheduleCount = 0;
		long unScheduleCount = 0;
		long onTimeCount=0;
		ModelMapper mapper=new ModelMapper();
		List<String> behindScheduleDescriptionList=new ArrayList<>();
		List<String> unScheduleDescriptionList=new ArrayList<>();
		List<String> completedDescList = new ArrayList<>();
		for (Task task : taskCreatedList) {
			for (SubTask subTask : task.getSubTasks()) {
				lastDay=DateUtil.getStartOfDay(lastDay, Calendar.getInstance());
				log.info("last Day: "+lastDay);
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.equals(lastDay))
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {

						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});
						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty()) {
							if(subTaskDTO.isBehindSchedule())
							{
								behindScheduleCount = behindScheduleCount + 1;
								behindScheduleDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setBehindSchedule(Constants.TRUE);
							}
							else {
								onTimeCount = onTimeCount+1;
								completedDescList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setReqCompletedTask(Constants.TRUE);
							}
						}
						else
						{
							unScheduleCount = unScheduleCount+1;
							unScheduleDescriptionList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUnScheduleTask(Constants.TRUE);
						}

						// commented no need on warning for this realse said by deepa mam on 24-07-19
						/*if(subTaskDTO.isWarning())
					{
						warningCount = 	warningCount + 1;
						warningDescriptionList.add(subTaskDTO.getDescription());
						projectSummaryDetails.setWarning(Constants.TRUE);
					}
						 * else
					{
						onTrackCount = onTrackCount + 1;
						ontrackDescriptionList.add(subTaskDTO.getDescription());
						projectSummaryDetails.setOnTrack(Constants.TRUE);

					}*/
					} catch (Exception e) {
						log.error("mapperexceptions "+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setBehindScheduleList(behindScheduleDescriptionList);
		projectSummaryDetails.setReqCompletedTaskList(completedDescList);
		projectSummaryDetails.setUnScheduleResultList(unScheduleDescriptionList);
		/*projectSummaryDetails.setWarningList(warningDescriptionList);
		projectSummaryDetails.setOnTrackList(ontrackDescriptionList);*/
		if(behindScheduleCount != 0)
			summaryResultList  = getResult(summaryResultList, behindScheduleCount, Constants.BEHINDSCHEDULE);
		if(unScheduleCount != 0)
			summaryResultList  = getResult(summaryResultList, unScheduleCount, Constants.UNSCHEDULED);
		if(onTimeCount != 0)
			summaryResultList = getTaskResult(summaryResultList, onTimeCount, Constants.False, Constants.TRUE);
		/*if(onTrackCount != 0)
			summaryResultList  = getResult(summaryResultList, onTrackCount, "onTrack");
		if(warningCount != 0)
			summaryResultList  = getResult(summaryResultList, warningCount, Constants.WARNING);*/
		return summaryResultList;
	}

	/**
	 * Count of required completion tasks.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskCreatedList the task created list
	 * @param lastDay the last day
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	/*private List<String> countOfRequiredCompletionTasksDaily(List<String> summaryResultList, List<Task> taskCreatedList,
			Date lastDay, ProjectSummaryDetailsDTO projectSummaryDetails) {
		long taskCount = 0;
		//		long subTaskCount = 0;
		Date finalRequiredDate = null;
		List<String> completedDescList = new ArrayList<>();
		for (Task taskObj : taskCreatedList) {
			if(!taskObj.getSubTasks().isEmpty() && taskObj.isChild())
			{
				finalRequiredDate = LookaheadAccessServiceImpl.getRequiredCompletionDate(taskObj.getRequiredCompletionDate(), 
						taskObj.getDeadLineDate());
				List<SubTask> subTaskList=taskObj.getSubTasks().stream().filter(subTask -> subTask.isActive() &&
						DateUtil.parseDateToLocalDateTime(subTask.getCreatedOn()).equals(lastDay) && 
						subTask.getExpectedCompletionDate().equals(lastDay)).collect(Collectors.toList());
				//				subTaskCount = subTaskCount + subTaskList.size();
				for (SubTask subTaskObj : subTaskList) {
					finalRequiredDate = LookaheadAccessServiceImpl.getRequiredCompletionDate(subTaskObj.getRequiredCompletionDate(), 
							subTaskObj.getDeadLineDate());
					if(finalRequiredDate != null && finalRequiredDate.equals(subTaskObj.getRequiredCompletionDate()))
					{
						taskCount = taskCount + 1;
						completedDescList.add(subTaskObj.getDescription());	
					}
				}
				projectSummaryDetails.setReqCompletedTask(Constants.TRUE);
			}
			else if(finalRequiredDate != null && finalRequiredDate.equals(taskObj.getRequiredCompletionDate()) 
					&& taskObj.getExpectedCompletionDate().equals(lastDay)){
				taskCount = taskCount + 1;
				completedDescList.add(taskObj.getDescription());
				projectSummaryDetails.setReqCompletedTask(Constants.TRUE);
			}
		}
		projectSummaryDetails.setReqCompletedTaskList(completedDescList);
		if(taskCount != 0)
			summaryResultList = getTaskResult(summaryResultList, taskCount, Constants.FALSE, Constants.TRUE);
		return summaryResultList;
	}*/


	/**
	 * Count of modify tasks.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskCreatedList the task created list
	 * @param lastDay the last day
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfModifyTasksDaily(List<String> summaryResultList, List<Task> taskCreatedList, Date lastDay,
			ProjectSummaryDetailsDTO projectSummaryDetails) {
		long taskCount = 0;
		//		long subTaskCount = 0;
		taskCreatedList=taskCreatedList.stream().filter(taskObj -> taskObj.getCreatedOn() != taskObj.getModifiedOn())
				.collect(Collectors.toList());
		List<String> modifyDescList= new ArrayList<>();
		for (Task taskObj : taskCreatedList) {
			if(!taskObj.getSubTasks().isEmpty() && taskObj.isChild())
			{
				List<SubTask> subTaskList= taskObj.getSubTasks().stream().filter(subTaskObj -> subTaskObj.isActive() &&
						subTaskObj.getCreatedOn() != subTaskObj.getModifiedOn() &&
						subTaskObj.getModifiedOn().toLocalDate().equals(DateUtil.getLocalDate(lastDay)) &&
						!subTaskObj.getCreatedOn().toLocalDate().equals(DateUtil.getLocalDate(lastDay))).collect(Collectors.toList());
				//				subTaskCount = subTaskCount + subTaskList.size();
				if(!subTaskList.isEmpty()) {
					for (SubTask subTaskObj : subTaskList) {
						taskCount = taskCount + 1;
						modifyDescList.add(subTaskObj.getDescription());
						projectSummaryDetails.setModifyTask(Constants.TRUE);
					}	
				}
			}
			else if(taskObj.getCreatedOn() != taskObj.getModifiedOn() 
					&& taskObj.getModifiedOn().toLocalDate().equals(DateUtil.getLocalDate(lastDay)) &&
					!taskObj.getCreatedOn().toLocalDate().equals(DateUtil.getLocalDate(lastDay))){
				taskCount = taskCount + 1;
				modifyDescList.add(taskObj.getDescription());
				projectSummaryDetails.setModifyTask(Constants.TRUE);
			}
		}
		projectSummaryDetails.setModifyTaskList(modifyDescList);
		if(taskCount != 0)
			summaryResultList = getTaskResult(summaryResultList, taskCount, Constants.False, Constants.False);
		return summaryResultList;
	}


	/**
	 * Count of new tasks.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskCreatedList the task created list
	 * @param lastDay the last day
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfNewTasksDaily(List<String> summaryResultList, List<Task> taskCreatedList, Date lastDay,
			ProjectSummaryDetailsDTO projectSummaryDetails) {
		long taskCount = 0;
		//		long subTaskCount = 0;
		List<String> newDescList=new ArrayList<>();
		for (Task taskObj : taskCreatedList) {
			if(!taskObj.getSubTasks().isEmpty() && taskObj.isChild())
			{
				List<SubTask> subTaskList=taskObj.getSubTasks().stream().filter(subTask -> subTask.isActive() && 
						subTask.getCreatedOn().toLocalDate().equals(DateUtil.getLocalDate(lastDay))).collect(Collectors.toList());
				//				subTaskCount = subTaskCount + subTaskList.size();
				if(!subTaskList.isEmpty()) {
					for (SubTask subTaskObj : subTaskList) {
						taskCount = taskCount + 1;
						newDescList.add(subTaskObj.getDescription());
						projectSummaryDetails.setNewTask(Constants.TRUE);
					}
				}
			}
			else if(taskObj.getCreatedOn().toLocalDate().equals(DateUtil.getLocalDate(lastDay))){
				taskCount = taskCount + 1;
				newDescList.add(taskObj.getDescription());
				projectSummaryDetails.setNewTask(Constants.TRUE);
			}
		}
		projectSummaryDetails.setNewTaskList(newDescList);
		if(taskCount != 0)
			summaryResultList = getTaskResult(summaryResultList, taskCount, Constants.TRUE, Constants.False);
		return summaryResultList;
	}


	/**
	 * Count of behind schedule task.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskCreatedList the task created list
	 * @param lastWeekFromDate the last week from date
	 * @param lastWeekToDate the last week to date
	 * @param floatDays the float days
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	private List<String> countOfBehindScheduleTask(List<String> summaryResultList, List<Task> taskCreatedList, Date lastWeekFromDate, Date lastWeekToDate,
			int floatDays, ProjectSummaryDetailsDTO projectSummaryDetails) {
		//		long onTrackCount = 0;
		//		long warningCount = 0;
		long onTimeCount=0;
		long behindScheduleCount = 0;
		long unScheduleCount = 0;
		ModelMapper mapper=new ModelMapper();
		List<String> behindScheduleDescriptionList=new ArrayList<>();
		List<String> completedDescList= new ArrayList<>();
		List<String> unScheduleDescList= new ArrayList<>();
		//		List<String> ontrackDescriptionList=new ArrayList<>();
		//		List<String> warningDescriptionList=new ArrayList<>();
		for (Task task : taskCreatedList) {
			List<SubTask> subTaskList=task.getSubTasks();
			for (SubTask subTask : subTaskList) {
				Date finalRequiredDate = DateUtil.getStartOfDay(LookaheadAccessServiceImpl.getRequiredCompletionDate(subTask.getRequiredCompletionDate(), subTask.getDeadLineDate()), Calendar.getInstance());
				if(subTask.isActive() && finalRequiredDate != null && finalRequiredDate.compareTo(DateUtil.getStartOfDay(lastWeekFromDate, Calendar.getInstance())) >= 0 && finalRequiredDate.compareTo(DateUtil.getStartOfDay(lastWeekToDate, Calendar.getInstance())) <= 0)
				{
					SubTaskDTO subTaskDTO = null;
					List<ScheduleDTO> scheduleDTOList = null;
					try {
						subTaskDTO = mapper.map(subTask, SubTaskDTO.class);
						scheduleDTOList = subTaskDTO.getSchedules().stream().map(scheduleObj -> mapper.map(scheduleObj, ScheduleDTO.class)).
								collect(Collectors.toList());
						scheduleDTOList.forEach(scheduleDTO -> {
							LocalDate localWorkingDate = LocalDate.parse(scheduleDTO.getWorkingDate(), DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS_SSS.toString()));
							Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
							scheduleDTO.setWorkingDateInDate(workingDate);
						});

						scheduleDTOList = LookaheadAccessServiceImpl.trackingSubTaskStatus(finalRequiredDate, scheduleDTOList, subTaskDTO, floatDays);
						if(!scheduleDTOList.isEmpty()){
							if(subTaskDTO.isBehindSchedule())
							{
								behindScheduleCount = behindScheduleCount+1;
								behindScheduleDescriptionList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setBehindSchedule(Constants.TRUE);
							}
							else {
								onTimeCount = onTimeCount+1;
								completedDescList.add(subTaskDTO.getDescription());
								projectSummaryDetails.setReqCompletedTask(Constants.TRUE);
							}

							// commented no need on warning for this realse said by deepa mam on 24-07-19
							/*if(subTaskDTO.isWarning())
					{
						warningCount = 	warningCount + 1;
						warningDescriptionList.add(subTaskDTO.getDescription());
						projectSummaryDetails.setWarning(Constants.TRUE);
					}
							 * else
					{
						onTrackCount = onTrackCount + 1;
						ontrackDescriptionList.add(subTaskDTO.getDescription());
						projectSummaryDetails.setOnTrack(Constants.TRUE);

					}*/
						}
						else
						{
							unScheduleCount = unScheduleCount+1;
							unScheduleDescList.add(subTaskDTO.getDescription());
							projectSummaryDetails.setUnScheduleTask(Constants.TRUE);
						}
					} catch (Exception e) {
						throw new CustomRuntimeException(""+e.getMessage());
					}
				}
			}
		}
		projectSummaryDetails.setBehindScheduleList(behindScheduleDescriptionList);
		projectSummaryDetails.setReqCompletedTaskList(completedDescList);
		projectSummaryDetails.setUnScheduleResultList(unScheduleDescList);
		/*projectSummaryDetails.setWarningList(warningDescriptionList);
		projectSummaryDetails.setOnTrackList(ontrackDescriptionList);*/
		if(behindScheduleCount != 0)
			summaryResultList  = getResult(summaryResultList, behindScheduleCount, Constants.BEHINDSCHEDULE);
		if(unScheduleCount != 0)
			summaryResultList  = getResult(summaryResultList, unScheduleCount, Constants.UNSCHEDULED);
		if(onTimeCount != 0)
			summaryResultList = getTaskResult(summaryResultList, onTimeCount, Constants.False, Constants.TRUE);
		/*if(onTrackCount != 0)
			summaryResultList  = getResult(summaryResultList, onTrackCount, "onTrack");
		if(warningCount != 0)
			summaryResultList  = getResult(summaryResultList, warningCount, Constants.WARNING);*/
		return summaryResultList;
	}


	/**
	 * Count of tasks.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskList the task list
	 * @param lastWeekFromDate the last week from date
	 * @param lastWeekToDate the last week to date
	 * @param projectSummaryDetails the project summary details
	 * @return the long    
	 */
	public List<String> countOfNewTasks(List<String> summaryResultList, List<Task> taskList, Date lastWeekFromDate, Date lastWeekToDate, 
			ProjectSummaryDetailsDTO projectSummaryDetails)
	{
		long taskCount = 0;
		//		long subTaskCount = 0;
		List<String> newDescList=new ArrayList<>();
		for (Task taskObj : taskList) {
			if(!taskObj.getSubTasks().isEmpty() && taskObj.isChild())
			{
				List<SubTask> subTaskList=taskObj.getSubTasks().stream().filter(subTask -> subTask.isActive() && 
						subTask.getCreatedOn().toLocalDate().isAfter(DateUtil.getLocalDate(lastWeekFromDate)) && 
						subTask.getCreatedOn().toLocalDate().isBefore(DateUtil.getLocalDate(lastWeekToDate))).collect(Collectors.toList());
				//				subTaskCount = subTaskCount + subTaskList.size();
				if(!subTaskList.isEmpty()) {
					for (SubTask subTaskObj : subTaskList) {
						taskCount = taskCount + 1;
						newDescList.add(subTaskObj.getDescription());
						projectSummaryDetails.setNewTask(Constants.TRUE);
					}
				}
			}
			else if (taskObj.getCreatedOn().toLocalDate().isAfter(DateUtil.getLocalDate(lastWeekFromDate)) && 
					taskObj.getCreatedOn().toLocalDate().isBefore(DateUtil.getLocalDate(lastWeekToDate))){
				taskCount = taskCount + 1;
				newDescList.add(taskObj.getDescription());
				projectSummaryDetails.setNewTask(Constants.TRUE);
			}
		}
		projectSummaryDetails.setNewTaskList(newDescList);
		if(taskCount != 0)
			summaryResultList = getTaskResult(summaryResultList, taskCount, Constants.TRUE, Constants.False);
		// sub task list displaying has been commented on 19th july as per deppa mam information.
		//		if(subTaskCount != 0)
		//			summaryResultList = getSubtaskResult(summaryResultList, subTaskCount, Constants.TRUE, Constants.FALSE);
		return summaryResultList;
	}

	/**
	 * Count of required completion tasks.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskList the task list
	 * @param lastWeekFromDate the last week from date
	 * @param lastWeekToDate the last week to date
	 * @param projectSummaryDetails the project summary details
	 * @return the list
	 */
	/*public List<String> countOfRequiredCompletionTasks(List<String> summaryResultList, List<Task> taskList, Date lastWeekFromDate, Date lastWeekToDate,
			ProjectSummaryDetailsDTO projectSummaryDetails)
	{
		long taskCount = 0;
		//		long subTaskCount = 0;
		List<String> completedDescList= new ArrayList<>();
		Date finalRequiredDate = null;
		for (Task taskObj : taskList) {
			finalRequiredDate = LookaheadAccessServiceImpl.getRequiredCompletionDate(taskObj.getRequiredCompletionDate(), taskObj.getDeadLineDate());
			if(!taskObj.getSubTasks().isEmpty() && taskObj.isChild()){
				for (SubTask subTaskObj : taskObj.getSubTasks()) {
					finalRequiredDate = LookaheadAccessServiceImpl.getRequiredCompletionDate(subTaskObj.getRequiredCompletionDate(), subTaskObj.getDeadLineDate());
					if(finalRequiredDate != null && subTaskObj.isActive() &&
							subTaskObj.getExpectedCompletionDate().after(lastWeekFromDate) && 
							subTaskObj.getExpectedCompletionDate().before(lastWeekToDate))
					{
						taskCount = taskCount + 1;
						completedDescList.add(subTaskObj.getDescription());	
						projectSummaryDetails.setReqCompletedTask(Constants.TRUE);
					}
				}

			}
			else if(finalRequiredDate != null  && finalRequiredDate.after(lastWeekFromDate) && 
					finalRequiredDate.before(lastWeekToDate))	{
				taskCount = taskCount + 1;
				completedDescList.add(taskObj.getDescription());
				projectSummaryDetails.setReqCompletedTask(Constants.TRUE);
			}
		}
		projectSummaryDetails.setReqCompletedTaskList(completedDescList);
		if(taskCount != 0)
			summaryResultList = getTaskResult(summaryResultList, taskCount, Constants.FALSE, Constants.TRUE);
		// sub task list displaying has been commented on 19th july as per deppa mam information.
		//		if(subTaskCount != 0)
		//			summaryResultList = getSubtaskResult(summaryResultList, subTaskCount, Constants.FALSE, Constants.TRUE);
		return summaryResultList;
	}*/

	/**
	 * Count of modify tasks.
	 *
	 * @param summaryResultList the summary result list
	 * @param taskList the task list
	 * @param lastWeekFromDate the last week from date
	 * @param lastWeekToDate the last week to date
	 * @param projectSummaryDetails the project summary details
	 * @return the long
	 */
	public List<String> countOfModifyTasks(List<String> summaryResultList, List<Task> taskList, Date lastWeekFromDate, Date lastWeekToDate,
			ProjectSummaryDetailsDTO projectSummaryDetails)
	{
		long taskCount = 0;
		//		long subTaskCount = 0;
		taskList=taskList.stream().filter(taskObj -> taskObj.getCreatedOn() != taskObj.getModifiedOn())
				.collect(Collectors.toList());
		List<String> modifyDescList= new ArrayList<>();
		for (Task taskObj : taskList) {
			if(!taskObj.getSubTasks().isEmpty() && taskObj.isChild())
			{
				List<SubTask> subTaskList= taskObj.getSubTasks().stream().filter(subTaskObj -> subTaskObj.isActive() &&
						subTaskObj.getCreatedOn() != subTaskObj.getModifiedOn() &&
						!subTaskObj.getCreatedOn().toLocalDate().isAfter(DateUtil.getLocalDate(lastWeekFromDate)) && 
						subTaskObj.getModifiedOn().toLocalDate().isAfter(DateUtil.getLocalDate(lastWeekFromDate)) && 
						subTaskObj.getModifiedOn().toLocalDate().isBefore(DateUtil.getLocalDate(lastWeekToDate))).collect(Collectors.toList());
				//				subTaskCount = subTaskCount + subTaskList.size();
				if(!subTaskList.isEmpty()) {
					for (SubTask subTaskObj : subTaskList) {
						taskCount = taskCount + 1;
						modifyDescList.add(subTaskObj.getDescription());
						projectSummaryDetails.setModifyTask(Constants.TRUE);
					}					
				}
			}
			else if(taskObj.getCreatedOn() != taskObj.getModifiedOn() &&
					taskObj.getModifiedOn().toLocalDate().isAfter(DateUtil.getLocalDate(lastWeekFromDate)) && 
					taskObj.getModifiedOn().toLocalDate().isBefore(DateUtil.getLocalDate(lastWeekToDate)) &&
					!taskObj.getCreatedOn().toLocalDate().isAfter(DateUtil.getLocalDate(lastWeekFromDate)))
			{
				taskCount = taskCount + 1;
				modifyDescList.add(taskObj.getDescription());
				projectSummaryDetails.setModifyTask(Constants.TRUE);
			}
		}
		projectSummaryDetails.setModifyTaskList(modifyDescList);
		if(taskCount != 0)
			summaryResultList = getTaskResult(summaryResultList, taskCount, Constants.False, Constants.False);
		// sub task list displaying has been commented on 19th july as per deppa mam information.
		//		if(subTaskCount != 0)
		//			summaryResultList = getSubtaskResult(summaryResultList, subTaskCount, Constants.FALSE, Constants.FALSE);
		return summaryResultList;	
	}

	/**
	 * Gets the result.
	 *
	 * @param result the result
	 * @param count the count
	 * @param trackingValue the tracking value
	 * @return the result
	 */
	public List<String> getResult(List<String> result, long count, String trackingValue)
	{
		if(trackingValue.equals(Constants.BEHINDSCHEDULE))
		{
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneBehindScheduleTask());
			else
				result.add(count+" "+projSummaryConfig.getManyBehindScheduleTask());
		}
		else if(trackingValue.equals(Constants.WARNING))
		{
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneWarningTask());
			else 
				result.add(count+" "+projSummaryConfig.getManyWarningTask());
		}
		else if(trackingValue.equals(Constants.UNSCHEDULED))
		{
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneUnScheduleTask());
			else 
				result.add(count+" "+projSummaryConfig.getManyUnScheduleTask());
		}else{
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneReqCompletedTask());
			else 
				result.add(count+" "+projSummaryConfig.getManyReqCompletedTask());
		}
		return result;
	}

	/**
	 * Gets the future result.
	 *
	 * @param result the result
	 * @param count the count
	 * @param trackingValue the tracking value
	 * @return the future result
	 */
	public List<String> getFutureResult(List<String> result, long count, String trackingValue)
	{
		if(trackingValue.equals(Constants.BEHINDSCHEDULE))
		{
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneFutureBehindScheduleTask());
			else
				result.add(count+" "+projSummaryConfig.getManyFutureBehindScheduleTask());
		}
		else if(trackingValue.equals(Constants.WARNING))
		{
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneFutureWarningTask());
			else 
				result.add(count+" "+projSummaryConfig.getManyFutureWarningTask());
		}else if(trackingValue.equals(Constants.ONTIME)){
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneFutureCompletedTask());
			else 
				result.add(count+" "+projSummaryConfig.getManyFutureCompletedTask());
		}else {
			if(count == 1)
				result.add(count+" "+projSummaryConfig.getOneUnScheduleTask());
			else 
				result.add(count+" "+projSummaryConfig.getManyUnScheduleTask());
		}
		return result;
	}

	/**
	 * Gets the task result.
	 * New/Modify Result
	 *
	 * @param result the result
	 * @param count the count
	 * @param isNew the is new
	 * @param isCompleted the is completed
	 * @return the task result
	 */
	public List<String> getTaskResult(List<String> result, long count, boolean isNew, boolean isCompleted)
	{
		if(count == 1 && isNew && !isCompleted)
			result.add(count+" "+projSummaryConfig.getOneNewTask());
		else if(count == 1 && !isNew && !isCompleted)
			result.add(count+" "+projSummaryConfig.getOneModifyTask());
		else if(count == 1 && !isNew && isCompleted)
			result.add(count+" "+projSummaryConfig.getOneReqCompletedTask());
		else if(count > 1 && !isNew && isCompleted)	
			result.add(count+" "+projSummaryConfig.getManyReqCompletedTask());
		else if(count > 1 && isNew && !isCompleted)
			result.add(count+" "+projSummaryConfig.getManyNewTask());
		else 
			result.add(count+" "+projSummaryConfig.getManyModifyTask());
		return result;
	}

	/**
	 * Gets the subtask result.
	 * New/Modify Result
	 *
	 * @param result the result
	 * @param count the count
	 * @param isNew the is new
	 * @param isCompleted the is completed
	 * @return the subtask result
	 */
	public List<String> getSubtaskResult(List<String> result,long count, boolean isNew, boolean isCompleted)
	{
		if(count==1 && isNew && !isCompleted)
			result.add(count+" "+projSummaryConfig.getOneNewSubtask());
		else if(count == 1 && !isNew && !isCompleted)
			result.add(count+" "+projSummaryConfig.getOneModifySubtask());
		else if(count > 1 && isNew && !isCompleted)
			result.add(count+" "+projSummaryConfig.getManyNewSubtask());
		else if(count == 1 && !isNew && isCompleted)
			result.add(count+" "+projSummaryConfig.getOneReqCompletedSubtask());
		else if(count > 1 && !isNew && isCompleted)	
			result.add(count+" "+projSummaryConfig.getManyReqCompletedSubtask());
		else 
			result.add(count+" "+projSummaryConfig.getManyModifySubtask());
		return result;
	}

	/**
	 * To Calculate Task summary details.
	 *
	 * @param taskList the task list
	 * @return the string buffer
	 */
	public StringBuilder taskSummaryDetails(List<Task> taskList)
	{
		Integer count = null;
		Integer warningCount = null;

		StringBuilder taskSummary= new StringBuilder();
		for (Task task : taskList){
			count = findUpcomingTasks(task);
			warningCount = findWarningTasks(task);
		}
		if(count!= null)
			taskSummary.append(count.toString()).append(projSummaryConfig.getManyBehindScheduleTask()).append('\n');
		if(warningCount!= null)
			taskSummary.append(" "+warningCount.toString()).append(projSummaryConfig.getManyWarningTask());
		return taskSummary;
	}

	/**
	 * Find warning tasks.
	 *
	 * @param taskObj the task obj
	 * @return the integer
	 */
	private Integer findWarningTasks(Task taskObj) {
		Integer count = 0;
		Date currentDate= new Date();
		List<SubTask> subList=new ArrayList<>();
		if(null != taskObj.getDeadLineDate()) {
			subList=taskObj.getSubTasks().stream().filter(subTask -> 
			subTask.getDeadLineDate().after(currentDate))
					.collect(Collectors.toList()); 
		}
		count=subList.size();

		return count;
	}

	/**
	 * Find upcoming tasks.
	 *
	 * @param taskObj the task obj
	 * @return the integer
	 */
	private Integer findUpcomingTasks(Task taskObj) {
		Integer count = 0;
		Date currentDate= new Date();
		List<SubTask> subList=new ArrayList<>();
		if(null != taskObj.getExpectedCompletionDate()) {
			subList=taskObj.getSubTasks().stream().filter(subTask -> 
			subTask.getExpectedCompletionDate().after(currentDate))
					.collect(Collectors.toList()); 
		}
		count=subList.size();

		return count;
	}

	/**
	 * Send project summary details mail.
	 *
	 * @param email the email
	 * @param projectSummaryDetails the body
	 */
	@Override
	public void sendProjectSummaryDetailsMail(String email, ProjectSummaryDetailsDTO projectSummaryDetails) {
		if(projectSummaryDetails.getLastDay() != null || projectSummaryDetails.getFutureDay() != null)
			messageQueueService.sendProjectSummaryMailDaily(email, projSummaryConfig.getDailyTemplateName()," - "+projSummaryConfig.getMailSubject(), projectSummaryDetails);
		else 
			messageQueueService.sendProjectSummaryMail(email, projSummaryConfig.getWeeklyTemplateName()," - "+projSummaryConfig.getMailSubject(), projectSummaryDetails);

	}
}