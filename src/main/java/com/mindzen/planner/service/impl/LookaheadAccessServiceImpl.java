package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.ERR;
import static com.mindzen.infra.api.response.ApiResponseConstants.INS;
import static com.mindzen.infra.api.response.ApiResponseConstants.REC;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCSFLY;
import static com.mindzen.infra.api.response.ApiResponseConstants.UPD;
import static org.mockito.Mockito.description;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.query.criteria.internal.expression.function.SubstringFunction;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.primitives.Longs;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.AppConfig;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.ImportExcelTaskDTO;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadFilterDTO;
import com.mindzen.planner.dto.LookaheadInfoDTO;
import com.mindzen.planner.dto.LookaheadJasperReportDetailDTO;
import com.mindzen.planner.dto.LookaheadJasperReportRootDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.Pagination;
import com.mindzen.planner.dto.ScheduleDTO;
import com.mindzen.planner.dto.SubTaskDTO;
import com.mindzen.planner.dto.TaskDTO;
import com.mindzen.planner.dto.TaskStatusDTO;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.entity.Schedule;
import com.mindzen.planner.entity.SubTask;
import com.mindzen.planner.entity.Task;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.repository.ScheduleRepository;
import com.mindzen.planner.repository.SubTaskRepository;
import com.mindzen.planner.repository.TaskRepository;
import com.mindzen.planner.service.LookaheadAccessService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;
import com.mindzen.planner.util.ExcelUtil;
import com.mindzen.planner.util.LookaheadHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class LookaheadAccessServiceImpl.
 */
@Service

/** The Constant log. */
@Slf4j
public class LookaheadAccessServiceImpl implements LookaheadAccessService {

	/** The task repository. */
	TaskRepository taskRepository;

	/** The sub task repository. */
	SubTaskRepository subTaskRepository;

	/** The schedule repository. */
	ScheduleRepository scheduleRepository;

	/** The project repository. */
	ProjectRepository projectRepository;

	/** The project user repository. */
	ProjectUserRepository projectUserRepository;

	/** The message queue service. */
	MessageQueueService messageQueueService;

	/** The app config. */
	AppConfig appConfig;

	/** The project company repository. */
	ProjectCompanyRepository projectCompanyRepository;

	/** The entity manager. */
	@PersistenceContext
	private EntityManager entityManager;

	/** The object mapper. */
	private ObjectMapper objectMapper;

	/**
	 * Instantiates a new lookahead access service impl.
	 *
	 * @param taskRepository           the task repository
	 * @param subTaskRepository        the sub task repository
	 * @param scheduleRepository       the schedule repository
	 * @param projectRepository        the project repository
	 * @param messageQueueService      the message queue service
	 * @param projectUserRepository    the project user repository
	 * @param appConfig                the app config
	 * @param projectCompanyRepository the project company repository
	 */
	public LookaheadAccessServiceImpl(TaskRepository taskRepository, SubTaskRepository subTaskRepository,
			ScheduleRepository scheduleRepository, ProjectRepository projectRepository,
			MessageQueueService messageQueueService, ProjectUserRepository projectUserRepository, AppConfig appConfig,
			ProjectCompanyRepository projectCompanyRepository, ObjectMapper objectMapper) {
		this.taskRepository = taskRepository;
		this.subTaskRepository = subTaskRepository;
		this.scheduleRepository = scheduleRepository;
		this.projectRepository = projectRepository;
		this.messageQueueService = messageQueueService;
		this.projectUserRepository = projectUserRepository;
		this.appConfig = appConfig;
		this.projectCompanyRepository = projectCompanyRepository;
		this.objectMapper = objectMapper;
	}

	@Override
	// @Transactional
	public MSAResponse createLookaheadForImportExcel(LookaheadDTO lookaheadDTO, Long trackId) {
		if (null != lookaheadDTO.getTasks() && !lookaheadDTO.getTasks().isEmpty()) {
			Project project = projectRepository.findById(lookaheadDTO.getProjectId()).get();
			List<TaskDTO> taskDTOList = lookaheadDTO.getTasks();
			for (TaskDTO taskDTO : taskDTOList) {
				Task task = new Task();
				task.setChild(taskDTO.isChild());
				task.setActive(true);
				task.setProjectId(project.getId());
				if (null != taskDTO.getDescription()) {
					task.setDescription(taskDTO.getDescription());
					task.setActivityDescription(taskDTO.getDescription());
				}

				if (taskDTO.getTaskStartDate() != null) {
					task.setTaskStartDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskStartDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setImportStartDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskStartDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				} else {
					task.setTaskStartDate(null);
					task.setImportStartDate(null);
				}

				if (taskDTO.getRequiredCompletionDate() != null) {
					task.setRequiredCompletionDate(DateUtil.stringToDateViaInstant(taskDTO.getRequiredCompletionDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setImportTargetCompletionDate(DateUtil.stringToDateViaInstant(
							taskDTO.getRequiredCompletionDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));

				} else {
					task.setRequiredCompletionDate(null);
					task.setImportTargetCompletionDate(null);
				}

				if (taskDTO.getTaskFinishDate() != null) {
					task.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setImportFinishDate(DateUtil.stringToDateViaInstant(taskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				} else {
					task.setExpectedCompletionDate(null);
					task.setImportFinishDate(null);
				}

				if (null != taskDTO.getTaskType())
					task.setTaskType(taskDTO.getTaskType());
				if (null != taskDTO.getProjectCompanyId())
					task.setProjectCompanyId(taskDTO.getProjectCompanyId());
				if (null != taskDTO.getSpecDivision())
					task.setSpecDivision(taskDTO.getSpecDivision());

				task.setActivityId(taskDTO.getActivityId());
				if (taskDTO.getImportUniqueId() != null)
					task.setImportUniqueId(taskDTO.getImportUniqueId());
				task.setImportId(trackId);
				task.setNote(taskDTO.getImportNotes());

				task = taskRepository.save(task);

				subTaskGenerateForTask(taskDTO);
				if (null != taskDTO.getSubTasks() && !taskDTO.getSubTasks().isEmpty()) {
					List<SubTaskDTO> subTaskDTOList = taskDTO.getSubTasks();
					for (SubTaskDTO subTaskDTO : subTaskDTOList) {
						SubTask subTask = new SubTask();
						subTask.setTask(task);
						subTask.setActive(true);

						if (null != subTaskDTO.getDescription()) {
							subTask.setDescription(subTaskDTO.getDescription());
							subTask.setActivityDescription(subTaskDTO.getDescription());
						}

						if (subTaskDTO.getTaskStartDate() != null) {
							subTask.setTaskStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
									DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							subTask.setImportStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
									DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						} else {
							subTask.setTaskStartDate(null);
							subTask.setImportStartDate(null);
						}

						if (subTaskDTO.getRequiredCompletionDate() != null) {
							subTask.setRequiredCompletionDate(
									DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
											DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							subTask.setImportTargetCompletionDate(
									DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
											DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						} else {
							subTask.setRequiredCompletionDate(null);
							subTask.setImportTargetCompletionDate(null);
						}

						if (subTaskDTO.getTaskFinishDate() != null) {
							subTask.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(
									subTaskDTO.getTaskFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							subTask.setImportFinishDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
									DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						} else {
							subTask.setExpectedCompletionDate(null);
							subTask.setImportFinishDate(null);
						}
						if (null != subTaskDTO.getTaskType())
							subTask.setTaskType(subTaskDTO.getTaskType());
						if (null != subTaskDTO.getSpecDivision())
							subTask.setSpecDivision(subTaskDTO.getSpecDivision());
						if (null != subTaskDTO.getProjectCompanyId())
							subTask.setProjectCompanyId(subTaskDTO.getProjectCompanyId());

						subTask.setActivityId(taskDTO.getActivityId());
						if (taskDTO.getImportUniqueId() != null)
							subTask.setImportUniqueId(taskDTO.getImportUniqueId());

						subTask = subTaskRepository.save(subTask);
						generateSchedulesForNewTask(subTaskDTO, subTask, project);
					}
				}
			}
		}
		return new MSAResponse(true, HttpStatus.OK, Constants.SUCCESS, null);
	}

	public void updateLookaheadForImportFile(LookaheadDTO lookaheadDTO, Long trackId) {
		Project project = projectRepository.findById(lookaheadDTO.getProjectId()).get();
		List<SubTaskDTO> subTaskList = new ArrayList<>();
		lookaheadDTO.getTasks().forEach(task -> {
			subTaskList.addAll(task.getSubTasks());
		});
		subTaskList.forEach(subTaskDTO -> {
			Task task = updateconvertSubTaskDTOToTask(subTaskDTO, project, trackId, true);
			if (!task.isChild())
				task.setChild(false);
			// if (!task.isLookAheadChange()) {
			taskRepository.save(task);
			SubTask subTask = updateconvertSubTaskDTOToSubTask(subTaskDTO, task);
			subTaskRepository.save(subTask);
			generateSchedulesForNewTaskFromUpload(subTaskDTO, subTask, project);

			// }

			// taskRepository.updateImportMppIntoTaskTable(task.getActivityDescription(),task.getDescription(),task.getTaskStartDate(),task.getImportStartDate(),
			// task.getRequiredCompletionDate(),task.getExpectedCompletionDate(),task.getImportFinishDate(),task.getImportTargetCompletionDate(),task.getTaskType(),task.getSpecDivision(),task.getActivityId(),task.getImportNotes(),true,false,
			// task.getProjectCompanyId(),task.getImportUniqueId(),task.getImportId(),task.getProjectId());

			// subTaskRepository.updateImportMppIntoSubTaskTable(subTask.getActivityDescription(),subTask.getDescription(),subTask.getTaskStartDate(),subTask.getImportStartDate(),
			// subTask.getRequiredCompletionDate(),subTask.getExpectedCompletionDate(),subTask.getImportFinishDate(),subTask.getImportTargetCompletionDate(),subTask.getTaskType(),subTask.getSpecDivision(),subTask.getImportNotes(),true,
			// subTask.getProjectCompanyId(),subTask.getImportUniqueId(),subTask.getActivityId());
			//

		});

	}

	private SubTask updateconvertSubTaskDTOToSubTask(SubTaskDTO subTaskDTO, Task task) {
		SubTask subTask = new SubTask();
		subTask.setTask(task);
		subTask.setActive(true);
		SubTask taskIdFromImport = subTaskRepository.findByTaskIdFromImport(task, task.getImportUniqueId());
		if (taskIdFromImport != null) {

			subTask.setId(taskIdFromImport.getId());

		}

		if (null != task.getDescription()) {
			subTask.setDescription(task.getDescription());
			subTask.setActivityDescription(task.getDescription());
		}

		if (task.getTaskStartDate() != null) {
			subTask.setTaskStartDate(task.getTaskStartDate());

		} else {
			subTask.setTaskStartDate(null);
		}

		if (task.getRequiredCompletionDate() != null) {
			subTask.setRequiredCompletionDate(task.getRequiredCompletionDate());
		} else {
			subTask.setRequiredCompletionDate(null);
		}
		if (task.getImportDeadLineDate() != null) {
			subTask.setImportDeadLineDate(task.getRequiredCompletionDate());
		} else {
			subTask.setImportDeadLineDate(null);
		}

		if (subTaskDTO.getTaskFinishDate() != null) {
			subTask.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		} else {
			subTask.setExpectedCompletionDate(null);
		}

		if (task.getExpectedCompletionDate() != null)
			subTask.setExpectedCompletionDate(task.getExpectedCompletionDate());
		if (task.getImportTargetCompletionDate() != null) {
			subTask.setImportTargetCompletionDate(task.getImportTargetCompletionDate());
		}
		if (task.getImportFinishDate() != null)
			subTask.setImportFinishDate(task.getImportFinishDate());
		if (task.getDeadLineDate() != null) {
			subTask.setDeadLineDate(task.getDeadLineDate());
		} else {
			subTask.setDeadLineDate(null);
		}

		if (null != task.getTaskType())
			subTask.setTaskType(task.getTaskType());
		if (null != task.getSpecDivision())
			subTask.setSpecDivision(task.getSpecDivision());
		if (null != task.getTrade())
			subTask.setTrade(task.getTrade());
		if (null != task.getBallInCourt())
			subTask.setBallInCourt(task.getBallInCourt());
		if (null != subTaskDTO.getProjectCompanyId())
			subTask.setProjectCompanyId(subTaskDTO.getProjectCompanyId());
		if (subTaskDTO.getActivityId() != null) {
			String activityDescription = subTaskDTO.getActivityId() + "-" + subTaskDTO.getDescription();
			subTask.setDescription(activityDescription);
			subTask.setActivityId(task.getActivityId());
		}

		subTask.setImportUniqueId(task.getImportUniqueId());
		subTask.setImportNotes(task.getImportNotes());

		return subTask;
	}

	private Task updateconvertSubTaskDTOToTask(SubTaskDTO subTaskDTO, Project project, Long trackId,
			boolean isUpdateMpp) {
		Task task = new Task();
		task.setActive(true);
		task.setProjectId(project.getId());
		task.setLookAheadChange(false);

		Task MatchingtaskUniqueIdsList = taskRepository
				.findByTaskId(Integer.parseInt(subString(subTaskDTO.getImportUniqueId())), project.getId(), trackId);
		if (MatchingtaskUniqueIdsList != null) {
			task.setId(MatchingtaskUniqueIdsList.getId());
			if (MatchingtaskUniqueIdsList.isLookAheadChange()) {
				task.setLookAheadChange(true);
			}
			if (MatchingtaskUniqueIdsList.isChild()) {
				task.setChild(true);
			}
			if (MatchingtaskUniqueIdsList.getBallInCourt() != null)
				task.setBallInCourt(MatchingtaskUniqueIdsList.getBallInCourt());

			if (MatchingtaskUniqueIdsList.getTaskType() != null
					&& !MatchingtaskUniqueIdsList.getTaskType().contains("null"))
				task.setTaskType(MatchingtaskUniqueIdsList.getTaskType());

			if (MatchingtaskUniqueIdsList.getTrade() != null)
				task.setTrade(MatchingtaskUniqueIdsList.getTrade());
			if (MatchingtaskUniqueIdsList.getSpecDivision() != null)
				task.setSpecDivision(MatchingtaskUniqueIdsList.getSpecDivision());

			if (MatchingtaskUniqueIdsList.isLookAheadChange()) {

				if (null != subTaskDTO.getDescription()) {

					if (subTaskDTO.getActivityId() != null) {

						String activityId = subTaskDTO.getDescription().substring(0, 2);
						String activityIdContainshypen = Character.toString((activityId.charAt(1)));
						if (activityIdContainshypen.contains("-")) {
							task.setActivityDescription(subTaskDTO.getDescription());
						}
						if (activityId.contains(subTaskDTO.getActivityId())) {
							String activityDescription = subTaskDTO.getActivityDescription()
									.replaceFirst(subTaskDTO.getActivityId(), "");
							task.setActivityDescription(activityDescription);

						}
					}
					// task.setDescription(subTaskDTO.getDescription());
					task.setActivityDescription(subTaskDTO.getDescription());

				}

				if (subTaskDTO.getTaskStartDate() != null) {
					task.setImportStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));

				} else {
					task.setImportStartDate(null);
				}

				if (subTaskDTO.getRequiredCompletionDate() != null) {
					task.setImportDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				} else {
					task.setRequiredCompletionDate(null);
				}

				if (subTaskDTO.getTaskFinishDate() != null) {
					task.setImportFinishDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setImportTargetCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				} else {
					task.setImportFinishDate(null);
					task.setImportTargetCompletionDate(null);
				}
				if (null != subTaskDTO.getProjectCompanyId())
					task.setProjectCompanyId(subTaskDTO.getProjectCompanyId());
				if (subTaskDTO.getActivityId() != null) {
					String activityDescription = subTaskDTO.getActivityId() + "-" + subTaskDTO.getDescription();
					task.setDescription(activityDescription);
					task.setActivityId(subTaskDTO.getActivityId());

				}
				if (subTaskDTO.getImportUniqueId() != null)
					task.setImportUniqueId(Integer.parseInt(subString(subTaskDTO.getImportUniqueId())));
				task.setImportId(trackId);
				task.setImportNotes(subTaskDTO.getNote());

				task.setExpectedCompletionDate(MatchingtaskUniqueIdsList.getExpectedCompletionDate());
				task.setTaskStartDate(MatchingtaskUniqueIdsList.getTaskStartDate());
				task.setRequiredCompletionDate(MatchingtaskUniqueIdsList.getRequiredCompletionDate());
				task.setDeadLineDate(MatchingtaskUniqueIdsList.getDeadLineDate());
				task.setDescription(MatchingtaskUniqueIdsList.getDescription());
				if (MatchingtaskUniqueIdsList.getNote() != null)
					task.setNote(MatchingtaskUniqueIdsList.getNote());

			} else {
				if (null != subTaskDTO.getDescription()) {
					task.setDescription(subTaskDTO.getDescription());

					task.setActivityDescription(subTaskDTO.getDescription());
				}

				if (subTaskDTO.getTaskStartDate() != null) {
					task.setTaskStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setImportStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				} else {
					task.setTaskStartDate(null);
					task.setImportStartDate(null);
				}

				if (subTaskDTO.getRequiredCompletionDate() != null) {
					task.setImportDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				} else {
					task.setRequiredCompletionDate(null);
					task.setImportDeadLineDate(null);
				}

				if (subTaskDTO.getTaskFinishDate() != null) {
					task.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setImportFinishDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setImportTargetCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					task.setRequiredCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				} else {
					task.setExpectedCompletionDate(null);
					task.setImportFinishDate(null);
					task.setImportTargetCompletionDate(null);
				}
				if (null != subTaskDTO.getProjectCompanyId())
					task.setProjectCompanyId(subTaskDTO.getProjectCompanyId());

				if (subTaskDTO.getActivityId() != null) {
					String activityDescription = subTaskDTO.getActivityId() + "-" + subTaskDTO.getDescription();
					task.setDescription(activityDescription);
					task.setActivityId(subTaskDTO.getActivityId());
				}
				if (subTaskDTO.getImportUniqueId() != null)
					task.setImportUniqueId(Integer.parseInt(subString(subTaskDTO.getImportUniqueId())));
				task.setImportId(trackId);
				task.setImportNotes(subTaskDTO.getNote());
			}
		} else {
			if (null != subTaskDTO.getDescription()) {
				task.setDescription(subTaskDTO.getDescription());

				task.setActivityDescription(subTaskDTO.getDescription());
			}

			if (subTaskDTO.getTaskStartDate() != null) {
				task.setTaskStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				task.setImportStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			} else {
				task.setTaskStartDate(null);
				task.setImportStartDate(null);
			}

			if (subTaskDTO.getRequiredCompletionDate() != null) {
				task.setImportDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				task.setDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			} else {
				task.setRequiredCompletionDate(null);
				task.setImportDeadLineDate(null);
			}

			if (subTaskDTO.getTaskFinishDate() != null) {
				task.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				task.setImportFinishDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				task.setImportTargetCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				task.setRequiredCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
						DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			} else {
				task.setExpectedCompletionDate(null);
				task.setImportFinishDate(null);
				task.setImportTargetCompletionDate(null);
			}

			if (null != subTaskDTO.getTaskType())
				task.setTaskType(subTaskDTO.getTaskType());
			if (null != subTaskDTO.getProjectCompanyId())
				task.setProjectCompanyId(subTaskDTO.getProjectCompanyId());
			if (null != subTaskDTO.getSpecDivision())
				task.setSpecDivision(subTaskDTO.getSpecDivision());

			if (subTaskDTO.getActivityId() != null) {
				String activityDescription = subTaskDTO.getActivityId() + "-" + subTaskDTO.getDescription();
				task.setDescription(activityDescription);
				task.setActivityId(subTaskDTO.getActivityId());
			}
			if (subTaskDTO.getImportUniqueId() != null)
				task.setImportUniqueId(Integer.parseInt(subString(subTaskDTO.getImportUniqueId())));
			task.setImportId(trackId);
			task.setImportNotes(subTaskDTO.getNote());
		}

		//

		return task;
	}

	public void createLookaheadForImportFile(LookaheadDTO lookaheadDTO, Long trackId) {
		Project project = projectRepository.findById(lookaheadDTO.getProjectId()).get();
		List<SubTaskDTO> subTaskList = new ArrayList<>();
		lookaheadDTO.getTasks().forEach(task -> {
			log.info("SubTask", task.getSubTasks());
			subTaskList.addAll(task.getSubTasks());
		});
		Long start = System.currentTimeMillis();
		// List<Task>
		// uploadTaskDataExist=taskRepository.findAllBySubtaskListAndSchedule(project.getId(),trackId);
		subTaskList.forEach(subTaskDTO -> {
			Long startTimeForTask = System.currentTimeMillis();

			Task task = convertSubTaskDTOToTask(subTaskDTO, project, trackId, false);
			task.setChild(false);
			taskRepository.save(task);

			Long endTimeForTask = System.currentTimeMillis();
			log.info("consumed time for TASK " + (endTimeForTask - startTimeForTask));

			Long startTimeForSubTask = System.currentTimeMillis();
			SubTask subTask = convertSubTaskDTOToSubTask(subTaskDTO, task);
			subTaskRepository.save(subTask);
			Long endTimeForSubTask = System.currentTimeMillis();
			log.info("consumed time for SUBTASK " + (endTimeForSubTask - startTimeForSubTask));

			Long startTimeForSchedules = System.currentTimeMillis();
			generateSchedulesForNewTaskFromUpload(subTaskDTO, subTask, project);
			Long endTimeForSchedules = System.currentTimeMillis();
			log.info("consumed time for SCHEDULES " + (endTimeForSchedules - startTimeForSchedules));

		});
		Long end = System.currentTimeMillis();
		log.info("consumed time for task,subtask,schedule table " + (end - start));

	}

	private SubTask convertSubTaskDTOToSubTask(SubTaskDTO subTaskDTO, Task task) {
		SubTask subTask = new SubTask();
		subTask.setTask(task);
		subTask.setActive(true);
		List<SubTask> taskIdPresent = subTaskRepository.findByTaskId(task.getId());
		if (taskIdPresent != null) {
			for (SubTask subTask2 : taskIdPresent) {
				subTask.setId(subTask2.getId());

			}
		}

		if (null != subTaskDTO.getDescription()) {
			subTask.setDescription(subTaskDTO.getDescription());
			subTask.setActivityDescription(subTaskDTO.getDescription());
		}

		if (subTaskDTO.getTaskStartDate() != null) {
			subTask.setTaskStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			subTask.setImportStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		} else {
			subTask.setTaskStartDate(null);
			subTask.setImportStartDate(null);
		}

		if (subTaskDTO.getRequiredCompletionDate() != null) {

			subTask.setImportDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			subTask.setDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		} else {
			subTask.setRequiredCompletionDate(null);
			subTask.setImportDeadLineDate(null);
			subTask.setDeadLineDate(null);
		}

		if (subTaskDTO.getTaskFinishDate() != null) {
			subTask.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			subTask.setImportFinishDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			subTask.setImportTargetCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			subTask.setRequiredCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		} else {
			subTask.setExpectedCompletionDate(null);
			subTask.setImportFinishDate(null);
			subTask.setImportTargetCompletionDate(null);
			subTask.setRequiredCompletionDate(null);

		}
		if (null != subTaskDTO.getTaskType())
			subTask.setTaskType(subTaskDTO.getTaskType());
		if (null != subTaskDTO.getSpecDivision())
			subTask.setSpecDivision(subTaskDTO.getSpecDivision());
		if (null != subTaskDTO.getProjectCompanyId())
			subTask.setProjectCompanyId(subTaskDTO.getProjectCompanyId());
		if (subTaskDTO.getActivityId() != null) {
			String activityDescription = subTaskDTO.getActivityId() + "-" + subTaskDTO.getDescription();
			subTask.setDescription(activityDescription);
			subTask.setActivityId(task.getActivityId());
		}

		subTask.setImportUniqueId(task.getImportUniqueId());
		subTask.setImportNotes(task.getImportNotes());

		return subTask;
	}

	private Task convertSubTaskDTOToTask(SubTaskDTO subTaskDTO, Project project, Long trackId, boolean isUpdateMpp) {
		Task task = new Task();
		task.setActive(true);
		task.setProjectId(project.getId());
		task.setLookAheadChange(false);

		// Task MatchingtaskUniqueIdsList = taskRepository
		// .findByTaskId(Integer.parseInt(subString(subTaskDTO.getImportUniqueId())),
		// project.getId(), trackId);

		if (null != subTaskDTO.getDescription()) {
			task.setDescription(subTaskDTO.getDescription());

			task.setActivityDescription(subTaskDTO.getDescription());
		}

		if (subTaskDTO.getTaskStartDate() != null) {
			task.setTaskStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			task.setImportStartDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskStartDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		} else {
			task.setTaskStartDate(null);
			task.setImportStartDate(null);
		}

		if (subTaskDTO.getRequiredCompletionDate() != null) {
			task.setImportDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			task.setDeadLineDate(DateUtil.stringToDateViaInstant(subTaskDTO.getRequiredCompletionDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		} else {
			task.setRequiredCompletionDate(null);
			task.setImportDeadLineDate(null);
		}

		if (subTaskDTO.getTaskFinishDate() != null) {
			task.setExpectedCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			task.setImportFinishDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			task.setImportTargetCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
			task.setRequiredCompletionDate(DateUtil.stringToDateViaInstant(subTaskDTO.getTaskFinishDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		} else {
			task.setExpectedCompletionDate(null);
			task.setImportFinishDate(null);
			task.setImportTargetCompletionDate(null);
		}

		if (null != subTaskDTO.getTaskType())
			task.setTaskType(subTaskDTO.getTaskType());
		if (null != subTaskDTO.getProjectCompanyId())
			task.setProjectCompanyId(subTaskDTO.getProjectCompanyId());
		if (null != subTaskDTO.getSpecDivision())
			task.setSpecDivision(subTaskDTO.getSpecDivision());

		if (subTaskDTO.getActivityId() != null) {
			String activityDescription = subTaskDTO.getActivityId() + "-" + subTaskDTO.getDescription();
			task.setDescription(activityDescription);
			task.setActivityId(subTaskDTO.getActivityId());
		}
		if (subTaskDTO.getImportUniqueId() != null)
			task.setImportUniqueId(Integer.parseInt(subString(subTaskDTO.getImportUniqueId())));
		task.setImportId(trackId);
		task.setImportNotes(subTaskDTO.getNote());

		//

		return task;
	}

	private void subTaskGenerateForTask(TaskDTO taskDTO) {
		if (!taskDTO.isChild()) {
			List<SubTaskDTO> subTasks = new ArrayList<>();
			SubTaskDTO subTaskDTO = new SubTaskDTO();
			subTaskDTO.setDescription(taskDTO.getDescription());
			if (taskDTO.getTaskStartDate() != null)
				subTaskDTO.setTaskStartDate(taskDTO.getTaskStartDate());
			else
				subTaskDTO.setTaskStartDate(null);
			if (taskDTO.getRequiredCompletionDate() != null)
				subTaskDTO.setRequiredCompletionDate(taskDTO.getRequiredCompletionDate());
			else
				subTaskDTO.setRequiredCompletionDate(null);
			if (taskDTO.getTaskFinishDate() != null)
				subTaskDTO.setTaskFinishDate(taskDTO.getTaskFinishDate());
			else
				subTaskDTO.setTaskFinishDate(null);
			subTaskDTO.setTaskType(taskDTO.getTaskType());
			subTaskDTO.setProjectCompanyId(taskDTO.getProjectCompanyId());
			subTaskDTO.setSpecDivision(taskDTO.getSpecDivision());
			subTasks.add(subTaskDTO);
			taskDTO.setSubTasks(subTasks);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindzen.planner.service.LookaheadAccessService#loadAllTasks()
	 */
	@Override
	public MSAResponse loadAllTasks() {
		List<Task> tasks = taskRepository.findAll();
		return new MSAResponse(true, HttpStatus.OK, REC + UPD + SUCSFLY, tasks);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindzen.planner.service.LookaheadAccessService#createLookahead(com.
	 * mindzen.planner.dto.LookaheadDTO)
	 */
	@Transactional
	@Override
	public MSAResponse createLookahead(LookaheadDTO lookaheadDTO) {
		deleteTaskByIds(lookaheadDTO.taskIds, lookaheadDTO.subTaskIds, lookaheadDTO.scheduleIds);
		List<String> taskProjectCompanyIdAvailability = new ArrayList<>();
		List<String> subTaskProjectCompanyIdAvailability = new ArrayList<>();
		Project project = projectRepository.findById(lookaheadDTO.getProjectId()).get();
		if (null != lookaheadDTO.getTasks() && !lookaheadDTO.getTasks().isEmpty()) {
			Collections.reverse(lookaheadDTO.getTasks());
			List<TaskDTO> taskDTOList = lookaheadDTO.getTasks();
			for (TaskDTO taskDTO : taskDTOList) {
				Task task = null;
				if ((null != taskDTO.getId()) && (taskDTO.getId() > 0)) {
					Optional<Task> taskOption = taskRepository.findById(taskDTO.getId());
					if (taskOption.isPresent()) {
						task = taskOption.get();
					}
				}
				if (null != task) {
					task = LookaheadHelper.setTaskPayload(task, taskDTO);
					task.setChild(taskDTO.isChild());

				} else {
					task = new Task();
					task = LookaheadHelper.setTaskPayload(task, taskDTO);
					task.setChild(taskDTO.isChild());
					task.setActive(true);
					task.setProjectId(lookaheadDTO.getProjectId());
				}
				task = taskRepository.save(task);
				if (null != taskDTO.getSubTasks() && !taskDTO.getSubTasks().isEmpty()) {
					Collections.reverse(taskDTO.getSubTasks());
					List<SubTaskDTO> subTaskDTOList = taskDTO.getSubTasks();
					for (SubTaskDTO subTaskDTO : subTaskDTOList) {
						SubTask subTask = null;
						if ((null != subTaskDTO.getId()) && (subTaskDTO.getId() > 0)) {
							Optional<SubTask> subTaskOption = subTaskRepository.findById(subTaskDTO.getId());
							if (subTaskOption.isPresent()) {
								subTask = subTaskOption.get();
							}
						}
						if (null != subTask) {
							// subTask = LookaheadHelper.setSubTaskPayload(subTask,subTaskDTO);
						} else {
							subTask = new SubTask();
							subTask.setTask(task);
							subTask = LookaheadHelper.setSubTaskPayload(subTask, subTaskDTO);
							subTask.setActive(true);
						}
						subTask = subTaskRepository.save(subTask);
						generateSchedulesForNewTask(subTaskDTO, subTask, project);
					}
				}

			}
		}
		MSAResponse msaResponse = null;
		String msg = REC + INS + SUCSFLY;
		if (lookaheadDTO.isSubmit()
				&& (!"".equals(lookaheadDTO.getLookaheadDate()) || null != lookaheadDTO.getLookaheadDate())) {
			msaResponse = sendLookaheadPDFMail(lookaheadDTO);
			msg = msaResponse.getMessage();
		}

		if (null != lookaheadDTO.getTasks() && taskProjectCompanyIdAvailability.isEmpty()
				&& subTaskProjectCompanyIdAvailability.isEmpty()) {
			return new MSAResponse(true, HttpStatus.OK, msg, null);
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND,
					"Rsponsibility Missing Task Sno " + taskProjectCompanyIdAvailability.toString(),
					"Rsponsibility Missing Sub-Task Sno" + subTaskProjectCompanyIdAvailability.toString(), "E-001",
					null);
		}

	}

	// this block will handle to generate schedules for new Task
	private void generateSchedulesForNewTask(SubTaskDTO subTaskDTO, SubTask subTask, Project project) {
		if (null != subTaskDTO.getTaskStartDate() && !subTaskDTO.getTaskStartDate().equals("")
				&& null != subTaskDTO.getTaskFinishDate() && !subTaskDTO.getTaskFinishDate().equals("")) {
			LocalDate taskStartDate = dateToLocalDate(subTask.getTaskStartDate());
			LocalDate taskEndDate = dateToLocalDate(subTask.getExpectedCompletionDate());
			List<LocalDate> betweenDays = Stream.iterate(taskStartDate, d -> d.plusDays(1))
					.limit(ChronoUnit.DAYS.between(taskStartDate, taskEndDate) + 1).collect(Collectors.toList());
			Long subTaskId = subTask.getId();
			List<ScheduleDTO> scheduleDTOList = subTaskDTO.getSchedules();
			List<Schedule> scheduleList = new ArrayList<>();
			List<String> leaveDaysList = new ArrayList<>(
					Arrays.asList(project.getLeaveDays().toUpperCase().split(",")));

			betweenDays.forEach(localDate -> {
				if (!leaveDaysList.contains(localDate.getDayOfWeek().toString())) {
					Schedule schedule = new Schedule();
					List<Schedule> subTaskAlreadyExistsInSchedules = scheduleRepository
							.findBySubTaskId(subTask.getId());
					if (subTaskAlreadyExistsInSchedules != null) {
						for (Schedule scheduleListFromSubTask : subTaskAlreadyExistsInSchedules) {
							schedule.setId(scheduleListFromSubTask.getId());
							scheduleRepository.delete(schedule);
						}
					}
					schedule.setWorkerCount(0L);
					schedule.setWorkingDate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
					schedule.setSubTask(subTask);
					scheduleList.add(schedule);
				}
			});
			if (scheduleDTOList == null)
				scheduleDTOList = new ArrayList<>();
			for (ScheduleDTO scheduleDTO : scheduleDTOList) {
				Schedule schedule = new Schedule();
				schedule.setSubTask(subTask);
				if (null != scheduleDTO.getWorkerCount())
					schedule.setWorkerCount(scheduleDTO.getWorkerCount());
				else
					schedule.setWorkerCount(0L);

				if (null != scheduleDTO.getWorkingDate())
					schedule.setWorkingDate(DateUtil.stringToDateViaInstant(scheduleDTO.getWorkingDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));

				scheduleList.add(schedule);

			}

			scheduleList.forEach(schedule -> {
				Optional<List<Schedule>> scheduleOptional = scheduleRepository.findBySubTaskIdAndWorkingDate(subTaskId,
						schedule.getWorkingDate());
				if (!scheduleOptional.isPresent())
					scheduleRepository.save(schedule);
			});
		}
	}

	private void generateSchedulesForNewTaskFromUpload(SubTaskDTO subTaskDTO, SubTask subTask, Project project) {
		if (null != subTaskDTO.getTaskStartDate() && !subTaskDTO.getTaskStartDate().equals("")
				&& null != subTaskDTO.getTaskFinishDate() && !subTaskDTO.getTaskFinishDate().equals("")) {
			LocalDate taskStartDate = dateToLocalDate(subTask.getTaskStartDate());
			LocalDate taskEndDate = dateToLocalDate(subTask.getExpectedCompletionDate());
			List<LocalDate> betweenDays = Stream.iterate(taskStartDate, d -> d.plusDays(1))
					.limit(ChronoUnit.DAYS.between(taskStartDate, taskEndDate) + 1).collect(Collectors.toList());
			List<ScheduleDTO> scheduleDTOList = subTaskDTO.getSchedules();
			List<Schedule> scheduleList = new ArrayList<>();
			List<String> leaveDaysList = new ArrayList<>(
					Arrays.asList(project.getLeaveDays().toUpperCase().split(",")));
			List<Schedule> subTaskAlreadyExistsInSchedules = scheduleRepository.findBySubTaskId(subTask.getId());
			if (subTaskAlreadyExistsInSchedules != null && !subTaskAlreadyExistsInSchedules.isEmpty()) {
				List<Long> scheduleIds = subTaskAlreadyExistsInSchedules.stream().map(subtaskIds -> subtaskIds.getId())
						.collect(Collectors.toList());
				if (scheduleIds != null && !scheduleIds.isEmpty()) {
					scheduleRepository.deleteExistsSchedule(scheduleIds);
				}
			}
			betweenDays.forEach(localDate -> {
				if (!leaveDaysList.contains(localDate.getDayOfWeek().toString())) {
					Schedule schedule = new Schedule();
					schedule.setWorkerCount(0L);
					schedule.setWorkingDate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
					schedule.setSubTask(subTask);
					scheduleList.add(schedule);
				}
			});
			if (scheduleDTOList == null)
				scheduleDTOList = new ArrayList<>();
			for (ScheduleDTO scheduleDTO : scheduleDTOList) {
				Schedule schedule = new Schedule();
				schedule.setSubTask(subTask);
				if (null != scheduleDTO.getWorkerCount())
					schedule.setWorkerCount(scheduleDTO.getWorkerCount());
				else
					schedule.setWorkerCount(0L);

				if (null != scheduleDTO.getWorkingDate())
					schedule.setWorkingDate(DateUtil.stringToDateViaInstant(scheduleDTO.getWorkingDate(),
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));

				scheduleList.add(schedule);

			}
			// List<Schedule> scheduleOptional =
			// scheduleRepository.findBySubTaskId(subTaskId);
			// List<Date> workingDates = scheduleList.stream().map(workingDate ->
			// workingDate.getWorkingDate())
			// .collect(Collectors.toList());
			//
			// List<Date>workingDatesInSubTask=scheduleOptional.stream().map(workingDateExists->workingDateExists.getWorkingDate())
			// .collect(Collectors.toList());
			scheduleList.forEach(schedule -> {

				// if (!scheduleOptional.isPresent())
				scheduleRepository.save(schedule);
			});
		}
	}

	public LocalDate dateToLocalDate(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindzen.planner.service.LookaheadAccessService#updateLookahead(com.
	 * mindzen.planner.dto.LookaheadDTO)
	 */
	@Transactional
	@Override
	public MSAResponse updateLookahead(LookaheadDTO lookaheadDTO) {
		deleteTaskByIds(lookaheadDTO.taskIds, lookaheadDTO.subTaskIds, lookaheadDTO.scheduleIds);
		Project project = projectRepository.findById(lookaheadDTO.getProjectId()).get();
		List<String> taskProjectCompanyIdAvailability = new ArrayList<>();
		List<String> subTaskProjectCompanyIdAvailability = new ArrayList<>();
		if (null != lookaheadDTO.getTasks() && !lookaheadDTO.getTasks().isEmpty()) {
			Collections.reverse(lookaheadDTO.getTasks());
			List<TaskDTO> taskDTOList = lookaheadDTO.getTasks();
			for (TaskDTO taskDTO : taskDTOList) {

				if (null != taskDTO.getId()) {
					Optional<Task> taskOptional = taskRepository.findById(taskDTO.getId());
					if (taskOptional.isPresent()) {
						Task task = taskOptional.get();
						task = LookaheadHelper.setTaskPayload(task, taskDTO);
						if (task.getsNo() != null) {
							Double sno = Double.valueOf(task.getsNo());
							if (sno >= 1000)
								sno = sno - 980;
							else
								sno = sno + 1;
							task.setsNo(sno.toString());
						} else
							task.setsNo("1");
						task = taskRepository.save(task);
						if (null != taskDTO.getSubTasks() && !taskDTO.getSubTasks().isEmpty()) {
							Collections.reverse(taskDTO.getSubTasks());
							List<SubTaskDTO> subTaskDTOList = taskDTO.getSubTasks();
							Task taskAssign = task; // global variable cant access so use locally
							for (SubTaskDTO subTaskDTO : subTaskDTOList) {
								if (null != subTaskDTO.getId()) {
									Optional<SubTask> subTaskOptional = subTaskRepository.findById(subTaskDTO.getId());
									if (subTaskOptional.isPresent()) {
										SubTask subTask = subTaskOptional.get();
										if (null != subTaskDTO.getTaskStartDate()
												&& !subTaskDTO.getTaskStartDate().equals("")
												&& null != subTaskDTO.getTaskFinishDate()
												&& !subTaskDTO.getTaskFinishDate().equals("") && subTaskDTO.isPopup()) {
											List<Object[]> limitDateList = scheduleRepository
													.getStartDateEndDate(subTask.getId());
											if (!limitDateList.isEmpty()) {
												try {
													String taskStartDate = DateUtil.convertDate(
															new SimpleDateFormat(
																	DateUtil.Formats.YYYYMMDD_HYPHEN.toString())
																			.parse(limitDateList.get(0)[0] + ""),
															DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
													String taskEndDate = DateUtil.convertDate(
															new SimpleDateFormat(
																	DateUtil.Formats.YYYYMMDD_HYPHEN.toString())
																			.parse(limitDateList.get(0)[1] + ""),
															DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
													if (subTaskDTO.getTaskStartDate().equals(taskStartDate)
															&& subTaskDTO.getTaskFinishDate().equals(taskEndDate))
														subTaskDTO.setPopup(false);
													else
														subTaskDTO.setPopup(true);
												} catch (Exception e) {
													log.error(
															"Date Conversion error & Msg: " + e.getLocalizedMessage());
												}
											} else
												subTaskDTO.setPopup(true);
										}
										subTask = LookaheadHelper.setSubTaskPayload(subTask, subTaskDTO);
										subTask.setTask(taskAssign);
										if (subTask.getsNo() != null) {
											Double sno = Double.valueOf(subTask.getsNo());
											if (sno >= 1000)
												sno = sno - 980;
											else
												sno = sno + 1;
											subTask.setsNo(sno.toString());
										} else
											subTask.setsNo("1");
										subTask = subTaskRepository.save(subTask);
										generateSchedulesForExistTask(subTaskDTO, subTask, task, project);
									}
								}
							}
						}

					}
				}
			}
		}
		String msg = REC + UPD + SUCSFLY;
		MSAResponse msaResponse = null;
		if (lookaheadDTO.isSubmit()
				&& (!"".equals(lookaheadDTO.getLookaheadDate()) || null != lookaheadDTO.getLookaheadDate())) {
			msaResponse = sendLookaheadPDFMail(lookaheadDTO);
			msg = msaResponse.getMessage();
		}

		if (null != lookaheadDTO.getTasks() && !lookaheadDTO.getTasks().isEmpty()
				&& taskProjectCompanyIdAvailability.isEmpty() && subTaskProjectCompanyIdAvailability.isEmpty()) {
			return new MSAResponse(true, HttpStatus.OK, msg, null);
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND,
					"Rsponsibility Missing Task Sno " + taskProjectCompanyIdAvailability.toString(),
					"Rsponsibility Missing Sub-Task Sno" + subTaskProjectCompanyIdAvailability.toString(), "E-001",
					null);
		}

	}

	private void generateSchedulesForExistTask(SubTaskDTO subTaskDTO, SubTask subTask, Task task, Project project) {
		List<Schedule> scheduleList = new ArrayList<>();
		List<Long> deleteScheduleIds = new ArrayList<>();
		Set<Date> scheduleDays = new HashSet<>();
		final SubTask subTaskAssign = subTask;
		List<String> leaveDaysList = new ArrayList<>(Arrays.asList(project.getLeaveDays().toUpperCase().split(",")));
		if (null != subTaskDTO.getTaskStartDate() && !subTaskDTO.getTaskStartDate().equals("")
				&& null != subTaskDTO.getTaskFinishDate() && !subTaskDTO.getTaskFinishDate().equals("")
				&& subTaskDTO.isPopup()) {
			LocalDate taskStartDate = dateToLocalDate(subTaskAssign.getTaskStartDate());
			LocalDate taskEndDate = dateToLocalDate(subTaskAssign.getExpectedCompletionDate());
			List<LocalDate> betweenDays = Stream.iterate(taskStartDate, d -> d.plusDays(1))
					.limit(ChronoUnit.DAYS.between(taskStartDate, taskEndDate) + 1).collect(Collectors.toList());
			betweenDays.forEach(localDate -> {
				if (!leaveDaysList.contains(localDate.getDayOfWeek().toString())) {
					Schedule schedule = new Schedule();
					schedule.setWorkerCount(0L);
					schedule.setWorkingDate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
					schedule.setSubTask(subTaskAssign);
					scheduleList.add(schedule);
				}
			});
		}
		if (null != subTaskDTO.getSchedules() && !subTaskDTO.getSchedules().isEmpty()) {
			List<ScheduleDTO> scheduleDTOList = subTaskDTO.getSchedules();
			scheduleDTOList.forEach(scheduleDTO -> {
				Schedule schedule = null;
				if (null != scheduleDTO.getId()) {
					Optional<Schedule> scheduleOptional = scheduleRepository.findById(scheduleDTO.getId());
					if (scheduleOptional.isPresent()) {
						schedule = scheduleOptional.get();
						if (null == scheduleDTO.getWorkerCount() && null == scheduleDTO.getWorkingDate()
								&& null != scheduleDTO.getId()) {
							deleteScheduleIds.add(scheduleDTO.getId());
						} else {
							if (null != scheduleDTO.getWorkerCount())
								schedule.setWorkerCount(scheduleDTO.getWorkerCount());
							else
								schedule.setWorkerCount(0L);
							if (null != scheduleDTO.getWorkingDate())
								schedule.setWorkingDate(DateUtil.stringToDateViaInstant(scheduleDTO.getWorkingDate(),
										DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							schedule.setSubTask(subTaskAssign);
							scheduleList.add(schedule);
						}
					}
				} else {
					schedule = new Schedule();
					if (null != scheduleDTO.getWorkingDate())
						schedule.setWorkingDate(DateUtil.stringToDateViaInstant(scheduleDTO.getWorkingDate(),
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					schedule.setSubTask(subTaskAssign);
					if (null != scheduleDTO.getWorkerCount()) {
						schedule.setWorkerCount(scheduleDTO.getWorkerCount());
						scheduleList.add(schedule);
					}
				}
			});
		}

		if (scheduleList != null && !scheduleList.isEmpty()) {
			Long subTaskId = subTaskAssign.getId();
			scheduleList.forEach(schedule -> {
				scheduleDays.add(schedule.getWorkingDate());
				Optional<List<Schedule>> scheduleOptional = scheduleRepository.findBySubTaskIdAndWorkingDate(subTaskId,
						schedule.getWorkingDate());
				if (!scheduleOptional.isPresent()) {
					scheduleRepository.save(schedule);
				} else {
					log.info("schedule already exists");
					if (!scheduleOptional.get().get(0).getWorkerCount().equals(schedule.getWorkerCount())) {
						Schedule schedules = scheduleOptional.get().get(0);
						schedules.setWorkerCount(schedule.getWorkerCount());
						scheduleRepository.save(schedule);
					}
				}
			});
		}
		deleteScheduleIds.forEach(scheduleId -> deleteScheduleById(scheduleId));
		if (scheduleDays != null && !scheduleDays.isEmpty()) {
			if (null != subTaskDTO.getTaskStartDate() && !subTaskDTO.getTaskStartDate().equals("")
					&& null != subTaskDTO.getTaskFinishDate() && !subTaskDTO.getTaskFinishDate().equals("")) {
				LocalDate taskStartDate = dateToLocalDate(subTaskAssign.getTaskStartDate());
				LocalDate taskEndDate = dateToLocalDate(subTaskAssign.getExpectedCompletionDate());
				List<LocalDate> betweenDays = Stream.iterate(taskStartDate, d -> d.plusDays(1))
						.limit(ChronoUnit.DAYS.between(taskStartDate, taskEndDate) + 1).collect(Collectors.toList());
				betweenDays.forEach(localDate -> {
					if (!leaveDaysList.contains(localDate.getDayOfWeek().toString()))
						scheduleDays.add(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
				});
			}
			if (!scheduleDays.isEmpty()) {
				Date taskStartDate = scheduleDays.stream().min(Date::compareTo).get();
				Date taskEndDate = scheduleDays.stream().max(Date::compareTo).get();
				subTask.setTaskStartDate(taskStartDate);
				subTask.setExpectedCompletionDate(taskEndDate);
				SubTask subTaskUpdate = subTaskRepository.save(subTask);
				// from gant via update will be update in task table also
				task.setTaskStartDate(taskStartDate);
				task.setExpectedCompletionDate(taskEndDate);
				task.setId(task.getId());
				Task taskUpdate = taskRepository.save(task);
				// upto this to save in task table also.
				deleteSubTaskSchedules(subTaskUpdate);
			}
		}
		if (subTaskDTO.getTaskStartDate() == null && subTaskDTO.getTaskFinishDate() == null && subTaskDTO.isPopup())
			scheduleRepository.deleteScheduleBySubTaskId(subTaskDTO.getId());
	}

	private void deleteSubTaskSchedules(SubTask subTaskAssign) {
		scheduleRepository.deleteScheduleBeforeStartDate(subTaskAssign.getId(), subTaskAssign.getTaskStartDate());
		scheduleRepository.deleteScheduleAfterEndDate(subTaskAssign.getId(), subTaskAssign.getExpectedCompletionDate());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mindzen.planner.service.LookaheadAccessService#lookaheadInfo(java.lang.
	 * Long)
	 */
	@Override
	public MSAResponse lookaheadInfo(Long projectId) {
		Long userId = UserInfoContext.getUserThreadLocal();
		LookaheadInfoDTO lookaheadInfoDTO = new LookaheadInfoDTO();
		String userTimeZone = projectUserRepository.findTimeZoneByUserId(userId);
		LocalDate currentLocalDate = DateUtil.utcZoneDateTimeToZoneDateTimeConvert(LocalDateTime.now(), userTimeZone)
				.toLocalDate();
		ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId, userId, true, false);
		if (null == projectUser)
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "User not associated with the project",
					null);
		Project project = projectRepository.findById(projectId).get();
		if (!"".equals(project.getLeaveDays()) && null != project.getLeaveDays())
			lookaheadInfoDTO.setLeaveDays(Arrays.asList(project.getLeaveDays().split(",")));
		lookaheadInfoDTO.setWeekStartDay(project.getWeekStartDay());
		lookaheadInfoDTO.setFloatDays(project.getFloatDays());
		if (project.getTrade() != null) {
			lookaheadInfoDTO.setTrade(Arrays.asList(project.getTrade().split(",")));
		} else {
			lookaheadInfoDTO.setTrade(Collections.emptyList());
		}
		if (project.getSpecDivision() != null) {
			lookaheadInfoDTO.setSpecDivision(Arrays.asList(project.getSpecDivision().split("\\|\\|")));
		} else {
			lookaheadInfoDTO.setSpecDivision(Collections.emptyList());
		}
		LocalDate localStartDate = DateUtil.weekStartDateFromLocalDate(currentLocalDate, project.getWeekStartDay());
		DateTimeFormatter dateTimeFormatter = DateUtil
				.getDateTimeFormatter(DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		String currentDate = DateUtil.getFormattedDate(currentLocalDate, dateTimeFormatter);
		String lookaheadDate = DateUtil.getFormattedDate(localStartDate, dateTimeFormatter);
		lookaheadInfoDTO.setCurrentDate(currentDate);
		lookaheadInfoDTO.setLookaheadDate(lookaheadDate);
		if (!"".equals(projectUser.getAccess()) || null != projectUser.getAccess())
			lookaheadInfoDTO.setLookaheadAccess(projectUser.getAccess().split(","));
		lookaheadInfoDTO.setProjectRole(projectUser.getProjectRole());
		if (null != projectUser.getProjectCompany()) {
			lookaheadInfoDTO.setProjectCompanyId(projectUser.getProjectCompany().getId());
			String projectCompanyName = projectCompanyRepository
					.findCompanyById(projectUser.getProjectCompany().getCompanyId());
			lookaheadInfoDTO.setProjectCompany(projectCompanyName);
		}
		return new MSAResponse(true, HttpStatus.OK, SUCS, lookaheadInfoDTO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mindzen.planner.service.LookaheadAccessService#lookaheadById(java.lang.
	 * Long, java.lang.Long)
	 */
	@Override
	public MSAResponse lookaheadById(Long taskId, Long subTaskId) {
		Boolean isDeleted = false;
		if ((null != taskId) || null != subTaskId) {
			if (null != taskId)
				isDeleted = deleteTaskById(taskId);
			if (null != subTaskId)
				isDeleted = deleteSubTaskById(subTaskId);
		}
		if (isDeleted) {
			return new MSAResponse(true, HttpStatus.OK, "Task Deleted Successfully", null);
		} else {
			return new MSAResponse(false, HttpStatus.NOT_FOUND, ERR, "E-001", null);
		}
	}

	/**
	 * Delete task by ids.
	 *
	 * @param taskIds     the task ids
	 * @param subTaskIds  the sub task ids
	 * @param scheduleIds the schedule ids
	 * @return true, if successful
	 */
	public boolean deleteTaskByIds(Long[] taskIds, Long[] subTaskIds, Long[] scheduleIds) {
		if (null != taskIds && taskIds.length > 0) {
			Arrays.asList(taskIds).stream().forEach(taskId -> {
				if (null != taskId)
					deleteTaskById(taskId);
			});
		}

		if (null != subTaskIds && subTaskIds.length > 0) {
			Arrays.asList(subTaskIds).stream().forEach(subTaskId -> {
				if (null != subTaskId)
					deleteSubTaskById(subTaskId);
			});
		}

		if (null != scheduleIds && scheduleIds.length > 0) {
			Arrays.asList(scheduleIds).stream().forEach(scheduleId -> {
				if (null != scheduleId)
					deleteScheduleById(scheduleId);
			});
		}
		return true;
	}

	/**
	 * Delete task by id.
	 *
	 * @param taskId the task id
	 * @return true, if successful
	 */
	@Transactional
	private boolean deleteTaskById(Long taskId) {
		Optional<Task> taskOption = taskRepository.findById(taskId);
		if (taskOption.isPresent()) {
			Task task = taskOption.get();
			task.setActive(false);
			taskRepository.save(task);
			subTaskRepository.setInActivateByTaskId(task);
			return true;
		}
		return false;
	}

	/**
	 * Delete sub task by id.
	 *
	 * @param subTaskId the sub task id
	 * @return true, if successful
	 */
	@Transactional
	private boolean deleteSubTaskById(Long subTaskId) {
		Optional<SubTask> subTaskOption = subTaskRepository.findById(subTaskId);
		if (subTaskOption.isPresent()) {
			SubTask subTask = subTaskOption.get();
			subTask.setActive(false);
			subTaskRepository.save(subTask);
			return true;
		}
		return false;
	}

	/**
	 * Delete schedule by id.
	 *
	 * @param scheduleId the schedule id
	 * @return true, if successful
	 */
	private boolean deleteScheduleById(Long scheduleId) {
		scheduleRepository.deleteScheduleById(scheduleId);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindzen.planner.service.LookaheadAccessService#jasperReportData(com.
	 * mindzen.planner.dto.LookaheadDTO)
	 */
	@Override
	public MSAResponse jasperReportData(LookaheadListDTO lookaheadListDTO) {
		Long userId = UserInfoContext.getUserThreadLocal();
		Long projectId = lookaheadListDTO.getProjectId();
		ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId, userId, true, false);

		if (null == projectUser)
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "User not associated with the project",
					null);
		int reportDays = 28;

		LocalDate lookaheadStartDate = DateUtil.stringToLocalDateViaInstant(lookaheadListDTO.getLookaheadDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());

		LocalDate localStartDate = DateUtil.weekStartDateFromLocalDate(lookaheadStartDate,
				projectUser.getProject().getWeekStartDay());

		LocalDate localEndDate = DateUtil.plusNumberDaysInLocalDate(localStartDate, reportDays - 1);

		Date startDate = DateUtil.convertToDateViaInstant(localStartDate);
		Date endDate = DateUtil.convertToDateViaInstant(localEndDate);

		List<LocalDate> localDates = DateUtil.getLocalDatesList(localStartDate, reportDays);
		log.debug("Local Dates: " + localDates);

		LookaheadJasperReportRootDTO lookaheadJasperReportRootDTO = new LookaheadJasperReportRootDTO();
		List<LookaheadJasperReportDetailDTO> lookaheadReport = new ArrayList<>();

		String week_1_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate);
		String week_2_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate.plusDays(6));
		String week_3_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate.plusDays(13));
		String week_4_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate.plusDays(20));

		Project project = projectRepository.findById(projectId).get();
		// String projectCompany =
		// projectUserRepository.findCompanyNameByProjectIdAndUserId(projectId,
		// project.getCreatedBy()).trim();
		String projectCompany = projectUserRepository.findcompanyNameByCreatedBy(project.getCreatedBy());

		String projectName = project.getName();
		final String projectLocation = LookaheadHelper.fullProjectLocation(project.getCity(), project.getState());
		List<Object[]> tasks = null;
		List<Object[]> taskAndSubTasks = null;
		String projectRole = projectUser.getProjectRole();
		Long projectCompanyId = projectUser.getProjectCompany().getId();

		// List<Object[]> schedules = crosstabData(localDates, lookaheadStartDate,
		// localEndDate, projectId, userId, projectRole);

		// check availability of schedules available or not
		List<Object[]> schedules = crosstabData(localDates, lookaheadStartDate, localEndDate, projectId,
				projectCompanyId, projectRole);

		if (null == schedules || schedules.isEmpty())
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, " No scheduled Task and sub Task in Lookahead  ",
					null);

		// if(!Constants.SUB_CONTRACTOR.equals(projectRole)) {
		if (!projectRole.contains(Constants.SC_USERS)) {
			tasks = taskRepository.findByWorkingDate(startDate, endDate, projectId);
			taskAndSubTasks = taskRepository.findTaskAndSubtasks(startDate, endDate, projectId);
		} else {
			// tasks = taskRepository.findByWorkingDate(startDate, endDate, projectId,
			// userId);
			// taskAndSubTasks = taskRepository.findTaskAndSubtasks(startDate, endDate,
			// projectId, userId);
			tasks = taskRepository.findByWorkingDate(startDate, endDate, projectId, projectCompanyId);
			taskAndSubTasks = taskRepository.findTaskAndSubtasks(startDate, endDate, projectId, projectCompanyId);
		}

		String timeZone = projectUserRepository.findTimeZoneByUserId(userId);
		LocalDateTime currentTimeByUserZone = DateUtil.utcZoneDateTimeToZoneDateTimeConvert(LocalDateTime.now(),
				timeZone);

		Set<Long> taskIdSet = new HashSet<>();
		for (Object[] task : tasks) {
			Long taskId = ((BigInteger) task[0]).longValue();
			taskIdSet.add(taskId);
			if (((boolean) task[7])) {
				LookaheadJasperReportDetailDTO lookaheadJasperData = new LookaheadJasperReportDetailDTO();
				lookaheadJasperData.setSno((String) task[8]);
				lookaheadJasperData.setTaskId(taskId);
				lookaheadJasperData.setSubTaskId(1L);
				lookaheadJasperData.setTaskDescription((String) task[1]);
				if (!((boolean) task[7]))
					lookaheadJasperData.setResponsibility((String) task[5]);
				else
					lookaheadJasperData.setResponsibility("");
				lookaheadJasperData.setComments((String) task[6] == null ? "" : (String) task[6]);
				lookaheadJasperData = LookaheadHelper.loadProjectInfo(lookaheadJasperData, projectCompany,
						projectLocation, projectName);
				lookaheadJasperData = LookaheadHelper.loadWeekMonth(lookaheadJasperData, week_1_month, week_2_month,
						week_3_month, week_4_month);
				lookaheadJasperData = LookaheadHelper.fillDayAndDate(lookaheadJasperData, localDates);
				lookaheadJasperData = LookaheadHelper.fillEmptyDayValues(lookaheadJasperData);
				lookaheadJasperData.setCurrentTime(
						currentTimeByUserZone.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm")).toString());
				lookaheadJasperData.setTask(true);

				lookaheadReport.add(lookaheadJasperData);
			}
		}

		Set<Long> subTaskIdSet = new HashSet<>();

		taskAndSubTasks.stream().forEach(taskAndSubTask -> {

			schedules.stream().filter(
					schedule -> ((Integer) schedule[0]).longValue() == (((BigInteger) taskAndSubTask[6]).longValue()))
					.forEach(schedule -> {

						String taskDescription = "";
						String responsibility = "";
						String comments = "";
						boolean isTask = false;
						String sno = null;
						Long taskId = ((BigInteger) taskAndSubTask[0]).longValue();
						Long subTaskId = ((BigInteger) taskAndSubTask[6]).longValue();
						taskIdSet.add(taskId);
						subTaskIdSet.add(subTaskId);

						if (!(boolean) taskAndSubTask[4]) {
							sno = (String) taskAndSubTask[5];
							taskDescription = (String) taskAndSubTask[1];
							responsibility = (String) taskAndSubTask[2];
							comments = (String) taskAndSubTask[3];
							isTask = true;
						} else {
							sno = (String) taskAndSubTask[10];
							taskDescription = (String) taskAndSubTask[7];
							responsibility = (String) taskAndSubTask[8];
							comments = (String) taskAndSubTask[9];
						}

						LookaheadJasperReportDetailDTO lookaheadJasperData = new LookaheadJasperReportDetailDTO();
						lookaheadJasperData.setCurrentTime(currentTimeByUserZone
								.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm")).toString());
						lookaheadJasperData.setTask(isTask);
						lookaheadJasperData.setSno(sno);
						lookaheadJasperData.setTaskId(taskId);
						lookaheadJasperData.setSubTaskId(subTaskId);
						lookaheadJasperData.setTaskDescription(taskDescription);
						lookaheadJasperData.setResponsibility(responsibility);
						lookaheadJasperData.setComments(comments == null ? "" : comments);

						lookaheadJasperData = LookaheadHelper.loadProjectInfo(lookaheadJasperData, projectCompany,
								projectLocation, projectName);
						lookaheadJasperData = LookaheadHelper.loadWeekMonth(lookaheadJasperData, week_1_month,
								week_2_month, week_3_month, week_4_month);
						lookaheadJasperData = LookaheadHelper.fillDayAndDate(lookaheadJasperData, localDates);

						lookaheadJasperData.setDay_1_value(LookaheadHelper.nullCheck(schedule[1]));
						lookaheadJasperData.setDay_2_value(LookaheadHelper.nullCheck(schedule[2]));
						lookaheadJasperData.setDay_3_value(LookaheadHelper.nullCheck(schedule[3]));
						lookaheadJasperData.setDay_4_value(LookaheadHelper.nullCheck(schedule[4]));
						lookaheadJasperData.setDay_5_value(LookaheadHelper.nullCheck(schedule[5]));
						lookaheadJasperData.setDay_6_value(LookaheadHelper.nullCheck(schedule[6]));
						lookaheadJasperData.setDay_7_value(LookaheadHelper.nullCheck(schedule[7]));
						lookaheadJasperData.setDay_8_value(LookaheadHelper.nullCheck(schedule[8]));
						lookaheadJasperData.setDay_9_value(LookaheadHelper.nullCheck(schedule[9]));
						lookaheadJasperData.setDay_10_value(LookaheadHelper.nullCheck(schedule[10]));
						lookaheadJasperData.setDay_11_value(LookaheadHelper.nullCheck(schedule[11]));
						lookaheadJasperData.setDay_12_value(LookaheadHelper.nullCheck(schedule[12]));
						lookaheadJasperData.setDay_13_value(LookaheadHelper.nullCheck(schedule[13]));
						lookaheadJasperData.setDay_14_value(LookaheadHelper.nullCheck(schedule[14]));
						lookaheadJasperData.setDay_15_value(LookaheadHelper.nullCheck(schedule[15]));
						lookaheadJasperData.setDay_16_value(LookaheadHelper.nullCheck(schedule[16]));
						lookaheadJasperData.setDay_17_value(LookaheadHelper.nullCheck(schedule[17]));
						lookaheadJasperData.setDay_18_value(LookaheadHelper.nullCheck(schedule[18]));
						lookaheadJasperData.setDay_19_value(LookaheadHelper.nullCheck(schedule[19]));
						lookaheadJasperData.setDay_20_value(LookaheadHelper.nullCheck(schedule[20]));
						lookaheadJasperData.setDay_21_value(LookaheadHelper.nullCheck(schedule[21]));
						lookaheadJasperData.setDay_22_value(LookaheadHelper.nullCheck(schedule[22]));
						lookaheadJasperData.setDay_23_value(LookaheadHelper.nullCheck(schedule[23]));
						lookaheadJasperData.setDay_24_value(LookaheadHelper.nullCheck(schedule[24]));
						lookaheadJasperData.setDay_25_value(LookaheadHelper.nullCheck(schedule[25]));
						lookaheadJasperData.setDay_26_value(LookaheadHelper.nullCheck(schedule[26]));
						lookaheadJasperData.setDay_27_value(LookaheadHelper.nullCheck(schedule[27]));
						lookaheadJasperData.setDay_28_value(LookaheadHelper.nullCheck(schedule[28]));
						lookaheadReport.add(lookaheadJasperData);
					});
		});

		if (appConfig.isLookaheadPdfReportEmptySchedule()) {
			log.info("Including empty schedules");
			List<Object[]> noScheduledTasks = null;
			List<Object[]> noScheduledSubTasks = null;
			// String createdByQuery = "AND t.created_by = "+userId;
			String createdByQuery = "AND st.project_company_id = " + projectCompanyId;
			String and = " AND ";
			// if(!Constants.SUB_CONTRACTOR.equals(projectUser.getProjectRole())) {
			if (!projectUser.getProjectRole().contains(Constants.SC_USERS)) {
				createdByQuery = "";
			}
			String taskGroupBy = " GROUP BY t.id ORDER BY t.id";

			String noScheduledTaskQuery = "SELECT t.id AS task_id, t.description AS task_description, "
					+ "t.visibility AS task_visibility, "
					+ "t.trade AS task_trade, t.task_type, t.ball_in_court AS task_ball_in_court, t.note AS task_note, "
					+ "t.is_child, t.sno AS sno FROM sub_task st JOIN task t ON st.task_id=t.id " + "WHERE "
					+ "st.id not in (select sub_task_id from schedule) " + and + "t.project_id = " + projectId + "\n"
					+ createdByQuery + and + "t.is_active=true\n" + and + "st.is_active=true\n";
			noScheduledTasks = generateDynamicQueryResult("st", lookaheadListDTO, noScheduledTaskQuery, taskGroupBy);

			for (Object[] task : noScheduledTasks) {
				Long taskId = ((BigInteger) task[0]).longValue();
				if (!taskIdSet.contains(taskId)) {
					taskIdSet.add(taskId);
					LookaheadJasperReportDetailDTO lookaheadJasperData = new LookaheadJasperReportDetailDTO();
					lookaheadJasperData.setSno((String) task[8]);
					lookaheadJasperData.setTaskId(((BigInteger) task[0]).longValue());
					lookaheadJasperData.setSubTaskId(1L);
					lookaheadJasperData.setTaskDescription((String) task[1]);
					lookaheadJasperData.setResponsibility((String) task[5]);
					lookaheadJasperData.setComments((String) task[6]);

					lookaheadJasperData = LookaheadHelper.loadProjectInfo(lookaheadJasperData, projectCompany,
							projectLocation, projectName);
					lookaheadJasperData = LookaheadHelper.loadWeekMonth(lookaheadJasperData, week_1_month, week_2_month,
							week_3_month, week_4_month);
					lookaheadJasperData = LookaheadHelper.fillDayAndDate(lookaheadJasperData, localDates);
					lookaheadJasperData = LookaheadHelper.fillEmptyDayValues(lookaheadJasperData);

					lookaheadReport.add(lookaheadJasperData);
				}
			}

			String subTaskGroupBy = " GROUP BY st.id ORDER BY st.id";

			String noScheduledSubTaskQuery = "SELECT st.id AS sub_task_id, st.description AS sub_task_description, "
					+ "st.visibility AS sub_task_visibility, st.trade AS sub_task_trade, "
					+ "st.task_type AS sub_task_type, st.ball_in_court AS sub_task_ball_in_court, "
					+ "st.note AS sub_task_note, st.task_id, st.sno AS sno "
					+ "FROM sub_task st JOIN task t ON st.task_id=t.id " + "WHERE "
					+ "st.id not in (select sub_task_id from schedule)" + and + "t.project_id = " + projectId + "\n"
					+ createdByQuery + and + "t.is_active=true\n" + and + "st.is_active=true\n";
			noScheduledSubTasks = generateDynamicQueryResult("st", lookaheadListDTO, noScheduledSubTaskQuery,
					subTaskGroupBy);

			for (Object[] subTask : noScheduledSubTasks) {
				Long subTaskId = ((BigInteger) subTask[0]).longValue();
				if (!subTaskIdSet.contains(subTaskId)) {
					subTaskIdSet.add(subTaskId);
					LookaheadJasperReportDetailDTO lookaheadJasperData = new LookaheadJasperReportDetailDTO();
					lookaheadJasperData.setSno((String) subTask[8]);
					lookaheadJasperData.setTaskId(subTaskId);
					lookaheadJasperData.setSubTaskId(1L);
					lookaheadJasperData.setTaskDescription((String) subTask[1]);
					lookaheadJasperData.setResponsibility((String) subTask[5]);
					lookaheadJasperData.setComments((String) subTask[6]);

					lookaheadJasperData = LookaheadHelper.loadProjectInfo(lookaheadJasperData, projectCompany,
							projectLocation, projectName);
					lookaheadJasperData = LookaheadHelper.loadWeekMonth(lookaheadJasperData, week_1_month, week_2_month,
							week_3_month, week_4_month);
					lookaheadJasperData = LookaheadHelper.fillDayAndDate(lookaheadJasperData, localDates);
					lookaheadJasperData = LookaheadHelper.fillEmptyDayValues(lookaheadJasperData);

					lookaheadReport.add(lookaheadJasperData);
				}
			}
		}

		List<LookaheadJasperReportDetailDTO> lookaheadSortedReport = lookaheadReport.stream()
				.sorted(Comparator.comparingLong(LookaheadJasperReportDetailDTO::getTaskId)
						.thenComparingLong(LookaheadJasperReportDetailDTO::getSubTaskId))
				.parallel().collect(Collectors.toList());

		lookaheadJasperReportRootDTO.setLookaheads(lookaheadSortedReport);

		long reportTimestamp = ZonedDateTime.now().toInstant().toEpochMilli();
		String fileName = "lookahead-" + reportTimestamp + ".json";
		String json = LookaheadHelper.writeJsonToFile(lookaheadJasperReportRootDTO, fileName);
		String senderName = projectRepository.findUserNameByUserId(userId);
		if (null != json || "".equals(json))
			if (null != lookaheadListDTO.getEmail() && lookaheadListDTO.getEmail().length > 0) {
				List<String> emailList = Arrays.asList(lookaheadListDTO.getEmail()).stream().map(String::toLowerCase)
						.collect(Collectors.toList());
				messageQueueService.sendJsonReport(senderName, projectName, fileName, String.join(",", emailList),
						reportTimestamp, json, "lookaheadReport");
			} else
				log.info("no emil found");
		else
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "Lookahead Report Json Not created", null);
		return new MSAResponse(true, HttpStatus.OK, "Mail Sent " + SUCSFLY, lookaheadJasperReportRootDTO);
	}

	/**
	 * Crosstab data.
	 *
	 * @param localDates       the local dates
	 * @param startDate        the start date
	 * @param endDate          the end date
	 * @param projectId        the project id
	 * @param projectCompanyId the project company id
	 * @param projectRole      the project role
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	// public List<Object[]> crosstabData(List<LocalDate> localDates, LocalDate
	// startDate, LocalDate endDate, Long projectId, Long userId, String
	// projectRole) {
	public List<Object[]> crosstabData(List<LocalDate> localDates, LocalDate startDate, LocalDate endDate,
			Long projectId, Long projectCompanyId, String projectRole) {
		String crosstabQuery = LookaheadHelper.pivotQuery(localDates, startDate, endDate, projectId, projectCompanyId,
				projectRole);
		Query query = entityManager.createNativeQuery(crosstabQuery);
		return query.getResultList();
	}

	/**
	 * Send lookahead PDF mail.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the MSA response
	 */
	private MSAResponse sendLookaheadPDFMail(LookaheadDTO lookaheadDTO) {
		LookaheadListDTO lookaheadListDTO = new LookaheadListDTO();
		Long projectId = lookaheadDTO.getProjectId();
		Long userId = UserInfoContext.getUserThreadLocal();

		ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId, userId, true, false);
		if (null == projectUser)
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "User not associated with the project",
					null);

		List<String[]> projectRole = null;
		String responseMessage = null;
		// if(Constants.GENERAL_CONTRACTOR.equals(projectUser.getProjectRole())) {
		if (projectUser.getProjectRole().contains(Constants.GC_USERS)) {
			projectRole = projectRepository.findUserMailByProjectRole(projectId, Constants.OWNER);
			responseMessage = "Plan Submitted " + SUCSFLY + ". Mail has been sent to Project Owner";
		}
		// else if(Constants.SUB_CONTRACTOR.equals(projectUser.getProjectRole())) {
		else if (projectUser.getProjectRole().contains(Constants.SC_USERS)) {
			projectRole = projectRepository.findUserMailByProjectRole(projectId, Constants.GC_PROJECT_ADMIN);
			responseMessage = "Plan Submitted " + SUCSFLY + ". Mail has been sent to GC-Project Admin";
		}
		String email = "";
		for (Object[] strings : projectRole) {
			email += (String) strings[1] + ",";
		}
		lookaheadListDTO.setLookaheadDate(lookaheadDTO.getLookaheadDate());
		lookaheadListDTO.setEmail(email.split(","));
		lookaheadListDTO.setProjectId(projectId);
		// MSAResponse msaResponse = jasperReportData(lookaheadListDTO);

		LookaheadFilterDTO filter = new LookaheadFilterDTO();
		filter.setScheduleAvailability("scheduled");
		lookaheadListDTO.setFilter(filter);
		MSAResponse msaResponse = downloadPDF(lookaheadListDTO);

		if (msaResponse.isSuccess()) {
			log.info("Project report mail sent to " + projectUser.getProjectRole());
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, responseMessage, null);
		} else {
			log.error("Project report not sent to " + projectUser.getProjectRole());
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR,
					"Plan Submitted " + SUCSFLY + " But Mail Not Sent. " + msaResponse.getMessage(), null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mindzen.planner.service.LookaheadAccessService#listLookahead(com.mindzen.
	 * planner.dto.LookaheadListDTO)
	 *
	 * , t.specDivision AS spec_division
	 */
	@Override
	/*
	 * 
	 * Don't change the any condition and code because progress completion also use
	 * this service Some condition for percent complete check twice then update
	 * 
	 */
	public MSAResponse listLookahead(LookaheadListDTO lookaheadListDTO, Long daysCount, boolean isLookahead) {
		// Long userId = UserInfoContext.getUserThreadLocal();
		Long start = System.currentTimeMillis();
		Long projectId = lookaheadListDTO.getProjectId();
		List<Object[]> projectCompaniesList = projectCompanyRepository.findCompaniesByProjectId(projectId);
		ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId,
				lookaheadListDTO.getUserId(), true, false);
		if (null == projectUser)
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "User not associated with the project",
					null);
		Long projectCompanyId = projectUser.getProjectCompany().getId();
		LookaheadDTO lookaheadDTO = new LookaheadDTO();
		LocalDate lookaheadStartDate = DateUtil.stringToLocalDateViaInstant(lookaheadListDTO.getLookaheadDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		LocalDate localEndDate = DateUtil.plusNumberDaysInLocalDate(lookaheadStartDate,
				Integer.valueOf(daysCount.toString()));

		Date startDate = DateUtil.convertToDateViaInstant(lookaheadStartDate);
		Date endDate = DateUtil.convertToDateViaInstant(localEndDate);

		List<Object[]> tasks = new ArrayList<>();
		List<Object[]> noScheduledTasks = new ArrayList<>();
		List<Object[]> subTasks = new ArrayList<>();
		List<Object[]> noScheduledSubTasks = new ArrayList<>();
		List<Object[]> schedules = new ArrayList<>();

		String scheduleAvailability = lookaheadListDTO.getFilter().getScheduleAvailability();
		boolean availableSchedules = false;
		boolean emptySchedules = false;
		if (Constants.ALL.equalsIgnoreCase(scheduleAvailability)) {
			availableSchedules = true;
			emptySchedules = true;
		} else if (Constants.SCHEDULED.equalsIgnoreCase(scheduleAvailability))
			availableSchedules = true;
		else if (Constants.UNSCHEDULED.equalsIgnoreCase(scheduleAvailability))
			emptySchedules = true;

		String createdByQuery = "AND st.project_company_id IN ( " + projectCompanyId + " ) ";
		String and = " AND ";
		if (!projectUser.getProjectRole().contains(Constants.SC_USERS)) {
			createdByQuery = "";
			if (!Constants.UNSCHEDULED.equalsIgnoreCase(scheduleAvailability))
				schedules = scheduleRepository.findByWorkingDate(startDate, endDate, projectId);
		} else {
			if (!Constants.UNSCHEDULED.equalsIgnoreCase(scheduleAvailability))
				schedules = scheduleRepository.findByWorkingDate(startDate, endDate, projectId, projectCompanyId);
		}
		String taskGroupBy = " GROUP BY t.id ORDER BY t.id desc";
		String subTaskGroupBy = " GROUP BY st.id ,st.task_start_date, st.expected_completion_date,st.required_completion_date,st.dead_line_date, t.activity_id, t.activity_description, t.import_start_date, t.import_finish_date, t.import_target_completion_date, t.import_deadline_date, t.import_notes ORDER BY st.id, st.task_id ";
		if (availableSchedules) {
			StringBuilder taskQueryBuilder = new StringBuilder();
			StringBuilder subTaskQueryBuilder = new StringBuilder();
			String taskQuery = "SELECT t.id AS task_id, t.description AS task_description, t.visibility AS task_visibility,t.trade AS task_trade,"
					+ " t.task_type, t.ball_in_court AS task_ball_in_court,\n"
					+ "t.note AS task_note, t.is_child, t.sno AS sno,"
					+ "t.project_company_id AS projectCompanyId , t.task_start_date,t.expected_completion_date,"
					+ "t.required_completion_date,t.dead_line_date, t.spec_division AS specDivision, "
					+ "t.activity_id, t.activity_description, t.import_start_date, "
					+ "t.import_finish_date, t.import_target_completion_date, t.import_deadline_date, t.import_notes,t.is_lookahead_change   "
					+ "FROM schedule s\n" + "JOIN sub_task st\n" + "ON s.sub_task_id=st.id\n" + "JOIN task t\n"
					+ "ON st.task_id=t.id\n" + "WHERE\n" + " t.project_id = " + projectId + "\n" + createdByQuery + and
					+ "t.is_active=true\n" + and + "st.is_active=true\n";
			String subTaskQuery = "SELECT st.id AS sub_task_id, st.description AS sub_task_description, st.visibility AS sub_task_visibility, \n"
					+ "st.trade AS sub_task_trade, st.task_type AS sub_task_type, \n"
					+ "st.ball_in_court AS sub_task_ball_in_court, st.note AS sub_task_note, st.task_id, \n"
					+ "st.sno AS sno \n"
					+ " ,st.project_company_id , min(st.task_start_date) as tsd ,max(st.expected_completion_date) as ecd ,"
					+ "max(st.required_completion_date) as rcd ,max(st.dead_line_date) as dld, st.spec_division AS specDivision,   "
					+ "st.activity_id, st.activity_description, st.import_start_date, "
					+ "st.import_finish_date, st.import_target_completion_date, st.import_deadline_date,st.import_notes,st.is_lookahead_change  "
					+ "FROM schedule s \n" + "JOIN sub_task st \n" + "ON s.sub_task_id=st.id \n" + "JOIN task t \n"
					+ "ON st.task_id=t.id \n" + "WHERE \n" + " t.project_id = " + projectId + "\n" + createdByQuery
					+ and + "t.is_active=true \n" + and + "st.is_active=true\n";
			if (!lookaheadListDTO.isSearch()) {
				String rangeQuery = " And s.working_date BETWEEN '" + lookaheadStartDate + "'" + and + "'"
						+ localEndDate + "' ";
				taskQueryBuilder.append(taskQuery + rangeQuery);
				subTaskQueryBuilder.append(subTaskQuery + rangeQuery);
			} else {
				taskQueryBuilder.append(taskQuery);
				subTaskQueryBuilder.append(subTaskQuery);
			}
			CompletableFuture<List<Object[]>> taskObject = CompletableFuture.supplyAsync(() -> {
				return generateDynamicQueryResult("st", lookaheadListDTO, taskQueryBuilder.toString(), taskGroupBy);
			});
			CompletableFuture<List<Object[]>> subTaskObject = CompletableFuture.supplyAsync(() -> {
				return generateDynamicQueryResult("st", lookaheadListDTO, subTaskQueryBuilder.toString(),
						subTaskGroupBy);
			});
			try {
				tasks = taskObject.get();
				subTasks = subTaskObject.get();
			} catch (Exception e) {
				e.printStackTrace();
				log.info(" schedule availabile task and subtask ");
			}
		}
		if (emptySchedules) {
			String noScheduledTaskQuery = "SELECT t.id AS task_id, t.description AS task_description, "
					+ "t.visibility AS task_visibility, "
					+ "t.trade AS task_trade, t.task_type, t.ball_in_court AS task_ball_in_court, t.note AS task_note, "
					+ "t.is_child, t.sno AS sno, "
					+ "t.project_company_id,t.task_start_date,t.expected_completion_date,t.required_completion_date,t.dead_line_date, t.spec_division AS specDivision,  "
					+ "t.activity_id, t.activity_description, t.import_start_date, "
					+ "t.import_finish_date, t.import_target_completion_date, t.import_deadline_date, t.import_notes,t.is_lookahead_change "
					+ "FROM sub_task st JOIN task t ON st.task_id=t.id left join schedule se on se.sub_task_id = st.id "
					+ "WHERE se.id is null and " + "t.project_id = " + projectId + "\n" + createdByQuery + and
					+ "t.is_active=true\n" + and + "st.is_active=true\n";
			String noScheduledSubTaskQuery = "SELECT st.id AS sub_task_id, st.description AS sub_task_description, "
					+ "st.visibility AS sub_task_visibility, st.trade AS sub_task_trade, "
					+ "st.task_type AS sub_task_type, st.ball_in_court AS sub_task_ball_in_court, "
					+ "st.note AS sub_task_note, st.task_id, st.sno AS sno "
					+ " ,st.project_company_id  , min(st.task_start_date) as tsd ,max(st.expected_completion_date) as ecd ,"
					+ "max(st.required_completion_date) as rcd ,max(st.dead_line_date) as dld,st.spec_division AS specDivision, "
					+ "t.activity_id, t.activity_description, t.import_start_date, "
					+ "t.import_finish_date, t.import_target_completion_date, t.import_deadline_date, t.import_notes ,st.is_lookahead_change "
					+ "FROM sub_task st JOIN task t ON st.task_id=t.id left join schedule se on se.sub_task_id = st.id "
					+ "WHERE " + "se.id is null" + and + "t.project_id = " + projectId + "\n" + createdByQuery + and
					+ "t.is_active=true\n" + and + "st.is_active=true\n";
			CompletableFuture<List<Object[]>> taskObject = CompletableFuture.supplyAsync(() -> {
				return generateDynamicQueryResult("st", lookaheadListDTO, noScheduledTaskQuery, taskGroupBy);
			});
			CompletableFuture<List<Object[]>> subTaskObject = CompletableFuture.supplyAsync(() -> {
				return generateDynamicQueryResult("st", lookaheadListDTO, noScheduledSubTaskQuery, subTaskGroupBy);
			});
			try {
				noScheduledTasks = taskObject.get();
				noScheduledSubTasks = subTaskObject.get();
			} catch (Exception e) {
				e.printStackTrace();
				log.info(" unschedule availabile task and subtask ");
			}
		}

		List<Object[]> mergedTask = Stream.concat(tasks.stream(), noScheduledTasks.stream()).distinct()
				.collect(Collectors.toList());
		List<Object[]> mergedSubTasks = Stream.concat(subTasks.stream(), noScheduledSubTasks.stream()).distinct()
				.collect(Collectors.toList());

		if (lookaheadListDTO.isSearch() && lookaheadListDTO.getSearchTerm() != null
				&& lookaheadListDTO.getSearchTerm() != "") {
			Set<Long> taskIdList = new HashSet<>();
			Set<Long> subTaskOfTaskIdList = new HashSet<>();
			taskIdList.add(0L);
			subTaskOfTaskIdList.add(0L);
			mergedTask.forEach(task -> taskIdList.add(((BigInteger) task[0]).longValue()));
			mergedSubTasks.forEach(subTask -> subTaskOfTaskIdList.add(((BigInteger) subTask[7]).longValue()));
			List<Object[]> searchSubTask = subTaskRepository.findByTaskId(taskIdList, projectId);
			List<Object[]> searchTask = taskRepository.findById(subTaskOfTaskIdList, projectId);
			mergedSubTasks.addAll(searchSubTask);
			mergedTask.addAll(searchTask);
			List<Object[]> duplicateRemovedTask = mergedTask.stream().distinct().collect(Collectors.toList());
			Collections.sort(duplicateRemovedTask, new Comparator<Object[]>() {
				public int compare(Object[] s1, Object[] s2) {
					Long s1TaskId = ((BigInteger) s1[0]).longValue();
					Long s2TaskId = ((BigInteger) s2[0]).longValue();
					return s2TaskId.compareTo(s1TaskId);
				}
			});
			mergedTask.clear();
			mergedTask.addAll(duplicateRemovedTask);
		}

		CompletableFuture<List<Object[]>> taskCompletableFuture = CompletableFuture.completedFuture(mergedTask);
		CompletableFuture<List<Object[]>> subTaskCompletableFuture = CompletableFuture.completedFuture(mergedSubTasks);
		CompletableFuture<List<Object[]>> scheduleCompletableFuture = CompletableFuture.completedFuture(schedules);

		CompletableFuture<List<TaskDTO>> mergeTasks = taskCompletableFuture.thenApply(mergedTasks -> {
			List<TaskDTO> taskDTOs = new ArrayList<>();
			Set<Long> taskIdSet = new HashSet<>();
			mergedTasks.forEach(task -> {
				Long taskId = ((BigInteger) task[0]).longValue();
				if (!taskIdSet.contains(taskId)) {
					taskIdSet.add(taskId);
					TaskDTO taskDTO = new TaskDTO();
					taskDTO.setId(taskId);
					taskDTO.setDescription((String) task[1]);
					taskDTO.setVisibility((String) task[2]);
					taskDTO.setTrade((String) task[3]);
					taskDTO.setTaskType((String) task[4]);
					taskDTO.setBallInCourt((String) task[5]);
					taskDTO.setNote((String) task[6]);
					taskDTO.setChild((boolean) task[7]);

					if (task[10] != null) {
						LocalDate localTaskStartDate = ((Timestamp) task[10]).toLocalDateTime().toLocalDate();
						Date taskStartDate = DateUtil.convertToDateViaInstant(localTaskStartDate);
						taskDTO.setTaskStartDateInDate(taskStartDate);
						taskDTO.setTaskStartDate(
								DateUtil.convertDate(taskStartDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					if (task[11] != null) {
						LocalDate localExpectedCompletionDate = ((Timestamp) task[11]).toLocalDateTime().toLocalDate();
						Date expectedCompletionDate = DateUtil.convertToDateViaInstant(localExpectedCompletionDate);
						taskDTO.setExpectedCompletionDateInDate(expectedCompletionDate);
						taskDTO.setTaskFinishDate(DateUtil.convertDate(expectedCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					if (task[12] != null) {
						LocalDate localRequiredEndDate = ((Timestamp) task[12]).toLocalDateTime().toLocalDate();
						Date requiredCompletionDate = DateUtil.convertToDateViaInstant(localRequiredEndDate);
						taskDTO.setRequiredCompletionDateInDate(requiredCompletionDate);
						taskDTO.setRequiredCompletionDate(DateUtil.convertDate(requiredCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					if (task[13] != null) {
						LocalDate localDeadLineEndDate = ((Timestamp) task[13]).toLocalDateTime().toLocalDate();
						Date deadLineDate = DateUtil.convertToDateViaInstant(localDeadLineEndDate);
						taskDTO.setDeadLineDateInDate(deadLineDate);
						taskDTO.setDeadLineDate(
								DateUtil.convertDate(deadLineDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					if (null != task[9]) {
						Long taskProjectCompanyId = ((BigInteger) task[9]).longValue();
						String ballInCourt = findResponsibilityByProjectCompanyListAndId(projectCompaniesList,
								taskProjectCompanyId);
						taskDTO.setBallInCourt(ballInCourt);
						taskDTO.setProjectCompanyId(taskProjectCompanyId);
					}

					if (task[5] != null)
						taskDTO.setBallInCourt((String) task[5]);

					taskDTO.setSpecDivision((String) task[14]);

					// Code Added for Import Functionality task
					taskDTO.setActivityId((String) task[15]);
					taskDTO.setActivityDescription((String) task[16]);
					taskDTO.setDescription(taskDTO.getDescription());

					// if (taskDTO.getActivityId() != null) {
					//
					// String activityId = taskDTO.getDescription().substring(0, 2);
					// String activityIdContainshypen = Character.toString((activityId.charAt(1)));
					// if (activityIdContainshypen.contains("-")) {
					// taskDTO.setDescription(taskDTO.getDescription());
					// }
					// if (activityId.contains(taskDTO.getActivityId())) {
					// } else {
					// String activityDescription = taskDTO.getActivityId() + "-" +
					// taskDTO.getDescription();
					// taskDTO.setDescription(activityDescription);
					// }
					// // char str[] = activitDescription.toCharArray();
					// // String description =activityDescription(str, str.length);
					// //
					// // int n = str.length;
					// // String description = removeDuplicate(str, n);
					//
					// }

					if (task[17] != null) {
						LocalDate localImportStartDate = ((Timestamp) task[17]).toLocalDateTime().toLocalDate();
						Date importStartDate = DateUtil.convertToDateViaInstant(localImportStartDate);
						taskDTO.setImportStartDate(
								DateUtil.convertDate(importStartDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						taskDTO.setImportStartDate(
								DateUtil.convertDate(importStartDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}

					if (task[18] != null) {
						LocalDate localImportFinishEndDate = ((Timestamp) task[18]).toLocalDateTime().toLocalDate();
						Date importFinishDate = DateUtil.convertToDateViaInstant(localImportFinishEndDate);
						taskDTO.setImportFinishDate(
								DateUtil.convertDate(importFinishDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						taskDTO.setImportFinishDate(
								DateUtil.convertDate(importFinishDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}

					if (task[19] != null) {
						LocalDate localImportCompletionDate = ((Timestamp) task[19]).toLocalDateTime().toLocalDate();
						Date importCompletionDate = DateUtil.convertToDateViaInstant(localImportCompletionDate);
						taskDTO.setImportTargetCompletionDate(DateUtil.convertDate(importCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						taskDTO.setImportTargetCompletionDate(DateUtil.convertDate(importCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}

					if (task[20] != null) {
						LocalDate localImportDeadLineEndDate = ((Timestamp) task[20]).toLocalDateTime().toLocalDate();
						Date importDeadLineDate = DateUtil.convertToDateViaInstant(localImportDeadLineEndDate);
						taskDTO.setImportDeadLineDate(
								DateUtil.convertDate(importDeadLineDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						taskDTO.setImportDeadLineDate(
								DateUtil.convertDate(importDeadLineDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					taskDTO.setImportNotes((String) task[21]);
					if (task[22] != null)
						taskDTO.setLookAheadChange((boolean) task[22]);
					taskDTOs.add(taskDTO);
				}
			});
			return taskDTOs;
		});

		CompletableFuture<List<SubTaskDTO>> mergeSubTask = subTaskCompletableFuture.thenApply(mergedSubTask -> {
			List<SubTaskDTO> subTaskDTOs = new ArrayList<>();
			Set<Long> subTaskIdSet = new HashSet<>();
			mergedSubTask.forEach(subTask -> {
				Long subTaskId = ((BigInteger) subTask[0]).longValue();
				if (!subTaskIdSet.contains(subTaskId)) {
					subTaskIdSet.add(subTaskId);
					SubTaskDTO subTaskDTO = new SubTaskDTO();
					subTaskDTO.setId(subTaskId);
					subTaskDTO.setDescription((String) subTask[1]);
					subTaskDTO.setVisibility((String) subTask[2]);
					subTaskDTO.setTrade((String) subTask[3]);
					subTaskDTO.setTaskType((String) subTask[4]);
					subTaskDTO.setBallInCourt((String) subTask[5]);
					subTaskDTO.setNote((String) subTask[6]);
					subTaskDTO.setTaskId(((BigInteger) subTask[7]).longValue());
					subTaskDTO.setProjectCompanyId(((BigInteger) subTask[9]).longValue());
					if (subTaskDTO.getProjectCompanyId() != null) {
						Long subTaskProjectCompanyId = subTaskDTO.getProjectCompanyId();
						String ballInCourt = findResponsibilityByProjectCompanyListAndId(projectCompaniesList,
								subTaskProjectCompanyId);
						subTaskDTO.setBallInCourt(ballInCourt);
						subTaskDTO.setProjectCompanyId(subTaskProjectCompanyId);
					}
					if (subTask[5] != null)
						subTaskDTO.setBallInCourt((String) subTask[5]);

					if (subTask[10] != null) {
						LocalDate localTaskStartDate = ((Timestamp) subTask[10]).toLocalDateTime().toLocalDate();
						Date taskStartDate = DateUtil.convertToDateViaInstant(localTaskStartDate);
						subTaskDTO.setTaskStartDateInDate(taskStartDate);
						subTaskDTO.setTaskStartDate(
								DateUtil.convertDate(taskStartDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					if (subTask[11] != null) {
						LocalDate localExpectedCompletionDate = ((Timestamp) subTask[11]).toLocalDateTime()
								.toLocalDate();
						Date expectedCompletionDate = DateUtil.convertToDateViaInstant(localExpectedCompletionDate);
						subTaskDTO.setExpectedCompletionDateInDate(expectedCompletionDate);
						subTaskDTO.setTaskFinishDate(DateUtil.convertDate(expectedCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					if (subTask[12] != null) {
						LocalDate localRequiredEndDate = ((Timestamp) subTask[12]).toLocalDateTime().toLocalDate();
						Date requiredCompletionDate = DateUtil.convertToDateViaInstant(localRequiredEndDate);
						subTaskDTO.setRequiredCompletionDateInDate(requiredCompletionDate);
						subTaskDTO.setRequiredCompletionDate(DateUtil.convertDate(requiredCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					if (subTask[13] != null) {
						LocalDate localDeadLineEndDate = ((Timestamp) subTask[13]).toLocalDateTime().toLocalDate();
						Date deadLineDate = DateUtil.convertToDateViaInstant(localDeadLineEndDate);
						subTaskDTO.setDeadLineDateInDate(deadLineDate);
						subTaskDTO.setDeadLineDate(
								DateUtil.convertDate(deadLineDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					subTaskDTO.setSpecDivision((String) subTask[14]);

					// Code Added for Import Functionality Subtask
					if (subTask[15] != null)
						subTaskDTO.setActivityId((String) subTask[15]);
					if (subTask[16] != null)
						subTaskDTO.setActivityDescription((String) subTask[16]);

					if (subTask[17] != null) {
						LocalDate localImportStartDate = ((Timestamp) subTask[17]).toLocalDateTime().toLocalDate();
						Date importStartDate = DateUtil.convertToDateViaInstant(localImportStartDate);
						subTaskDTO.setImportStartDate(
								DateUtil.convertDate(importStartDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						subTaskDTO.setImportStartDate(
								DateUtil.convertDate(importStartDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}

					if (subTask[18] != null) {
						LocalDate localImportFinishDate = ((Timestamp) subTask[18]).toLocalDateTime().toLocalDate();
						Date importFinishDate = DateUtil.convertToDateViaInstant(localImportFinishDate);
						subTaskDTO.setImportFinishDate(
								DateUtil.convertDate(importFinishDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						subTaskDTO.setImportFinishDate(
								DateUtil.convertDate(importFinishDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}

					if (subTask[19] != null) {
						LocalDate localImportCompletionDate = ((Timestamp) subTask[19]).toLocalDateTime().toLocalDate();
						Date importCompletionDate = DateUtil.convertToDateViaInstant(localImportCompletionDate);
						subTaskDTO.setImportTargetCompletionDate(DateUtil.convertDate(importCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						subTaskDTO.setImportTargetCompletionDate(DateUtil.convertDate(importCompletionDate,
								DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}

					if (subTask[20] != null) {
						LocalDate localImportDeadLineEndDate = ((Timestamp) subTask[20]).toLocalDateTime()
								.toLocalDate();
						Date importDeadLineDate = DateUtil.convertToDateViaInstant(localImportDeadLineEndDate);
						subTaskDTO.setImportDeadLineDate(
								DateUtil.convertDate(importDeadLineDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
						subTaskDTO.setImportDeadLineDate(
								DateUtil.convertDate(importDeadLineDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					}
					subTaskDTO.setImportNotes((String) subTask[21]);
					if (subTask[22] != null)
						subTaskDTO.setLookAheadChange((boolean) subTask[22]);
					subTaskDTOs.add(subTaskDTO);
				}
			});
			return subTaskDTOs;
		});

		CompletableFuture<List<ScheduleDTO>> asyncSchedule = scheduleCompletableFuture.thenApply(scheduled -> {
			List<ScheduleDTO> scheduleDTOs = new ArrayList<>();
			scheduled.forEach(schedule -> {
				ScheduleDTO scheduleDTO = new ScheduleDTO();
				scheduleDTO.setId(((BigInteger) schedule[0]).longValue());
				LocalDate localWorkingDate = ((Timestamp) schedule[1]).toLocalDateTime().toLocalDate();
				Date workingDate = DateUtil.convertToDateViaInstant(localWorkingDate);
				scheduleDTO.setWorkingDateInDate(workingDate);
				scheduleDTO.setWorkingDate(
						DateUtil.convertDate(workingDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
				scheduleDTO.setWorkerCount(((BigInteger) schedule[2]).longValue());
				scheduleDTO.setSubTaskId(((BigInteger) schedule[3]).longValue());
				scheduleDTOs.add(scheduleDTO);
			});
			return scheduleDTOs;
		});

		List<TaskDTO> taskDTOs = new ArrayList<>();
		List<SubTaskDTO> subTaskDTOs = new ArrayList<>();
		List<ScheduleDTO> scheduleDTOs = new ArrayList<>();
		try {
			taskDTOs = mergeTasks.get();
			subTaskDTOs = mergeSubTask.get();
			scheduleDTOs = asyncSchedule.get();
		} catch (Exception e) {
			log.info("Task & sub Task & schedule Concurrency");
		}

		Integer pageNo, pageSize;
		pageNo = lookaheadListDTO.getPageNo();
		pageSize = lookaheadListDTO.getPageSize();
		if (null == pageNo)
			pageNo = 0;
		else
			pageNo = pageNo - 1;
		if (null == pageSize)
			pageSize = 30;
		Pageable pageRequest = PageRequest.of(pageNo, pageSize);
		int from = Math.max(0, pageRequest.getPageNumber() * pageRequest.getPageSize());
		;
		/*
		 * Set<Long> taskIdCount = new HashSet<>(); mergedTask.forEach(task ->{ Long
		 * taskId = ((BigInteger) task[0]).longValue(); taskIdCount.add(taskId); });
		 */

		List<TaskDTO> resultTaskDTOs = new ArrayList<>();
		CompletableFuture<List<TaskDTO>> mergeReult = mergeResult(taskDTOs, subTaskDTOs, scheduleDTOs, startDate,
				endDate, projectUser.getProject());
		try {
			if (null != mergeReult)
				resultTaskDTOs = mergeReult.get();
		} catch (Exception e) {
			log.info(" final Task & sub Task & schedule merge ");
		}

		final List<String> taskStatus = lookaheadListDTO.getFilter().getTaskStatus();

		if (isLookahead) {
			List<TaskDTO> afterTaskStatusFilter = new ArrayList<>();
			if (taskStatus != null && !taskStatus.isEmpty()) {
				TaskStatusDTO dto = new TaskStatusDTO();
				for (String taskDTO : taskStatus) {
					if (taskDTO.equalsIgnoreCase("On Track")) {
						dto.setOnTrack(true);
					} else if (taskDTO.equalsIgnoreCase("Need Attention")) {
						dto.setWarning(true);
					} else if (taskDTO.equalsIgnoreCase("Behind Schedule")) {
						dto.setBehindSchedule(true);
					}
				}

				for (TaskDTO taskDTO : resultTaskDTOs) {
					List<SubTaskDTO> subTaskDtoList = new ArrayList<>();
					List<SubTaskDTO> subTaskDtoBefore = taskDTO.getSubTasks();
					int i = 0;
					for (SubTaskDTO subTaskDTO : subTaskDtoBefore) {
						boolean on = false;
						boolean warn = false;
						boolean behind = false;
						if (dto.isOnTrack() && (subTaskDTO.isOnTrack() && !subTaskDTO.isWarning()
								&& !subTaskDTO.isBehindSchedule())) {
							on = true;
						}
						if (dto.isWarning() && (subTaskDTO.isWarning() && !subTaskDTO.isBehindSchedule())) {
							// true false
							warn = true;
						}
						if (dto.isBehindSchedule() && (subTaskDTO.isBehindSchedule())) {
							behind = true;
						}
						if (on || warn || behind) {
							i++;
							subTaskDtoList.add(subTaskDTO);

						}

					}
					if (i > 0) {
						taskDTO.setSubTasks(subTaskDtoList);
						afterTaskStatusFilter.add(taskDTO);
					}
				}
				resultTaskDTOs = afterTaskStatusFilter;
			}
		}

		Set<Long> taskDTOIds = new HashSet<>();
		Set<Long> subTaskDTOIds = new HashSet<>();
		if ((lookaheadListDTO.isSearch() && lookaheadListDTO.getSearchTerm() != null
				&& lookaheadListDTO.getSearchTerm() != "")) {
			for (TaskDTO task : taskDTOs) {
				taskDTOIds.add(task.getId());
			}
			subTaskDTOs.forEach(subTask -> subTaskDTOIds.add(subTask.getId()));
		}

		if ((taskStatus != null && !taskStatus.isEmpty())) {
			Stream<Long> tasksStream = resultTaskDTOs.stream().map(obj -> obj.getId());
			taskDTOIds = tasksStream.collect(Collectors.toSet());
			List<List<Long>> subTasksList = resultTaskDTOs.stream()
					.map(obj -> obj.getSubTasks().stream().map(obj2 -> obj2.getId()).collect(Collectors.toList()))
					.collect(Collectors.toList());
			for (List<Long> list : subTasksList) {
				subTaskDTOIds.addAll(list);
			}
		}
		int totalRecords = resultTaskDTOs.size();
		int taskSno = totalRecords;
		for (TaskDTO taskDTO : resultTaskDTOs) {
			taskDTO.setSNo("" + taskSno + "");
			int subTaskSno = taskDTO.getSubTasks().size();
			for (SubTaskDTO subTaskDTO : taskDTO.getSubTasks()) {
				subTaskDTO.setSNo("" + taskSno + "." + subTaskSno);
				subTaskSno--;
			}
			taskSno--;
		}
		List<TaskDTO> taskResult = resultTaskDTOs.stream().skip(from).limit(pageSize).collect(Collectors.toList());
		resultTaskDTOs.clear();
		if (isLookahead) {
			CompletableFuture<String> returnStatus = CompletableFuture.supplyAsync(() -> {
				taskResult.forEach(taskObj -> {
					if (!taskObj.isChild()) {
						List<Object[]> limitDateList = scheduleRepository
								.getStartDateEndDate(taskObj.getSubTasks().get(0).getId());
						if (!limitDateList.isEmpty()) {
							try {
								taskObj.getSubTasks().get(0)
										.setTaskStartDate(DateUtil.convertDate(
												new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString())
														.parse(limitDateList.get(0)[0] + ""),
												DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
								taskObj.getSubTasks().get(0)
										.setTaskFinishDate(DateUtil.convertDate(
												new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString())
														.parse(limitDateList.get(0)[1] + ""),
												DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
							} catch (Exception e) {
								log.error("Date Conversion error & Msg: " + e.getLocalizedMessage());
							}
						}
					} else {
						List<SubTaskDTO> subTaskDTOList = taskObj.getSubTasks();
						List<SubTaskDTO> subTaksObjs = new ArrayList<>();
						subTaskDTOList.forEach(subTaskObj -> {
							List<Object[]> limitDateList = scheduleRepository.getStartDateEndDate(subTaskObj.getId());
							if (!limitDateList.isEmpty()) {
								try {
									subTaskObj.setTaskStartDate(DateUtil.convertDate(
											new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString())
													.parse(limitDateList.get(0)[0] + ""),
											DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
									subTaskObj.setTaskFinishDate(DateUtil.convertDate(
											new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HYPHEN.toString())
													.parse(limitDateList.get(0)[1] + ""),
											DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
								} catch (Exception e) {
									log.error("Date Conversion error & Msg: " + e.getLocalizedMessage());
								}
							}
							subTaksObjs.add(subTaskObj);
						});
						taskObj.setSubTasks(subTaksObjs);
					}
				});
				return SUCS;
			});
			CompletableFuture.allOf(returnStatus).join();
			/*
			 * try { returnStatus.get(); } catch (Exception e) {
			 * log.error("Task startDate and End date conversion error & Msg: "
			 * +e.getLocalizedMessage()); }
			 */
		}
		resultTaskDTOs.addAll(taskResult);
		if ((lookaheadListDTO.isSearch() && lookaheadListDTO.getSearchTerm() != null
				&& lookaheadListDTO.getSearchTerm() != "") || (taskStatus != null && !taskStatus.isEmpty())) {
			long[] taskIds = Longs.toArray(taskDTOIds);
			long[] subTaskIds = Longs.toArray(subTaskDTOIds);
			lookaheadDTO.setTaskIds(ArrayUtils.toObject(taskIds));
			lookaheadDTO.setSubTaskIds(ArrayUtils.toObject(subTaskIds));
		} else {
			Stream<Long> tasksStream = resultTaskDTOs.stream().map(obj -> obj.getId());
			Long[] taskIds = tasksStream.toArray(Long[]::new);
			lookaheadDTO.setTaskIds(taskIds);
			List<Long> subTaskIdsList = new ArrayList<>();
			List<List<Long>> subTasksList = resultTaskDTOs.stream()
					.map(obj -> obj.getSubTasks().stream().map(obj2 -> obj2.getId()).collect(Collectors.toList()))
					.collect(Collectors.toList());
			for (List<Long> list : subTasksList) {
				subTaskIdsList.addAll(list);
			}
			lookaheadDTO.setSubTaskIds(subTaskIdsList.stream().toArray(Long[]::new));
		}

		lookaheadDTO.setTasks(resultTaskDTOs);
		lookaheadDTO.setProjectId(projectId);
		Long end = System.currentTimeMillis();
		Pagination pagination = new Pagination();
		pagination.setCurrentPageNumber(pageRequest.getPageNumber());
		pagination.setPaginationSize(pageRequest.getPageSize());
		pagination.setTotalRecords(totalRecords);

		return new MSAResponse(true, SUCS, lookaheadDTO, pagination, HttpStatus.OK);
	}

	public String removeDuplicate(char[] str, int n) {
		int index = 0;
		for (int i = 0; i < n; i++) {
			int j;
			for (j = 0; j < i; j++) {
				if (str[i] == str[j]) {
					break;
				}
			}
			if (j == i) {
				str[index++] = str[i];
			}
		}
		return String.valueOf(Arrays.copyOf(str, index));
	}

	public String activityDescription(char[] str, int n) {
		for (int i = 0; i < n; i++) {

		}
		return null;
	}

	/**
	 * Find responsibility by project company list and id.
	 *
	 * @param projectCompaniesList the project companies list
	 * @param taskProjectCompanyId the task project company id
	 * @return the string
	 */
	private String findResponsibilityByProjectCompanyListAndId(List<Object[]> projectCompaniesList,
			Long taskProjectCompanyId) {
		StringBuilder ballInCourt = new StringBuilder();
		projectCompaniesList.forEach(company -> {
			if (company[2] != null && taskProjectCompanyId != null) {
				Long proCompanyId = ((BigInteger) company[2]).longValue();
				if (taskProjectCompanyId.equals(proCompanyId)) {
					ballInCourt.setLength(0);
					ballInCourt.append((String) company[1]);
				}

			}

		});
		return ballInCourt.toString();
	}

	/**
	 * Merge result.
	 *
	 * @param taskDTOs     the task DT os
	 * @param subTaskDTOs  the sub task DT os
	 * @param scheduleDTOs the schedule DT os
	 * @param startDate    the start date
	 * @param endDate      the end date
	 * @param project      the project
	 * @param totalRecords
	 * @return the completable future
	 */
	@Async
	public CompletableFuture<List<TaskDTO>> mergeResult(List<TaskDTO> taskDTOs, List<SubTaskDTO> subTaskDTOs,
			List<ScheduleDTO> scheduleDTOs, Date startDate, Date endDate, Project project) {
		List<TaskDTO> resultTaskDTOs = new ArrayList<>();
		Collections.sort(taskDTOs, Comparator.comparingLong(TaskDTO::getId).reversed());
		int taskSno = taskDTOs.size();
		for (TaskDTO taskDTO : taskDTOs) {
			taskDTO.setSNo("" + taskSno + "");
			Long subTaskSno = subTaskDTOs.stream().filter(subTask -> taskDTO.getId().equals(subTask.getTaskId()))
					.distinct().count();
			List<SubTaskDTO> resultSubTaskDTOs = new ArrayList<>();
			Collections.sort(subTaskDTOs, Comparator.comparingLong(SubTaskDTO::getId).reversed());
			for (SubTaskDTO subTaskDTO : subTaskDTOs) {
				Date reqCompletionDt = getRequiredCompletionDate(subTaskDTO.getRequiredCompletionDateInDate(),
						subTaskDTO.getDeadLineDateInDate());
				if (taskDTO.getId().equals(subTaskDTO.getTaskId())) {
					subTaskDTO.setSNo("" + taskSno + "." + subTaskSno);
					List<ScheduleDTO> resultScheduleDTOs = new ArrayList<>();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMM-yyyy");
					Optional<LocalDate> subTaskMinDate = scheduleDTOs.stream()
							.filter(schedule -> subTaskDTO.getId().equals(schedule.getSubTaskId()))
							.map(schedule -> LocalDate.parse(schedule.getWorkingDate(), formatter))
							.min(LocalDate::compareTo);
					Optional<LocalDate> subTaskMaxDate = scheduleDTOs.stream()
							.filter(schedule -> subTaskDTO.getId().equals(schedule.getSubTaskId()))
							.map(schedule -> LocalDate.parse(schedule.getWorkingDate(), formatter))
							.max(LocalDate::compareTo);
					if (subTaskMinDate.isPresent())
						subTaskDTO.setStartDate(subTaskMinDate.get().format(formatter));
					if (subTaskMaxDate.isPresent())
						subTaskDTO.setEndDate(subTaskMaxDate.get().format(formatter));
					List<ScheduleDTO> scheduleDTOList = scheduleDTOs.stream()
							.filter(scheduleDTO -> subTaskDTO.getId().equals(scheduleDTO.getSubTaskId()))
							.collect(Collectors.toList());
					for (ScheduleDTO scheduleDTO : scheduleDTOList) {
						subTaskDTO.setOnTrack(true);
						scheduleDTO.setOnTrack(true);
						resultScheduleDTOs.add(scheduleDTO);
					}
					resultScheduleDTOs = trackingSubTaskStatus(reqCompletionDt, resultScheduleDTOs, subTaskDTO,
							project.getFloatDays());
					subTaskDTO.setSchedules(resultScheduleDTOs);
					resultSubTaskDTOs.add(subTaskDTO);
					subTaskSno--;
				}
			}
			taskDTO.setSubTasks(resultSubTaskDTOs);
			resultTaskDTOs.add(taskDTO);
			taskSno--;
		}
		return CompletableFuture.completedFuture(resultTaskDTOs);
	}

	/**
	 * Gets the date after subtract.
	 *
	 * @param finalRcd       the final rcd
	 * @param finalFloatDays the final float days
	 * @return the date after subtract
	 */
	private static Date getDateAfterSubtract(Date finalRcd, Integer finalFloatDays) {
		Instant before = finalRcd.toInstant().minus(Duration.ofDays(finalFloatDays));
		return Date.from(before);
	}

	/**
	 * Generate dynamic query result.
	 *
	 * @param tableAlias       the table alias
	 * @param lookaheadListDTO the lookahead list DTO
	 * @param start            the start
	 * @param end              the end
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> generateDynamicQueryResult(String tableAlias, LookaheadListDTO lookaheadListDTO, String start,
			String end) {
		String whereClause = "";
		if (null != lookaheadListDTO.getFilter()) {
			whereClause += LookaheadHelper.ballInCourtFilter(tableAlias, lookaheadListDTO.getFilter().getBallInCourt());
			whereClause += LookaheadHelper.visibilityFilter(tableAlias,
					lookaheadListDTO.getFilter().getAccessibility());
			whereClause += LookaheadHelper.taskTypeFilter(tableAlias, lookaheadListDTO.getFilter().getTask());
			whereClause += LookaheadHelper.tradeFilter(tableAlias, lookaheadListDTO.getFilter().getTrade());
			whereClause += LookaheadHelper.specDivisionFilter(tableAlias,
					lookaheadListDTO.getFilter().getSpecDivision());
			log.info("Query: " + start + whereClause + end);
		}
		String searchConditionQuery = lookaheadSearch(lookaheadListDTO, end);
		Query query = entityManager.createNativeQuery(start + searchConditionQuery + whereClause + end);
		return query.getResultList();
	}

	private String lookaheadSearch(LookaheadListDTO lookaheadListDTO, String end) {
		String searchConditionQuery = "";
		if (lookaheadListDTO.isSearch()) {
			if (lookaheadListDTO.getSearchTerm() != null && !lookaheadListDTO.getSearchTerm().equals("")) {
				String searchTerm = lookaheadListDTO.getSearchTerm().toLowerCase().trim().replaceAll("'", "''");
				if (end.contains("GROUP BY t.id ORDER BY t.id")) {
					searchConditionQuery = " LOWER(t.description) like '%" + searchTerm + "%'"
							+ " or LOWER(t.visibility) like '%" + searchTerm + "%'"
							+ " or LOWER(t.activity_description) like '%" + searchTerm + "%'"
							+ " or LOWER(t.activity_id) like '%" + searchTerm + "%'" + " or LOWER(t.trade) like '%"
							+ searchTerm + "%'" + " or LOWER(t.task_type) like '%" + searchTerm + "%'"
							+ " or LOWER(t.note) like '%" + searchTerm + "%'" + " or t.project_company_id in ("
							+ " select id from project_companies where  project_id = " + lookaheadListDTO.getProjectId()
							+ " and company_id "
							+ " in (select id from company where id in (select company_id from project_companies where "
							+ " project_id =" + lookaheadListDTO.getProjectId() + ") and LOWER(name) like '%"
							+ searchTerm + "%'))" + " or LOWER(t.spec_division) like '%" + searchTerm + "%'";
				} else if (end.contains("GROUP BY st.id") && end.contains("ORDER BY st.id")) {
					searchConditionQuery = " LOWER(st.description) like '%" + searchTerm + "%'"
							+ " or LOWER(st.activity_description) like '%" + searchTerm + "%'"
							+ " or LOWER(t.activity_id) like '%" + searchTerm + "%'"
							+ " or LOWER(st.visibility) like '%" + searchTerm + "%'" + " or LOWER(st.trade) like '%"
							+ searchTerm + "%'" + " or LOWER(st.task_type) like '%" + searchTerm + "%'"
							+ " or LOWER(st.note) like '%" + searchTerm + "%'" + " or st.project_company_id in ("
							+ " select id from project_companies where  project_id = " + lookaheadListDTO.getProjectId()
							+ " and company_id "
							+ " in (select id from company where id in (select company_id from project_companies where "
							+ " project_id =" + lookaheadListDTO.getProjectId() + ") and LOWER(name) like '%"
							+ searchTerm + "%'))" + " or LOWER(st.spec_division) like '%" + searchTerm + "%'";
				}
			}
		}
		if (searchConditionQuery != null && searchConditionQuery != "")
			searchConditionQuery = " and (" + searchConditionQuery + ")";
		log.info("Search Condition Query: " + searchConditionQuery);
		return searchConditionQuery;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mindzen.planner.service.LookaheadAccessService#getLookahead(com.mindzen.
	 * planner.dto.LookaheadListDTO)
	 */
	@Override
	public MSAResponse getLookahead(LookaheadListDTO lookaheadListDTO, Long daysCount, boolean isLookahead) {
		lookaheadListDTO.setUserId(UserInfoContext.getUserThreadLocal());
		// lookaheadListDTO.setUserId(656L);
		CompletableFuture<MSAResponse> response = CompletableFuture.supplyAsync(() -> {
			return listLookahead(lookaheadListDTO, daysCount, isLookahead);
		});
		try {
			return response.get();
		} catch (Exception e) {
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, ERR, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mindzen.planner.service.LookaheadAccessService#downloadPDF(com.mindzen.
	 * planner.dto.LookaheadListDTO)
	 */
	@Override
	public MSAResponse downloadPDF(LookaheadListDTO lookaheadListDTO) {

		Long userId = UserInfoContext.getUserThreadLocal();
		Long projectId = lookaheadListDTO.getProjectId();
		List<Object[]> projectCompaniesList = projectCompanyRepository.findCompaniesByProjectId(projectId);
		ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId, userId, true, false);
		if (null == projectUser)
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "User not associated with the project",
					null);
		int reportDays = 28;

		LocalDate lookaheadStartDate = DateUtil.stringToLocalDateViaInstant(lookaheadListDTO.getLookaheadDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		LocalDate localEndDate = DateUtil.plusNumberDaysInLocalDate(lookaheadStartDate, reportDays - 1);

		List<LocalDate> localDates = DateUtil.getLocalDatesList(lookaheadStartDate, reportDays);
		log.debug("Local Dates: " + localDates);

		LookaheadJasperReportRootDTO lookaheadJasperReportRootDTO = new LookaheadJasperReportRootDTO();
		List<LookaheadJasperReportDetailDTO> lookaheadReport = new ArrayList<>();

		String week_1_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate);
		String week_2_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate.plusDays(6));
		String week_3_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate.plusDays(13));
		String week_4_month = DateUtil.monthFromLocalDateWeek(lookaheadStartDate.plusDays(20));

		Project project = projectRepository.findById(projectId).get();
		Integer floatDays = project.getFloatDays() + 1;
		String leaveDays = project.getLeaveDays();
		final String[] leaveDay = leaveDays.split(",");

		List<Boolean> weekOffList = localDates.stream().map(obj -> {
			boolean dayOff = false;
			for (int i = 0; i < leaveDay.length; i++) {
				if (obj.getDayOfWeek().name().equalsIgnoreCase(leaveDay[i])) {
					dayOff = true;
				}
			}
			return dayOff;
		}).collect(Collectors.toList());

		String projectCompany = projectUserRepository.findcompanyNameByCreatedBy(project.getCreatedBy());

		String projectName = project.getName();
		final String projectLocation = LookaheadHelper.fullProjectLocation(project.getCity(), project.getState());

		List<Object[]> tasks = new ArrayList<>();
		List<Object[]> taskAndSubTasks = new ArrayList<>();
		List<Object[]> noScheduledTasks = new ArrayList<>();
		List<Object[]> noScheduledSubTasks = new ArrayList<>();

		String projectRole = projectUser.getProjectRole();
		Long projectCompanyId = projectUser.getProjectCompany().getId();

		String scheduleAvailability = lookaheadListDTO.getFilter().getScheduleAvailability();
		boolean availableSchedules = false;
		boolean emptySchedules = false;
		final List<Object[]> schedules = crosstabData(localDates, lookaheadStartDate, localEndDate, projectId,
				projectCompanyId, projectRole);

		if (Constants.ALL.equalsIgnoreCase(scheduleAvailability)) {
			availableSchedules = true;
			emptySchedules = true;
		} else if (Constants.SCHEDULED.equalsIgnoreCase(scheduleAvailability))
			availableSchedules = true;
		else if (Constants.UNSCHEDULED.equalsIgnoreCase(scheduleAvailability)) {
			emptySchedules = true;
			schedules.clear();
		}
		String createdByQuery = "AND st.project_company_id = " + projectCompanyId;
		String and = " AND ";
		if (!projectRole.contains(Constants.SC_USERS))
			createdByQuery = "";
		if (availableSchedules) {
			StringBuilder taskQueryBuilder = new StringBuilder();
			StringBuilder subTaskQueryBuilder = new StringBuilder();
			String taskQuery = "SELECT "
					+ "t.id AS task_id, t.description AS task_description, t.visibility AS task_visibility, "
					+ "t.trade AS task_trade, t.task_type, t.ball_in_court AS task_ball_in_court, "
					+ "t.note AS task_note, t.is_child, t.sno AS sno ,t.task_start_date,t.expected_completion_date,t.required_completion_date,t.dead_line_date, "
					+ "t.project_company_id FROM schedule s " + "JOIN sub_task st " + "ON s.sub_task_id=st.id "
					+ "JOIN task t " + "ON st.task_id=t.id " + "WHERE\n" + "t.project_id = " + projectId + "\n"
					+ createdByQuery + and + "t.is_active=true\n" + and + "st.is_active=true\n";

			String subTaskQuery = "SELECT " + "t.id AS task_id, t.description AS task_description, "
					+ "t.ball_in_court AS task_ball_in_court, t.note AS task_note, " + "t.is_child, t.sno AS task_sno, "
					+ "st.id AS sub_task_id, st.description AS sub_task_description, "
					+ "st.ball_in_court AS sub_task_ball_in_court, st.note AS sub_task_note, st.sno AS sub_task_sno , min(st.task_start_date) as tsd ,max(st.expected_completion_date) as ecd ,max(st.required_completion_date) as rcd ,max(st.dead_line_date) as dld, "
					+ "t.project_company_id AS task_project_company_id,st.project_company_id AS subtask_project_company_id FROM schedule s "
					+ "JOIN sub_task st " + "ON s.sub_task_id=st.id " + "JOIN task t " + "ON st.task_id=t.id "
					+ "WHERE \n" + "t.project_id = " + projectId + "\n" + createdByQuery + and + "t.is_active=true \n"
					+ and + "st.is_active=true\n";

			if (!lookaheadListDTO.isSearch()) {
				String rangeQuery = " and s.working_date BETWEEN '" + lookaheadStartDate + "'" + and + "'"
						+ localEndDate + "'\n";
				taskQueryBuilder.append(taskQuery + rangeQuery);
				subTaskQueryBuilder.append(subTaskQuery + rangeQuery);
			} else {
				taskQueryBuilder.append(taskQuery);
				subTaskQueryBuilder.append(subTaskQuery);
			}
			tasks = generateDynamicQueryResult("st", lookaheadListDTO, taskQueryBuilder.toString(),
					" GROUP BY t.id ORDER BY t.id");
			taskAndSubTasks = generateDynamicQueryResult("st", lookaheadListDTO, subTaskQueryBuilder.toString(),
					"GROUP BY st.id, t.id ORDER BY t.id");
		}
		if (emptySchedules) {
			String noScheduledTaskQuery = "SELECT"
					+ " t.id AS task_id, t.description AS task_description, t.visibility AS task_visibility, "
					+ "t.trade AS task_trade, t.task_type, t.ball_in_court AS task_ball_in_court, "
					+ "t.note AS task_note, t.is_child, t.sno AS sno,t.task_start_date,t.expected_completion_date,t.required_completion_date,t.dead_line_date, "
					+ "t.project_company_id FROM sub_task st " + "JOIN task t "
					+ "ON st.task_id=t.id left join schedule se on se.sub_task_id = st.id  " + "WHERE "
					+ "se.id is null " + and + "t.project_id = " + projectId + "\n" + createdByQuery + and
					+ "t.is_active=true\n" + and + "st.is_active=true\n";
			noScheduledTasks = generateDynamicQueryResult("st", lookaheadListDTO, noScheduledTaskQuery,
					" GROUP BY t.id  ORDER BY t.id ");

			String noScheduledSubTaskQuery = "SELECT " + "t.id AS task_id, t.description AS task_description, "
					+ "t.ball_in_court AS task_ball_in_court, t.note AS task_note, " + "t.is_child, t.sno AS task_sno, "
					+ "st.id AS sub_task_id, st.description AS sub_task_description, "
					+ "st.ball_in_court AS sub_task_ball_in_court, st.note AS sub_task_note, st.sno AS sub_task_sno , min(st.task_start_date) as tsd ,max(st.expected_completion_date) as ecd ,max(st.required_completion_date) as rcd ,max(st.dead_line_date) as dld, "
					+ "t.project_company_id AS task_project_company_id,st.project_company_id AS subtask_project_company_id FROM sub_task st "
					+ "JOIN task t " + "ON st.task_id=t.id left join schedule se on se.sub_task_id = st.id " + "WHERE "
					+ " se.id is null " + and + "t.project_id = " + projectId + "\n" + createdByQuery + and
					+ "t.is_active=true\n" + and + "st.is_active=true\n";
			noScheduledSubTasks = generateDynamicQueryResult("st", lookaheadListDTO, noScheduledSubTaskQuery,
					" GROUP BY st.id, t.id  ORDER BY t.id");
		}

		List<Object[]> mergedTasks = Stream.concat(tasks.stream(), noScheduledTasks.stream()).distinct()
				.collect(Collectors.toList());

		List<Object[]> mergedSubTasks = Stream.concat(taskAndSubTasks.stream(), noScheduledSubTasks.stream()).distinct()
				.collect(Collectors.toList());

		String timeZone = projectUserRepository.findTimeZoneByUserId(userId);
		LocalDateTime currentTimeByUserZone = DateUtil.utcZoneDateTimeToZoneDateTimeConvert(LocalDateTime.now(),
				timeZone);

		Set<Long> taskIdSet = new HashSet<>();
		for (Object[] task : mergedTasks) {
			Long taskId = ((BigInteger) task[0]).longValue();
			if (!taskIdSet.contains(taskId)) {
				taskIdSet.add(taskId);
				if (((boolean) task[7])) {
					LookaheadJasperReportDetailDTO lookaheadJasperData = new LookaheadJasperReportDetailDTO();
					lookaheadJasperData.setSno((String) task[8]);
					lookaheadJasperData.setTaskId(taskId);
					lookaheadJasperData.setSubTaskId(1L);
					lookaheadJasperData.setTaskDescription((String) task[1]);
					if (!((boolean) task[7])) {
						lookaheadJasperData.setResponsibility((String) task[5]);
						Long taskProjectCompanyId = ((BigInteger) task[13]).longValue();
						String ballInCourt = findResponsibilityByProjectCompanyListAndId(projectCompaniesList,
								taskProjectCompanyId);
						lookaheadJasperData.setResponsibility(ballInCourt);
					} else
						lookaheadJasperData.setResponsibility("");
					lookaheadJasperData.setComments(task[6] == null ? "" : (String) task[6]);
					lookaheadJasperData = LookaheadHelper.loadProjectInfo(lookaheadJasperData, projectCompany,
							projectLocation, projectName);
					lookaheadJasperData = LookaheadHelper.loadWeekMonth(lookaheadJasperData, week_1_month, week_2_month,
							week_3_month, week_4_month);
					lookaheadJasperData = LookaheadHelper.fillDayAndDate(lookaheadJasperData, localDates);
					lookaheadJasperData = LookaheadHelper.fillEmptyDayValues(lookaheadJasperData);
					lookaheadJasperData = LookaheadHelper.fillTaskDayValuesCd(lookaheadJasperData);
					lookaheadJasperData.setTask(true);
					lookaheadJasperData.setCurrentTime(
							currentTimeByUserZone.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm")).toString());
					lookaheadReport.add(lookaheadJasperData);
				}
			}
		}
		Set<Long> subTaskIdSet = new HashSet<>();
		mergedSubTasks.stream().forEach(taskAndSubTask -> {

			schedules.stream().filter(
					schedule -> ((Integer) schedule[0]).longValue() == (((BigInteger) taskAndSubTask[6]).longValue()))
					.forEach(schedule -> {
						Long subTaskId = ((BigInteger) taskAndSubTask[6]).longValue();
						if (!subTaskIdSet.contains(subTaskId)) {
							subTaskIdSet.add(subTaskId);
							String taskDescription = "";
							String responsibility = "";
							String comments = "";
							boolean isTask = false;
							String sno = null;
							Long taskId = ((BigInteger) taskAndSubTask[0]).longValue();
							taskIdSet.add(taskId);
							subTaskIdSet.add(subTaskId);
							if (!(boolean) taskAndSubTask[4]) {
								sno = (String) taskAndSubTask[5];
								taskDescription = (String) taskAndSubTask[1];
								responsibility = (String) taskAndSubTask[2];
								comments = (String) taskAndSubTask[3];
								isTask = true;
								Long taskProjectCompanyId = ((BigInteger) taskAndSubTask[15]).longValue();
								responsibility = findResponsibilityByProjectCompanyListAndId(projectCompaniesList,
										taskProjectCompanyId);
							} else {
								sno = (String) taskAndSubTask[10];
								taskDescription = (String) taskAndSubTask[7];
								responsibility = (String) taskAndSubTask[8];
								comments = (String) taskAndSubTask[9];
								Long taskProjectCompanyId = ((BigInteger) taskAndSubTask[16]).longValue();
								responsibility = findResponsibilityByProjectCompanyListAndId(projectCompaniesList,
										taskProjectCompanyId);
							}
							BigInteger finalTaskId = BigInteger.valueOf(subTaskId);
							Timestamp finalCompletionEndTime = null;
							Timestamp requiredEndTime = null;
							Timestamp deadLineTime = null;
							Object[] subtask = mergedSubTasks.stream()
									.filter(obj -> ((BigInteger) obj[6]).equals(finalTaskId)).findAny().get();
							if (subtask[13] != null) {
								// 2019-06-14 00:00:00
								requiredEndTime = (Timestamp) subtask[13];
							}
							if (subtask[14] != null) {
								// 2019-06-14 00:00:00
								deadLineTime = (Timestamp) subtask[14];
							}

							if (requiredEndTime != null && deadLineTime != null) {
								if (requiredEndTime.after(deadLineTime) || requiredEndTime.equals(deadLineTime)) {
									finalCompletionEndTime = deadLineTime;
								} else {
									finalCompletionEndTime = requiredEndTime;
								}
							} else if (requiredEndTime != null) {
								finalCompletionEndTime = requiredEndTime;
							} else if (deadLineTime != null) {
								finalCompletionEndTime = deadLineTime;
							}

							List<Timestamp> floatDayslist = new ArrayList<>();

							if (floatDays != null && finalCompletionEndTime != null) {
								Date minimiumWarningdate = getDateAfterSubtract(finalCompletionEndTime, floatDays);
								for (int i = 1; i < schedule.length; i++) {
									if (null != schedule[i]) {
										String workingCount = (String) schedule[i];
										String[] workingCountAndWorkingDate = workingCount.split("_");
										String workingDate = workingCountAndWorkingDate[1];
										Timestamp workingDateTime = Timestamp.valueOf(workingDate);
										if ((workingDateTime.after(minimiumWarningdate)
												&& workingDateTime.before(finalCompletionEndTime))
												|| workingDateTime.equals(finalCompletionEndTime)) {
											floatDayslist.add(workingDateTime);
										}
									}
								}
							}

							LookaheadJasperReportDetailDTO lookaheadJasperData = new LookaheadJasperReportDetailDTO();
							lookaheadJasperData.setCurrentTime(currentTimeByUserZone
									.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm")).toString());
							lookaheadJasperData.setTask(isTask);
							lookaheadJasperData.setSno(sno);
							lookaheadJasperData.setTaskId(taskId);
							lookaheadJasperData.setSubTaskId(subTaskId);
							lookaheadJasperData.setTaskDescription(taskDescription);
							lookaheadJasperData.setResponsibility(responsibility);
							lookaheadJasperData.setComments(comments == null ? "" : comments);
							lookaheadJasperData = LookaheadHelper.loadProjectInfo(lookaheadJasperData, projectCompany,
									projectLocation, projectName);
							lookaheadJasperData = LookaheadHelper.loadWeekMonth(lookaheadJasperData, week_1_month,
									week_2_month, week_3_month, week_4_month);
							lookaheadJasperData = LookaheadHelper.fillDayAndDate(lookaheadJasperData, localDates);
							lookaheadJasperData.setDay_1_value(LookaheadHelper.nullCheck(schedule[1]));
							lookaheadJasperData.setDay_2_value(LookaheadHelper.nullCheck(schedule[2]));
							lookaheadJasperData.setDay_3_value(LookaheadHelper.nullCheck(schedule[3]));
							lookaheadJasperData.setDay_4_value(LookaheadHelper.nullCheck(schedule[4]));
							lookaheadJasperData.setDay_5_value(LookaheadHelper.nullCheck(schedule[5]));
							lookaheadJasperData.setDay_6_value(LookaheadHelper.nullCheck(schedule[6]));
							lookaheadJasperData.setDay_7_value(LookaheadHelper.nullCheck(schedule[7]));
							lookaheadJasperData.setDay_8_value(LookaheadHelper.nullCheck(schedule[8]));
							lookaheadJasperData.setDay_9_value(LookaheadHelper.nullCheck(schedule[9]));
							lookaheadJasperData.setDay_10_value(LookaheadHelper.nullCheck(schedule[10]));
							lookaheadJasperData.setDay_11_value(LookaheadHelper.nullCheck(schedule[11]));
							lookaheadJasperData.setDay_12_value(LookaheadHelper.nullCheck(schedule[12]));
							lookaheadJasperData.setDay_13_value(LookaheadHelper.nullCheck(schedule[13]));
							lookaheadJasperData.setDay_14_value(LookaheadHelper.nullCheck(schedule[14]));
							lookaheadJasperData.setDay_15_value(LookaheadHelper.nullCheck(schedule[15]));
							lookaheadJasperData.setDay_16_value(LookaheadHelper.nullCheck(schedule[16]));
							lookaheadJasperData.setDay_17_value(LookaheadHelper.nullCheck(schedule[17]));
							lookaheadJasperData.setDay_18_value(LookaheadHelper.nullCheck(schedule[18]));
							lookaheadJasperData.setDay_19_value(LookaheadHelper.nullCheck(schedule[19]));
							lookaheadJasperData.setDay_20_value(LookaheadHelper.nullCheck(schedule[20]));
							lookaheadJasperData.setDay_21_value(LookaheadHelper.nullCheck(schedule[21]));
							lookaheadJasperData.setDay_22_value(LookaheadHelper.nullCheck(schedule[22]));
							lookaheadJasperData.setDay_23_value(LookaheadHelper.nullCheck(schedule[23]));
							lookaheadJasperData.setDay_24_value(LookaheadHelper.nullCheck(schedule[24]));
							lookaheadJasperData.setDay_25_value(LookaheadHelper.nullCheck(schedule[25]));
							lookaheadJasperData.setDay_26_value(LookaheadHelper.nullCheck(schedule[26]));
							lookaheadJasperData.setDay_27_value(LookaheadHelper.nullCheck(schedule[27]));
							lookaheadJasperData.setDay_28_value(LookaheadHelper.nullCheck(schedule[28]));

							lookaheadJasperData.setDay_1_value_cd(LookaheadHelper.setColorCode(schedule[1],
									finalCompletionEndTime, weekOffList.get(0), floatDayslist));
							lookaheadJasperData.setDay_2_value_cd(LookaheadHelper.setColorCode(schedule[2],
									finalCompletionEndTime, weekOffList.get(1), floatDayslist));
							lookaheadJasperData.setDay_3_value_cd(LookaheadHelper.setColorCode(schedule[3],
									finalCompletionEndTime, weekOffList.get(2), floatDayslist));
							lookaheadJasperData.setDay_4_value_cd(LookaheadHelper.setColorCode(schedule[4],
									finalCompletionEndTime, weekOffList.get(3), floatDayslist));
							lookaheadJasperData.setDay_5_value_cd(LookaheadHelper.setColorCode(schedule[5],
									finalCompletionEndTime, weekOffList.get(4), floatDayslist));
							lookaheadJasperData.setDay_6_value_cd(LookaheadHelper.setColorCode(schedule[6],
									finalCompletionEndTime, weekOffList.get(5), floatDayslist));
							lookaheadJasperData.setDay_7_value_cd(LookaheadHelper.setColorCode(schedule[7],
									finalCompletionEndTime, weekOffList.get(6), floatDayslist));
							lookaheadJasperData.setDay_8_value_cd(LookaheadHelper.setColorCode(schedule[8],
									finalCompletionEndTime, weekOffList.get(7), floatDayslist));
							lookaheadJasperData.setDay_9_value_cd(LookaheadHelper.setColorCode(schedule[9],
									finalCompletionEndTime, weekOffList.get(8), floatDayslist));
							lookaheadJasperData.setDay_10_value_cd(LookaheadHelper.setColorCode(schedule[10],
									finalCompletionEndTime, weekOffList.get(9), floatDayslist));
							lookaheadJasperData.setDay_11_value_cd(LookaheadHelper.setColorCode(schedule[11],
									finalCompletionEndTime, weekOffList.get(10), floatDayslist));
							lookaheadJasperData.setDay_12_value_cd(LookaheadHelper.setColorCode(schedule[12],
									finalCompletionEndTime, weekOffList.get(11), floatDayslist));
							lookaheadJasperData.setDay_13_value_cd(LookaheadHelper.setColorCode(schedule[13],
									finalCompletionEndTime, weekOffList.get(12), floatDayslist));
							lookaheadJasperData.setDay_14_value_cd(LookaheadHelper.setColorCode(schedule[14],
									finalCompletionEndTime, weekOffList.get(13), floatDayslist));
							lookaheadJasperData.setDay_15_value_cd(LookaheadHelper.setColorCode(schedule[15],
									finalCompletionEndTime, weekOffList.get(14), floatDayslist));
							lookaheadJasperData.setDay_16_value_cd(LookaheadHelper.setColorCode(schedule[16],
									finalCompletionEndTime, weekOffList.get(15), floatDayslist));
							lookaheadJasperData.setDay_17_value_cd(LookaheadHelper.setColorCode(schedule[17],
									finalCompletionEndTime, weekOffList.get(16), floatDayslist));
							lookaheadJasperData.setDay_18_value_cd(LookaheadHelper.setColorCode(schedule[18],
									finalCompletionEndTime, weekOffList.get(17), floatDayslist));
							lookaheadJasperData.setDay_19_value_cd(LookaheadHelper.setColorCode(schedule[19],
									finalCompletionEndTime, weekOffList.get(18), floatDayslist));
							lookaheadJasperData.setDay_20_value_cd(LookaheadHelper.setColorCode(schedule[20],
									finalCompletionEndTime, weekOffList.get(19), floatDayslist));
							lookaheadJasperData.setDay_21_value_cd(LookaheadHelper.setColorCode(schedule[21],
									finalCompletionEndTime, weekOffList.get(20), floatDayslist));
							lookaheadJasperData.setDay_22_value_cd(LookaheadHelper.setColorCode(schedule[22],
									finalCompletionEndTime, weekOffList.get(21), floatDayslist));
							lookaheadJasperData.setDay_23_value_cd(LookaheadHelper.setColorCode(schedule[23],
									finalCompletionEndTime, weekOffList.get(22), floatDayslist));
							lookaheadJasperData.setDay_24_value_cd(LookaheadHelper.setColorCode(schedule[24],
									finalCompletionEndTime, weekOffList.get(23), floatDayslist));
							lookaheadJasperData.setDay_25_value_cd(LookaheadHelper.setColorCode(schedule[25],
									finalCompletionEndTime, weekOffList.get(24), floatDayslist));
							lookaheadJasperData.setDay_26_value_cd(LookaheadHelper.setColorCode(schedule[26],
									finalCompletionEndTime, weekOffList.get(25), floatDayslist));
							lookaheadJasperData.setDay_27_value_cd(LookaheadHelper.setColorCode(schedule[27],
									finalCompletionEndTime, weekOffList.get(26), floatDayslist));
							lookaheadJasperData.setDay_28_value_cd(LookaheadHelper.setColorCode(schedule[28],
									finalCompletionEndTime, weekOffList.get(27), floatDayslist));

							lookaheadReport.add(lookaheadJasperData);
						}
					});
			Long subTaskId = ((BigInteger) taskAndSubTask[6]).longValue();
			if (!subTaskIdSet.contains(subTaskId)) {
				subTaskIdSet.add(subTaskId);
				String taskDescription = "";
				String responsibility = "";
				String comments = "";
				boolean isTask = false;
				String sno = null;
				Long taskId = ((BigInteger) taskAndSubTask[0]).longValue();
				taskIdSet.add(taskId);
				subTaskIdSet.add(subTaskId);
				if (!(boolean) taskAndSubTask[4]) {
					sno = (String) taskAndSubTask[5];
					taskDescription = (String) taskAndSubTask[1];
					responsibility = (String) taskAndSubTask[2];
					comments = (String) taskAndSubTask[3];
					isTask = true;
					Long taskProjectCompanyId = ((BigInteger) taskAndSubTask[15]).longValue();
					responsibility = findResponsibilityByProjectCompanyListAndId(projectCompaniesList,
							taskProjectCompanyId);
				} else {
					sno = (String) taskAndSubTask[10];
					taskDescription = (String) taskAndSubTask[7];
					responsibility = (String) taskAndSubTask[8];
					comments = (String) taskAndSubTask[9];
					Long taskProjectCompanyId = ((BigInteger) taskAndSubTask[16]).longValue();
					responsibility = findResponsibilityByProjectCompanyListAndId(projectCompaniesList,
							taskProjectCompanyId);
				}
				LookaheadJasperReportDetailDTO lookaheadJasperData = new LookaheadJasperReportDetailDTO();
				lookaheadJasperData.setCurrentTime(
						currentTimeByUserZone.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm")).toString());
				lookaheadJasperData.setTask(isTask);
				lookaheadJasperData.setSno(sno);
				lookaheadJasperData.setTaskId(taskId);
				lookaheadJasperData.setSubTaskId(subTaskId);
				lookaheadJasperData.setTaskDescription(taskDescription);
				lookaheadJasperData.setResponsibility(responsibility);
				lookaheadJasperData.setComments(comments == null ? "" : comments);
				lookaheadJasperData = LookaheadHelper.loadProjectInfo(lookaheadJasperData, projectCompany,
						projectLocation, projectName);
				lookaheadJasperData = LookaheadHelper.loadWeekMonth(lookaheadJasperData, week_1_month, week_2_month,
						week_3_month, week_4_month);
				lookaheadJasperData = LookaheadHelper.fillDayAndDate(lookaheadJasperData, localDates);
				lookaheadJasperData = LookaheadHelper.fillEmptyDayValues(lookaheadJasperData);
				lookaheadReport.add(lookaheadJasperData);
			}
		});

		List<LookaheadJasperReportDetailDTO> lookaheadReportTaskStatus = new ArrayList<>();
		List<String> taskStatus = lookaheadListDTO.getFilter().getTaskStatus();
		if (lookaheadListDTO.isSearch() || (taskStatus != null && !taskStatus.isEmpty())) {
			Long[] taskIds = lookaheadListDTO.getTaskIds();
			Long[] subTaskIds = lookaheadListDTO.getSubTaskIds();
			List<Long> taskIdList = Arrays.asList(taskIds);
			List<Long> subTaskIdList = Arrays.asList(subTaskIds);
			lookaheadReportTaskStatus = lookaheadReport.stream()
					.filter(obj -> taskIdList.stream().anyMatch(obj2 -> obj2 != null && obj2.equals(obj.getTaskId()))
							&& (subTaskIdList.stream().anyMatch(obj3 -> obj3 != null && obj3.equals(obj.getSubTaskId()))
									|| obj.isTask()))
					.collect(Collectors.toList());
			lookaheadReport.clear();
			lookaheadReport.addAll(lookaheadReportTaskStatus);
		}

		List<Long> loadIds = new ArrayList<>();
		if (null != lookaheadListDTO.getColumnName()
				&& lookaheadListDTO.getColumnName().equalsIgnoreCase("RESPONSIBILITY")) {
			if (null != lookaheadListDTO.getSortType() && lookaheadListDTO.getSortType().equalsIgnoreCase("DESC")) {
				List<LookaheadJasperReportDetailDTO> lookaheadSortedReport = lookaheadReport.stream().sorted(Comparator
						.comparing(LookaheadJasperReportDetailDTO::getResponsibility, String.CASE_INSENSITIVE_ORDER)
						.reversed()).parallel().collect(Collectors.toList());
				lookaheadReport.clear();
				lookaheadReport.addAll(lookaheadSortedReport);
			} else {
				List<LookaheadJasperReportDetailDTO> lookaheadSortedReport = lookaheadReport.stream().sorted(Comparator
						.comparing(LookaheadJasperReportDetailDTO::getResponsibility, String.CASE_INSENSITIVE_ORDER))
						.parallel().collect(Collectors.toList());
				List<LookaheadJasperReportDetailDTO> responsibilityReport = lookaheadReport.stream()
						.filter(report -> report.getResponsibility() != "")
						.sorted(Comparator.comparing(LookaheadJasperReportDetailDTO::getResponsibility,
								String.CASE_INSENSITIVE_ORDER))
						.parallel().collect(Collectors.toList());
				responsibilityReport.forEach(report -> loadIds.add(report.getTaskId()));
				lookaheadReport.clear();
				lookaheadReport.addAll(lookaheadSortedReport);
			}
		} else if (null != lookaheadListDTO.getColumnName()
				&& lookaheadListDTO.getColumnName().equalsIgnoreCase("SCHEDULES")) {
			if (null != lookaheadListDTO.getSortType() && lookaheadListDTO.getSortType().equalsIgnoreCase("DESC")) {
				List<LookaheadJasperReportDetailDTO> unscheduledreport = lookaheadReport.stream()
						.filter(report -> !report.getDay_1_value().equals("x") && !report.getDay_2_value().equals("x")
								&& !report.getDay_3_value().equals("x") && !report.getDay_4_value().equals("x")
								&& !report.getDay_5_value().equals("x") && !report.getDay_6_value().equals("x")
								&& !report.getDay_7_value().equals("x") && !report.getDay_8_value().equals("x")
								&& !report.getDay_9_value().equals("x") && !report.getDay_10_value().equals("x")
								&& !report.getDay_11_value().equals("x") && !report.getDay_12_value().equals("x")
								&& !report.getDay_13_value().equals("x") && !report.getDay_14_value().equals("x")
								&& !report.getDay_15_value().equals("x") && !report.getDay_16_value().equals("x")
								&& !report.getDay_17_value().equals("x") && !report.getDay_18_value().equals("x")
								&& !report.getDay_19_value().equals("x") && !report.getDay_20_value().equals("x")
								&& !report.getDay_21_value().equals("x") && !report.getDay_22_value().equals("x")
								&& !report.getDay_23_value().equals("x") && !report.getDay_24_value().equals("x")
								&& !report.getDay_25_value().equals("x") && !report.getDay_26_value().equals("x")
								&& !report.getDay_27_value().equals("x") && !report.getDay_28_value().equals("x"))
						.collect(Collectors.toList());
				Set<Long> unscheduledIds = new HashSet<>();
				unscheduledreport.stream().forEach(report -> unscheduledIds.add(report.getTaskId()));
				List<LookaheadJasperReportDetailDTO> scheduledSortedReport = lookaheadReport.stream()
						.filter(report -> report.getDay_1_value().equals("x") || report.getDay_2_value().equals("x")
								|| report.getDay_3_value().equals("x") || report.getDay_4_value().equals("x")
								|| report.getDay_5_value().equals("x") || report.getDay_6_value().equals("x")
								|| report.getDay_7_value().equals("x") || report.getDay_8_value().equals("x")
								|| report.getDay_9_value().equals("x") || report.getDay_10_value().equals("x")
								|| report.getDay_11_value().equals("x") || report.getDay_12_value().equals("x")
								|| report.getDay_13_value().equals("x") || report.getDay_14_value().equals("x")
								|| report.getDay_15_value().equals("x") || report.getDay_16_value().equals("x")
								|| report.getDay_17_value().equals("x") || report.getDay_18_value().equals("x")
								|| report.getDay_19_value().equals("x") || report.getDay_20_value().equals("x")
								|| report.getDay_21_value().equals("x") || report.getDay_22_value().equals("x")
								|| report.getDay_23_value().equals("x") || report.getDay_24_value().equals("x")
								|| report.getDay_25_value().equals("x") || report.getDay_26_value().equals("x")
								|| report.getDay_27_value().equals("x") || report.getDay_28_value().equals("x"))
						.sorted(Comparator.comparing(LookaheadJasperReportDetailDTO::getDay_1_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_2_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_3_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_4_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_5_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_6_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_7_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_8_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_9_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_10_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_11_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_12_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_13_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_14_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_15_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_16_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_17_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_18_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_19_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_20_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_21_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_22_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_23_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_24_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_25_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_26_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_27_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_28_value))
						.parallel().collect(Collectors.toList());
				List<Long> scheduledIds = new ArrayList<>();
				scheduledSortedReport.forEach(report -> scheduledIds.add(report.getTaskId()));
				List<Long> unscheduledId = new ArrayList<>(unscheduledIds);
				unscheduledId.removeAll(scheduledIds);
				loadIds.addAll(unscheduledId);
				loadIds.addAll(scheduledIds);
				List<LookaheadJasperReportDetailDTO> lookaheadSortedReport = lookaheadReport.stream()
						.sorted(Comparator.comparing(LookaheadJasperReportDetailDTO::getDay_1_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_2_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_3_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_4_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_5_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_6_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_7_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_8_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_9_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_10_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_11_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_12_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_13_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_14_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_15_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_16_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_17_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_18_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_19_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_20_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_21_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_22_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_23_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_24_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_25_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_26_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_27_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_28_value))
						.parallel().collect(Collectors.toList());
				lookaheadReport.clear();
				lookaheadReport.addAll(lookaheadSortedReport);
			} else {
				List<LookaheadJasperReportDetailDTO> lookaheadSortedReport = lookaheadReport.stream()
						.sorted(Comparator.comparing(LookaheadJasperReportDetailDTO::getDay_1_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_2_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_3_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_4_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_5_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_6_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_7_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_8_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_9_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_10_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_11_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_12_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_13_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_14_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_15_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_16_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_17_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_18_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_19_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_20_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_21_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_22_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_23_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_24_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_25_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_26_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_27_value)
								.thenComparing(LookaheadJasperReportDetailDTO::getDay_28_value).reversed())
						.parallel().collect(Collectors.toList());
				lookaheadReport.clear();
				lookaheadReport.addAll(lookaheadSortedReport);
			}
		} else {
			if (null != lookaheadListDTO.getSortType() && lookaheadListDTO.getSortType().equalsIgnoreCase("asc")) {
				List<LookaheadJasperReportDetailDTO> lookaheadSortedReport = lookaheadReport.stream()
						.sorted(Comparator.comparingLong(LookaheadJasperReportDetailDTO::getTaskId)
								.thenComparingLong(LookaheadJasperReportDetailDTO::getSubTaskId))
						.parallel().collect(Collectors.toList());
				lookaheadReport.clear();
				lookaheadReport.addAll(lookaheadSortedReport);
			} else {
				List<LookaheadJasperReportDetailDTO> lookaheadSortedReport = lookaheadReport.stream()
						.sorted(Comparator.comparingLong(LookaheadJasperReportDetailDTO::getTaskId)
								.thenComparingLong(LookaheadJasperReportDetailDTO::getSubTaskId).reversed())
						.parallel().collect(Collectors.toList());
				lookaheadReport.clear();
				lookaheadReport.addAll(lookaheadSortedReport);
			}
		}
		Map<Long, List<LookaheadJasperReportDetailDTO>> map = new LinkedHashMap<>();

		if (null != loadIds && !loadIds.isEmpty()) {
			loadIds.forEach(id -> map.put(id, null));
		}

		for (LookaheadJasperReportDetailDTO lookaheadJasperReportDetailDTO : lookaheadReport) {
			if (map.get(lookaheadJasperReportDetailDTO.getTaskId()) != null) {
				List<LookaheadJasperReportDetailDTO> existList = map.get(lookaheadJasperReportDetailDTO.getTaskId());
				existList.add(lookaheadJasperReportDetailDTO);
				map.put(lookaheadJasperReportDetailDTO.getTaskId(), existList);
			} else {
				List<LookaheadJasperReportDetailDTO> newList = new ArrayList<>();
				newList.add(lookaheadJasperReportDetailDTO);
				map.put(lookaheadJasperReportDetailDTO.getTaskId(), newList);
			}
		}
		List<LookaheadJasperReportDetailDTO> finalList = new ArrayList<>();
		Map<Long, List<LookaheadJasperReportDetailDTO>> internalList = new HashMap<>();
		map.entrySet().forEach(obj -> {
			List<LookaheadJasperReportDetailDTO> mapList = obj.getValue();
			Long taskid = mapList.get(0).getTaskId();
			mapList.add(0, getTask(lookaheadReport, taskid));
			internalList.put(obj.getKey(), mapList);
			finalList.addAll(mapList);
		});
		List<LookaheadJasperReportDetailDTO> result = new ArrayList<>();
		result = finalList.stream().distinct().collect(Collectors.toList());
		lookaheadJasperReportRootDTO.setLookaheads(result);
		if (null == lookaheadReport || lookaheadReport.isEmpty())
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, " Tasks & SubTask Not Found", null);
		long reportTimestamp = ZonedDateTime.now().toInstant().toEpochMilli();
		String fileName = "lookahead-" + reportTimestamp + ".json";
		String json = LookaheadHelper.writeJsonToFile(lookaheadJasperReportRootDTO, fileName);
		String senderName = projectRepository.findUserNameByUserId(userId);
		if (null != json || "".equals(json))
			if (null != lookaheadListDTO.getEmail() && lookaheadListDTO.getEmail().length > 0) {
				List<String> emailList = Arrays.asList(lookaheadListDTO.getEmail()).stream().map(String::toLowerCase)
						.collect(Collectors.toList());
				messageQueueService.sendJsonReport(senderName, projectName, fileName, String.join(",", emailList),
						reportTimestamp, json, "lookaheadReport");
			} else
				log.info("no emil found");
		else
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "Lookahead Report Json Not created", null);
		return new MSAResponse(true, HttpStatus.OK, "Mail Sent " + SUCSFLY, lookaheadJasperReportRootDTO);
	}

	/**
	 * Query result.
	 *
	 * @param query the query
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> queryResult(String query) {
		return entityManager.createNativeQuery(query).getResultList();
	}

	/**
	 * Gets the task.
	 *
	 * @param lookaheadReport the lookahead report
	 * @param taskid          the taskid
	 * @return the task
	 */
	private LookaheadJasperReportDetailDTO getTask(List<LookaheadJasperReportDetailDTO> lookaheadReport, Long taskid) {
		return lookaheadReport.stream().filter(obj -> obj.isTask() == true && obj.getTaskId().equals(taskid))
				.findFirst().get();
	}

	/**
	 * Gets the required completion date.
	 *
	 * @param requiredCompletionDt the required completion dt
	 * @param deadLineDt           the dead line dt
	 * @return the required completion date
	 */
	public static Date getRequiredCompletionDate(Date requiredCompletionDt, Date deadLineDt) {
		Date reqCompletionDt = null;
		if (requiredCompletionDt != null && deadLineDt != null) {
			if (requiredCompletionDt.after(deadLineDt) || requiredCompletionDt.equals(deadLineDt)) {
				reqCompletionDt = deadLineDt;
			} else {
				reqCompletionDt = requiredCompletionDt;
			}
		} else if (requiredCompletionDt != null) {
			reqCompletionDt = requiredCompletionDt;
		} else if (deadLineDt != null) {
			reqCompletionDt = deadLineDt;
		}
		return reqCompletionDt;
	}

	/**
	 * Tracking sub task status.
	 *
	 * @param reqCompletionDt    the req completion dt
	 * @param resultScheduleDTOs the result schedule DT os
	 * @param subTaskDTO         the sub task DTO
	 * @param projectFloatDays   the project float days
	 * @return the list
	 */
	public static List<ScheduleDTO> trackingSubTaskStatus(Date reqCompletionDt, List<ScheduleDTO> resultScheduleDTOs,
			SubTaskDTO subTaskDTO, int projectFloatDays) {
		if (reqCompletionDt != null) {
			Date finalRcd = reqCompletionDt;
			resultScheduleDTOs.stream().filter(obj -> finalRcd.before(obj.getWorkingDateInDate()))
					.collect(Collectors.toList()).forEach(obj -> {
						obj.setOnTrack(false);
						obj.setBehindSchedule(true);
						subTaskDTO.setBehindSchedule(true);
					});
			int floatDays = 0;
			floatDays = projectFloatDays + 1;
			final Integer finalFloatDays = floatDays;
			Date minimumWarningReqCompletionDt = getDateAfterSubtract(finalRcd, finalFloatDays);
			List<ScheduleDTO> warningScheduleDays = resultScheduleDTOs.stream()
					.filter(obj -> (obj.getWorkingDateInDate().after(minimumWarningReqCompletionDt)
							&& obj.getWorkingDateInDate().before(finalRcd))
							|| obj.getWorkingDateInDate().equals(finalRcd))
					.collect(Collectors.toList());
			warningScheduleDays.forEach(obj -> {
				obj.setWarning(true);
				obj.setOnTrack(false);
				subTaskDTO.setWarning(true);
			});

		}
		return resultScheduleDTOs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mindzen.planner.service.LookaheadAccessService#uploadExcelToLookahead(
	 * java.io.InputStream, java.lang.String)
	 */
	@Override
	@Transactional
	public MSAResponse uploadExcelToLookahead(FileInputStream inputStream, String name, Long projectId,
			Long projCompanyId) {
		MSAResponse msaResponse;
		Map<String, List<ImportExcelTaskDTO>> importExcelTOObjList;
		LookaheadDTO lookaheadDTO = new LookaheadDTO();
		List<TaskDTO> taskDTOList = new ArrayList<>();
		if (inputStream == null)
			importExcelTOObjList = ExcelUtil.transformExcelFileToObject(name);
		else
			importExcelTOObjList = ExcelUtil.transformExcelStreamToObject(inputStream, name);
		if (importExcelTOObjList.isEmpty())
			return new MSAResponse(false, HttpStatus.OK, "Upload fails!!!", Collections.emptyList());
		else {
			Long[] emptyArray = null;
			lookaheadDTO.setTaskIds(emptyArray);
			lookaheadDTO.setSubTaskIds(emptyArray);
			lookaheadDTO.setScheduleIds(emptyArray);
			lookaheadDTO.setProjectId(projectId);
			lookaheadDTO.setSubmit(false);
			lookaheadDTO.setLookaheadDate(
					DateUtil.getLocalDateToString(LocalDate.now(), DateUtil.Formats.DDMMYYYY_HYPHEN.toString()));
			for (Entry<String, List<ImportExcelTaskDTO>> importExcelList : importExcelTOObjList.entrySet()) {
				taskDTOList.add(transformExcelObjectToLookahead(importExcelList, projCompanyId, projectId));
				lookaheadDTO.setTasks(taskDTOList);
			}

			log.info("Lookahead JSON: " + lookaheadDTO);
			msaResponse = createLookahead(lookaheadDTO);
		}
		return msaResponse;

	}

	/**
	 * Transform excel object to lookahead.
	 *
	 * @param importExcelList the import excel list
	 * @param projCompanyId
	 * @param projectId
	 * @return the task DTO
	 */
	public TaskDTO transformExcelObjectToLookahead(Entry<String, List<ImportExcelTaskDTO>> importExcelList,
			Long projCompanyId, Long projectId) {
		List<SubTaskDTO> subTaskList = new ArrayList<>();
		TaskDTO taskDTO = new TaskDTO();
		log.info(importExcelList.getKey());
		taskDTO.setDescription(importExcelList.getKey());
		Long projectCompanyId = null;

		if (!importExcelList.getValue().isEmpty()) {
			for (ImportExcelTaskDTO importObj : importExcelList.getValue()) {
				// taskDTO.setDescription(importObj.getTaskDescription());

				// if(importObj.getTaskDescription() != null)
				// subTaskList = new ArrayList<>();
				SubTaskDTO subTaskDTO = new SubTaskDTO();

				subTaskDTO.setDescription(importObj.getSubTaskDescription());

				if (importObj.getStartDate() != null)
					subTaskDTO.setTaskStartDate(importObj.getStartDate().replace("/", "-"));
				else
					subTaskDTO.setTaskStartDate(null);

				if (importObj.getTargetCompletionDate() != null)
					subTaskDTO.setRequiredCompletionDate(importObj.getTargetCompletionDate().replace("/", "-"));
				else
					subTaskDTO.setRequiredCompletionDate(null);

				if (importObj.getFinishDate() != null)
					subTaskDTO.setTaskFinishDate(importObj.getFinishDate().replace("/", "-"));
				else
					subTaskDTO.setTaskFinishDate(null);

				subTaskDTO.setActivityId(importObj.getActivityId());
				if (importObj.getImportUniqueId() != null && !importObj.getImportUniqueId().equals("null"))
					subTaskDTO.setImportUniqueId(importObj.getImportUniqueId());
				subTaskDTO.setActivityDescription(importObj.getTaskDescription());

				subTaskDTO.setTaskType(importObj.getTaskType());
				subTaskDTO.setBallInCourt(importObj.getResponsibility());
				subTaskDTO.setProjectCompanyId(projCompanyId);
				subTaskDTO.setSpecDivision(importObj.getSpecDivision());
				projectCompanyId = projectCompanyRepository.findCompaniesByProjectIdAndCompanyName(projectId,
						importObj.getResponsibility());
				if (projectCompanyId != null)
					subTaskDTO.setProjectCompanyId(projectCompanyId);
				else
					subTaskDTO.setProjectCompanyId(projCompanyId);
				// its generated by lookahead create method
				// createScheduleList(subTaskDTO, projectId);
				subTaskDTO.setNote(importObj.getNotes());
				subTaskList.add(subTaskDTO);
				if (importObj.getSubTaskDescription() != null) {
					taskDTOSetPayload(taskDTO, importObj, projectId, projectCompanyId);
				} else {
					taskDTOSetPayload(taskDTO, importObj, projectId, projCompanyId);
					// if(importExcelList.getValue().size()==1)
					// subTaskList=Collections.emptyList();
					// else
					subTaskList.stream().filter(subTask -> subTask.getDescription() == null)
							.forEach(subTask -> subTask.setDescription(importExcelList.getKey()));
				}
			}
		} else {
			subTaskList = Collections.emptyList();
		}
		taskDTO.setSubTasks(subTaskList);
		if (!subTaskList.isEmpty())
			taskDTO.setChild(true);
		return taskDTO;
	}

	private void taskDTOSetPayload(TaskDTO taskDTO, ImportExcelTaskDTO importObj, Long projectId,
			Long projectCompanyId) {
		if (importObj.getStartDate() != null)
			taskDTO.setTaskStartDate(importObj.getStartDate().replace("/", "-"));
		else
			taskDTO.setTaskStartDate(null);
		if (importObj.getTargetCompletionDate() != null)
			taskDTO.setRequiredCompletionDate(importObj.getTargetCompletionDate().replace("/", "-"));
		else
			taskDTO.setRequiredCompletionDate(null);
		if (importObj.getFinishDate() != null)
			taskDTO.setTaskFinishDate(importObj.getFinishDate().replace("/", "-"));
		else
			taskDTO.setTaskFinishDate(null);
		taskDTO.setTaskType(importObj.getTaskType());
		taskDTO.setBallInCourt(importObj.getResponsibility());
		taskDTO.setSpecDivision(importObj.getSpecDivision());
		Long projectCompanyIdByQuery = projectCompanyRepository.findCompaniesByProjectIdAndCompanyName(projectId,
				importObj.getResponsibility());
		if (projectCompanyIdByQuery != null)
			taskDTO.setProjectCompanyId(projectCompanyIdByQuery);
		else
			taskDTO.setProjectCompanyId(projectCompanyId);

		taskDTO.setActivityId(importObj.getActivityId());
		if (!importObj.getImportUniqueId().equalsIgnoreCase("null,") && importObj.getImportUniqueId() != null)// importObj.getImportUniqueId()!=null
		// ||!importObj.getImportUniqueId().("null")
		// ||
		{
			taskDTO.setImportUniqueId(Integer.parseInt(subString(importObj.getImportUniqueId())));// Integer.valueOf(importObj.getImportUniqueId()));
		}
		taskDTO.setNote(importObj.getNotes());

	}

	/**
	 * Creates the schedule list.
	 *
	 * @param subTaskDTO the sub task DTO
	 * @param projectId  the project id
	 * @return the sub task DTO
	 */
	private SubTaskDTO createScheduleList(SubTaskDTO subTaskDTO, Long projectId) {
		List<ScheduleDTO> schedules = new ArrayList<>();
		Optional<Project> projOpt = projectRepository.findById(projectId);
		List<LocalDate> workingDateList = DateUtil.getAllDatesBetween2LocalDate(
				DateUtil.getLocalDate(subTaskDTO.getTaskStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()),
				DateUtil.getLocalDate(subTaskDTO.getTaskFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
						.plusDays(1));
		if (workingDateList.isEmpty())
			return subTaskDTO;
		else {
			if (projOpt.isPresent()) {
				List<String> leaveDays = Arrays.asList(projOpt.get().getLeaveDays().split(","));
				for (String leaveDay : leaveDays) {
					workingDateList = workingDateList.stream()
							.filter(workDate -> !workDate.getDayOfWeek().toString().equalsIgnoreCase(leaveDay))
							.collect(Collectors.toList());
				}
				workingDateList.forEach(workingDate -> {
					ScheduleDTO scheduleDTO = new ScheduleDTO();
					scheduleDTO.setWorkingDate(
							DateUtil.getLocalDateToString(workingDate, DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
					schedules.add(scheduleDTO);
				});
				subTaskDTO.setSchedules(schedules);
			}
		}
		return subTaskDTO;
	}

	private String subString(String data) {
		return data.substring(0, data.length() - 1);
	}

	@Override
	public String exportMppFile(Long projectId) {
		/*
		 * Long start = System.currentTimeMillis(); LookaheadListDTO lookaheadListDTO =
		 * new LookaheadListDTO();
		 * 
		 * List<Object[]> minMaxDate =
		 * scheduleRepository.findMinMaxScheduleByProjectId(projectId); LocalDate
		 * startDate = LocalDate.parse(String.valueOf(minMaxDate.get(0)[0]).substring(0,
		 * 10)); LocalDate finishDate =
		 * LocalDate.parse(String.valueOf(minMaxDate.get(0)[1]).substring(0, 10)); Long
		 * betweenDaysCount = ChronoUnit.DAYS.between(startDate, finishDate);
		 * 
		 * lookaheadListDTO.setLookaheadDate(DateUtil.convertDate(DateUtil.
		 * convertToDateViaInstant(startDate),
		 * DateUtil.Formats.DDMMMYYYY_HYPHEN.toString()));
		 * lookaheadListDTO.setProjectId(projectId); lookaheadListDTO.setPageNo(1);
		 * lookaheadListDTO.setPageSize(10000000); LookaheadFilterDTO lookaheadFilterDTO
		 * = new LookaheadFilterDTO();
		 * lookaheadFilterDTO.setScheduleAvailability(Constants.ALL);
		 * lookaheadListDTO.setFilter(lookaheadFilterDTO); MSAResponse response =
		 * getLookahead(lookaheadListDTO, betweenDaysCount+1, true); LookaheadDTO
		 * lookaheadDTO = objectMapper.convertValue(response.getPayload(),
		 * LookaheadDTO.class); if(lookaheadDTO.getTasks() != null &&
		 * !lookaheadDTO.getTasks().isEmpty()) { try { List<TaskDTO> tasks =
		 * lookaheadDTO.getTasks(); String dataDir = "/home/mindzen/Desktop/AWS/temp/";
		 * com.aspose.tasks.Project project = new com.aspose.tasks.Project(dataDir +
		 * "Project1.mpp"); tasks.forEach(task -> { try { FileInputStream fstream = new
		 * FileInputStream(dataDir + "Aspose.Tasks.lic"); License license = new
		 * License(); license.setLicense(fstream);
		 * 
		 * com.aspose.tasks.Task localData =
		 * project.getRootTask().getChildren().add(task.getDescription());
		 * localData.set(Tsk.START, task.getTaskStartDateInDate());
		 * localData.set(Tsk.FINISH, task.getExpectedCompletionDateInDate()); } catch
		 * (Exception e) { System.out.println("File conversion error"); } });
		 * project.save(dataDir + "AfterLinking1.mpp", SaveFileFormat.MPP); } catch
		 * (Exception e) { System.out.println("File conversion error"); } } Long end =
		 * System.currentTimeMillis(); System.out.println("Time taken"+ (end-start));
		 */
		return null;
	}

}
// return
// taskStatus.stream().anyMatch(r->r.isBehindSchedule()==obj.isBehindSchedule()
// || r.isOnTrack()==obj.isOnTrack()
// || r.isWarning()==obj.isWarning());
