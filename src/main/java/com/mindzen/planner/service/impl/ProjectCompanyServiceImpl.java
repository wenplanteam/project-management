/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.ERR;
import static com.mindzen.infra.api.response.ApiResponseConstants.FND;
import static com.mindzen.infra.api.response.ApiResponseConstants.NOT;
import static com.mindzen.infra.api.response.ApiResponseConstants.REC;
import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.ProjectCompanyDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectCompany;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.service.ProjectCompanyService;
import com.mindzen.planner.util.Constants;

import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectCompanyServiceImpl.
 */
@Component

/** The Constant log. */
@Slf4j
public class ProjectCompanyServiceImpl  implements ProjectCompanyService{

	/** The project user repository. */
	ProjectUserRepository projectUserRepository;
	
	/** The project company repository. */
	ProjectCompanyRepository projectCompanyRepository;
	
	/** The project repository. */
	ProjectRepository projectRepository;

	/**
	 * Instantiates a new project company service impl.
	 *
	 * @param projectCompanyRepository the project company repository
	 * @param projectRepository the project repository
	 * @param projectUserRepository the project user repository
	 */
	public ProjectCompanyServiceImpl(ProjectCompanyRepository projectCompanyRepository, ProjectRepository projectRepository, ProjectUserRepository projectUserRepository) {
		this.projectCompanyRepository = projectCompanyRepository;
		this.projectRepository = projectRepository;
		this.projectUserRepository = projectUserRepository;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectCompanyService#getProjectCompanies(java.lang.Long)
	 */
	@Override
	public MSAResponse getProjectCompanies(Long projectId) {
		Long userId = UserInfoContext.getUserThreadLocal();
		MSAResponse msaResponse = null;
		if(null != projectId) {
			ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId, userId, true, false);
			ProjectCompanyDTO projectCompanyDTO = new ProjectCompanyDTO();
			List<CompanyDTO> companyDTOList = new ArrayList<>(); 
			Long companyId = null;
			String userCompany = null;
			String projectRole = null;
			Long projectCompanyId = null;
			if(null != projectUser.getProjectCompany()) {
				companyId = projectUser.getProjectCompany().getCompanyId();
				userCompany = projectCompanyRepository.findCompanyById(companyId);
				projectRole = projectUser.getProjectCompany().getProjectRole();
				projectCompanyId = projectUser.getProjectCompany().getId();
			}
			else {
				Object[] company = (Object[]) projectUserRepository.findCompanyByProjectIdAndUserId(projectId, userId);
				log.info("Company: {}", company);
				userCompany = (String) company[0];
				companyId = ((BigInteger) company[1]).longValue();
				projectRole = projectUser.getProjectRole();
				ProjectCompany projectCompany  = projectCompanyRepository.findByCompanyIdAndProjectId(companyId,projectId);
				projectCompanyId = projectCompany.getId();

			}
			//	if(Constants.SUB_CONTRACTOR.equalsIgnoreCase(projectUser.getProjectRole())) {
			if(projectUser.getProjectRole().contains(Constants.SC_USERS)) {
				projectCompanyDTO.setProjectId(projectId);
				CompanyDTO companyDTO =  new CompanyDTO();
				companyDTO.setId(companyId);
				companyDTO.setName(userCompany);
				companyDTO.setProjectRole(projectRole);
				companyDTO.setProjectCompanyId(projectCompanyId);
				Long projectCompanyAvailability = projectUserRepository.findProjectComapanyAvailabilityByProjectCompanyId(projectCompanyId);
				if(projectCompanyAvailability > 0)
					companyDTO.setProjectUserAvailable(false);
				else
					companyDTO.setProjectUserAvailable(true);
				companyDTOList.add(companyDTO);
				projectCompanyDTO.setProjectCompanies(companyDTOList);
				msaResponse = new MSAResponse(true, HttpStatus.OK, SUCS, projectCompanyDTO);
			}
			else {
				List<Object[]> projectCompanies = projectCompanyRepository.findCompaniesByprojectId(projectId);
				if(null != projectCompanies) {
					projectCompanies.forEach(projectCompany -> {
						projectCompanyDTO.setProjectId(((BigInteger) projectCompany[0]).longValue());
						CompanyDTO companyDTO =  new CompanyDTO();
						companyDTO.setId(((BigInteger) projectCompany[1]).longValue());
						companyDTO.setName((String) projectCompany[2]);
						companyDTO.setProjectRole((String) projectCompany[3]);
						ProjectCompany projectAssociatedCompany  = projectCompanyRepository.findByCompanyIdAndProjectId(companyDTO.getId(),projectId);
						companyDTO.setProjectCompanyId(projectAssociatedCompany.getId());
						Long taskCount = projectCompanyRepository.findBallInCourtAvailabilityInTaskByProjectIdAndProjectCompanyId(projectId, projectAssociatedCompany.getId());
						Long subTaskCount = projectCompanyRepository.findBallInCourtAvailabilityInSubTaskByProjectIdAndProjectCompanyId(projectId, projectAssociatedCompany.getId());
						if( 0 < taskCount || 0 < subTaskCount ) 
							companyDTO.setLookaheadAvailable(true);
						else
							companyDTO.setLookaheadAvailable(false);
						Long projectCompanyAvailability = projectUserRepository.findProjectComapanyAvailabilityByProjectCompanyId(projectAssociatedCompany.getId());
						if(projectCompanyAvailability > 0)
							companyDTO.setProjectUserAvailable(true);
						else
							companyDTO.setProjectUserAvailable(false);
						companyDTOList.add(companyDTO);
					});
					projectCompanyDTO.setProjectCompanies(companyDTOList);
					msaResponse = new MSAResponse(true, HttpStatus.OK, SUCS,projectCompanyDTO);
				}
				else 
					msaResponse = new MSAResponse(false, HttpStatus.BAD_REQUEST, REC + NOT + FND, "E-001", null);
			}
		}
		else 
			msaResponse = new MSAResponse(false, HttpStatus.BAD_REQUEST, ERR, "E-001", null);
		return msaResponse;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectCompanyService#insertProjectCompanies(com.mindzen.planner.dto.ProjectCompanyDTO)
	 */
	@Override
	public MSAResponse insertProjectCompanies(ProjectCompanyDTO projectCompanyDTO) {
		MSAResponse msaResponse = null;
		if(null != projectCompanyDTO.getDeleteCompanies() && !projectCompanyDTO.getDeleteCompanies().isEmpty())
			projectCompanyRepository.deleteCompaniesByProjectId(projectCompanyDTO.getProjectId(),projectCompanyDTO.getDeleteCompanies());
		List<ProjectCompany> projectCompanyList =  new ArrayList<>();
		if(null != projectCompanyDTO.getProjectId() && null != projectCompanyDTO.getProjectCompanies()) {
			Project project = projectRepository.findById(projectCompanyDTO.getProjectId()).get();
			projectCompanyDTO.getProjectCompanies().forEach(companies -> {
				ProjectCompany  projectCompany ;
				projectCompany = projectCompanyRepository.findCompanyAvailabilityByProjectId(projectCompanyDTO.getProjectId(),companies.getId());
				if(null != projectCompany) {
					projectCompany.setCompanyId(companies.getId());
					projectCompany.setProject(project);
					projectCompany.setActive(true);
					projectCompany.setProjectRole(companies.getProjectRole()); 
				}
				else {
					projectCompany = new ProjectCompany();
					projectCompany.setCompanyId(companies.getId());
					projectCompany.setProject(project);
					projectCompany.setActive(true);
					projectCompany.setProjectRole(companies.getProjectRole()); 
				}
				projectCompanyList.add(projectCompany);
			});
			List<ProjectCompany> projectCompaniesList = projectCompanyRepository.saveAll(projectCompanyList);
			List<Long> projectCompanyIds = new ArrayList<>();
			projectCompaniesList.forEach(projectCompany -> projectCompanyIds.add(projectCompany.getId()));
			msaResponse = new MSAResponse(true, HttpStatus.OK, "Companies Associated Successfully", projectCompanyIds);
		}
		else {
			msaResponse = new MSAResponse(false, HttpStatus.BAD_REQUEST, ERR, "E-001", null);
		}
		return msaResponse;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectCompanyService#userCompanyAssociateToProject(java.lang.Long)
	 */
	@Override
	public MSAResponse userCompanyAssociateToProject(Long userId) {
		Long companyId = projectCompanyRepository.findCompanyByUserId(userId);
		List<ProjectUser> projectUserList = projectUserRepository.findProjectUserByUserId(userId,true,false);
		projectUserList.forEach(projectUser -> {
			ProjectCompany projectCompany =
					projectCompanyRepository.findCompanyAvailabilityByProjectId(projectUser.getProject().getId(),companyId);
			//	findProjectCompanyByCompanyIdAndProjectId(companyId,projectUser.getProject().getId(),true,false);
			if(null ==projectCompany) {
				projectCompany = new ProjectCompany();
				projectCompany.setActive(true);
				projectCompany.setCompanyId(companyId);
				projectCompany.setProjectRole(projectUser.getProjectRole());
				projectCompany.setProject(projectUser.getProject());
				projectCompany = projectCompanyRepository.save(projectCompany);
			}
			else if(projectCompany.isActive() == false || projectCompany.isDeleted() == true) {
				projectCompany.setActive(true);
				projectCompany.setDeleted(false);
				projectCompany = projectCompanyRepository.save(projectCompany);
			}
			projectUser.setProjectCompany(projectCompany);
		});
		if (!projectUserList.isEmpty()) 
			projectUserRepository.saveAll(projectUserList);
		return new MSAResponse(true, HttpStatus.OK, SUCS, null);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectCompanyService#updateProjectCompanies(com.mindzen.planner.dto.UserDTO)
	 */
	@Override
	public MSAResponse updateProjectCompanies(UserDTO userDTO) {
		Long userId =  UserInfoContext.getUserThreadLocal();
		List<ProjectUser> projectUserList = projectUserRepository.
				findByUserIdAndIsDeletedAndCreatedBy(userDTO.getId(),false,userId);

		projectUserList.forEach(projectUser ->{
			ProjectCompany projectCompany =projectCompanyRepository.findCompanyAvailabilityByProjectId
					(projectUser.getProject().getId(), userDTO.getCompanyId());
			if(null == projectCompany) {
				projectCompany = new ProjectCompany();
				projectCompany.setProject(projectUser.getProject());
				projectCompany.setCompanyId(userDTO.getCompanyId());
				if(projectUser.getProjectRole().contains(Constants.GC_USERS ))
					projectCompany.setProjectRole(Constants.GENERAL_CONTRACTOR);
				else
					projectCompany.setProjectRole(Constants.SUB_CONTRACTOR);
				projectCompany.setActive(true);
				projectCompany.setDeleted(false);
				projectCompany = projectCompanyRepository.save(projectCompany);
			}
			else if(projectCompany.isActive() == false || projectCompany.isDeleted() == true) {
				projectCompany.setActive(true);
				projectCompany.setDeleted(false);
				projectCompany = projectCompanyRepository.save(projectCompany);
			}
			projectUser.setProjectCompany(projectCompany);
			projectUserRepository.save(projectUser);
		});
		return new MSAResponse(true, HttpStatus.OK, SUCS, null);
	}
}
