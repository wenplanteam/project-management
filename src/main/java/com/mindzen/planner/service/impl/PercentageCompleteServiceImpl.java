package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.SUCSFLY;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.util.GsonUtil;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadFilterDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.PercentCompleteJasperReportDetailDTO;
import com.mindzen.planner.dto.PercentCompleteJasperReportDetailRootDTO;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProgressCompletionResponseDTO;
import com.mindzen.planner.dto.TaskDTO;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.service.LookaheadAccessService;
import com.mindzen.planner.service.PercentageCompleteService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;
import com.mindzen.planner.util.LookaheadHelper;
import com.mindzen.planner.util.PathUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PercentageCompleteServiceImpl implements PercentageCompleteService {

	private ProjectRepository projectRepository;

	private LookaheadAccessService lookaheadAccessService;
	
	private ProjectServiceImpl projectServiceImpl;

	private ObjectMapper objectMapper;

	private ProjectUserRepository projectUserRepository;

	private MessageQueueService messageQueueService;

	public PercentageCompleteServiceImpl(ProjectRepository projectRepository,
			LookaheadAccessService lookaheadAccessService, ProjectServiceImpl projectServiceImpl,
			ObjectMapper objectMapper, ProjectUserRepository projectUserRepository,
			MessageQueueService messageQueueService) {
		super();
		this.projectRepository = projectRepository;
		this.lookaheadAccessService = lookaheadAccessService;
		this.projectServiceImpl = projectServiceImpl;
		this.objectMapper = objectMapper;
		this.projectUserRepository = projectUserRepository;
		this.messageQueueService = messageQueueService;
	}

	@Override
	public MSAResponse downloadPercentageCompletePDF(ProgressCompletionRequestDTO progressCompletionRequestDTO) {
		Optional<Project> projectOptional = projectRepository.findById(progressCompletionRequestDTO.getProjectId());
		Long userId = UserInfoContext.getUserThreadLocal();
		List<PercentCompleteJasperReportDetailDTO> resultObj = new ArrayList<>();
		int from = 0;
		if(projectOptional.isPresent()) {
			List<ProgressCompletionResponseDTO> progressCompletionResList=new ArrayList<>();
			Project project = projectOptional.get();
			List<String> requestStatus = progressCompletionRequestDTO.getFilterDTO().getTaskStatus();
			CompletableFuture<MSAResponse> lookaheadResult = getLookaheadResult(progressCompletionRequestDTO);
			final MSAResponse msaResponse;
			final String exceptionMsg = "Interupted Exception";
			try {
				msaResponse = lookaheadResult.get();
			} catch (Exception e) {
				return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, exceptionMsg, Collections.emptyList());
			}
			if(!msaResponse.isSuccess()) {
				return new MSAResponse(false, HttpStatus.OK, "Tasks & SubTask Not Found", Collections.emptyList());
			}
			CompletableFuture<List<ProgressCompletionResponseDTO>> result= CompletableFuture.supplyAsync(() -> {
				LookaheadFilterDTO lookaheadFilter = progressCompletionRequestDTO.getFilterDTO();
				LookaheadDTO lookaheadDTO = objectMapper.convertValue(msaResponse.getPayload(), LookaheadDTO.class);
				List<TaskDTO> taskDTOList = lookaheadDTO.getTasks();
				if(!taskDTOList.isEmpty()) {
					lookaheadFilter.setTaskStatus(requestStatus);
					progressCompletionRequestDTO.setFilterDTO(lookaheadFilter);
					progressCompletionRequestDTO.setPageSize(100000);
					projectServiceImpl.transformTaskDetails(progressCompletionResList, taskDTOList, project.getFloatDays(), 
							progressCompletionRequestDTO,from);
				}
				return progressCompletionResList;
			});
			try {
				result.get();
			} catch (Exception e) {
				return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, exceptionMsg, Collections.emptyList());
			}
			
			CompletableFuture<List<PercentCompleteJasperReportDetailDTO>> pdfData =  CompletableFuture.supplyAsync(()-> {
				percentCompletePDFData(project,progressCompletionRequestDTO,progressCompletionResList,resultObj,userId);
				return resultObj;
			});
			try {
				pdfData.get();
			} catch (Exception e) {
				return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, exceptionMsg, Collections.emptyList());
			}
		}
		else
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, "Project Not Found", null);
		if (resultObj.isEmpty())
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, " Tasks & SubTask Not Found", null);
		PercentCompleteJasperReportDetailRootDTO report = new PercentCompleteJasperReportDetailRootDTO();
		report.setPercentComplete(resultObj);
		long reportTimestamp = ZonedDateTime.now().toInstant().toEpochMilli();
		String fileName = "percentageComplete-" + reportTimestamp + ".json";
		String json = writeJsonToFile(report, fileName);
		String senderName = projectRepository.findUserNameByUserId(userId);
		if (null != json || !"".equals(json))
			if (null != progressCompletionRequestDTO.getEmail() && progressCompletionRequestDTO.getEmail().length > 0) {
				List<String> emailList = Arrays.asList(progressCompletionRequestDTO.getEmail()).stream().map(String::toLowerCase)
						.collect(Collectors.toList());
				messageQueueService.sendJsonReport(senderName, projectOptional.get().getName(), fileName, String.join(",", emailList),
						reportTimestamp, json, "percentComplete");
			} else
				log.info("no emil found");
		else
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "Percent Complete Report Json Not created", null);
		return new MSAResponse(true, HttpStatus.OK, "Mail Sent " + SUCSFLY, report);
	}

	private void percentCompletePDFData(Project project, ProgressCompletionRequestDTO progressCompletionRequestDTO, 
			List<ProgressCompletionResponseDTO> progressCompletionResList, List<PercentCompleteJasperReportDetailDTO> resultObj, Long userId) {
		String projectName= project.getName();
		String projectLocation = LookaheadHelper.fullProjectLocation(project.getCity(), project.getState());
		String projectCompany = projectUserRepository.findcompanyNameByCreatedBy(project.getCreatedBy());
		String dateRange = progressCompletionRequestDTO.getFromDate() +" to "+ progressCompletionRequestDTO.getToDate();
	//	LocalDateTime currentDateTime = LocalDateTime.now();
	//	LocalTime localTime = currentDateTime.toLocalTime();
		
		String timeZone = projectUserRepository.findTimeZoneByUserId(userId);
		LocalDateTime currentTimeByUserZone = DateUtil.utcZoneDateTimeToZoneDateTimeConvert(LocalDateTime.now(),timeZone);
		String currentTime = currentTimeByUserZone.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm")).toString();
		
		final String emptySpace = "  ";
		progressCompletionResList.forEach(progressCompletion -> {
			PercentCompleteJasperReportDetailDTO taskReport = new PercentCompleteJasperReportDetailDTO();
			taskReport.setProjectName(projectName);
			taskReport.setProjectLocation(projectLocation);
			taskReport.setProjectCompany(projectCompany);
			taskReport.setDateRange(dateRange);
			taskReport.setSno(String.valueOf(progressCompletion.getTaskId()));
			taskReport.setTaskDescription(progressCompletion.getTaskDescription());
			if(progressCompletion.getSubTaskDetails().isEmpty()) {
				Long totalDays = progressCompletion.getTotalNoOfWorkingDays();
				taskReport.setTotalNoOfDays(String.valueOf(totalDays));
				taskReport.setNoOfCompletedDays(String.valueOf(totalDays-progressCompletion.getNumberOfCompletedDays()));
				taskReport.setTaskStartDate(progressCompletion.getStartDate());
				taskReport.setTaskFinishDate(progressCompletion.getFinishDate());
				taskReport.setTargetCompletionDate(progressCompletion.getTargetCompletionDate()==  null ? "":progressCompletion.getTargetCompletionDate());
				taskReport.setResponsibility(progressCompletion.getBallInCourt()==  null ? "":progressCompletion.getBallInCourt());
				taskReport.setPercentageCompleted(String.valueOf(progressCompletion.getPercentageCompletion())+"%");
				taskReport.setTaskStatus(progressCompletion.getTrackingStatus());
			}
			else {
				taskReport.setTotalNoOfDays("");
				taskReport.setNoOfCompletedDays("");
				taskReport.setTaskStartDate("");
				taskReport.setTaskFinishDate("");
				taskReport.setTargetCompletionDate("");
				taskReport.setResponsibility("");
				taskReport.setPercentageCompleted("");
				taskReport.setTaskStatus("");
			}
			taskReport.setCurrentTime(currentTime);
			resultObj.add(taskReport);
			progressCompletion.getSubTaskDetails().forEach(subTaskProgress ->{
				PercentCompleteJasperReportDetailDTO report = new PercentCompleteJasperReportDetailDTO();
				report.setProjectName(projectName.toString());
				report.setProjectLocation(projectLocation);
				report.setProjectCompany(projectCompany);
				report.setDateRange(dateRange);
				report.setSno(String.valueOf(subTaskProgress.getSubTaskId()));
				report.setTaskDescription(emptySpace+subTaskProgress.getSubTaskDescription());
				Long totalDays = subTaskProgress.getTotalNoOfWorkingDays();
				report.setTotalNoOfDays(String.valueOf(totalDays));
				report.setNoOfCompletedDays(String.valueOf(totalDays - subTaskProgress.getNumberOfCompletedDays()));
				report.setTaskStartDate(subTaskProgress.getStartDate());
				report.setTaskFinishDate(subTaskProgress.getFinishDate());
				report.setTargetCompletionDate(subTaskProgress.getTargetCompletionDate()==  null ? "":subTaskProgress.getTargetCompletionDate());
				report.setResponsibility(subTaskProgress.getBallInCourt());
				report.setPercentageCompleted(String.valueOf(subTaskProgress.getPercentageCompletion())+"%");
				report.setTaskStatus(subTaskProgress.getTrackingStatus());
				report.setCurrentTime(currentTime);
				resultObj.add(report);
			});
		});
		
	}

	@Async
	private CompletableFuture<MSAResponse> getLookaheadResult(@Valid ProgressCompletionRequestDTO progressCompletionRequestDTO) {
		LookaheadListDTO lookaheadListDTO = new LookaheadListDTO();
		LookaheadFilterDTO lookaheadFilter = progressCompletionRequestDTO.getFilterDTO();
		lookaheadListDTO.setProjectId(progressCompletionRequestDTO.getProjectId());
		lookaheadListDTO.setLookaheadDate(progressCompletionRequestDTO.getFromDate());

		// for get all records in the particular time line
		lookaheadListDTO.setPageNo(1);
		lookaheadListDTO.setPageSize(100000);

		lookaheadFilter.setScheduleAvailability(Constants.SCHEDULED);
		lookaheadFilter.setTaskStatus(new ArrayList<>());
		lookaheadListDTO.setFilter(lookaheadFilter);
		lookaheadListDTO.setSearch(progressCompletionRequestDTO.isSearch());
		lookaheadListDTO.setSearchTerm(progressCompletionRequestDTO.getSearchTerm());
		LocalDate fromDate = DateUtil.stringToLocalDateViaInstant(progressCompletionRequestDTO.getFromDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		LocalDate toDate = DateUtil.stringToLocalDateViaInstant(progressCompletionRequestDTO.getToDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		Long count = ChronoUnit.DAYS.between(fromDate, toDate);
		//MSAResponse msaResponse = lookaheadAccessService.getLookahead(lookaheadListDTO, count);
		boolean isLookahead = false;
		return CompletableFuture.completedFuture(lookaheadAccessService.getLookahead(lookaheadListDTO, count, isLookahead));
	}

	public static String writeJsonToFile(PercentCompleteJasperReportDetailRootDTO lookaheadJasperReportRootDTO, String fileName) {
		String json = GsonUtil.toJsonStringFromObject(lookaheadJasperReportRootDTO);
		try {
			Path path = Paths.get(PathUtil.WENPLAN_LOOKAHED_JSON_HOME+PathUtil.FILE_SEPARATOR+fileName);
			Files.write(path, json.getBytes());
		}catch(Exception e) {
			log.error(e.getMessage());
		}
		return json;
	}

}
