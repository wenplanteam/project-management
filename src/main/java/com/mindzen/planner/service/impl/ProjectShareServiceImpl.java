/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.SUCS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.ProjectUserDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.entity.NotificationEntity;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectCompany;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.integration.MessageQueueService;
import com.mindzen.planner.repository.NotificationRepository;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.service.ProjectShareService;
import com.mindzen.planner.util.Constants;

import lombok.extern.slf4j.Slf4j;


// TODO: Auto-generated Javadoc
/**
 * The Class ProjectShareServiceImpl.
 */
@Component

/** The Constant log. */
@Slf4j
public class ProjectShareServiceImpl implements ProjectShareService {

	/** The project user repository. */
	ProjectUserRepository projectUserRepository;
	
	/** The message queue service. */
	MessageQueueService messageQueueService;	
	
	/** The project repository. */
	ProjectRepository projectRepository;
	
	/** The project company repository. */
	ProjectCompanyRepository projectCompanyRepository;
	
	/** The notification repository. */
	NotificationRepository notificationRepository;

	/** The response. */
	MSAResponse response = null;

	/**
	 * Instantiates a new project share service impl.
	 *
	 * @param projectUserRepository the project user repository
	 * @param messageQueueService the message queue service
	 * @param projectRepository the project repository
	 * @param projectCompanyRepository the project company repository
	 */
	public ProjectShareServiceImpl(ProjectUserRepository projectUserRepository, MessageQueueService messageQueueService, ProjectRepository projectRepository, ProjectCompanyRepository projectCompanyRepository,
			NotificationRepository notificationRepository) {
		this.projectUserRepository = projectUserRepository;
		this.messageQueueService = messageQueueService;
		this.projectRepository = projectRepository;
		this.projectCompanyRepository = projectCompanyRepository;
		this.notificationRepository = notificationRepository;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectShareService#shareProject(com.mindzen.planner.dto.ProjectUserDTO)
	 */
	@Override
	public MSAResponse shareProject(ProjectUserDTO projectUserDTO) {
		Project project = projectRepository.findById(projectUserDTO.getProjectId()).get();
		List<Long> errorIds = new ArrayList<>();
		if(null  != projectUserDTO.getDeleteUsers() && !projectUserDTO.getDeleteUsers().isEmpty()) {
			projectUserRepository.deleteProjectUsersByMultipleIds(project, projectUserDTO.getDeleteUsers());
			projectUserDTO.getDeleteUserDTO().forEach(projectUser -> {
				messageQueueService.sendProjectShareMail(projectUser.getEmail().toLowerCase(), "project_remove_email_template.html", project.getName(), null, null,"Project Permission Update");
			});
		}
		projectUserDTO.getProjectUsers().forEach(userDTO -> {
			try {
				Long projectId = projectUserDTO.getProjectId();
				ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId, userDTO.getId());
				if(null == projectUser) {
					projectUser = new ProjectUser();
					sendMailAndUpdateProjectUser(userDTO, project, projectUser);
				}
				else
					sendMailAndUpdateProjectUser(userDTO, project, projectUser);
			} catch (Exception e) {
				log.error(e.getMessage());
				errorIds.add(userDTO.getId());
			}
		});
		return new MSAResponse(true, HttpStatus.OK, "Mail Sent Successfully", errorIds);
	}


	/**
	 * Send mail and update project user.
	 *
	 * @param userDTO the user DTO
	 * @param project the project
	 * @param projectUser the project user
	 */
	private void sendMailAndUpdateProjectUser(UserDTO userDTO, Project project, ProjectUser projectUser) {
		String oldProjectUserRole = "";

		String mailSubject= "";
		if(null == projectUser.getId())
			mailSubject = "WenPlan Project Invitation";
		else
			mailSubject = "Project Permission Update";

		if(null != projectUser.getProjectRole())
			oldProjectUserRole = projectUser.getProjectRole();
		String newProjectUserRole = userDTO.getProjectRole();
		String userRole = null;
		List<String> existingAccesses = null;
		int existingAccessesCount =0;
		if(projectUser.getAccess() !=  null) {
			existingAccesses = Arrays.asList(projectUser.getAccess().split(","));
			existingAccessesCount = existingAccesses.size();
		}
		if(!projectUser.isIsActive()&&projectUser.isDeleted())
			oldProjectUserRole = "";
		if(newProjectUserRole.equals(Constants.GC_PROJECT_ADMIN))
			userRole = Constants.ACCESS_VIEW+","+Constants.ACCESS_EDIT+","+Constants.ACCESS_APPROVE;
		else if(newProjectUserRole.equals(Constants.GC_PROJECT_MANAGER))
			userRole = Constants.ACCESS_VIEW+","+Constants.ACCESS_EDIT;
		else if(newProjectUserRole.equals(Constants.GC_OBSERVER))
			userRole = Constants.ACCESS_VIEW;
		else if(newProjectUserRole.equals(Constants.SC_MANAGER))
			userRole = Constants.ACCESS_VIEW+","+Constants.ACCESS_EDIT;
		else if(newProjectUserRole.equals(Constants.SC_OBSERVER))
			userRole = Constants.ACCESS_VIEW;
		else
			userRole = Constants.ACCESS_VIEW;
		List<String> newAccesses = Arrays.asList(userRole.split(","));
		int newAccessesCount = newAccesses.size();
		if((existingAccessesCount != newAccessesCount ) || (!oldProjectUserRole.equals(newProjectUserRole))) {
			projectUser.setUserId(userDTO.getId());
			projectUser.setProject(project);
			if(userDTO.isActive() && userDTO.isEmailVerified())
				projectUser.setIsActive(true);
			else
				projectUser.setIsActive(false);
			projectUser.setAccess(userRole);
			projectUser.setIsDeleted(false);
			if(null != userDTO.getCompanyId()) {
				ProjectCompany projectCompany = 
						//projectCompanyRepository.findByProjectAndCompanyIdAndActiveAndDeleted(project, 
						//userDTO.getCompanyId(),true,false);//findById(userDTO.getProjectCompanyId()).get()
						projectCompanyRepository.findCompanyAvailabilityByProjectId(project.getId(), 
								userDTO.getCompanyId());
				if(null == projectCompany) {
					projectCompany = new ProjectCompany();
					projectCompany.setProject(project);
					projectCompany.setCompanyId(userDTO.getCompanyId());
					if(newProjectUserRole.contains(Constants.GC_USERS ))
						projectCompany.setProjectRole(Constants.GENERAL_CONTRACTOR);
					else
						projectCompany.setProjectRole(Constants.SUB_CONTRACTOR);
					projectCompany.setActive(true);
					projectCompany.setDeleted(false);
					projectCompany = projectCompanyRepository.save(projectCompany);
				}
				else if(projectCompany.isActive() == false || projectCompany.isDeleted() == true) {
					projectCompany.setActive(true);
					projectCompany.setDeleted(false);
					projectCompany = projectCompanyRepository.save(projectCompany);
				}
				projectUser.setProjectCompany(projectCompany);
			}
			projectUser.setProjectRole(newProjectUserRole);
			projectUser.setNotification(saveDefaultNotificaton());
			projectUserRepository.save(projectUser);
			String templateFileName = "";
			if(userDTO.isEmailSend()) 
				log.info("Email not sent");
			else {
				if(userDTO.isActive() && userDTO.isEmailVerified()) {
					if(existingAccessesCount == 0 )
						templateFileName = "project_invite_login_email_template.html";
					else
						templateFileName = "project_revoke_email_template.html";
				}
				else
					templateFileName = "project_invite_signup_email_template.html";
				String senderName=projectRepository.findUserNameByUserId(UserInfoContext.getUserThreadLocal());
				messageQueueService.sendProjectShareMail(userDTO.getEmail().toLowerCase(), templateFileName, project.getName(), userDTO.getFirstName()+" "+userDTO.getLastName(), senderName,mailSubject);
			}

		}

	}

	private NotificationEntity saveDefaultNotificaton() {
		NotificationEntity notificationEntity=new NotificationEntity();
		notificationEntity.setNotification(Constants.False);
		notificationEntity.setType("");
		notificationEntity.setFrequency("");
		notificationEntity.setDayOfTheWeek("");
		notificationEntity.setInformation("");
		notificationEntity=notificationRepository.save(notificationEntity);
		return notificationEntity;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectShareService#editShareProject(com.mindzen.planner.dto.ProjectUserDTO)
	 */
	@Override
	public MSAResponse editShareProject(ProjectUserDTO projectUserDTO) {
		Long projectId = projectUserDTO.getProjectId();
		List<UserDTO> listUsers = projectUserDTO.getProjectUsers();
		listUsers.forEach(userDTO -> {
			ProjectUser projectUser = projectUserRepository.findByProjectIdAndUserId(projectId, userDTO.getId(), true, false);
			if (projectUser != null) {
				if (null == userDTO.getLookaheadAccess()) {
					projectUser.setIsActive(false);
					projectUser.setIsDeleted(true);
				}
				else {
					StringBuilder sb = new StringBuilder();
					String[] lookAgeadAccess = userDTO.getLookaheadAccess();
					if (null != lookAgeadAccess) {
						for (String lookerHeadAccesses : lookAgeadAccess) {
							sb.append(lookerHeadAccesses + ",");
						}
					}
					if (sb.length() > 0) {
						sb.setLength(sb.length() - 1);
					}
					projectUser.setAccess(sb.toString());
				}

				projectUser.setAccess(String.join(",", Arrays.asList(userDTO.getLookaheadAccess())));
				projectUserRepository.save(projectUser);
			}
		});
		return new MSAResponse(true, HttpStatus.OK, "Your privileges has been changed", null);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectShareService#updateProjectSharedUserDetails(com.mindzen.planner.dto.UserDTO)
	 */
	@Override
	public MSAResponse updateProjectSharedUserDetails(UserDTO userDTO) {
		List<ProjectUser> projectUserList = projectUserRepository.findByUserId(userDTO.getId());
		List<ProjectUser> projectUsers = new ArrayList<>();
		projectUserList.forEach(projectUser -> {
			if(!projectUser.isIsActive() && !projectUser.isDeleted()) {
				projectUser.setIsActive(true);
				projectUsers.add(projectUser);
			}
			ProjectCompany projectCompany = 
					projectCompanyRepository.findCompanyAvailabilityByProjectId(projectUser.getProject().getId(), 
							userDTO.getCompanyId());
			if(null == projectCompany) {
				projectCompany = new ProjectCompany();
				projectCompany.setProject(projectUser.getProject());
				projectCompany.setCompanyId(userDTO.getCompanyId());
				projectCompany.setProjectRole(projectUser.getProjectRole());
				projectCompany.setProjectRole(Constants.SUB_CONTRACTOR);
				projectCompany.setActive(true);
				projectCompany.setDeleted(false);
				projectCompany = projectCompanyRepository.save(projectCompany);
			}
			else if(projectCompany.isActive() == false || projectCompany.isDeleted() == true) {
				projectCompany.setActive(true);
				projectCompany.setDeleted(false);
				projectCompany = projectCompanyRepository.save(projectCompany);
			}
		});
		if(!projectUsers.isEmpty())
			projectUserRepository.saveAll(projectUsers);
		return new MSAResponse(true, HttpStatus.OK, SUCS, null);
	}

}
