package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.FND;
import static com.mindzen.infra.api.response.ApiResponseConstants.NOT;
import static com.mindzen.infra.api.response.ApiResponseConstants.REC;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.common.net.MediaType;
import com.mindzen.entity.Track;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.mail.dto.AttachmentDTO;
import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.ImportExcelTaskDTO;
import com.mindzen.planner.dto.ImportHistoryDTO;
import com.mindzen.planner.dto.ImportMPPDTO;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.Pagination;
import com.mindzen.planner.dto.TaskDTO;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.repository.CommonRepository;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.repository.ScheduleRepository;
import com.mindzen.planner.repository.SubTaskRepository;
import com.mindzen.planner.repository.TaskRepository;
import com.mindzen.planner.service.ImportService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;
import com.mindzen.repository.TempRepository;
import com.mindzen.repository.TrackRepository;
import com.mindzen.service.UploadFileService;
import com.mindzen.service.impl.UploadFileServiceImpl;
import com.mindzen.util.Utility;
import com.thoughtworks.xstream.io.xml.TraxSource;

import edu.emory.mathcs.backport.java.util.Arrays;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.sf.mpxj.ActivityCode;
import net.sf.mpxj.ActivityCodeContainer;
import net.sf.mpxj.CustomField;
import net.sf.mpxj.CustomFieldContainer;
import net.sf.mpxj.EventManager;
import net.sf.mpxj.FieldType;
import net.sf.mpxj.FilterContainer;
import net.sf.mpxj.ProjectFile;
import net.sf.mpxj.Task;
import net.sf.mpxj.TaskContainer;
import net.sf.mpxj.primavera.PrimaveraXERFileReader;
import net.sf.mpxj.primavera.XerFieldType;
import net.sf.mpxj.reader.ProjectReader;
import net.sf.mpxj.reader.ProjectReaderUtility;
import net.sf.mpxj.reader.UniversalProjectReader;

@Service
@Slf4j
public class ImportServiceImpl extends UploadFileServiceImpl implements ImportService {

	@Autowired
	TrackRepository trackRepository;

	@Autowired
	UploadFileService uploadFileServiceImpl;

	@Autowired
	ProjectUserRepository projectUserRepository;

	@Autowired
	ProjectCompanyRepository projectCompanyRepository;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	CommonRepository commonRepository;

	@Autowired
	LookaheadAccessServiceImpl lookaheadPage;

	@Autowired
	TaskRepository taskRepository;

	@Autowired
	SubTaskRepository subTaskRepository;

	@Autowired
	ScheduleRepository scheduleRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private Utility utility;

	String specDivisionContants = "--select--";

	public static final String IN_PROGRESS = "In Progress";
	public static final String COMPLETED = "Completed";
	public static final String USER_HOME = System.getProperty("user.home");
	public static final String FOLDER_SEPARATOR = "/";
	// public static final String DUPLICATES_CSV = "duplicates.csv";
	public static final String ERROR_CSV = "error.csv"; // all errors should written in the error csv
//	public static final String DOCKER_FILE_DIRECTORY = "/home/admin1/Downloads/";
	 public static final String DOCKER_FILE_DIRECTORY = "/home/mindzen/ip/";

	public Track uploadFileTrack(Long projectId, Long userId, String fileName, boolean isUpdate) {
		Track track = new Track();
		track.setUserId(userId);
		track.setProjectId(projectId);
		track.setInsertStatus(COMPLETED);
		track.setRecordStatus(IN_PROGRESS);
		track.setServerFileName(fileName);
		int subStringPosition = fileName.indexOf('_') + 1;
		track.setFileName(fileName.substring(subStringPosition, fileName.length()));
		track.setImportDate(LocalDateTime.now());
		track.setActive(true);
		track.setDelete(false);
		track.setUpdate(isUpdate);
		track = trackRepository.save(track);
		return track;
	}

	public Track updateImportExcelTrack(Long projectId, Long userId, String fileName, Long trackId) {
		Track track = new Track();
		track.setUserId(userId);
		track.setProjectId(projectId);
		track.setInsertStatus(COMPLETED);
		track.setRecordStatus(IN_PROGRESS);
		track.setServerFileName(fileName);
		int subStringPosition = fileName.indexOf('_') + 1;
		track.setFileName(fileName.substring(subStringPosition, fileName.length()));
		track.setImportDate(LocalDateTime.now());
		track.setActive(false);
		track.setDelete(true);
		track.setId(trackId);
		track = trackRepository.save(track);
		return track;
	}

	@Override
	@SneakyThrows
	public MSAResponse importExcel(String fileName, Long projectId, Long projectCompanyId) {
		Long start = System.currentTimeMillis();
		CompletableFuture<MSAResponse> result = callAsyncImportExcel(fileName, projectId, projectCompanyId);
		CompletableFuture.allOf(result).join();
		Long end = System.currentTimeMillis();
		log.info("consumed time " + (end - start));
		return result.get();
	}

	@Async
	@SneakyThrows
	public CompletableFuture<MSAResponse> callAsyncImportExcel(String fileName, Long projectId, Long projectCompanyId) {
		List<String> errorList = new ArrayList<>();
		String errorDestFileName = "error_" + System.currentTimeMillis() + ".xlsx";
		Long userId = UserInfoContext.getUserThreadLocal();
		Track track = uploadFileTrack(projectId, userId, fileName, false); // update excel function will not implemented
		// on this phase
		Long trackId = track.getId();
		Path filePath = Paths.get(USER_HOME + FOLDER_SEPARATOR + "mindzen" + FOLDER_SEPARATOR + trackId);
		Files.createDirectories(filePath);
		Files.copy(Paths.get(DOCKER_FILE_DIRECTORY + fileName), Paths.get(filePath + FOLDER_SEPARATOR + fileName),
				StandardCopyOption.REPLACE_EXISTING);
		MSAResponse response = callImportExcelComponentCall(filePath, projectId, userId, fileName, trackId, errorList,
				errorDestFileName);
		if (!response.isSuccess() && response.getHttpStatus().equals(HttpStatus.BAD_REQUEST)) {
			updateImportExcelTrack(projectId, userId, fileName, trackId);

			return badRequestReponseBuild(errorList, filePath, errorDestFileName, response);
		} else if (!response.isSuccess() && response.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
			updateImportExcelTrack(projectId, userId, fileName, trackId);
			return CompletableFuture.completedFuture(new MSAResponse(false, HttpStatus.PRECONDITION_FAILED,
					response.getMessage(), fileName, response.getMessage(), errorList));
		} else {
			return importExcelData(trackId, projectId, errorList, projectCompanyId, filePath, errorDestFileName,
					response, false);
		}
	}

	/*
	 * mpp files only need to handle the don't check the duplicates inside of the
	 * files
	 */
	private CompletableFuture<MSAResponse> importExcelData(Long trackId, Long projectId, List<String> errorList,
			Long projectCompanyId, Path filePath, String errorDestFileName, MSAResponse response, boolean isMpp) {
		Map<String, List<ImportExcelTaskDTO>> importData = new HashMap<>();
		List<Object[]> excelDataList = commonRepository.findByTrackId(trackId);
		if (excelDataList != null && !excelDataList.isEmpty()) {
			convertFormatObj(excelDataList, importData, projectId, errorList, trackId, isMpp);
			createLookaheadResponse(projectId, projectCompanyId, importData, trackId, isMpp);
		}
		if (errorList != null && !errorList.isEmpty()) {
			sendErrorRecords(errorList, filePath, null, ERROR_CSV);
			copyExcelFileToDcokerPath(filePath, errorDestFileName);
			String msg = "Duplicates or existing task found.";
			if (importData != null && !importData.isEmpty() && importData.size() > 0) {
				msg = msg + "Some imported successfully, please check after some time";
			}
			return CompletableFuture.completedFuture(
					new MSAResponse(false, HttpStatus.BAD_REQUEST, msg, errorDestFileName, "error occured", errorList));
		}
		copyExcelFileToDcokerPath(filePath, errorDestFileName);
		return CompletableFuture.completedFuture(
				new MSAResponse(true, HttpStatus.OK, "Your data imported successfully, please check after some time",
						errorDestFileName, response.getMessage(), errorList));

	}

	private CompletableFuture<MSAResponse> badRequestReponseBuild(List<String> errorList, Path filePath,
			String errorDestFileName, MSAResponse response) {
		sendErrorRecords(errorList, filePath, null, ERROR_CSV);
		copyExcelFileToDcokerPath(filePath, errorDestFileName);
		String msg = response.getMessage();
		if (msg.contains("Size") && msg.contains("Index"))
			msg = "Headers Missing";
		return CompletableFuture.completedFuture(new MSAResponse(false, HttpStatus.BAD_REQUEST, msg, errorDestFileName,
				response.getMessage(), errorList));
	}

	@SneakyThrows
	private MSAResponse callImportExcelComponentCall(Path filePath, Long projectId, Long userId, String fileName,
			Long trackId, List<String> errorList, String errorDestFileName) {
		CompletableFuture<MSAResponse> componentResponse = CompletableFuture.supplyAsync(() -> {
			return importExcelStatus(filePath, projectId, userId, fileName, trackId, errorList, errorDestFileName);
		});
		CompletableFuture.allOf(componentResponse);
		return componentResponse.get();
	}

	private MSAResponse importExcelStatus(Path filePath, Long projectId, Long userId, String fileName, Long trackId,
			List<String> errorList, String errorDestFileName) {
		try {
			return importExcel(filePath, projectId, userId, null, fileName, trackId, errorList);
		} catch (Exception e) {
			return new MSAResponse(false, HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), errorDestFileName,
					"sheet import error", errorList);
		}
	}

	public MSAResponse createLookaheadResponse(Long projectId, Long projectCompanyId,
			Map<String, List<ImportExcelTaskDTO>> importData, Long trackId, boolean isMpp) {
		List<TaskDTO> taskDTOList = new ArrayList<>();
		Long[] emptyArray = null;
		LookaheadDTO lookaheadDTO = new LookaheadDTO();
		lookaheadDTO.setTaskIds(emptyArray);
		lookaheadDTO.setSubTaskIds(emptyArray);
		lookaheadDTO.setScheduleIds(emptyArray);
		lookaheadDTO.setProjectId(projectId);
		lookaheadDTO.setSubmit(false);
		lookaheadDTO.setLookaheadDate(null);
		for (Entry<String, List<ImportExcelTaskDTO>> importExcelList : importData.entrySet()) {
			if (importExcelList != null && importExcelList.getValue() != null
					&& !importExcelList.getValue().isEmpty()) {
				taskDTOList.add(
						lookaheadPage.transformExcelObjectToLookahead(importExcelList, projectCompanyId, projectId));
				lookaheadDTO.setTasks(taskDTOList);
			}
		}
		CompletableFuture.allOf(CompletableFuture.runAsync(() -> {
			try {
				if (isMpp)
					lookaheadPage.createLookaheadForImportFile(lookaheadDTO, trackId);
				else
					lookaheadPage.createLookaheadForImportExcel(lookaheadDTO, trackId);
			} catch (Exception e) {
				log.info("excepption while insert into excel " + e.getMessage());
			}
			String deleteQuery = "delete from temp_success  where track_id = " + trackId;
			log.info("delete the temp sucees records & query :" + deleteQuery);
			utility.runQuery(deleteQuery);
		}));
		return null; // no use to capture the response
	}

	public MSAResponse updateLookaheadResponse(Long projectId, Long projectCompanyId,
			Map<String, List<ImportExcelTaskDTO>> importData, Long trackId, boolean isMpp, boolean isUpdateMpp) {
		Optional<Project> project = projectRepository.findById(projectId);

		List<TaskDTO> taskDTOList = new ArrayList<>();
		Long[] emptyArray = null;
		LookaheadDTO lookaheadDTO = new LookaheadDTO();
		lookaheadDTO.setTaskIds(emptyArray);
		lookaheadDTO.setSubTaskIds(emptyArray);
		lookaheadDTO.setScheduleIds(emptyArray);
		lookaheadDTO.setProjectId(projectId);
		lookaheadDTO.setSubmit(false);
		lookaheadDTO.setLookaheadDate(null);
		for (Entry<String, List<ImportExcelTaskDTO>> importExcelList : importData.entrySet()) {
			if (importExcelList != null && importExcelList.getValue() != null
					&& !importExcelList.getValue().isEmpty()) {
				taskDTOList.add(
						lookaheadPage.transformExcelObjectToLookahead(importExcelList, projectCompanyId, projectId));
				lookaheadDTO.setTasks(taskDTOList);
			}
		}
		CompletableFuture.allOf(CompletableFuture.runAsync(() -> {
			try {
				if (isMpp)
					lookaheadPage.createLookaheadForImportFile(lookaheadDTO, trackId);
				else if (isUpdateMpp) {
					lookaheadPage.updateLookaheadForImportFile(lookaheadDTO, trackId);
				} else
					lookaheadPage.createLookaheadForImportExcel(lookaheadDTO, trackId);
			} catch (Exception e) {
				log.info("excepption while insert into excel " + e.getMessage());
			}
			List<com.mindzen.planner.entity.Task> checkingExistingTaskAvailableOrNot = taskRepository
					.findByTaskImportId(trackId, projectId);
			List<TaskDTO> lookAheadData = lookaheadDTO.getTasks();
			com.mindzen.planner.entity.Task updateTaskForSoftDeleteRecord = new com.mindzen.planner.entity.Task();

			List<Integer> importUniqueIds = lookAheadData.stream().map((lookahead) -> lookahead.getImportUniqueId())
					.collect(Collectors.toList());
			List<Integer> taskImportUniqueIds = checkingExistingTaskAvailableOrNot.stream()
					.map((taskTableImportUniqueId) -> taskTableImportUniqueId.getImportUniqueId())
					.collect(Collectors.toList());

			List<Integer> unavailableTaskInUpdates = taskImportUniqueIds.stream()
					.filter(unavailable -> !importUniqueIds.contains(unavailable)).collect(Collectors.toList());

			System.out.println(unavailableTaskInUpdates);

			unavailableTaskInUpdates.forEach(taskUpdate -> {
				com.mindzen.planner.entity.Task updatetaskByImportUniqueId = taskRepository.findByTaskId(taskUpdate,
						projectId, trackId);
				updateVersionOldRecordInTaskTable(updatetaskByImportUniqueId, updateTaskForSoftDeleteRecord);
			});

			String deleteQuery = "delete from temp_success  where track_id = " + trackId;
			log.info("delete the temp sucees records & query :" + deleteQuery);
			utility.runQuery(deleteQuery);
		}));
		return null; // no use to capture the response
	}

	private void updateVersionOldRecordInTaskTable(com.mindzen.planner.entity.Task updatetaskByImportUniqueId,
			com.mindzen.planner.entity.Task updateTaskForSoftDeleteRecord) {
		updateTaskForSoftDeleteRecord.setId(updatetaskByImportUniqueId.getId());
		updateTaskForSoftDeleteRecord.setActive(false);
		updateTaskForSoftDeleteRecord.setActivityDescription(updatetaskByImportUniqueId.getActivityDescription());
		updateTaskForSoftDeleteRecord.setActivityId(updatetaskByImportUniqueId.getActivityId());
		updateTaskForSoftDeleteRecord.setChild(updatetaskByImportUniqueId.isChild());
		updateTaskForSoftDeleteRecord.setDeadLineDate(updatetaskByImportUniqueId.getDeadLineDate());
		updateTaskForSoftDeleteRecord.setDescription(updatetaskByImportUniqueId.getDescription());
		updateTaskForSoftDeleteRecord.setExpectedCompletionDate(updatetaskByImportUniqueId.getExpectedCompletionDate());
		updateTaskForSoftDeleteRecord.setImportDeadLineDate(updatetaskByImportUniqueId.getImportDeadLineDate());
		updateTaskForSoftDeleteRecord.setImportFinishDate(updatetaskByImportUniqueId.getImportFinishDate());
		updateTaskForSoftDeleteRecord.setImportId(updatetaskByImportUniqueId.getImportId());
		updateTaskForSoftDeleteRecord.setImportNotes(updatetaskByImportUniqueId.getImportNotes());
		updateTaskForSoftDeleteRecord.setImportStartDate(updatetaskByImportUniqueId.getImportStartDate());
		updateTaskForSoftDeleteRecord
				.setImportTargetCompletionDate(updatetaskByImportUniqueId.getImportTargetCompletionDate());
		updateTaskForSoftDeleteRecord.setImportUniqueId(updatetaskByImportUniqueId.getImportUniqueId());
		updateTaskForSoftDeleteRecord.setLookAheadChange(updatetaskByImportUniqueId.isLookAheadChange());
		updateTaskForSoftDeleteRecord.setProjectCompanyId(updatetaskByImportUniqueId.getProjectCompanyId());
		updateTaskForSoftDeleteRecord.setProjectId(updatetaskByImportUniqueId.getProjectId());
		updateTaskForSoftDeleteRecord.setRequiredCompletionDate(updatetaskByImportUniqueId.getRequiredCompletionDate());
		updateTaskForSoftDeleteRecord.setsNo(updatetaskByImportUniqueId.getsNo());
		updateTaskForSoftDeleteRecord.setTaskStartDate(updatetaskByImportUniqueId.getTaskStartDate());
		updateTaskForSoftDeleteRecord.setTaskType(updatetaskByImportUniqueId.getTaskType());
		updateTaskForSoftDeleteRecord.setTrade(updatetaskByImportUniqueId.getTrade());
		updateTaskForSoftDeleteRecord.setVisibility(updatetaskByImportUniqueId.getVisibility());
		taskRepository.save(updateTaskForSoftDeleteRecord);

	}

	@SneakyThrows
	private void copyExcelFileToDcokerPath(Path filePath, String errorDestFileName) {
		File fileToMove = new File(filePath + FOLDER_SEPARATOR + ERROR_CSV);
		File destination = new File(DOCKER_FILE_DIRECTORY);
		File targetFile = new File(destination, errorDestFileName);
		com.google.common.io.Files.copy(fileToMove, targetFile);
	}

	private void convertFormatObj(List<Object[]> excelDataList, Map<String, List<ImportExcelTaskDTO>> importData,
			Long projectId, List<String> duplicatesList, Long trackId, boolean isMpp) {

		List<ImportExcelTaskDTO> dataList = null;
		List<Object[]> formattedData = new ArrayList<>();
		excelDataList.forEach(excelData -> {
			Object[] localData = new Object[12];
			int i = 0;
			for (Object data : excelData) {
				data = "|" + "\"" + data;
				localData[i] = data;
				i = i + 1;
			}
			i = 0;
			formattedData.add(localData);
		});
		excelDataList.clear();
		excelDataList.addAll(formattedData);
		List<Object[]> sampledata = new ArrayList<>(excelDataList);
		StringBuilder description = new StringBuilder();
		sampledata.forEach(data -> {
			String taskValue = String.valueOf((String) data[1]).replace("|\"", "").trim();
			if (taskValue != null && !taskValue.equals("")) {
				description.setLength(0);
				description.append(taskValue);
			} else {
				data[1] = description.toString();
				data[1] = "|\"" + data[1];
			}
			data[0] = "";
		});
		List<String> excelConvertedDataList = new ArrayList<>();
		sampledata.forEach(data -> findDuplicates(data, excelConvertedDataList));
		Set<String> nonDuplicateExcelData = new HashSet<>();
		Set<String> duplicateRecords = excelConvertedDataList.stream().filter(n -> !nonDuplicateExcelData.add(n))
				.collect(Collectors.toSet());
		if (isMpp)
			duplicateRecords.forEach(data -> nonDuplicateExcelData.add(data));
		else {
			duplicateRecords.forEach(data -> {
				data = data + "duplicate record";
				duplicatesList.add(data);
			});
		}
		excelDataList.clear();
		nonDuplicateExcelData.forEach(data -> {
			data = subString(data);
			List<Object[]> convertedData = new ArrayList<>(Arrays.asList(data.split("\\|\"")));
			excelDataList.add(convertedData.stream().toArray(Object[]::new));
		});

		List<String> taskName = new ArrayList<>();
		for (Object[] data : excelDataList) {
			String taskValue = String.valueOf((String) data[1]).trim();
			String currentTaskName = String.valueOf((String) data[1]).trim();
			taskValue = subString(taskValue);
			currentTaskName = subString(currentTaskName);
			if (taskName.contains(taskValue))
				taskValue = "";
			else
				taskName.add(taskValue);
			if (taskValue != null && !taskValue.equals("")) {
				dataList = new ArrayList<>();
				ImportExcelTaskDTO rowObject = excelDataConverter(data, currentTaskName, projectId, duplicatesList,
						trackId, isMpp);
				dataList = importData.get(currentTaskName);
				if (dataList == null)
					dataList = new ArrayList<>();
				if (rowObject != null)
					dataList.add(rowObject);
				importData.put(currentTaskName, dataList);
			} else {
				dataList = importData.get(currentTaskName);
				if (dataList == null)
					dataList = new ArrayList<>();
				ImportExcelTaskDTO rowObject = excelDataConverter(data, currentTaskName, projectId, duplicatesList,
						trackId, isMpp);
				if (rowObject != null) {
					rowObject.setTaskDescription(null);
					dataList.add(rowObject);
				}
				importData.put(currentTaskName, dataList);
			}
		}
	}

	@SuppressWarnings({ "unlikely-arg-type", "unchecked" })
	private void updateconvertFormatObj(List<Object[]> excelDataList, Map<String, List<ImportExcelTaskDTO>> importData,
			Long projectId, List<String> errorListInUpload, Long trackId, boolean isMpp, boolean isUpdateMpp) {

		List<ImportExcelTaskDTO> dataList = null;
		List<Object[]> formattedData = new ArrayList<>();

		excelDataList.forEach(excelData -> {
			Object[] localData = new Object[12];
			int i = 0;
			for (Object data : excelData) {
				data = "|" + "\"" + data;
				localData[i] = data;
				i = i + 1;
			}
			i = 0;
			formattedData.add(localData);
		});
		excelDataList.clear();
		excelDataList.addAll(formattedData);
		List<Object[]> sampledata = new ArrayList<>(excelDataList);
		StringBuilder description = new StringBuilder();
		sampledata.forEach(data -> {
			String taskValue = String.valueOf((String) data[1]).replace("|\"", "").trim();
			if (taskValue != null && !taskValue.equals("")) {
				description.setLength(0);
				description.append(taskValue);
			} else {
				data[1] = description.toString();
				data[1] = "|\"" + data[1];
			}
			data[0] = "";
		});
		List<String> excelConvertedDataList = new ArrayList<>();
		sampledata.forEach(data -> findDuplicates(data, excelConvertedDataList));
		Set<String> nonDuplicateExcelData = new HashSet<>();
		Set<String> duplicateRecords = excelConvertedDataList.stream().filter(n -> !nonDuplicateExcelData.add(n))
				.collect(Collectors.toSet());
		if (isMpp)
			duplicateRecords.forEach(data -> nonDuplicateExcelData.add(data));
		else {
			duplicateRecords.forEach(data -> {
				data = data + "duplicate record";
				errorListInUpload.add(data);
			});
		}
		excelDataList.clear();
		nonDuplicateExcelData.forEach(data -> {
			data = subString(data);
			List<Object[]> convertedData = new ArrayList<>(Arrays.asList(data.split("\\|\"")));
			excelDataList.add(convertedData.stream().toArray(Object[]::new));
		});

		List<String> taskName = new ArrayList<>();
		ImportExcelTaskDTO rowObject;
		Long start = System.currentTimeMillis();

		for (Object[] data : excelDataList) {
			String taskValue = String.valueOf((String) data[1]).trim();
			String currentTaskName = String.valueOf((String) data[1]).trim();
			taskValue = subString(taskValue);
			currentTaskName = subString(currentTaskName);
			if (taskName.contains(taskValue))
				taskValue = "";
			else
				taskName.add(taskValue);
			if (currentTaskName != null && !currentTaskName.equals("") && !currentTaskName.equals("null,")
					&& !currentTaskName.equals("null")) {
				int taskNameLength = currentTaskName.length();
				if (taskNameLength <= 94) {
					String startDateBoundary = new String((String) data[3]).trim();
					String finishDateBoundary = new String((String) data[4]).trim();
					startDateBoundary = subString(startDateBoundary);
					finishDateBoundary = subString(finishDateBoundary);
					LocalDate taskStartDate = DateUtil.stringToLocalDateViaInstant(startDateBoundary,
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
					LocalDate taskEndDate = DateUtil.stringToLocalDateViaInstant(finishDateBoundary,
							DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
					List<LocalDate> betweenDays = Stream.iterate(taskStartDate, d -> d.plusDays(1))
							.limit(ChronoUnit.DAYS.between(taskStartDate, taskEndDate) + 1)
							.collect(Collectors.toList());
					Optional<Project> project = projectRepository.findById(projectId);

					List<String> leaveDaysList = new ArrayList<>(
							Arrays.asList(project.get().getLeaveDays().toUpperCase().split(",")));
					if (!leaveDaysList.contains(taskStartDate.getDayOfWeek().toString())
							&& !leaveDaysList.contains(taskEndDate.getDayOfWeek().toString())) {
						// System.out.println(taskStartDate.getDayOfWeek());
						// System.out.println(taskEndDate.getDayOfWeek());

						dataList = new ArrayList<>();

						if (isUpdateMpp) {
							rowObject = updateMppexcelDataConverter(data, currentTaskName, projectId, errorListInUpload,
									trackId, isMpp, isUpdateMpp);
						} else {
							rowObject = updateMppexcelDataConverter(data, currentTaskName, projectId, errorListInUpload,
									trackId, isMpp, false);
						}

						dataList = importData.get(currentTaskName);
						if (dataList == null)
							dataList = new ArrayList<>();
						if (rowObject != null)
							dataList.add(rowObject);
						importData.put(currentTaskName, dataList);
					} else {
						StringBuilder leaveDaysContains = new StringBuilder();
						List<String> dataLists = new ArrayList<>(Arrays.asList(data));
						dataLists.forEach(error -> {
							if (error != null && !error.equalsIgnoreCase(",") && !error.equalsIgnoreCase("null,")
									&& !error.isEmpty()) {
								leaveDaysContains.append(error + ",");
							}
						});
						leaveDaysContains.append(
								"This task was not imported since the start date/end date falls on a project Non-Work day. Update Project>Plan Configuration>Non-Work Days and re-import.");
						errorListInUpload.add(leaveDaysContains.toString());

					}
				} else {
					StringBuilder taskDescriptionisNull = new StringBuilder();
					List<String> dataLists = new ArrayList<>(Arrays.asList(data));
					dataLists.forEach(error -> {
						if (error != null && !error.equalsIgnoreCase(",") && !error.equalsIgnoreCase("null,")
								&& !error.isEmpty()) {
							taskDescriptionisNull.append(error + ",");
						}
					});
					taskDescriptionisNull.append("Task Description Is More than 100 Characters");
					errorListInUpload.add(taskDescriptionisNull.toString());
				}
			} else {
				StringBuilder taskDescriptionisNull = new StringBuilder();
				List<String> dataLists = new ArrayList<>(Arrays.asList(data));
				dataLists.forEach(error -> {
					if (error != null && !error.equalsIgnoreCase(",") && !error.equalsIgnoreCase("null,")
							&& !error.isEmpty()) {
						taskDescriptionisNull.append(error + ",");
					}
				});
				taskDescriptionisNull.append("Task Description Field is Missing");
				errorListInUpload.add(taskDescriptionisNull.toString());
			}
		}
		Long end = System.currentTimeMillis();

		log.info("Consumed time for tempsuccess table" + (end - start));

	}

	private String subString(String data) {
		return data.substring(0, data.length() - 1);
	}

	private synchronized static void findDuplicates(Object[] data, List<String> duplicatesList) {
		StringBuilder duplicateMessages = new StringBuilder();
		List<String> dataList = new ArrayList<>(Arrays.asList(data));
		dataList.forEach(error -> duplicateMessages.append(error + ","));
		duplicatesList.add(duplicateMessages.toString());
	}

	private ImportExcelTaskDTO excelDataConverter(Object[] data, String taskDescription, Long projectId,
			List<String> duplicatesList, Long trackId, boolean isMpp) {
		String subTaskDescription = new String((String) data[2]).trim();
		String startDate = new String((String) data[3]).trim();
		String finishDate = new String((String) data[4]).trim();
		startDate = subString(startDate);
		finishDate = subString(finishDate);
		subTaskDescription = subString(subTaskDescription);
		LocalDate startDateFormat;
		LocalDate finishDateFormat;
		if (startDate == null || (startDate != null && startDate.equals(""))) {
			startDateFormat = null;
			startDate = null;
		} else
			startDateFormat = DateUtil.stringToLocalDateViaInstant(startDate,
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		if (finishDate == null || (finishDate != null && finishDate.equals(""))) {
			finishDateFormat = null;
			finishDate = null;
		} else
			finishDateFormat = DateUtil.stringToLocalDateViaInstant(finishDate,
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		boolean status = checkTaskAndSubTaskAvalability(taskDescription, subTaskDescription, startDateFormat,
				finishDateFormat, projectId);
		boolean existingRecordStatus = checkTaskAndSubTaskAvalabilityInTempSuccess(taskDescription, subTaskDescription,
				startDate, finishDate, projectId);
		if (isMpp) {
			status = true;
		}
		if (existingRecordStatus)
			insertRecordIntoTempSuccessTable(data, projectId, trackId);
		if (status && existingRecordStatus) {
			ImportExcelTaskDTO excelData = new ImportExcelTaskDTO();
			String targetDate = new String((String) data[5]).trim();
			targetDate = subString(targetDate);
			if (targetDate != null && targetDate.equals("null"))
				targetDate = "";
			if (subTaskDescription != null && subTaskDescription.equals("null"))
				subTaskDescription = "";
			excelData.setTaskDescription(new String((String) data[1]).trim().equals("") ? null : (String) data[1]);
			excelData.setSubTaskDescription(subTaskDescription.equals("") ? null : subTaskDescription);
			excelData.setStartDate(startDate);
			excelData.setFinishDate(finishDate);
			excelData.setTargetCompletionDate(targetDate.equals("") ? null : targetDate);
			excelData.setTaskType(subString((String) data[6]));
			excelData.setResponsibility(subString((String) data[7]));
			excelData.setSpecDivision(subString((String) data[8]));

			excelData.setActivityId(subString((String) data[9]));
			excelData.setImportUniqueId((String) data[10]);
			// excelData.setNotes((String) data[11]);

			return excelData;
		} else {
			StringBuilder duplicateMessages = new StringBuilder();
			List<String> dataList = new ArrayList<>(Arrays.asList(data));
			dataList.forEach(error -> duplicateMessages.append(error + ","));
			duplicateMessages.append("existing sub task found");
			duplicatesList.add(duplicateMessages.toString());
			return null;
		}
	}

	private ImportExcelTaskDTO updateMppexcelDataConverter(Object[] data, String taskDescription, Long projectId,
			List<String> errorListInUpload, Long trackId, boolean isMpp, boolean isUpdateMpp) {

		// LocalDate startDateFormat;
		// LocalDate finishDateFormat;
		// if (startDate == null || (startDate != null && startDate.equals(""))) {
		// startDateFormat = null;
		// startDate = null;
		// } else
		// startDateFormat = DateUtil.stringToLocalDateViaInstant(startDate,
		// DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		// if (finishDate == null || (finishDate != null && finishDate.equals(""))) {
		// finishDateFormat = null;
		// finishDate = null;
		// } else
		// finishDateFormat = DateUtil.stringToLocalDateViaInstant(finishDate,
		// DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		// boolean status = checkTaskAndSubTaskAvalability(taskDescription,
		// subTaskDescription, startDateFormat,
		// finishDateFormat, projectId);
		// boolean existingRecordStatus =
		// checkTaskAndSubTaskAvalabilityInTempSuccess(taskDescription,
		// subTaskDescription,
		// startDate, finishDate, projectId);
		ImportExcelTaskDTO excelData = new ImportExcelTaskDTO();
		Long start = System.currentTimeMillis();

		insertRecordIntoTempSuccessTable(data, projectId, trackId);
		Long end = System.currentTimeMillis();
		log.info("consumed time " + (end - start));

		String startDate = new String((String) data[3]).trim();
		String finishDate = new String((String) data[4]).trim();
		String targetDate = new String((String) data[5]).trim();
		startDate = subString(startDate);
		finishDate = subString(finishDate);
		targetDate = subString(targetDate);
		String subTaskDescription = new String((String) data[2]).trim();
		subTaskDescription = subString(subTaskDescription);

		if (targetDate != null && targetDate.equals("null"))
			targetDate = "";
		if (subTaskDescription != null && subTaskDescription.equals("null"))
			subTaskDescription = "";
		excelData.setTaskDescription(new String((String) data[1]).trim().equals("") ? null : (String) data[1]);
		excelData.setSubTaskDescription(subTaskDescription.equals("") ? null : subTaskDescription);
		excelData.setStartDate(startDate);
		excelData.setFinishDate(finishDate);
		excelData.setTargetCompletionDate(targetDate.equals("") ? null : targetDate);
		excelData.setTaskType(subString((String) data[6]));
		excelData.setResponsibility(subString((String) data[7]));

		excelData.setSpecDivision(specDivisionContants);

		excelData.setActivityId(subString((String) data[9]));
		excelData.setImportUniqueId((String) data[10]);

		return excelData;

	}

	@Transactional
	private void insertRecordIntoTempSuccessTable(Object[] data, Long projectId, Long trackId) {
		List<String> dataList = new ArrayList<>(Arrays.asList(data));
		StringBuilder query = new StringBuilder(" insert into temp_success (project_id,status,track_id");
		for (int i = 1; i <= dataList.size(); i++) {
			query.append(",field_" + i);
		}
		query.append(" ) values (" + projectId + ",'" + IN_PROGRESS + "'," + trackId);
		dataList.forEach(datas -> query.append(",'" + subString(datas) + "'"));
		query.append(")");
		CompletableFuture.allOf(CompletableFuture.runAsync(() -> utility.runQuery(query.toString()))).join();
	}

	private boolean checkTaskAndSubTaskAvalabilityInTempSuccess(String taskDescription, String subTaskDescription,
			String startDate, String finishDate, Long projectId) {
		taskDescription = taskDescription.replaceAll("'", "''");
		subTaskDescription = subTaskDescription.replaceAll("'", "''");
		if (subTaskDescription != null && subTaskDescription.equals(""))
			subTaskDescription = taskDescription;
		String taskAndSubTaskAvailabilityCheckQuery = "";
		if ((startDate == null || (startDate != null && startDate.equals(""))) || finishDate == null
				|| (finishDate != null && finishDate.equals(""))) {
			taskAndSubTaskAvailabilityCheckQuery = "select COUNT(*) from temp_success where field_2 ='"
					+ taskDescription + "' and field_3 ='" + subTaskDescription + "' and project_id =" + projectId
					+ " and field_4=' ' and field_5=' ' ";
		} else
			taskAndSubTaskAvailabilityCheckQuery = "select COUNT(*) from temp_success where field_2 ='"
					+ taskDescription + "' and field_3 ='" + subTaskDescription + "' and field_4='" + startDate
					+ "' and field_5='" + finishDate + "' and " + "project_id =" + projectId;
		Query query = entityManager.createNativeQuery(taskAndSubTaskAvailabilityCheckQuery);
		int count = ((Number) query.getSingleResult()).intValue();
		if (count > 0)
			return false;
		else
			return true;
	}

	private boolean checkTaskAndSubTaskAvalability(String taskDescription, String subTaskDescription,
			LocalDate startDateFormat, LocalDate finishDateFormat, Long projectId) {
		taskDescription = taskDescription.replaceAll("'", "''");
		subTaskDescription = subTaskDescription.replaceAll("'", "''");
		String query = "";
		if (subTaskDescription != null && subTaskDescription.equals(""))
			subTaskDescription = taskDescription;
		if ((startDateFormat == null || (startDateFormat != null && startDateFormat.equals("")))
				|| finishDateFormat == null || (finishDateFormat != null && finishDateFormat.equals(""))) {
			query = " select st.id from task t join sub_task st on t.id =st.task_id where t.project_id = " + projectId
					+ "  and t.description= '" + taskDescription + "' and st.description = '" + subTaskDescription
					+ "' and st.task_start_date is null and  st.expected_completion_date is null"
					+ " and t.is_active = true and st.is_active = true ";
		} else {
			query = " select st.id from task t join sub_task st on t.id =st.task_id where t.project_id = " + projectId
					+ "  and t.description= '" + taskDescription + "' and st.description = '" + subTaskDescription
					+ "' and st.task_start_date = '" + startDateFormat + "' and  st.expected_completion_date= '"
					+ finishDateFormat + "' and t.is_active = true and st.is_active = true ";
		}
		String subTaskGroupBy = " GROUP BY st.id ORDER BY st.id, st.task_id ";
		List<Object[]> task = lookaheadPage.generateDynamicQueryResult("st", new LookaheadListDTO(), query,
				subTaskGroupBy);
		if (task.isEmpty())
			return true;
		else
			return false;
	}

	@Override
	@SneakyThrows
	public MSAResponse importMppFile(String fileName, Long projectId, Long projectCompanyId, boolean isRenew,
			Long importTrackId) {
		List<Task> taskList = new ArrayList<>();
		String filePath = DOCKER_FILE_DIRECTORY + fileName;
		writeCSV(DOCKER_FILE_DIRECTORY + FOLDER_SEPARATOR + ERROR_CSV, new StringBuilder());
		ProjectReader reader = ProjectReaderUtility.getProjectReader(filePath);
		ProjectFile projectFile = reader.read(filePath);
		projectFile.getTasks().forEach(taskList::add);
		taskList = taskList.stream().filter(task -> !task.getSummary()).collect(Collectors.toList());
		Long start = System.currentTimeMillis();

		if (!isRenew) {
			List<ImportMPPDTO> importMPPDTOList = new ArrayList<>();
			taskList.forEach(task -> {
				System.out.println(task.getSummary());
				ImportMPPDTO importMPPDTO;
				importMPPDTO = taskDataConvertToDto(task);
				importMPPDTO.setIsChild(false);
				importMPPDTO.setSubTaskDescription(null);
				importMPPDTOList.add(importMPPDTO);
			});

			Long end = System.currentTimeMillis();
			log.info("consumed time for itrate the mpp " + (end - start));

			return insertDTOValuesToTempTable(importMPPDTOList, fileName, DOCKER_FILE_DIRECTORY, projectId,
					projectCompanyId, true);
		} else {
			return new MSAResponse(false, HttpStatus.INTERNAL_SERVER_ERROR, "Renew not yet implemented", null);
		}
	}

	/*
	 * @Override
	 * 
	 * @SneakyThrows public MSAResponse importMppFile(String fileName, Long
	 * projectId, Long projectCompanyId) { List<Task> taskList = new ArrayList<>();
	 * Set<Integer> taskIds = new HashSet<>(); String filePath =
	 * DOCKER_FILE_DIRECTORY+fileName;
	 * writeCSV(DOCKER_FILE_DIRECTORY+FOLDER_SEPARATOR+ERROR_CSV,new
	 * StringBuilder()); ProjectReader reader =
	 * ProjectReaderUtility.getProjectReader(filePath); ProjectFile projectFile =
	 * reader.read(filePath); projectFile.getTasks().forEach(taskList::add);
	 * taskList = taskList.stream().skip(1).collect(Collectors.toList());
	 * List<ImportMPPDTO> importMPPDTOList = new ArrayList<>(); if(taskList != null
	 * && !taskList.isEmpty()) { Task task = taskList.get(0);
	 * task.getChildTasks().forEach(child ->taskIds.add(child.getUniqueID()));
	 * taskIds.forEach(taskId ->{ ImportMPPDTO importMPPDTO; Task localTask =
	 * projectFile.getTaskByUniqueID(taskId); importMPPDTO =
	 * taskDataConvertToDto(localTask); if(localTask.getChildTasks() != null &&
	 * !localTask.getChildTasks().isEmpty()) { List<ImportMPPDTO> childTaskDTOList =
	 * new ArrayList<>(); List<Task> childTasks = localTask.getChildTasks();
	 * childTasks.forEach(childTask
	 * ->childTaskDTOList.add(taskDataConvertToDto(childTask)));
	 * importMPPDTO.setSubTasks(childTaskDTOList); importMPPDTO.setIsChild(true); }
	 * else { importMPPDTO.setIsChild(false);
	 * importMPPDTO.setSubTaskDescription(null); }
	 * importMPPDTOList.add(importMPPDTO); }); } return
	 * insertDTOValuesToTempTable(importMPPDTOList,fileName,DOCKER_FILE_DIRECTORY,
	 * projectId,projectCompanyId); }
	 */

	// @Override
	// @SneakyThrows
	// public MSAResponse importXerFile(String fileName, Long projectId, Long
	// projectCompanyId, boolean isRenew) {
	// List<Task> taskList = new ArrayList<>();
	// Set<Integer> taskIds = new HashSet<>();
	// List<Integer> parentId = new ArrayList<>();
	// List<ImportMPPDTO> importMPPDTOList = new ArrayList<>();
	// String filePath = DOCKER_FILE_DIRECTORY + fileName;
	// writeCSV(DOCKER_FILE_DIRECTORY + FOLDER_SEPARATOR + ERROR_CSV, new
	// StringBuilder());
	//
	//
	// PrimaveraXERFileReader reader = new PrimaveraXERFileReader();
	// reader.setMatchPrimaveraWBS(false);
	//// Map<FieldType, String> activityFieldMap = reader.getActivityFieldMap();
	//// boolean value= activityFieldMap.containsValue("task_code");
	// ProjectFile projectFile = reader.read(filePath);
	//// CustomFieldContainer values=projectFile.getCustomFields();
	// System.out.println(projectFile);
	// projectFile.getTasks().forEach(taskList::add);
	// taskList.stream().forEach(task1 -> {
	// task1.getText(0)
	// System.out.println(task1);
	// parentId.add(task1.getUniqueID());
	// });
	// if (parentId != null && !parentId.isEmpty()) {
	// parentId.forEach(id -> {
	// Task task = projectFile.getTaskByUniqueID(id);
	// task.getChildTasks().forEach(child -> taskIds.add(child.getUniqueID()));
	// });
	// taskIds.forEach(taskId -> {
	// ImportMPPDTO importMPPDTO;
	// Task localTask = projectFile.getTaskByUniqueID(taskId);
	// importMPPDTO = taskDataConvertToDto(localTask);
	// if (localTask.getChildTasks() != null &&
	// !localTask.getChildTasks().isEmpty()) {
	// List<ImportMPPDTO> childTaskDTOList = new ArrayList<>();
	// List<Task> childTasks = localTask.getChildTasks();
	// childTasks.forEach(childTask ->
	// childTaskDTOList.add(taskDataConvertToDto(childTask)));
	// importMPPDTO.setSubTasks(childTaskDTOList);
	// importMPPDTO.setIsChild(true);
	// } else {
	// importMPPDTO.setIsChild(false);
	// importMPPDTO.setSubTaskDescription(null);
	// }
	// importMPPDTOList.add(importMPPDTO);
	// });
	// }
	// return insertDTOValuesToTempTable(importMPPDTOList, fileName,
	// DOCKER_FILE_DIRECTORY, projectId,
	// projectCompanyId, true);
	// }
	@Override
	@SneakyThrows
	public MSAResponse importXerFile(String fileName, Long projectId, Long projectCompanyId, boolean isRenew) {
		List<Task> taskList = new ArrayList<>();
		Set<Integer> taskIds = new HashSet<>();
		List<Integer> parentId = new ArrayList<>();
		List<ImportMPPDTO> importMPPDTOList = new ArrayList<>();
		String filePath = DOCKER_FILE_DIRECTORY + fileName;
		writeCSV(DOCKER_FILE_DIRECTORY + FOLDER_SEPARATOR + ERROR_CSV, new StringBuilder());

		PrimaveraXERFileReader reader = new PrimaveraXERFileReader();
		reader.setMatchPrimaveraWBS(false);
		// Map<FieldType, String> activityFieldMap = reader.getActivityFieldMap();
		// boolean value= activityFieldMap.containsValue("task_code");
		ProjectFile projectFile = reader.read(filePath);
		// CustomFieldContainer values=projectFile.getCustomFields();
		System.out.println(projectFile);
		projectFile.getTasks().forEach(taskList::add);
		taskList.forEach(taskId -> {
			String value = taskId.getText(1);
			log.info(value);// ImportMPPDTO importMPPDTO;

			// ImportMPPDTO importMPPDTO;
			// importMPPDTO = taskDataConvertToDto(localTask);
			// List<ImportMPPDTO> childTaskDTOList = new ArrayList<>();
			// List<Task> childTasks = localTask.getChildTasks();
			// childTasks.forEach(childTask ->
			// childTaskDTOList.add(taskDataConvertToDto(childTask)));
			// importMPPDTO.setSubTasks(childTaskDTOList);
			//
			// importMPPDTOList.add(importMPPDTO);
		});

		return insertDTOValuesToTempTable(importMPPDTOList, fileName, DOCKER_FILE_DIRECTORY, projectId,
				projectCompanyId, true);
	}

	private MSAResponse insertDTOValuesToTempTable(List<ImportMPPDTO> importMPPDTOList, String fileName,
			String filePath, Long projectId2, Long projectCompanyId2, boolean isUpdate) throws Exception {
		Long userId = UserInfoContext.getUserThreadLocal();
		Long projectId = projectId2;
		Long projectCompanyId = projectCompanyId2;
		List<String> errorList = new ArrayList<>();
		String errorDestFileName = "error_" + System.currentTimeMillis() + ".xlsx";
		Track track = uploadFileTrack(projectId, userId, fileName, isUpdate); // Importing track
		Long trackId = track.getId();
		Long start = System.currentTimeMillis();
		importMppDataToTable(importMPPDTOList, trackId, IN_PROGRESS);
		Long end = System.currentTimeMillis();
		log.info("consumed time for temp_table " + (end - start));

		MSAResponse response = new MSAResponse(true, HttpStatus.OK, Constants.SUCCESS, new ArrayList<>());
		return uploadImportExcelData(trackId, projectId, errorList, projectCompanyId, Paths.get(filePath),
				errorDestFileName, response, isUpdate).get();
	}

	private CompletableFuture<MSAResponse> uploadImportExcelData(Long trackId, Long projectId, List<String> errorList,
			Long projectCompanyId, Path filePath, String errorDestFileName, MSAResponse response, boolean isMpp) {
		Map<String, List<ImportExcelTaskDTO>> importData = new HashMap<>();
		List<Object[]> excelDataList = commonRepository.findByTrackId(trackId);
		StringBuilder errorHeaders = new StringBuilder();

		if (excelDataList != null && !excelDataList.isEmpty()) {
			updateconvertFormatObj(excelDataList, importData, projectId, errorList, trackId, isMpp, false);
			createLookaheadResponse(projectId, projectCompanyId, importData, trackId, isMpp);
		}
		if (errorList != null && !errorList.isEmpty()) {
			String dataHeader = "TASK_NAME,,TASK_START_DATE,,TASK_END_DATE,,TARGET_COMPLETION_DATE,,ACTIVITY_ID,,IMPORT_UNIQUE_ID,,ERROR_MESSAGE";
			String header[] = dataHeader.split(",");
			List<String> errorListHeaders = new ArrayList<String>();
			errorListHeaders = Arrays.asList(header);
			errorListHeaders.forEach(columnHeaders -> {
				// if(columnHeaders!=null&&
				// !columnHeaders.equalsIgnoreCase(",")&&!columnHeaders.equalsIgnoreCase("null,")&&!columnHeaders.isEmpty())
				// {
				errorHeaders.append(columnHeaders + ",");
				// }
			});
			errorList.add(0, errorHeaders.toString());
			sendErrorRecords(errorList, filePath, null, ERROR_CSV);
			copyExcelFileToDcokerPath(filePath, errorDestFileName);
			String msg = "Some data have been successfully imported. Please check after a few minutes. Check the information excel for the non-imported tasks";
			if (importData != null && !importData.isEmpty() && importData.size() > 0) {
				msg = "Some data have been successfully imported. Please check after a few minutes. Check the information excel for the non-imported tasks";
			}
			return CompletableFuture.completedFuture(
					new MSAResponse(false, HttpStatus.BAD_REQUEST, msg, errorDestFileName, "error occured", errorList));
		}
		copyExcelFileToDcokerPath(filePath, errorDestFileName);
		return CompletableFuture.completedFuture(new MSAResponse(true, HttpStatus.OK,
				"Your data imported successfully, please check after a few minutes", errorDestFileName,
				response.getMessage(), errorList));

	}

	// public void sendErrorRecordsFound(List<String> errorList, Path filePath,
	// String email, String errorCsv) {
	// if (!errorList.isEmpty()) {
	// errorList.forEach(errorMessages -> {
	// try {
	// FileWriter writter = new FileWriter(filePath + FOLDER_SEPARATOR + errorCsv,
	// true);
	// writter.write(errorMessages.toString() + NEW_LINE_CHARACTER);
	// writter.close();
	// } catch (IOException e) {
	// log.error("exception : " + e.getLocalizedMessage());
	// }
	// });
	// }
	// }

	private MSAResponse updateDTOValuesToTempTable(List<ImportMPPDTO> importMPPDTOList, String fileName,
			String filePath, Long projectId2, Long projectCompanyId2, boolean isUpdate, Long importHistoryId)
			throws Exception {
		Long userId = UserInfoContext.getUserThreadLocal();
		Long projectId = projectId2;
		Long projectCompanyId = projectCompanyId2;
		List<String> errorList = new ArrayList<>();
		String errorDestFileName = "error_" + System.currentTimeMillis() + ".xlsx";
		commonRepository.deleteByTrackid(importHistoryId);
		Track track = updateFileTrack(projectId, userId, fileName, isUpdate, importHistoryId); // Importing track

		Long trackId = track.getId();

		updateImportMppDataToTable(importMPPDTOList, trackId, IN_PROGRESS);
		MSAResponse response = new MSAResponse(true, HttpStatus.OK, Constants.SUCCESS, new ArrayList<>());
		return updateImportExcelData(trackId, projectId, errorList, projectCompanyId, Paths.get(filePath),
				errorDestFileName, response, isUpdate).get();
	}

	private Track updateFileTrack(Long projectId, Long userId, String fileName, boolean isUpdate, Long trackId) {
		Track track = new Track();
		track.setUserId(userId);
		track.setProjectId(projectId);
		track.setInsertStatus(COMPLETED);
		track.setRecordStatus(IN_PROGRESS);
		track.setServerFileName(fileName);
		int subStringPosition = fileName.indexOf('_') + 1;
		track.setFileName(fileName.substring(subStringPosition, fileName.length()));
		track.setImportDate(LocalDateTime.now());
		track.setActive(true);
		track.setDelete(false);
		track.setUpdate(isUpdate);
		track.setId(trackId);
		track = trackRepository.save(track);
		return track;
	}

	private CompletableFuture<MSAResponse> updateImportExcelData(Long trackId, Long projectId, List<String> errorList,
			Long projectCompanyId, Path filePath, String errorDestFileName, MSAResponse response, boolean isMpp) {
		Map<String, List<ImportExcelTaskDTO>> importData = new HashMap<>();
		boolean isUpdateMpp = true;
		StringBuilder errorHeaders = new StringBuilder();

		List<Object[]> excelDataList = commonRepository.findByTrackId(trackId);
		if (excelDataList != null && !excelDataList.isEmpty()) {
			updateconvertFormatObj(excelDataList, importData, projectId, errorList, trackId, isMpp, isUpdateMpp);
			updateLookaheadResponse(projectId, projectCompanyId, importData, trackId, false, isUpdateMpp);
		}
		if (errorList != null && !errorList.isEmpty()) {
			String dataHeader = "TASK_NAME,,TASK_START_DATE,,TASK_END_DATE,,TARGET_COMPLETION_DATE,,ACTIVITY_ID,,IMPORT_UNIQUE_ID,,ERROR_MESSAGE";
			String header[] = dataHeader.split(",");
			List<String> errorListHeaders = new ArrayList<String>();
			errorListHeaders = Arrays.asList(header);
			errorListHeaders.forEach(columnHeaders -> {
				// if(columnHeaders!=null&&
				// !columnHeaders.equalsIgnoreCase(",")&&!columnHeaders.equalsIgnoreCase("null,")&&!columnHeaders.isEmpty())
				// {
				errorHeaders.append(columnHeaders + ",");
				// }
			});
			errorList.add(0, errorHeaders.toString());
			sendErrorRecords(errorList, filePath, null, ERROR_CSV);
			copyExcelFileToDcokerPath(filePath, errorDestFileName);
			String msg = "Duplicates or existing task found.";
			if (importData != null && !importData.isEmpty() && importData.size() > 0) {
				msg = "Some data have been successfully updated. Please check after a few minutes. Check the information excel for the non-imported tasks";
			}
			return CompletableFuture.completedFuture(
					new MSAResponse(false, HttpStatus.BAD_REQUEST, msg, errorDestFileName, "error occured", errorList));
		}
		copyExcelFileToDcokerPath(filePath, errorDestFileName);
		return CompletableFuture.completedFuture(new MSAResponse(true, HttpStatus.OK,
				"Your data is updated successfully, please check after a few minutes.", errorDestFileName,
				response.getMessage(), errorList));

	}

	private void importMppDataToTable(List<ImportMPPDTO> importMPPDTOList, Long trackId, String inProgress) {
		Long totalFields = 12L;
		String query = " insert into temp  ";
		StringBuilder queryColumns = new StringBuilder(" (status,track_id");
		for (int i = 1; i <= totalFields; i++) {
			queryColumns.append(",field_" + i);
		}
		queryColumns.append(")");
		importMPPDTOList.forEach(importTaskDto -> {
			StringBuilder queryBuilder = new StringBuilder();
			try {
				if (!importTaskDto.getIsChild()) {
					String taskDescription = null;
					String description = importTaskDto.getTaskDescription();
					if (description != null) {
						taskDescription = description.replaceAll("'", "''");
					}
					queryBuilder
							.append(" values ('" + inProgress + "'," + trackId + "," + null + ",'" + taskDescription
									+ "'," + null + ",'"
									+ DateUtil.convertDate(
											importTaskDto.getStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
									+ "','"
									+ DateUtil.convertDate(
											importTaskDto.getFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
									+ "','"
									+ DateUtil.convertDate(importTaskDto.getTargetCompletionDate(),
											DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
									+ "'," + importTaskDto.getTaskType() + ",'" + importTaskDto.getResponsibility()
									+ "'," + importTaskDto.getSpecDivision() + ",'" + importTaskDto.getActivityId()
									+ "','" + importTaskDto.getUniqueId() + "','" + importTaskDto.getNotes() + "' ) ");
					log.info(query + queryColumns.toString() + queryBuilder.toString());
					utility.runQuery(query + queryColumns.toString() + queryBuilder.toString());

					queryBuilder.setLength(0);
				} else {
					String description = importTaskDto.getTaskDescription();
					String subTaskDescription = description.replaceAll("'", "''");
					importTaskDto.getSubTasks().forEach(importSubTaskDto -> {
						queryBuilder.append(" values ('" + inProgress + "'," + trackId + "," + null + ",'"
								+ subTaskDescription + "','" + importSubTaskDto.getSubTaskDescription() + "','"
								+ DateUtil.convertDate(
										importSubTaskDto.getStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
								+ "','"
								+ DateUtil.convertDate(
										importSubTaskDto.getFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
								+ "','"
								+ DateUtil.convertDate(importSubTaskDto.getTargetCompletionDate(),
										DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
								+ "'," + importSubTaskDto.getTaskType() + ",'" + importSubTaskDto.getResponsibility()
								+ "'," + importSubTaskDto.getSpecDivision() + ",'" + importSubTaskDto.getActivityId()
								+ "','" + importTaskDto.getUniqueId() + "','" + importSubTaskDto.getNotes() + "' ) ");
						utility.runQuery(query + queryColumns.toString() + queryBuilder.toString());
						queryBuilder.setLength(0);

					});
				}

			} catch (Exception e) {
				log.info("Excepption while insert into temp table " + e.getMessage());
			}

		});
	}

	private void updateImportMppDataToTable(List<ImportMPPDTO> importMPPDTOList, Long trackId, String inProgress) {
		Long totalFields = 12L;
		String query = " insert into temp  ";
		StringBuilder queryColumns = new StringBuilder(" (status,track_id");
		for (int i = 1; i <= totalFields; i++) {
			queryColumns.append(",field_" + i);
		}
		queryColumns.append(")");
		importMPPDTOList.forEach(importTaskDto -> {
			StringBuilder queryBuilder = new StringBuilder();
			try {
				if (!importTaskDto.getIsChild()) {

					String description = importTaskDto.getTaskDescription();
					String taskDescription = description.replaceAll("'", "''");
					queryBuilder
							.append(" values ('" + inProgress + "'," + trackId + "," + null + ",'" + taskDescription
									+ "'," + null + ",'"
									+ DateUtil.convertDate(
											importTaskDto.getStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
									+ "','"
									+ DateUtil.convertDate(
											importTaskDto.getFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
									+ "','"
									+ DateUtil.convertDate(importTaskDto.getTargetCompletionDate(),
											DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
									+ "'," + importTaskDto.getTaskType() + ",'" + importTaskDto.getResponsibility()
									+ "'," + importTaskDto.getSpecDivision() + ",'" + importTaskDto.getActivityId()
									+ "','" + importTaskDto.getUniqueId() + "','" + importTaskDto.getNotes() + "' ) ");
					log.info(query + queryColumns.toString() + queryBuilder.toString());
					utility.runQuery(query + queryColumns.toString() + queryBuilder.toString());

					queryBuilder.setLength(0);
				} else {
					String description = importTaskDto.getTaskDescription();
					String subTaskDescription = description.replaceAll("'", "''");
					importTaskDto.getSubTasks().forEach(importSubTaskDto -> {
						queryBuilder.append(" values ('" + inProgress + "'," + trackId + "," + null + ",'"
								+ subTaskDescription + "','" + importSubTaskDto.getSubTaskDescription() + "','"
								+ DateUtil.convertDate(
										importSubTaskDto.getStartDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
								+ "','"
								+ DateUtil.convertDate(
										importSubTaskDto.getFinishDate(), DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
								+ "','"
								+ DateUtil.convertDate(importSubTaskDto.getTargetCompletionDate(),
										DateUtil.Formats.DDMMMYYYY_HYPHEN.toString())
								+ "'," + importSubTaskDto.getTaskType() + ",'" + importSubTaskDto.getResponsibility()
								+ "'," + importSubTaskDto.getSpecDivision() + ",'" + importSubTaskDto.getActivityId()
								+ "','" + importTaskDto.getUniqueId() + "','" + importSubTaskDto.getNotes() + "' ) ");
						utility.runQuery(query + queryColumns.toString() + queryBuilder.toString());
						queryBuilder.setLength(0);

					});
				}

			} catch (Exception e) {
				log.info("Excepption while insert into temp table " + e.getMessage());
			}

		});
	}

	private ImportMPPDTO taskDataConvertToDto(Task localTask) {
		ImportMPPDTO importMPPDTO = new ImportMPPDTO();
		importMPPDTO.setTaskDescription(localTask.getName());
		importMPPDTO.setSubTaskDescription(localTask.getName());
		importMPPDTO.setStartDate(localTask.getStart());
		importMPPDTO.setFinishDate(localTask.getFinish());
		importMPPDTO.setResponsibility("");
		importMPPDTO.setSubTasks(Collections.emptyList());
		importMPPDTO.setUniqueId(localTask.getUniqueID());
		importMPPDTO.setActivityId(localTask.getID());
		importMPPDTO.setTargetCompletionDate(localTask.getLateFinish());
		importMPPDTO.setNotes(localTask.getNotes());
//		importMPPDTO.setDeadLineDate(localTask.getLateFinish());

		return importMPPDTO;
	}

	@Override
	public MSAResponse importHistory(Long projectId, Integer pageNo, Integer pageSize) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateUtil.Formats.DDMMMYYYY_HHMMSS_SSS.toString());
		List<ImportHistoryDTO> historyList = new ArrayList<>();
		PageRequest pageRequest = PageRequest.of(pageNo, pageSize, Sort.Direction.DESC, "id");

		List<Object[]> projectCompaniesList = projectCompanyRepository.findCompaniesByProjectId(projectId);
		List<ProjectUser> projectUsers = projectUserRepository.findByProjectId(projectId);
		Page<Track> trackHistory = trackRepository.findByProjectId(pageRequest, projectId);
		List<Track> trackHistoryList = trackHistory.getContent();
		trackHistoryList.forEach(track -> {
			String serverFileName = track.getServerFileName();
			Long userId = track.getUserId();
			ImportHistoryDTO importHistoryDTO = new ImportHistoryDTO();
			importHistoryDTO.setImportHistoryId(track.getId());
			importHistoryDTO
					.setFileName(serverFileName.substring(serverFileName.indexOf('_') + 1, serverFileName.length()));
			importHistoryDTO.setServerFileName(serverFileName);
			ProjectUser projectUser = projectUsers.stream().filter(user -> user.getUserId() == userId).findFirst()
					.get();
			Long projectCompanyId = projectUser.getProjectCompany().getId();
			projectCompaniesList.stream().forEach(company -> {
				BigInteger companyId = (BigInteger) company[2];
				if (companyId.equals(BigInteger.valueOf(projectCompanyId))) {
					String companyName = (String) company[1];
					importHistoryDTO.setCompanyName(companyName.trim());
				}
			});
			importHistoryDTO.setLastImportedBy(projectRepository.findUserNameByUserId(userId).trim());
			if (track.getImportDate() != null) {
				LocalDateTime importedTime = DateUtil.utcZoneDateTimeToZoneDateTimeConvert(track.getImportDate(),
						projectUserRepository.findTimeZoneByUserId(userId));
				importHistoryDTO.setLastImportedDate(formatter.format(importedTime));
			}
			importHistoryDTO.setDelete(track.isDelete());
			importHistoryDTO.setUpdate(track.isUpdate());
			historyList.add(importHistoryDTO);
		});
		if (trackHistory.hasContent()) {
			Pagination pagination = new Pagination();
			pagination.setCurrentPageNumber(pageRequest.getPageNumber());
			pagination.setPaginationSize(pageRequest.getPageSize());
			pagination.setTotalRecords(trackHistory.getTotalElements());
			return new MSAResponse(true, REC + FND, historyList, pagination, HttpStatus.OK);
		} else {
			String msg = REC + NOT + FND;
			if (null == trackHistoryList)
				msg = "No Records";
			return new MSAResponse(false, HttpStatus.NOT_FOUND, msg, "E-001", null);
		}

	}

	@Override
	@SneakyThrows
	public MSAResponse updateImportMppFile(String fileName, Long projectId, Long projectCompanyId,
			Long importHistoryId) {
		List<Task> taskList = new ArrayList<>();
		String filePath = DOCKER_FILE_DIRECTORY + fileName;
		writeCSV(DOCKER_FILE_DIRECTORY + FOLDER_SEPARATOR + ERROR_CSV, new StringBuilder());
		List<com.mindzen.planner.entity.Task> taskListFromTable = taskRepository.findByTaskImportId(importHistoryId,
				projectId);
		ProjectReader reader = ProjectReaderUtility.getProjectReader(filePath);
		ProjectFile projectFile = reader.read(filePath);
		projectFile.getTasks().forEach(taskList::add);
		List<ImportMPPDTO> importMPPDTOList = new ArrayList<>();
		taskList = taskList.stream().filter(task -> !task.getSummary()).collect(Collectors.toList());

		// for (com.mindzen.planner.entity.Task taskListFromDB : taskListFromTable) {
		taskList.forEach(task -> {
			System.out.println(task.getUniqueID());
			com.mindzen.planner.entity.Task taskFromImportUniqueId = taskRepository.findByTaskId(task.getUniqueID(),
					projectId, importHistoryId);
			// if(taskFromImportUniqueId!=null) {
			// if(task.getUniqueID().equals(taskListFromDB.getImportUniqueId())){
			log.info("UniqueId Matched");
			ImportMPPDTO importMPPDTO;
			importMPPDTO = taskDataConvertToDto(task);
			importMPPDTO.setIsChild(false);
			importMPPDTO.setSubTaskDescription(null);
			importMPPDTOList.add(importMPPDTO);
			// }

		});

		return updateDTOValuesToTempTable(importMPPDTOList, fileName, DOCKER_FILE_DIRECTORY, projectId,
				projectCompanyId, true, importHistoryId);

	}

	@Override
	public MSAResponse clearSchedule(Long projectId, Long projectCompanyId, Long importHistoryId) {
		List<com.mindzen.planner.entity.Task> taskDetails = taskRepository.findByTaskImportTrackId(importHistoryId,
				projectId);
		clearTrackid(importHistoryId);
		taskDetails.forEach(task -> {
			if (!task.isLookAheadChange()) {
				task.setActive(false);
				task.setId(task.getId());
				taskRepository.save(task);
			} else {
				task.setImportDeadLineDate(null);
				task.setImportStartDate(null);
				task.setImportNotes(null);
				task.setImportFinishDate(null);
				task.setImportTargetCompletionDate(null);
				task.setId(task.getId());
				task.setActivityDescription(null);
				task.setActivityId(null);
				taskRepository.save(task);
			}
		});
		MSAResponse response = new MSAResponse(true, HttpStatus.OK, Constants.SUCCESS, new ArrayList<>());
		return new MSAResponse(true, HttpStatus.OK, "Your data is successfully cleared.", null);

	}

	private Track clearTrackid(Long trackId) {
		Optional<Track> trackDetails = trackRepository.findById(trackId);
		Track track = new Track();
		track.setFileName(trackDetails.get().getFileName());
		track.setActive(false);
		track.setDelete(true);
		track.setId(trackId);
		track.setImportDate(trackDetails.get().getImportDate());
		track.setInsertStatus(trackDetails.get().getInsertStatus());
		track.setProjectId(trackDetails.get().getProjectId());
		track.setRecordStatus(trackDetails.get().getRecordStatus());
		track.setServerFileName(trackDetails.get().getServerFileName());
		track.setUserId(trackDetails.get().getUserId());

		trackRepository.save(track);

		return track;
	}

}
