/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProjectSummaryDetailsDTO;
import com.mindzen.planner.entity.ProjectUser;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProjectEmailNotificationService.
 */
public interface ProjectEmailNotificationService {

	/**
	 * Gets the task summary by project user object.
	 *
	 * @author Raja Gopal MRG
	 * @param projectUserObject the project id
	 * @return the task summary by project id
	 */
	public MSAResponse getTaskSummaryByProjectUserObject(ProjectUser projectUserObject);
	
	/**
	 * Gets the task summary by project user id.
	 *
	 * @param projectUserId the project user id
	 * @return the task summary by project user id
	 */
	public MSAResponse getTaskSummaryByProjectUserId(Long projectUserId);
	
	/**
	 * Gets the task summary by project id.
	 *
	 * @author Raja Gopal MRG
	 * @param projectId the project id
	 * @return the task summary by project id
	 */
	public MSAResponse getTaskSummaryByProjectId(Long projectId);

	public void sendProjectSummaryDetailsMail(String email, ProjectSummaryDetailsDTO projectSummaryDetails);
	
}
