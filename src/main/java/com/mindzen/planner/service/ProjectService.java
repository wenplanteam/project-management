/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProjectDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProjectService.
 *
 */
public interface ProjectService {
	
	/**
	 * Project list with pagination.
	 *
	 * @param pageRequest the page request
	 * @param status list of projects
	 * @param searchTerm the search term
	 * @param created the created
	 * @return the MSA response
	 */
	public MSAResponse projects(Pageable pageRequest, Boolean status, String searchTerm, Long created);
	
	/**
	 * Post project.
	 *
	 * @param projectDTO the project DTO
	 * @return creating projects
	 */
	public MSAResponse postProject(ProjectDTO projectDTO);

	/**
	 * Put project.
	 *
	 * @param projectDTO the project DTO
	 * @return  updating projects
	 */
	public MSAResponse putProject(ProjectDTO projectDTO);
	
	/**
	 * Find by project id.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	public MSAResponse findByProjectId(Long projectId);

	/**
	 * Delete project.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	public MSAResponse deleteProject(Long[] projectId);

	/**
	 * Gets the project info.
	 *
	 * @param projectId the project id
	 * @return api response
	 */
//	public MSAResponse findByProjectForPlanSharing(Long projectId);

	public MSAResponse getProjectInfo(Long projectId);

	/**
	 * Insert project info.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	public MSAResponse insertProjectInfo(ProjectDTO projectDTO);

	/**
	 * Insert trade.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	public MSAResponse insertTrade(ProjectDTO projectDTO);

	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 */
	public MSAResponse getTrade(Long projectId);
	
	/**
	 * Gets the spec division.
	 *
	 * @param projectId the project id
	 * @return the spec division
	 */
	public MSAResponse getSpecDivision(Long projectId);

	/**
	 * Insert spec division.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	public MSAResponse insertSpecDivision(ProjectDTO projectDTO);

	/**
	 * Gets the GC by user.
	 *
	 * @return the GC by user
	 */
	public MSAResponse getGCByUser();

	/**
	 * Insert default notification for project user.
	 */
	public void insertDefaultNotificationForProjectUser();

	/**
	 * Gets the progress completion by project id.
	 *
	 * @param projectCompletionRequestDTO the project completion request DTO
	 * @return the progress completion by project id
	 */
	public MSAResponse getProgressCompletionByProjectId(@Valid ProgressCompletionRequestDTO progressCompletionRequestDTO);

}
