/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProjectCompanyDTO;
import com.mindzen.planner.dto.UserDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProjectCompanyService.
 */
public interface ProjectCompanyService {

	/**
	 * Gets the project companies.
	 *
	 * @param projectId the project id
	 * @return the project companies
	 */
	public MSAResponse getProjectCompanies(Long projectId);

	/**
	 * Insert project companies.
	 *
	 * @param projectCompanyDTO the project company DTO
	 * @return the MSA response
	 */
	public MSAResponse insertProjectCompanies(ProjectCompanyDTO projectCompanyDTO);

	/**
	 * User company associate to project.
	 *
	 * @param userId the user id
	 * @return the MSA response
	 */
	public MSAResponse userCompanyAssociateToProject(Long userId);

	/**
	 * Update project companies.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	public MSAResponse updateProjectCompanies(UserDTO userDTO);

}
