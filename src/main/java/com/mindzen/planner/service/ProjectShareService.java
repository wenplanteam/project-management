/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.service;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.ProjectUserDTO;
import com.mindzen.planner.dto.UserDTO;
// TODO: Auto-generated Javadoc

/**
 * The Interface ProjectShareService.
 *
 * @author mindzen
 */
public interface ProjectShareService {

	/**
	 * Share project.
	 *
	 * @param projectUserDTO the project user DTO
	 * @return the MSA response
	 */
	public MSAResponse shareProject(ProjectUserDTO projectUserDTO);

	/**
	 * Edits the share project.
	 *
	 * @param projectUserDTO the project user DTO
	 * @return the MSA response
	 */
	MSAResponse editShareProject(ProjectUserDTO projectUserDTO);

	/**
	 * Update project shared user details.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	public MSAResponse updateProjectSharedUserDetails(UserDTO userDTO);

}
