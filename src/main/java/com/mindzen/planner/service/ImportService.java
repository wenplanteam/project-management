package com.mindzen.planner.service;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.TaskDTO;



public interface ImportService {

	public MSAResponse importExcel(String excelFileName, Long projectId, Long projectCompanyId) throws Exception;

	public MSAResponse importMppFile(String mppFileName, Long projectId, Long projectCompanyId, boolean isRenew, Long importId);

	public MSAResponse importXerFile(String xerFileName, Long projectId, Long projectCompanyId, boolean isRenew);

	public MSAResponse importHistory(Long projectId, Integer pageNo, Integer pageSize);

	public MSAResponse updateImportMppFile(String fileName, Long projectId, Long projectCompanyId,
			Long importHistoryId);

	public MSAResponse clearSchedule(Long projectId, Long projectCompanyId, Long importHistoryId);


}

