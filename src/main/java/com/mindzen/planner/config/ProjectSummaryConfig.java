/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;

/**
 * The Class ProjectSummaryConfig.
 */
@Configuration

/**
 * Checks if is project task summary.
 *
 * @return true, if is project task summary
 */

/**
 * Gets the tasks past due.
 *
 * @return the tasks past due
 */
@Getter
@PropertySource("classpath:/project_summary_config.properties")
public class ProjectSummaryConfig {

	/** The one new task. */
	@Value("${one_new_task}")
	private String oneNewTask;

	/** The one modify task. */
	@Value("${one_modify_task}")
	private String oneModifyTask;

	/** The one new subtask. */
	@Value("${one_new_subtask}")
	private String oneNewSubtask;

	/** The one modify subtask. */
	@Value("${one_modify_subtask}")
	private String oneModifySubtask;

	/** The many new task. */
	@Value("${many_new_task}")
	private String manyNewTask;

	/** The many modify task. */
	@Value("${many_modify_task}")
	private String manyModifyTask;

	/** The many new subtask. */
	@Value("${many_new_subtask}")
	private String manyNewSubtask;

	/** The many modify subtask. */
	@Value("${many_modify_subtask}")
	private String manyModifySubtask;

	/** The one req completed task. */
	@Value("${one_req_completed_task}")
	private String oneReqCompletedTask;

	/** The many req completed task. */
	@Value("${many_req_completed_task}")
	private String manyReqCompletedTask;

	/** The one req completed subtask. */
	@Value("${one_req_completed_subtask}")
	private String oneReqCompletedSubtask;

	/** The many req completed subtask. */
	@Value("${many_req_completed_subtask}")
	private String manyReqCompletedSubtask;

	/** The behind schedule task. */
	@Value("${one_behind_schedule_task}")
	private String oneBehindScheduleTask;

	/** The warning task. */
	@Value("${one_warning_task}")
	private String oneWarningTask;

	/** The one un schedule task. */
	@Value("${one_unschedule_task}")
	private String oneUnScheduleTask;

	/** The many un schedule task. */
	@Value("${many_unschedule_task}")
	private String manyUnScheduleTask;

	/** The behind schedule task. */
	@Value("${many_behind_schedule_task}")
	private String manyBehindScheduleTask;

	/** The warning task. */
	@Value("${many_warning_task}")
	private String manyWarningTask;

	/** The many future warning task. */
	@Value("${many_future_warning_task}")
	private String manyFutureWarningTask;

	/** The one future warning task. */
	@Value("${one_future_warning_task}")
	private String oneFutureWarningTask;

	/** The one future behind schedule task. */
	@Value("${one_future_behind_schedule_task}")
	private String oneFutureBehindScheduleTask;

	/** The many future behind schedule task. */
	@Value("${many_future_behind_schedule_task}")
	private String manyFutureBehindScheduleTask;
	
	/** The one future behind schedule task. */
	@Value("${one_future_completed_task}")
	private String oneFutureCompletedTask;

	/** The many future behind schedule task. */
	@Value("${many_future_completed_task}")
	private String manyFutureCompletedTask;

	/** The not found. */
	@Value("${not_found}")
	private String notFound;

	/** The template name. */
	@Value("${weekly_template_name}")
	private String weeklyTemplateName;

	/** The daily template name. */
	@Value("${daily_template_name}")
	private String dailyTemplateName;

	/** The mail subject. */
	@Value("${mail_subject}")
	private String mailSubject;

	/** The is project summary schedule. */
	@Value("${is_project_summary_schedule}")
	private boolean isProjectSummarySchedule;

	/** The throught email. */
	@Value("${through_email}")
	private String throughtEmail;

	/** The from application. */
	@Value("${from_application}")
	private String fromApplication;

	/** The project task summary. */
	@Value("${project_task_summary}")
	private String projectTaskSummary;

	/** The task in alert. */
	@Value("${task_in_alert}")
	private String taskInAlert;

	/** The newly added task. */
	@Value("${newly_added_task}")
	private String newlyAddedTask;

	/** The modified task. */
	@Value("${modified_task}")
	private String modifiedTask;

	/** The tasks past due. */
	@Value("${tasks_past_due}")
	private String tasksPastDue;

}
