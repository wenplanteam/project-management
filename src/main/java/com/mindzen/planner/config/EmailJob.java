/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.config;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * The Class EmailJob.
 */
@Component
public class EmailJob extends QuartzJobBean {
    
    /** The Constant logger. */
    private static final Logger logger = LoggerFactory.getLogger(EmailJob.class);

    /* (non-Javadoc)
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     */
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Executing Job with key parent class {}", jobExecutionContext.getJobDetail().getKey());
    }

}
