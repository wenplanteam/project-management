package com.mindzen.planner.config;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindzen.config.Property;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ExcelProperty {

	//private static String PROPERTY_CONFIG_FILE_NAME = "uploadFile.json";
	@Value("${config.fileName}")
	private String PROPERTY_CONFIG_FILE_NAME;
	public static JSONArray jsonArray;
	public static List<Property> properties = new ArrayList<>();

	@PostConstruct
	private void init(){
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					this.getClass().getResourceAsStream("/" + PROPERTY_CONFIG_FILE_NAME)));
			JSONParser parser = new JSONParser();
			jsonArray = (JSONArray) parser.parse(reader);
			ObjectMapper mapper = new ObjectMapper();
			properties = mapper.readValue(ExcelProperty.jsonArray.toJSONString(),new TypeReference<List<Property>>(){});
		} catch (Exception e) {
			log.error("File read issue excel component :" +e.getLocalizedMessage());
			log.error("File read issue excel component :" +e.getMessage());
		}
	}
}
