/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.config.audit;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import com.mindzen.planner.config.UserInfoContext;

// TODO: Auto-generated Javadoc
/**
 * The Class AuditorAwareImpl.
 *
 * @author Alexpandiyan Chokkan
 */
@Component
public class AuditorAwareImpl implements AuditorAware<Long> {

	/* (non-Javadoc)
	 * @see org.springframework.data.domain.AuditorAware#getCurrentAuditor()
	 */
	@Override
	public Optional<Long> getCurrentAuditor() {
		return Optional.ofNullable(UserInfoContext.getUserThreadLocal());
	}

}
