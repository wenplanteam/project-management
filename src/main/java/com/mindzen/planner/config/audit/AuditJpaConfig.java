/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.config.audit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

// TODO: Auto-generated Javadoc
/**
 * The Class AuditJpaConfig.
 *
 * @author Alexpandiyan Chokkan
 */
@Configuration
@EnableJpaAuditing(auditorAwareRef="auditorAware")
public class AuditJpaConfig {

	/**
	 * Auditor aware.
	 *
	 * @return the auditor aware impl
	 */
	@Bean
	public AuditorAwareImpl auditorAware() {
		return new AuditorAwareImpl();
	}
	
}
