/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * The Class AppConfig.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 25-Sep-2018
 */
@Configuration
public class AppConfig {

	/** The user verify host and port. */
	@Value("${spring.mail.user.verify.url.host-port}")
	private String userVerifyHostAndPort;

	/** The user verify path. */
	@Value("${spring.mail.user.verify.url.path}")
	private String userVerifyPath;
	
	/** The new user type. */
	@Value("${spring.mail.user.type.new}")
	private String newUserType;

	/** The shared user type. */
	@Value("${spring.mail.user.type.shared}")
	private String sharedUserType;
	
	/** The lookahead pdf report empty schedule. */
	@Value("${lookahead.pdf.report.empty.schedule}")
	private boolean lookaheadPdfReportEmptySchedule;

	/**
	 * Gets the user verify host and port.
	 *
	 * @return the user verify host and port
	 */
	public String getUserVerifyHostAndPort() {
		return userVerifyHostAndPort;
	}

	/**
	 * Sets the user verify host and port.
	 *
	 * @param userVerifyHostAndPort the new user verify host and port
	 */
	public void setUserVerifyHostAndPort(String userVerifyHostAndPort) {
		this.userVerifyHostAndPort = userVerifyHostAndPort;
	}

	/**
	 * Gets the user verify path.
	 *
	 * @return the user verify path
	 */
	public String getUserVerifyPath() {
		return userVerifyPath;
	}

	/**
	 * Sets the user verify path.
	 *
	 * @param userVerifyPath the new user verify path
	 */
	public void setUserVerifyPath(String userVerifyPath) {
		this.userVerifyPath = userVerifyPath;
	}

	/**
	 * Gets the new user type.
	 *
	 * @return the new user type
	 */
	public String getNewUserType() {
		return newUserType;
	}

	/**
	 * Sets the new user type.
	 *
	 * @param newUserType the new new user type
	 */
	public void setNewUserType(String newUserType) {
		this.newUserType = newUserType;
	}

	/**
	 * Gets the shared user type.
	 *
	 * @return the shared user type
	 */
	public String getSharedUserType() {
		return sharedUserType;
	}

	/**
	 * Sets the shared user type.
	 *
	 * @param sharedUserType the new shared user type
	 */
	public void setSharedUserType(String sharedUserType) {
		this.sharedUserType = sharedUserType;
	}
	
	/**
	 * Sets the lookahead pdf report empty schedule.
	 *
	 * @param lookaheadPdfReportEmptySchedule the lookaheadPdfReportEmptySchedule to set
	 */
	public void setLookaheadPdfReportEmptySchedule(boolean lookaheadPdfReportEmptySchedule) {
		this.lookaheadPdfReportEmptySchedule = lookaheadPdfReportEmptySchedule;
	}
	
	/**
	 * Checks if is lookahead pdf report empty schedule.
	 *
	 * @return the lookaheadPdfReportEmptySchedule
	 */
	public boolean isLookaheadPdfReportEmptySchedule() {
		return lookaheadPdfReportEmptySchedule;
	}
	
}
