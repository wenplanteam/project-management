/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.config;

import javax.sql.DataSource;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/**
 * The Class SchedulerConfiguration.
 */
@Configuration

/** The Constant log. */
@Slf4j
public class SchedulerConfiguration {
    
    /** The data source. */
    @Autowired
    DataSource dataSource;

/**
 * A factory for creating AutowireCapableBeanJob objects.
 */
public class AutowireCapableBeanJobFactory extends SpringBeanJobFactory {

    /** The bean factory. */
    private final AutowireCapableBeanFactory beanFactory;

    /**
     * Instantiates a new autowire capable bean job factory.
     *
     * @param beanFactory the bean factory
     */
    @Autowired
    public AutowireCapableBeanJobFactory(AutowireCapableBeanFactory beanFactory) {
       log.info("Bean factory must not be null "+beanFactory);
        this.beanFactory = beanFactory;
    }

    /* (non-Javadoc)
     * @see org.springframework.scheduling.quartz.SpringBeanJobFactory#createJobInstance(org.quartz.spi.TriggerFiredBundle)
     */
    @Override
    protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
        Object jobInstance = super.createJobInstance(bundle);
        this.beanFactory.autowireBean(jobInstance);
        this.beanFactory.initializeBean(jobInstance, null);
        return jobInstance;
    }
}

/**
 * Scheduler factory.
 *
 * @param applicationContext the application context
 * @return the scheduler factory bean
 */
@Bean
public SchedulerFactoryBean schedulerFactory(ApplicationContext applicationContext) {
    SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
    schedulerFactoryBean.setOverwriteExistingJobs(true);
    schedulerFactoryBean.setDataSource(dataSource);
    schedulerFactoryBean.setConfigLocation(new ClassPathResource("application.yml"));
    schedulerFactoryBean.setJobFactory(
            new AutowireCapableBeanJobFactory(applicationContext.getAutowireCapableBeanFactory()));
    return schedulerFactoryBean;
}

/**
 * Scheduler.
 *
 * @param applicationContext the application context
 * @return the scheduler
 * @throws SchedulerException the scheduler exception
 */
@Bean
public Scheduler scheduler(ApplicationContext applicationContext) throws SchedulerException {
    Scheduler scheduler = schedulerFactory(applicationContext).getScheduler();
    scheduler.start();
    return scheduler;
}
}