/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.scheduler;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.ProjectSummaryConfig;
import com.mindzen.planner.dto.NotificationDTO;
import com.mindzen.planner.dto.ProjectSummaryDetailsDTO;
import com.mindzen.planner.entity.NotificationEntity;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.repository.NotificationRepository;
import com.mindzen.planner.repository.ProjectRepository;
import com.mindzen.planner.repository.ProjectUserRepository;
import com.mindzen.planner.service.ProjectEmailNotificationService;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.EnumDays;

import edu.emory.mathcs.backport.java.util.Arrays;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ProjectEmailNotificationScheduler {

	/** The job builders. */
	@Autowired
	private JobBuilders jobBuilders;

	/** The mail job scheduler. */
	@Autowired
	private JobsScheduler mailJobScheduler;

	/** The proj repository. */
	@Resource
	private ProjectRepository projRepository;

	/** The proj user repository. */
	@Resource
	private ProjectUserRepository projUserRepository;

	/** The proj summary config. */
	@Autowired
	private ProjectSummaryConfig projSummaryConfig;

	/** The notification repo. */
	@Autowired
	private NotificationRepository notificationRepo;

	@Autowired
	private ProjectEmailNotificationService projectEmailNotificationService;

	/**
	 * Project summary scheduler.
	 */
	@SuppressWarnings("unchecked")
	@Scheduled(cron = "0 01 00 * * *", zone = Constants.DEFAULT_TIME_ZONE)
//	@Scheduled(fixedDelay=300000)
	public void projectSummaryScheduler() {
		Long mintues = 1L;
		LocalDateTime localTime = LocalDateTime.now();
		if (projSummaryConfig.isProjectSummarySchedule()) {
			List<Project> projectList = projRepository.findAllActiveProjects(Constants.False);
			List<NotificationDTO> notificationList = new ArrayList<NotificationDTO>();
			for (Project projectObj : projectList) {

//				List<ProjectUser> sc
				// user license and notification enable check.
				List<ProjectUser> projectUsers = projectObj.getProjectUser().stream()
						.filter(projUserObj -> Optional.ofNullable(
								projRepository.findUserIdByActiveLicense(projUserObj.getUserId(), Constants.TRUE))
								.orElse(false))
						.filter(projUserObj -> projUserObj.getNotification() != null
								&& Optional.ofNullable(projUserObj.getNotification().isNotification()).orElse(false))
						.filter(projUserObj -> projUserObj.getNotification() != null
								&& !projUserObj.getNotification().isScheduled())
						.collect(Collectors.toList());

				if (null != projectUsers && !projectUsers.isEmpty()) {
					projectUsers.forEach(projUserObj -> {
						log.info("projectEmailNotificationService.getTaskSummaryByProjectUserId() project user id " + projUserObj.getId());
						MSAResponse response = projectEmailNotificationService.getTaskSummaryByProjectUserId(projUserObj.getId());
						log.info("exit projectEmailNotificationService.getTaskSummaryByProjectUserId() project user id " + projUserObj.getId());
						if(response.getPayload() instanceof ProjectSummaryDetailsDTO) {
						
						ProjectSummaryDetailsDTO projectSummaryDetails = (ProjectSummaryDetailsDTO) response
								.getPayload();
						if (projectSummaryDetails != null && (projectSummaryDetails.isFutureSummaryResult()
								|| projectSummaryDetails.isSummaryResult())

						) {
							String zoneName = projUserRepository.findTimeZoneByUserId(projUserObj.getUserId());
							log.info("zone name :" + zoneName);
							if (zoneName == null) {
								zoneName = "America/Chicago";
							}
							ZoneId zoneId = ZoneId.of(zoneName);
							NotificationEntity notificationObj = projUserObj.getNotification();
							List<String> notifyTypeList = Arrays.asList(notificationObj.getType().split(","));
							if (!notifyTypeList.isEmpty()) {
								for (String typeObj : notifyTypeList) {
			                           log.info("start scheduling for project User Id " + projUserObj.getId());
									if (typeObj.equals(projSummaryConfig.getThroughtEmail())) {
										if (notificationObj.getFrequency() != null
												&& notificationObj.getFrequency().equalsIgnoreCase(Constants.WEEKLY)) {
											log.info("local server date and time " + localTime);
											ZonedDateTime dateTime = getScheduleWeekDate(localTime, zoneId,
													notificationObj.getDayOfTheWeek());
											if (dateTime.isBefore(ZonedDateTime.now(zoneId))) {
												log.error("dateTime must be after current time");
											}
											ZonedDateTime utcDateTime = zoneTimeToUTC(dateTime);
											notificationObj.setScheduledAt(utcDateTime.toLocalDateTime());
											notificationObj.setScheduled(Constants.TRUE);
											notificationObj.setMailSent(Constants.False);
											notificationObj = notificationRepo.save(notificationObj);

											NotificationDTO notificationDTO = new NotificationDTO();
											notificationDTO.setDayOfTheWeek(notificationObj.getDayOfTheWeek());
											notificationDTO.setZoneName(zoneName);
											notificationDTO.setFrequency(notificationObj.getFrequency());
											notificationDTO.setProjectUserId(projUserObj.getId());
											notificationDTO.setUtcScheduledDateTime(utcDateTime);
											notificationList.add(notificationDTO);
											log.info("schedule  at:" + notificationObj.getScheduledAt());

										} else {
											log.info("local server date and time " + localTime);
											ZonedDateTime dateTime = ZonedDateTime.of(localTime, zoneId);
											if (dateTime.isBefore(ZonedDateTime.now(zoneId))) {
												log.error("dateTime must be after current time");
											}
											ZonedDateTime utcDateTime = zoneTimeToUTC(dateTime);
											notificationObj.setScheduledAt(utcDateTime.toLocalDateTime());
											notificationObj.setScheduled(Constants.TRUE);
											notificationObj.setMailSent(Constants.False);
											notificationObj = notificationRepo.save(notificationObj);

											NotificationDTO notificationDTO = new NotificationDTO();
											notificationDTO.setDayOfTheWeek(null);
											notificationDTO.setZoneName(zoneName);
											notificationDTO.setFrequency(notificationObj.getFrequency());
											notificationDTO.setProjectUserId(projUserObj.getId());
											notificationDTO.setUtcScheduledDateTime(utcDateTime);
											notificationList.add(notificationDTO);
										}
									}
								}
							}

						}
					}
					});

				} else
					log.info("No Jobs are enabled mail configurations");
			}

			if (!notificationList.isEmpty()) {
				Map<NotificationDTO, List<Long>> map = new HashMap<NotificationDTO, List<Long>>();
				for (NotificationDTO notificationDTO : notificationList) {

					if (map.get(notificationDTO) != null) {
						List<Long> list = map.get(notificationDTO);
						list.add(notificationDTO.getProjectUserId());
						map.put(notificationDTO, list);
					} else {
						List<Long> list = new ArrayList<Long>();
						list.add(notificationDTO.getProjectUserId());
						map.put(notificationDTO, list);
					}
				}
				log.info(map.toString());
				for (Entry<NotificationDTO, List<Long>> entry : map.entrySet()) {
					mintues = mintues + 3;
					NotificationDTO key = entry.getKey();
					List<Long> value = entry.getValue();
					JobDetail simpleJobBuilder = jobBuilders.buildJobDetail(value);
					mailJobScheduler.createMailTriggerJob(key.getUtcScheduledDateTime().plusMinutes(mintues),
							simpleJobBuilder);

				}

			}

		} else
			log.info("Project Summary Schedule is in disable!");
	}

	/*
	 * public static void main(String[] args) { ZoneId zoneId =
	 * ZoneId.of("America/Chicago"); ZonedDateTime dateTime =ZonedDateTime.now();
	 * System.out.println(dateTime.withZoneSameInstant(zoneId)); }
	 */

	public static ZonedDateTime zoneTimeToUTC(ZonedDateTime zoneDateTime) {
		log.info("zoneDateTime value -- > " + zoneDateTime.toString());
		ZonedDateTime zonedUtcDateTime = zoneDateTime.withZoneSameInstant(ZoneOffset.UTC);
//		LocalDateTime localUtcDateTime = zonedUtcDateTime.toLocalDateTime();
		log.info("Zone To UTC ----> " + zonedUtcDateTime);
		return zonedUtcDateTime;
	}

	public static ZonedDateTime getScheduleWeekDate(LocalDateTime localTime, ZoneId zoneId, String frequencyOfTheWeek) {
		ZonedDateTime dateTime = ZonedDateTime.of(localTime, zoneId);
		ZonedDateTime scheduleDate = dateTime.with(getWeeksStartDay(frequencyOfTheWeek));
		if (scheduleDate.isBefore(dateTime)) {
			scheduleDate = scheduleDate.plusDays(7);
		}
		return scheduleDate;
	}

	private static TemporalAdjuster getWeeksStartDay(String weekStartDay) {
		if (EnumDays.SUNDAY.getDay().equalsIgnoreCase(weekStartDay))
			return DayOfWeek.SUNDAY;
		else if (EnumDays.MONDAY.getDay().equalsIgnoreCase(weekStartDay))
			return DayOfWeek.MONDAY;
		else if (EnumDays.TUESDAY.getDay().equalsIgnoreCase(weekStartDay))
			return DayOfWeek.TUESDAY;
		else if (EnumDays.WEDNESDAY.getDay().equalsIgnoreCase(weekStartDay))
			return DayOfWeek.WEDNESDAY;
		else if (EnumDays.THURSDAY.getDay().equalsIgnoreCase(weekStartDay))
			return DayOfWeek.THURSDAY;
		else if (EnumDays.FRIDAY.getDay().equalsIgnoreCase(weekStartDay))
			return DayOfWeek.FRIDAY;
		return DayOfWeek.SATURDAY;
	}

}