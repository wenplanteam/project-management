/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.scheduler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.springframework.stereotype.Component;

import com.mindzen.planner.config.UserInfoContext;

// TODO: Auto-generated Javadoc
/**
 * The Class JobBuilders.
 */
@Component
public class JobBuilders {

	//Instead of JobBuilder, TriggerBuilder, Scheduler
	/**
	 * Builds the job detail.
	 *
	 * @param value the schedule email request
	 * @return the job detail
	 */
	//We can create using @Bean JobDetailFactoryBean, SimpleTriggerFactoryBean, SchedulerFactoryBean
	public JobDetail buildJobDetail(List<Long> value) {
		JobDataMap jobDataMap = new JobDataMap();
		if(null != value)
		{
		//	UserInfoContext.setUserThreadLocal(value.getUserId());
			jobDataMap.put("projectUserObject", value);
			return JobBuilder.newJob(ProjectReportEmailSchedule.class)
					.withIdentity("project-summary-email-job-"+UserInfoContext.getUserThreadLocal()+LocalDateTime.now(),
							"project-summary-email-job-"+String.join(",", value.stream().map(obj->String.valueOf(obj)).collect(Collectors.toList())))
					.withDescription("Send Email Job")
					.usingJobData(jobDataMap)
					.storeDurably()
					.build();
		}
		return null;

	}

//	public JobDetail buildJobDetail(@Valid ScheduleEmailRequest scheduleEmailRequest) {
//		JobDataMap jobDataMap = new JobDataMap();
//
//		jobDataMap.put("email", scheduleEmailRequest.getEmail());
//		jobDataMap.put("subject", scheduleEmailRequest.getSubject());
//		jobDataMap.put("body", scheduleEmailRequest.getBody());
//		UserInfoContext.setUserThreadLocal(1L);
//		return JobBuilder.newJob(ProjectReportEmailSchedule.class)
//				.withIdentity(UserInfoContext.getUserThreadLocal().toString(), "email-jobs")
//				.withDescription("Send Email Job")
//				.usingJobData(jobDataMap)
//				.storeDurably()
//				.build();
//	}

}
