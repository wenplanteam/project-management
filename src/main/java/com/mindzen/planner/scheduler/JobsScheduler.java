/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.scheduler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindzen.planner.util.DateUtil;

import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/**
 * The Class EmailJobSchedulerController.
 */
@Component

/** The Constant log. */
@Slf4j
public class JobsScheduler{


	/** The scheduler. */
	@Autowired
	private Scheduler scheduler;

	/**
	 * Creates the job trigger.
	 *
	 * @param jobDetail the job detail
	 * @param startAt the start at
	 * @return the schedule email response
	 * @throws ParseException the parse exception
	 */
//	public ScheduleEmailResponse createTriggerJob(ScheduleEmailRequest scheduleEmailRequest, ZonedDateTime dateTime, JobDetail jobDetail, JobDetail cronJobDetail) {
//		try {
//			Trigger trigger = buildJobTrigger(jobDetail, dateTime);
//			Trigger cronTrigger = buildCronTrigger(cronJobDetail, scheduleEmailRequest.getCronTriggerTime());
//
//			scheduler.scheduleJob(jobDetail, trigger);
//			scheduler.scheduleJob(cronJobDetail, cronTrigger);
//
//			return new ScheduleEmailResponse(true,
//					jobDetail.getKey().getName() + ", "+ cronJobDetail.getKey().getName(), 
//					jobDetail.getKey().getGroup() + ", " + cronJobDetail.getKey().getGroup() , 
//					"Emails Scheduled Successfully!");
//		}    catch (SchedulerException | ParseException ex) {
//			log.error("Error scheduling email", ex);
//			return new ScheduleEmailResponse(false, "Error scheduling email. Please try later!");
//		}
//	}


	/**
	 * Builds the job trigger.
	 *
	 * @param jobDetail the job detail
	 * @param startAt the start at
	 * @return the trigger
	 * @throws ParseException 
	 */
	public Trigger buildJobTrigger(JobDetail jobDetail, ZonedDateTime startAt) throws ParseException {
		final DateFormat FORMATTER_YYYYMMDD_HH_MM_SS = new SimpleDateFormat(DateUtil.Formats.YYYYMMDD_HHMMSS.toString());
		log.info("Start At: "+Date.from(startAt.toInstant()));
		return TriggerBuilder.newTrigger()
				.forJob(jobDetail)
				.withIdentity(jobDetail.getKey().getName(), jobDetail.getKey().getGroup())
				.withDescription("Send Notification Email Trigger")
				.startAt(FORMATTER_YYYYMMDD_HH_MM_SS.parse(startAt.format(DateTimeFormatter.ofPattern(DateUtil.Formats.YYYYMMDD_HHMMSS.toString()))))
//				.startNow()
				.withPriority(1)
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionNextWithExistingCount())
				.build();
	}

	/**
	 * Builds the cron trigger.
	 *
	 * @param jobDetail the job detail
	 * @param cronExpression the cron expression
	 * @return the cron trigger
	 */
	public CronTrigger buildCronTrigger(JobDetail jobDetail, String cronExpression) {
		CronTrigger crontrigger = (CronTrigger)TriggerBuilder.newTrigger()
				.withIdentity(jobDetail.getKey().getName(), "cron-email-triggers")
				.withDescription("Send Email Trigger")
				.withSchedule(createSchedule(cronExpression)).build();
		return crontrigger;
	}

	/**
	 * Creates the schedule.
	 *
	 * @param cronExpression the cron expression
	 * @return the schedule builder
	 */
	private static ScheduleBuilder<?> createSchedule(String cronExpression){
		CronScheduleBuilder builder = CronScheduleBuilder.cronSchedule(cronExpression);
		return builder;
	}


	/**
	 * Creates the mail trigger job.
	 *
	 * @param dateTime the date time
	 * @param simpleJobDetail the simple job detail
	 * @return true, if successful
	 */
	public boolean createMailTriggerJob(ZonedDateTime dateTime, JobDetail simpleJobDetail) {
		try {
			Trigger simpleTrigger = buildJobTrigger(simpleJobDetail, dateTime);
			log.info("Start Time: "+simpleTrigger.getStartTime());
			Date scheduled = scheduler.scheduleJob(simpleJobDetail, simpleTrigger);
			log.info("Scheduled Time: "+scheduled);
			log.info(simpleJobDetail.getKey().getName() + ", " + simpleJobDetail.getKey().getGroup() );
			return true;
		}    catch (Exception ex) {
			log.error("Error scheduling email", ex);
			return false;
		}
	}

	/*public static void main(String[] args) {
		ZonedDateTime zdt = 
				  ZonedDateTime.of(2015, 10, 18, 0, 30, 0, 0, 
				    ZoneId.of("America/Sao_Paulo")); // switch to summer time
				ZonedDateTime zdt1 = zdt.truncatedTo(ChronoUnit.DAYS);
				ZonedDateTime zdt2 = zdt.toLocalDate().atStartOfDay(zdt.getZone());

				System.out.println(zdt); 
				System.out.println(zdt1);
				System.out.println(zdt2);	
	}*/
}
