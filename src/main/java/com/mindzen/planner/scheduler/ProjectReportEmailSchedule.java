/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.scheduler;

import java.util.List;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.EmailJob;
import com.mindzen.planner.dto.ProjectSummaryDetailsDTO;
import com.mindzen.planner.service.ProjectEmailNotificationService;

import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectReportEmailSchedule.
 * 
 * @author MRG
 */
@Component
/** The Constant log. */

/** The Constant log. */
@Slf4j
public class ProjectReportEmailSchedule extends EmailJob {

	/** The integration gateway. */
	@Autowired
	private ProjectEmailNotificationService projectEmailNotificationService;
	
	

	/* (non-Javadoc)
	 * @see com.mindzen.planner.config.EmailJob#executeInternal(org.quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		log.info("Email Jobs");
		log.info("Executing Project user Report Job with key {}", jobExecutionContext.getJobDetail().getKey());
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        @SuppressWarnings("unchecked")
		List<Long> projectUserIdList = (List<Long>) jobDataMap.get("projectUserObject");
        
        Runnable runnable = () -> {
            log.info("***** Project Summary Schdule Started *****");
            for (Long projectUserId : projectUserIdList) {
                System.out.println("Inside : " + Thread.currentThread().getName()+"project user id "+ projectUserId);
            	MSAResponse response =  projectEmailNotificationService.getTaskSummaryByProjectUserId(projectUserId);
            	ProjectSummaryDetailsDTO projectSummaryDetailsDTO = (ProjectSummaryDetailsDTO) response.getPayload();
            	 projectEmailNotificationService.sendProjectSummaryDetailsMail(projectSummaryDetailsDTO.getEmailId(), projectSummaryDetailsDTO);
    		}
           
        };

        Thread thread = new Thread(runnable);
        thread.start();
   
        log.info("***** "+jobExecutionContext.getJobDetail().getKey()+"Project Summary Schdule Ended *****");
	}
}
