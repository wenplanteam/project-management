/*
 * 
 * @author Raja Gopal MRG
 */
package com.mindzen.planner.converter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.mindzen.planner.config.UserInfoContext;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.entity.Project;
import com.mindzen.planner.entity.ProjectUser;
import com.mindzen.planner.repository.ProjectCompanyRepository;
import com.mindzen.planner.util.Constants;
import com.mindzen.planner.util.DateUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectConverter.
 */
public class ProjectConverter {

	/**
	 * converting List ofprojectDTOs entity to list of projects.
	 *
	 * @param projectDTOList the project DTO list
	 * @return the list
	 */
	public List<Project> projectDTOTOProject(List<ProjectDTO> projectDTOList) {
		List<Project> projectList = new ArrayList<Project>();
		projectDTOList.forEach(projectDTO -> {
			Project project = new Project();
			project.setId(projectDTO.getProjectId());
			project.setProjectNo(projectDTO.getProjectNo());
			project.setName(projectDTO.getProjectName());
			project.setDescription(projectDTO.getProjectDescription());
			project.setLicenseDistributionId(projectDTO.getLicenseDistributionId());
			project.setProjectManager(projectDTO.getProjectManager());
			project.setOwnerCompany(projectDTO.getProjectOwner());
			project.setJobPhone(projectDTO.getBusinessPhone());

			Date d = DateUtil.convertToDateViaInstant(LocalDate.now());
			Date estimatedStartDate = DateUtil.stringToDateViaInstant(projectDTO.getEstimatedStartDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
			Date estimatedEndDate = DateUtil.stringToDateViaInstant(projectDTO.getEstimatedEndDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());

			if (null == estimatedStartDate) {
				estimatedStartDate = d;
			} 
			if (null == estimatedEndDate) {
				estimatedEndDate = d;
			} 
			project.setEstimatedStartDate(estimatedStartDate);
			project.setEstimatedEndDate(estimatedEndDate);
			project.setProjectStage(projectDTO.getProjectStage());
			project.setCity(projectDTO.getCity());
			project.setZipCode(projectDTO.getZipCode());
			project.setState(projectDTO.getState());
			project.setCounty(projectDTO.getCounty());
			project.setCountry(projectDTO.getCountry());
			projectList.add(project);
		});
		return projectList;

	}

	/**
	 * converting projectDTO to project.
	 *
	 * @param projectDTO the project DTO
	 * @return the project
	 */
	public Project projectDTOTOProject(ProjectDTO projectDTO) {

		Project project = new Project();
		project.setProjectNo(projectDTO.getProjectNo());
		project.setName(projectDTO.getProjectName());
		project.setDescription(projectDTO.getProjectDescription());
		project.setLicenseDistributionId(projectDTO.getLicenseDistributionId());
		project.setProjectManager(projectDTO.getProjectManager());
		project.setOwnerCompany(projectDTO.getProjectOwner());
		project.setJobPhone(projectDTO.getBusinessPhone());

		Date d = DateUtil.convertToDateViaInstant(LocalDate.now());
		Date estimatedStartDate = d;
		Date estimatedEndDate = d;
		if (estimatedStartDate != null) {
			estimatedStartDate = DateUtil.stringToDateViaInstant(projectDTO.getEstimatedStartDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		} else {
			estimatedStartDate = d;
		}
		if (estimatedEndDate != null) {
			estimatedEndDate = DateUtil.stringToDateViaInstant(projectDTO.getEstimatedEndDate(),
					DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		} else {
			estimatedEndDate = d;
		}
		project.setEstimatedStartDate(estimatedStartDate);
		project.setEstimatedEndDate(estimatedEndDate);
		project.setProjectStage(projectDTO.getProjectStage());
		project.setCity(projectDTO.getCity());
		project.setZipCode(projectDTO.getZipCode());
		project.setState(projectDTO.getState());
		project.setCounty(projectDTO.getCounty());
		project.setCountry(projectDTO.getCountry());
		return project;

	}

	/**
	 * Gets the project DT os.
	 *
	 * @param projects the projects
	 * @param projectCompanyRepository the project company repository
	 * @return the project DT os
	 */
	public List<ProjectDTO> getProjectDTOs(List<Project> projects, ProjectCompanyRepository projectCompanyRepository) {
		List<ProjectDTO> projectDTOs = new ArrayList<>();
		projects.forEach(project -> {
			projectDTOs.add(getProjectDTO(project,projectCompanyRepository));
		});
		return projectDTOs;

	}

	/**
	 * converting projectDTO to project.
	 *
	 * @param project the project
	 * @param projectCompanyRepository the project company repository
	 * @return the project DTO
	 */
	public ProjectDTO getProjectDTO(Project project, ProjectCompanyRepository projectCompanyRepository) {

		ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setProjectId(project.getId());
		projectDTO.setProjectNo(project.getProjectNo());
		projectDTO.setProjectName(project.getName());
		projectDTO.setProjectDescription(project.getDescription());
		projectDTO.setLicenseDistributionId(project.getLicenseDistributionId());
		projectDTO.setProjectManager(project.getProjectManager());
		projectDTO.setProjectOwner(project.getOwnerCompany());
		projectDTO.setBusinessPhone(project.getJobPhone());

		String estimatedStartDate = DateUtil.convertDate(project.getEstimatedStartDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());
		String estimatedEndDate = DateUtil.convertDate(project.getEstimatedEndDate(),
				DateUtil.Formats.DDMMMYYYY_HYPHEN.toString());

		projectDTO.setEstimatedStartDate(estimatedStartDate);
		projectDTO.setEstimatedEndDate(estimatedEndDate);
		projectDTO.setProjectStage(project.getProjectStage());
		projectDTO.setCity(project.getCity());
		projectDTO.setZipCode(project.getZipCode());
		projectDTO.setState(project.getState());
		projectDTO.setCounty(project.getCounty());
		projectDTO.setCountry(project.getCountry());
		projectDTO.setProjectStatus(project.isIsActive() ? "Active" : "InActive");

		if(null != project.getProjectUser()) {
			Long userId = UserInfoContext.getUserThreadLocal();
			project.getProjectUser().forEach(projectUser -> {
				if(projectUser.getUserId() == userId) {
					projectDTO.setProjectRole(projectUser.getProjectRole());
					projectDTO.setEdit(false);
					projectDTO.setInactivate(false);
					projectDTO.setDelete(false);
					if(projectUser.getProjectRole().equals(Constants.GC_PROJECT_ADMIN)) {
						projectDTO.setEdit(true);
						projectDTO.setInactivate(true);
						projectDTO.setDelete(true);
					}
					else if (projectUser.getProjectRole().equals(Constants.GC_PROJECT_MANAGER))
						projectDTO.setEdit(true); 

					if(projectUser.getProjectRole().equals(Constants.GC_OBSERVER) ||
							projectUser.getProjectRole().equals(Constants.SC_OBSERVER) ||
							projectUser.getProjectRole().equals(Constants.OWNER))
						projectDTO.setShare(false);
					else
						projectDTO.setShare(true);
				}
				if(null != projectUser.getProjectCompany()) {
					if(projectUser.getProjectCompany().getProjectRole().equals(Constants.OWNER)){
						projectDTO.setProjectOwner(
								projectCompanyRepository.findCompanyById(projectUser.getProjectCompany().getId()));
					}
				}
			});
		}
		if(null != project.getTrade()) 
			projectDTO.setTrade(Arrays.asList(project.getTrade().split(",")));
		else 
			projectDTO.setTrade(null);
		if(null != project.getSpecDivision()) 
			projectDTO.setSpecDivision(Arrays.asList(project.getSpecDivision().split("\\|\\|")));
		else 
			projectDTO.setSpecDivision(null);
		return projectDTO;

	}

	/**
	 * Gets the project user.
	 *
	 * @param userDTO the user DTO
	 * @param project the project
	 * @param projectUser the project user
	 * @return the project user
	 */
	public ProjectUser getProjectUser(UserDTO userDTO, Project project, ProjectUser projectUser) {
		projectUser.setUserId(userDTO.getId());
		projectUser.setProject(project);
		projectUser.setAccess(String.join(",", Arrays.asList(userDTO.getLookaheadAccess())));

		if(userDTO.isActive() && userDTO.isEmailVerified())
			projectUser.setIsActive(true);
		else
			projectUser.setIsActive(false);

		String[] lookAheadAccess = userDTO.getLookaheadAccess();
		List<String> newAccesses = null;
		int newAccessesCount = 0;
		if (null != lookAheadAccess) {
			newAccesses = Arrays.asList(userDTO.getLookaheadAccess());
			newAccessesCount = newAccesses.size();
		}
		String projectRole = null;
		if(newAccessesCount == 3)
			projectRole = Constants.PROJECT_ADMIN;
		else if(newAccessesCount == 2)
			projectRole = Constants.SUB_CONTRACTOR;
		else
			projectRole = Constants.PROJECT_OWNER;

		projectUser.setProjectRole(projectRole);
		projectUser.setIsDeleted(false);

		return projectUser;
	}

}
