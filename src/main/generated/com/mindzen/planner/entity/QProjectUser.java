package com.mindzen.planner.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProjectUser is a Querydsl query type for ProjectUser
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectUser extends EntityPathBase<ProjectUser> {

    private static final long serialVersionUID = -1767044865L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProjectUser projectUser = new QProjectUser("projectUser");

    public final QBaseIdEntity _super = new QBaseIdEntity(this);

    public final StringPath access = createString("access");

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> createdOn = _super.createdOn;

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final BooleanPath isActive = createBoolean("isActive");

    public final BooleanPath isDeleted = createBoolean("isDeleted");

    //inherited
    public final NumberPath<Long> modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modifiedOn = _super.modifiedOn;

    public final QNotificationEntity notification;

    public final QProject project;

    public final QProjectCompany projectCompany;

    public final StringPath projectRole = createString("projectRole");

    public final NumberPath<Long> userId = createNumber("userId", Long.class);

    public QProjectUser(String variable) {
        this(ProjectUser.class, forVariable(variable), INITS);
    }

    public QProjectUser(Path<? extends ProjectUser> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProjectUser(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProjectUser(PathMetadata metadata, PathInits inits) {
        this(ProjectUser.class, metadata, inits);
    }

    public QProjectUser(Class<? extends ProjectUser> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.notification = inits.isInitialized("notification") ? new QNotificationEntity(forProperty("notification")) : null;
        this.project = inits.isInitialized("project") ? new QProject(forProperty("project")) : null;
        this.projectCompany = inits.isInitialized("projectCompany") ? new QProjectCompany(forProperty("projectCompany"), inits.get("projectCompany")) : null;
    }

}

