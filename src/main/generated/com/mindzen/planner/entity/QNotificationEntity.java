package com.mindzen.planner.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QNotificationEntity is a Querydsl query type for NotificationEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNotificationEntity extends EntityPathBase<NotificationEntity> {

    private static final long serialVersionUID = 1204189651L;

    public static final QNotificationEntity notificationEntity = new QNotificationEntity("notificationEntity");

    public final QBaseIdEntity _super = new QBaseIdEntity(this);

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> createdOn = _super.createdOn;

    public final StringPath dayOfTheWeek = createString("dayOfTheWeek");

    public final StringPath frequency = createString("frequency");

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final StringPath information = createString("information");

    public final BooleanPath isMailSent = createBoolean("isMailSent");

    public final BooleanPath isNotification = createBoolean("isNotification");

    public final BooleanPath isScheduled = createBoolean("isScheduled");

    public final DateTimePath<java.time.LocalDateTime> mailSentOn = createDateTime("mailSentOn", java.time.LocalDateTime.class);

    //inherited
    public final NumberPath<Long> modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modifiedOn = _super.modifiedOn;

    public final DateTimePath<java.time.LocalDateTime> scheduledAt = createDateTime("scheduledAt", java.time.LocalDateTime.class);

    public final StringPath type = createString("type");

    public QNotificationEntity(String variable) {
        super(NotificationEntity.class, forVariable(variable));
    }

    public QNotificationEntity(Path<? extends NotificationEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNotificationEntity(PathMetadata metadata) {
        super(NotificationEntity.class, metadata);
    }

}

