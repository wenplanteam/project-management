package com.mindzen.planner.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSubTask is a Querydsl query type for SubTask
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSubTask extends EntityPathBase<SubTask> {

    private static final long serialVersionUID = 1826377696L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSubTask subTask = new QSubTask("subTask");

    public final QBaseIdEntity _super = new QBaseIdEntity(this);

    public final StringPath activityDescription = createString("activityDescription");

    public final StringPath activityId = createString("activityId");

    public final StringPath ballInCourt = createString("ballInCourt");

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> createdOn = _super.createdOn;

    public final DateTimePath<java.util.Date> deadLineDate = createDateTime("deadLineDate", java.util.Date.class);

    public final StringPath description = createString("description");

    public final DateTimePath<java.util.Date> expectedCompletionDate = createDateTime("expectedCompletionDate", java.util.Date.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final DateTimePath<java.util.Date> importDeadLineDate = createDateTime("importDeadLineDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> importFinishDate = createDateTime("importFinishDate", java.util.Date.class);

    public final StringPath importNotes = createString("importNotes");

    public final DateTimePath<java.util.Date> importStartDate = createDateTime("importStartDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> importTargetCompletionDate = createDateTime("importTargetCompletionDate", java.util.Date.class);

    public final NumberPath<Integer> importUniqueId = createNumber("importUniqueId", Integer.class);

    public final BooleanPath isActive = createBoolean("isActive");

    public final BooleanPath isLookAheadChange = createBoolean("isLookAheadChange");

    //inherited
    public final NumberPath<Long> modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modifiedOn = _super.modifiedOn;

    public final StringPath note = createString("note");

    public final NumberPath<Long> projectCompanyId = createNumber("projectCompanyId", Long.class);

    public final DateTimePath<java.util.Date> requiredCompletionDate = createDateTime("requiredCompletionDate", java.util.Date.class);

    public final ListPath<Schedule, QSchedule> schedules = this.<Schedule, QSchedule>createList("schedules", Schedule.class, QSchedule.class, PathInits.DIRECT2);

    public final StringPath sNo = createString("sNo");

    public final StringPath specDivision = createString("specDivision");

    public final QTask task;

    public final DateTimePath<java.util.Date> taskStartDate = createDateTime("taskStartDate", java.util.Date.class);

    public final StringPath taskType = createString("taskType");

    public final StringPath trade = createString("trade");

    public final StringPath visibility = createString("visibility");

    public QSubTask(String variable) {
        this(SubTask.class, forVariable(variable), INITS);
    }

    public QSubTask(Path<? extends SubTask> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSubTask(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSubTask(PathMetadata metadata, PathInits inits) {
        this(SubTask.class, metadata, inits);
    }

    public QSubTask(Class<? extends SubTask> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.task = inits.isInitialized("task") ? new QTask(forProperty("task")) : null;
    }

}

