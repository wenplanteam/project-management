package com.mindzen.planner.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTask is a Querydsl query type for Task
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTask extends EntityPathBase<Task> {

    private static final long serialVersionUID = 461416298L;

    public static final QTask task = new QTask("task");

    public final QBaseIdEntity _super = new QBaseIdEntity(this);

    public final StringPath activityDescription = createString("activityDescription");

    public final StringPath activityId = createString("activityId");

    public final StringPath ballInCourt = createString("ballInCourt");

    public final BooleanPath child = createBoolean("child");

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> createdOn = _super.createdOn;

    public final DateTimePath<java.util.Date> deadLineDate = createDateTime("deadLineDate", java.util.Date.class);

    public final StringPath description = createString("description");

    public final DateTimePath<java.util.Date> expectedCompletionDate = createDateTime("expectedCompletionDate", java.util.Date.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final DateTimePath<java.util.Date> importDeadLineDate = createDateTime("importDeadLineDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> importFinishDate = createDateTime("importFinishDate", java.util.Date.class);

    public final NumberPath<Long> importId = createNumber("importId", Long.class);

    public final StringPath importNotes = createString("importNotes");

    public final DateTimePath<java.util.Date> importStartDate = createDateTime("importStartDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> importTargetCompletionDate = createDateTime("importTargetCompletionDate", java.util.Date.class);

    public final NumberPath<Integer> importUniqueId = createNumber("importUniqueId", Integer.class);

    public final BooleanPath isActive = createBoolean("isActive");

    public final BooleanPath isLookAheadChange = createBoolean("isLookAheadChange");

    //inherited
    public final NumberPath<Long> modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modifiedOn = _super.modifiedOn;

    public final StringPath note = createString("note");

    public final NumberPath<Long> projectCompanyId = createNumber("projectCompanyId", Long.class);

    public final NumberPath<Long> projectId = createNumber("projectId", Long.class);

    public final DateTimePath<java.util.Date> requiredCompletionDate = createDateTime("requiredCompletionDate", java.util.Date.class);

    public final StringPath sNo = createString("sNo");

    public final StringPath specDivision = createString("specDivision");

    public final ListPath<SubTask, QSubTask> subTasks = this.<SubTask, QSubTask>createList("subTasks", SubTask.class, QSubTask.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> taskStartDate = createDateTime("taskStartDate", java.util.Date.class);

    public final StringPath taskType = createString("taskType");

    public final StringPath trade = createString("trade");

    public final StringPath visibility = createString("visibility");

    public QTask(String variable) {
        super(Task.class, forVariable(variable));
    }

    public QTask(Path<? extends Task> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTask(PathMetadata metadata) {
        super(Task.class, metadata);
    }

}

