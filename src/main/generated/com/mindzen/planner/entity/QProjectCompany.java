package com.mindzen.planner.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProjectCompany is a Querydsl query type for ProjectCompany
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectCompany extends EntityPathBase<ProjectCompany> {

    private static final long serialVersionUID = -1816681783L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProjectCompany projectCompany = new QProjectCompany("projectCompany");

    public final QBaseIdEntity _super = new QBaseIdEntity(this);

    public final BooleanPath active = createBoolean("active");

    public final NumberPath<Long> companyId = createNumber("companyId", Long.class);

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> createdOn = _super.createdOn;

    public final BooleanPath deleted = createBoolean("deleted");

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final NumberPath<Long> modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modifiedOn = _super.modifiedOn;

    public final QProject project;

    public final StringPath projectRole = createString("projectRole");

    public QProjectCompany(String variable) {
        this(ProjectCompany.class, forVariable(variable), INITS);
    }

    public QProjectCompany(Path<? extends ProjectCompany> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProjectCompany(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProjectCompany(PathMetadata metadata, PathInits inits) {
        this(ProjectCompany.class, metadata, inits);
    }

    public QProjectCompany(Class<? extends ProjectCompany> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.project = inits.isInitialized("project") ? new QProject(forProperty("project")) : null;
    }

}

