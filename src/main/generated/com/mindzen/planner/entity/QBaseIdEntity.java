package com.mindzen.planner.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBaseIdEntity is a Querydsl query type for BaseIdEntity
 */
@Generated("com.querydsl.codegen.SupertypeSerializer")
public class QBaseIdEntity extends EntityPathBase<BaseIdEntity> {

    private static final long serialVersionUID = 1699685300L;

    public static final QBaseIdEntity baseIdEntity = new QBaseIdEntity("baseIdEntity");

    public final QBaseEntity _super = new QBaseEntity(this);

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> createdOn = _super.createdOn;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    //inherited
    public final NumberPath<Long> modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modifiedOn = _super.modifiedOn;

    public QBaseIdEntity(String variable) {
        super(BaseIdEntity.class, forVariable(variable));
    }

    public QBaseIdEntity(Path<? extends BaseIdEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBaseIdEntity(PathMetadata metadata) {
        super(BaseIdEntity.class, metadata);
    }

}

