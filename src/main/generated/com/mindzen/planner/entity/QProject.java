package com.mindzen.planner.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProject is a Querydsl query type for Project
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProject extends EntityPathBase<Project> {

    private static final long serialVersionUID = -909356268L;

    public static final QProject project = new QProject("project");

    public final QBaseIdEntity _super = new QBaseIdEntity(this);

    public final DateTimePath<java.util.Date> anchorDate = createDateTime("anchorDate", java.util.Date.class);

    public final StringPath anchorWeek = createString("anchorWeek");

    public final StringPath city = createString("city");

    public final StringPath country = createString("country");

    public final StringPath county = createString("county");

    //inherited
    public final NumberPath<Long> createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> createdOn = _super.createdOn;

    public final StringPath description = createString("description");

    public final DateTimePath<java.util.Date> estimatedEndDate = createDateTime("estimatedEndDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> estimatedStartDate = createDateTime("estimatedStartDate", java.util.Date.class);

    public final NumberPath<Integer> floatDays = createNumber("floatDays", Integer.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final BooleanPath isActive = createBoolean("isActive");

    public final BooleanPath isDeleted = createBoolean("isDeleted");

    public final StringPath jobPhone = createString("jobPhone");

    public final NumberPath<Long> latitude = createNumber("latitude", Long.class);

    public final StringPath leaveDays = createString("leaveDays");

    public final NumberPath<Long> licenseDistributionId = createNumber("licenseDistributionId", Long.class);

    public final NumberPath<Long> longitude = createNumber("longitude", Long.class);

    public final BooleanPath lookaheadNotification = createBoolean("lookaheadNotification");

    //inherited
    public final NumberPath<Long> modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modifiedOn = _super.modifiedOn;

    public final StringPath name = createString("name");

    public final StringPath ownerCompany = createString("ownerCompany");

    public final StringPath projectManager = createString("projectManager");

    public final StringPath projectNo = createString("projectNo");

    public final StringPath projectStage = createString("projectStage");

    public final ListPath<ProjectUser, QProjectUser> projectUser = this.<ProjectUser, QProjectUser>createList("projectUser", ProjectUser.class, QProjectUser.class, PathInits.DIRECT2);

    public final BooleanPath showProjectAddress = createBoolean("showProjectAddress");

    public final StringPath specDivision = createString("specDivision");

    public final StringPath state = createString("state");

    public final StringPath timezone = createString("timezone");

    public final StringPath trade = createString("trade");

    public final StringPath weekStartDay = createString("weekStartDay");

    public final StringPath zipCode = createString("zipCode");

    public QProject(String variable) {
        super(Project.class, forVariable(variable));
    }

    public QProject(Path<? extends Project> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProject(PathMetadata metadata) {
        super(Project.class, metadata);
    }

}

